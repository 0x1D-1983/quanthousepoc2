﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace EtxCapital.POC.PriceEngine.Business.Models
{
    public class Stock
    {
        [DisplayName("Internal Code")]
        public uint InternalCode { get; private set; }

        [DisplayName("Ask Price")]
        public decimal? AskPrice { get; set; }

        public decimal? AskPricePrev { get; set; }

        [DisplayName("Bid Price")]
        public decimal? BidPrice { get; set; }

        public decimal? BidPricePrev { get; set; }

        [DisplayName("Last Price")]
        public decimal? LastPrice { get; set; }

        [DisplayName("Last Trade Price")]
        public decimal? LastTradePrice { get; set; }

        [DisplayName("Last Trade Timestamp")]
        public DateTime? LastTradeTimestamp { get; set; }

        [DisplayName("Current Business Day")]
        public DateTime? CurrentBusinessDay { get; set; }

        [DisplayName("Daily Closing Ask Price")]
        public decimal? DailyClosingAskPrice { get; set; }

        [DisplayName("Daily Closing Ask Timestamp")]
        public DateTime? DailyClosingAskTimestamp { get; set; }

        [DisplayName("Daily Closing Bid Price")]
        public decimal? DailyClosingBidPrice { get; set; }

        [DisplayName("Daily Closing Bid Timestamp")]
        public DateTime? DailyClosingBidTimestamp { get; set; }

        [DisplayName("Daily Opening Price")]
        public decimal? DailyOpeningPrice { get; set; }

        [DisplayName("Daily Opening Price Timestamp")]
        public DateTime? DailyOpeningPriceTimestamp { get; set; }

        [DisplayName("Previous Daily Closing Price")]
        public decimal? PreviousDailyClosingPrice { get; set; }

        [DisplayName("Previous Daily Closing Price Timestamp")]
        public DateTime? PreviousDailyClosingPriceTimestamp { get; set; }

        public Stock(uint internalCode)
        {
            InternalCode = internalCode;
        }
    }
}
