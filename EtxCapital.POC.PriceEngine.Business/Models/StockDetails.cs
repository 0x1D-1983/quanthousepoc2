﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace EtxCapital.POC.PriceEngine.Business.Models
{
    public class StockDetails
    {
        [DisplayName("Internal Code")]
        public uint InternalCode { get; private set; }

        [DisplayName("Symbol")]
        public string Symbol { get; private set; }

        [DisplayName("Description")]
        public string Description { get; private set; }

        [DisplayName("ISIN")]
        public string ISIN { get; private set; }

        public StockDetails(uint internalCode, string symbol, string description, string isin)
        {
            InternalCode = internalCode;
            Symbol = symbol;
            Description = description;
            ISIN = isin;
        }
    }
}
