﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.Business.Models
{
    public class StockStore
    {
        private readonly Dictionary<uint, Stock> _stocks = new Dictionary<uint, Stock>();
        private readonly Dictionary<uint, StockDetails> _stockDetails = new Dictionary<uint, StockDetails>();

        public static List<string> Codes = new List<string>() { "768315389", "940275063", "940282090", "940279505", "940277943" };

        public StockStore()
        {
            InitStocks();
        }

        private void InitStocks()
        {
            foreach(var code in Codes)
            {
                uint codeuint = uint.Parse(code);
                _stocks.Add(codeuint, new Stock(codeuint));
            }

            _stockDetails.Add(768315389, new StockDetails(768315389, "VODl", "Vodafone Group PLC", "GB00BH4HKS39"));
            _stockDetails.Add(940275063, new StockDetails(940275063, "VODl", "Vodafone Group PLC", "GB00BH4HKS39"));
            _stockDetails.Add(940282090, new StockDetails(940282090, "PSRWp", "Invesco FTSE RAFI All World 3000 UCITS ETF", "IE00B23LNQ02"));
            _stockDetails.Add(940279505, new StockDetails(940279505, "VERXa", "Vanguard FTSE Developed Europe ex UK UCITS ETF", "IE00BKX55S42"));
            _stockDetails.Add(940277943, new StockDetails(940277943, "HUKXz", "HSBC FTSE 100 UCITS ETF", "IE00B42TW061"));
        }

        public IEnumerable<Stock> GetAllStocks()
        {
            return _stocks.Values;
        }

        public IEnumerable<StockDetails> GetAllStocksDetails()
        {
            return _stockDetails.Values;
        }

        public Stock GetByInternalCode(uint internalCode)
        {
            return _stocks[internalCode];
        }

        public StockDetails GetStockDetailsByInternalCode(uint internalCode)
        {
            return _stockDetails[internalCode];
        }

        public void UpdateStock(Stock stock)
        {
            if (!_stocks.ContainsKey(stock.InternalCode))
            {
                _stocks.Add(stock.InternalCode, stock);
            }
            else
            {
                if (stock.AskPrice != null) _stocks[stock.InternalCode].AskPrice = stock.AskPrice;
                if (stock.BidPrice != null) _stocks[stock.InternalCode].BidPrice = stock.BidPrice;
                if (stock.LastPrice != null) _stocks[stock.InternalCode].LastPrice = stock.LastPrice;
                if (stock.LastTradePrice != null) _stocks[stock.InternalCode].LastTradePrice = stock.LastTradePrice;
                if (stock.CurrentBusinessDay != null) _stocks[stock.InternalCode].CurrentBusinessDay = stock.CurrentBusinessDay;
                if (stock.DailyClosingAskPrice != null) _stocks[stock.InternalCode].DailyClosingAskPrice = stock.DailyClosingAskPrice;
                if (stock.DailyClosingAskTimestamp != null) _stocks[stock.InternalCode].DailyClosingAskTimestamp = stock.DailyClosingAskTimestamp;
                if (stock.DailyClosingBidPrice != null) _stocks[stock.InternalCode].DailyClosingBidPrice = stock.DailyClosingBidPrice;
                if (stock.DailyClosingBidTimestamp != null) _stocks[stock.InternalCode].DailyClosingBidTimestamp = stock.DailyClosingBidTimestamp;
                if (stock.DailyOpeningPrice != null) _stocks[stock.InternalCode].DailyOpeningPrice = stock.DailyOpeningPrice;
                if (stock.DailyOpeningPriceTimestamp != null) _stocks[stock.InternalCode].DailyOpeningPriceTimestamp = stock.DailyOpeningPriceTimestamp;
                if (stock.PreviousDailyClosingPrice != null) _stocks[stock.InternalCode].PreviousDailyClosingPrice = stock.PreviousDailyClosingPrice;
                if (stock.PreviousDailyClosingPriceTimestamp != null) _stocks[stock.InternalCode].PreviousDailyClosingPriceTimestamp = stock.PreviousDailyClosingPriceTimestamp;
            }
        }

        public void AddStockDetails(StockDetails details)
        {
            if (!_stockDetails.ContainsKey(details.InternalCode))
            {
                _stockDetails[details.InternalCode] = details;
            }
        }
    }
}
