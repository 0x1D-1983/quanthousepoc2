///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
// FeedOS
using FeedOSManaged;
using System.Threading;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Querying quotation historical data 
    /// </summary>
	//![snippet_refget_tc]
    class RequestReferentialGetTradeConditionsDictionary : Request
	//![snippet_refget_tc]
    {
        bool m_DumpTradeConditions = false;

        public RequestReferentialGetTradeConditionsDictionary(bool dumpTradeConditons)
        {
            m_DumpTradeConditions = dumpTradeConditons;
        }

        #region IRequest Members
        public override bool Parse(string[] args)
        {
            return true;
        }

		/** \cond refguide
		 * The following sample in .NET retrieves the trade conditions dictionary.
		 *
		 * It forwards the events to the trade conditions printer.
		 *
		 * <ol><li>The handler inherits the %Request interface:</li></ol>
		 * \snippet RequestReferentialGetTradeConditionsDictionary.cs snippet_refget_tc
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestReferentialGetTradeConditionsDictionary.cs snippet_refget_reg
		 *
		 * \endcond
		 */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
			//![snippet_refget_reg]
            ConfigureRequestEvents(connection);
            if (!m_DumpTradeConditions)
            {
                m_Connection.GetTradeConditionsDictionaryHandler +=
                		new FeedOSManaged.GetTradeConditionsDictionaryEventHandler(
                				PrinterHistoricalData.GetTradeConditionsDictionary);
            }
            else
            {
                m_Connection.GetTradeConditionsDictionaryHandler +=
                		new FeedOSManaged.GetTradeConditionsDictionaryEventHandler(
                				PrinterHistoricalData.PrintTradeConditionsDictionary);
            }
            m_Connection.ReferentialGetTradeConditionsDictionary(m_RequestID);
			//![snippet_refget_reg]
            return true;
        }

        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            if (!m_DumpTradeConditions)
            {
                m_Connection.GetTradeConditionsDictionaryHandler -= new FeedOSManaged.GetTradeConditionsDictionaryEventHandler(PrinterHistoricalData.GetTradeConditionsDictionary);
            }
            else
            {
                m_Connection.GetTradeConditionsDictionaryHandler -= new FeedOSManaged.GetTradeConditionsDictionaryEventHandler(PrinterHistoricalData.PrintTradeConditionsDictionary);
            }
        }

        #endregion

    }
}
