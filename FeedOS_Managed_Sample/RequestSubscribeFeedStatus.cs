///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Subscription to market status : retrieve status of current trading session for all the markets and keep it continually updated
    /// </summary>
    public class RequestSubscribeFeedStatus : Subscription
    {
        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if ((args != null) && (args.Length == 0))
            {
                return true;
            }
            else
            {
                Console.WriteLine("Invalid argument number for \"SubscribeFeedStatus\" request");
                return false;
            }
        }

        /** \cond refguide
         * This sample illustrates how to implement the feed status subscription to
         * be informed of servers and feed states, in real-time.
         *
         * <ol><li>Implement the event handlers associated with this request; 
         * that is, in addition to the common \c Started, \c Failed and
         * \c Aborted event handlers, those specific to this request:
         * \c FeedStatusSnapshot, \c FeedStatusUpdate and \c FeedStatusModified:
         * </li></ol>
         * \snippet PrinterConnection.cs snippet_feed_status_snapshot_handler
         * \snippet PrinterConnection.cs snippet_feed_status_update_handler
         * \snippet PrinterConnection.cs snippet_feed_status_modified_handler
         *
         * <ol><li>Register these callbacks:</li></ol>
         * \snippet RequestSubscribeFeedStatus.cs snippet_feed_status_register
         * 
         * <ol><li>Send the request to start the subscription:</li></ol>
         * \snippet RequestSubscribeFeedStatus.cs snippet_feed_status_request
         *
         * <ol><li>When you decide to stop the request (or when the server
         * aborts the subscription due to an error), unregister the event
         * handlers:</li></ol>
         * \snippet RequestSubscribeFeedStatus.cs snippet_feed_status_unregister
         *
         * \endcond
         */
        public override bool Start(FeedOSManaged.Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);
            //![snippet_feed_status_register]
            m_Connection.FeedStatusSnapshot += new FeedOSManaged.FeedStatusSnapshotEventHandler(PrinterConnection.FeedStatusSnapshot);
            m_Connection.FeedStatusUpdate += new FeedOSManaged.FeedStatusUpdateEventHandler(PrinterConnection.FeedStatusUpdate);
            m_Connection.FeedStatusModified += new FeedOSManaged.FeedStatusModifiedEventHandler(PrinterConnection.FeedStatusModified);
            //![snippet_feed_status_register]
            //![snippet_feed_status_request]
            m_Connection.SubscribeToFeedStatus(m_RequestID);
            //![snippet_feed_status_request]
            return true;
        }

        public override void Stop()
        {
            UnconfigureRequestEvents(m_Connection);
            m_Connection.RemoveRequest(m_RequestID);
            //![snippet_feed_status_unregister]
            m_Connection.FeedStatusSnapshot -= new FeedOSManaged.FeedStatusSnapshotEventHandler(PrinterConnection.FeedStatusSnapshot);
            m_Connection.FeedStatusUpdate -= new FeedOSManaged.FeedStatusUpdateEventHandler(PrinterConnection.FeedStatusUpdate);
            m_Connection.FeedStatusModified -= new FeedOSManaged.FeedStatusModifiedEventHandler(PrinterConnection.FeedStatusModified);
            //![snippet_feed_status_unregister]
        }
        #endregion
    }
}
