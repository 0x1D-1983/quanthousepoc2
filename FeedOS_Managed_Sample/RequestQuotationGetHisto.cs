///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2015
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
// FeedOS
using FeedOSManaged;
using System.Threading;
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public enum RequestGetHistoFlavor
    {
        DownloadAndSubscribe,
        IntradayExtendedNTicks,
        IntradayBarsExt
    }

    class RequestQuotationGetHisto : Request
    {
        #region Members
        RequestGetHistoFlavor m_HistoFlavor;
        List<string> m_PolymorphicInstrumentCodes = new List<string>();
        private Int32 m_NbTicks = 0;
        private UInt32 m_NbPoints = 0;
        private Boolean m_WithTradeCancelCorrection = true;
        private DateTime m_DateTimeBegin = new DateTime();
        private DateTime m_DateTimeEnd = new DateTime();
        private UInt16 m_Duration = 60;
        private bool m_IsSubscription;
        private UInt32 m_AdjustmentFactors = AdjustmentFactor.None;
        private PrinterHistoricalData m_Printer = new PrinterHistoricalData();
        private bool m_DumpTradeConditons = false;
        private UInt16 m_NbBarsBeforeBegin = 0;
        #endregion

        #region Constructor
        public RequestQuotationGetHisto(RequestGetHistoFlavor flavor)
        {
            m_HistoFlavor = flavor;
        }
        #endregion

        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args.Length == 0)
            {
                return false;
            }

            m_PolymorphicInstrumentCodes.Add(args[0]);

            if (m_HistoFlavor == RequestGetHistoFlavor.DownloadAndSubscribe)
            {
                if (args.Length > 1)
                {
                    m_NbPoints = Convert.ToUInt32(args[1]);
                }

                if (args.Length > 2)
                {
                    m_WithTradeCancelCorrection = Convert.ToUInt16(args[2]) > 0;
                }
                if (args.Length > 3)
                {
                    if (!DateTime.TryParseExact(args[3], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeBegin))
                    {
                        Console.WriteLine("Invalid begin date: " + args[3] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                        return false;
                    }
                }
                if (args.Length > 4)
                {
                    if (!DateTime.TryParseExact(args[4], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeEnd))
                    {
                        Console.WriteLine("Invalid end date: " + args[4] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                        return false;
                    }
                }

                m_IsSubscription = (m_NbPoints == 0) ? m_DateTimeEnd.Equals(DateTime.MinValue) : m_DateTimeBegin.Equals(DateTime.MinValue);
            }
            else if (m_HistoFlavor == RequestGetHistoFlavor.IntradayExtendedNTicks)
            {
                if (args.Length < 1)
                {
                    return false;
                }

                m_NbTicks = Convert.ToInt32(args[1]);

                if (args.Length > 2)
                {
                    if (!DateTime.TryParseExact(args[2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeBegin))
                    {
                        Console.WriteLine("Invalid begin date: " + args[2] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                        return false;
                    }
                }

                m_IsSubscription = false;
            }
            else if (m_HistoFlavor == RequestGetHistoFlavor.IntradayBarsExt)
            {
                if (args.Length < 1)
                {
                    return false;
                }

                if (args.Length > 1)
                {
                    m_Duration = Convert.ToUInt16(args[1]);
                }

                if (args.Length > 2)
                {
                    if (!DateTime.TryParseExact(args[2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeBegin))
                    {
                        Console.WriteLine("Invalid begin date: " + args[2] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                        return false;
                    }
                }

                if (args.Length > 3)
                {
                    if (!DateTime.TryParseExact(args[3], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeEnd))
                    {
                        Console.WriteLine("Invalid end date: " + args[3] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                        return false;
                    }
                }

                if (args.Length > 4)
                {
                    m_NbBarsBeforeBegin = Convert.ToUInt16(args[4]);
                }

                m_IsSubscription = m_DateTimeEnd.Equals(DateTime.MinValue);
            }
            else
            {
                return false;
            }

            return true;
        }

//Histo Ext Start
/** \cond refguide
 * This sample demonstrates how to use the \c DownloadHistoryIntradayExtended
 * subscription to download or download then subscribe to intraday historical
 * data. This request also gives access to trade cancels and corrections.
 * 
 * Implement and register handlers for common events (\c Started,
 * \c Failed, \c Succeeded).
 *
 * Implement event handlers specific to this request:
 * \snippet PrinterHistoricalData.cs snippet_iext_data_handler
 * 
 * \snippet PrinterHistoricalData.cs snippet_iext_cancel_handler
 * 
 * \snippet PrinterHistoricalData.cs snippet_iext_correction_handler
 * 
 * And register them in the connection object:
 * \snippet RequestQuotationGetHisto.cs snippet_iext_register
 * 
 * Set the input parameters as needed: list of instruments, begin and end
 * timestamp, number of points, whether or not trade cancels and corrections
 * must be also downloaded. Refer to the request documentation for a
 * comprehensive explanation of these parameters.
 * The request can now be sent:
 * \snippet RequestQuotationGetHisto.cs snippet_iext_request
 *
 * Implementation now depends on the subscription mode. If only historical data
 * was requested, the subscription will end itself by sending an abort message.
 * In this case users have to wait for the abort event handler to be called. If
 * real-time data was requested, the subscription will never end (unless an error
 * occurs), and users have to stop it when no longer needed by removing the 
 * request from the connection object:
 * \snippet RequestQuotationGetHisto.cs snippet_stop_request
 * 
 * When the subscription is over, don't forget to unregister the event handlers.
 * \endcond
 */
//Histo Ext End

//Histo Bars Start
/** \cond refguide
 * This sample demonstrates how to use the \c DownloadHistoryIntradayBarsExt
 * subscription to download or download then subscribe to historical bars.
 * This request also lets users choose to apply corporate action adjustment
 * factors if necessary.
 * 
 * Implement and register handlers for common events (\c Started,
 * \c Failed, \c Succeeded).
 *
 * Implement event handlers specific to this request:
 * \snippet PrinterIntradayBar.cs snippet_ibars_bars_handler
 * 
 * \snippet PrinterIntradayBar.cs snippet_ibars_delete_handler
 * 
 * \snippet PrinterIntradayBar.cs snippet_ibars_corrected_handler
 * 
 * And register them in the connection object:
 * \snippet RequestQuotationGetHisto.cs snippet_ibars_register
 * 
 * Set the input parameters as needed: list of instrument codes, begin and end
 * timestamp, duration of the bars, types of adjustment factors to apply.
 * Refer to the request documentation for a comprehensive explanation of these
 * parameters.
 * The request can now be sent:
 * \snippet RequestQuotationGetHisto.cs snippet_ibars_request
 *
 * Implementation now depends on the subscription mode. If only historical data
 * was requested, the subscription will end itself by sending an abort message.
 * In this case users have to wait for the abort event handler to be called. If
 * real-time data was requested, the subscription will never end (unless an error
 * occurs), and users have to stop it when no longer needed by removing the 
 * request from the connection object:
 * \snippet RequestQuotationGetHisto.cs snippet_stop_request
 * 
 * When the subscription is over, don't forget to unregister the event handlers.
 * \endcond
 */
//Histo Bars End

        public override bool Start(Connection connection, uint requestID)
        {
            GetTradeConditionsDictionnary(connection);

            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);

            switch (m_HistoFlavor)
            {
                case RequestGetHistoFlavor.DownloadAndSubscribe:
                {
                	//![snippet_iext_register]
                    m_Connection.HistoIntradayDataExtendedHandler += 
                    		new FeedOSManaged.HistoIntradayDataExtendedEventHandler(
                    				m_Printer.HistoIntradayDataExtended);
                    m_Connection.HistoIntradayCancelHandler += 
                    		new FeedOSManaged.HistoIntradayCancelEventHandler(
                    				m_Printer.HistoIntradayCancel);
                    m_Connection.HistoIntradayCorrectionHandler += 
                    		new FeedOSManaged.HistoIntradayCorrectionEventHandler(
                    				m_Printer.HistoIntradayCorrection);
                    //![snippet_iext_register]

                    if (m_IsSubscription)
                    {
                        Console.WriteLine("Subscribing to quotation data...");
                    }
                    else
                    {
                        Console.WriteLine("Getting quotation data...");
                    }
                    //![snippet_iext_request]
                    m_Connection.DownloadAndSubscribeIntradayDataExtended(
                    		m_PolymorphicInstrumentCodes, m_DateTimeBegin,
							m_DateTimeEnd, m_NbPoints, m_WithTradeCancelCorrection,
							m_RequestID);
                    //![snippet_iext_request]
                    return true;
                }

                case RequestGetHistoFlavor.IntradayExtendedNTicks:
                    {
                        m_Connection.HistoIntradayExtendedNTicksHandler += new FeedOSManaged.HistoIntradayExtendedNTicksEventHandler(m_Printer.HistoIntradayExtended);
                        Console.WriteLine("Getting quotation data...");
                        m_Connection.GetHistoIntradayExtendedNTicks(m_PolymorphicInstrumentCodes[0], m_DateTimeBegin, m_NbTicks, m_RequestID);
                        return true;
                    }

                case RequestGetHistoFlavor.IntradayBarsExt:
                {
                	//![snippet_ibars_register]
                    m_Connection.IntradayBarsHandler +=
                     		new FeedOSManaged.IntradayBarsEventHandler(
                      				PrinterIntradayBar.QuotationIntradayBar);
                    m_Connection.DeleteBarHandler +=
                       		new FeedOSManaged.DeleteBarEventHandler(
                      				PrinterIntradayBar.DeletedBar);
                    m_Connection.CorrectedBarHandler +=
                       		new FeedOSManaged.CorrectedBarEventHandler(
                       				PrinterIntradayBar.CorrectedBar);
                    //![snippet_ibars_register]
                        
                    if (m_IsSubscription)
                    {
                        Console.WriteLine("Subscribing to quotation data...");
                    }
                    else
                    {
                        Console.WriteLine("Getting quotation data...");
                    }
                    //![snippet_ibars_request]
                    m_Connection.DownloadHistoIntradayBarsExt(
                       		m_PolymorphicInstrumentCodes, m_DateTimeBegin,
							m_DateTimeEnd, m_Duration, m_AdjustmentFactors,
							m_NbBarsBeforeBegin, m_RequestID);
                    //![snippet_ibars_request]
                    return true;
                }

                default:
                    break;
            }

            return false;
        }
        public override uint Wait()
        {
            if (m_IsSubscription)
            {
                return WaitBehaviour();
            }
            return NoWaitBehaviour();
        }
        public override void Stop()
        {
        	//![snippet_stop_request]
            m_Connection.RemoveRequest(m_RequestID);
            //![snippet_stop_request]
            UnconfigureRequestEvents(m_Connection);

            switch (m_HistoFlavor)
            {
                case RequestGetHistoFlavor.DownloadAndSubscribe:
                    {
                        m_Connection.HistoIntradayDataExtendedHandler -= new FeedOSManaged.HistoIntradayDataExtendedEventHandler(m_Printer.HistoIntradayDataExtended);
                        m_Connection.HistoIntradayCancelHandler -= new FeedOSManaged.HistoIntradayCancelEventHandler(m_Printer.HistoIntradayCancel);
                        m_Connection.HistoIntradayCorrectionHandler -= new FeedOSManaged.HistoIntradayCorrectionEventHandler(m_Printer.HistoIntradayCorrection);
                        break;
                    }

                case RequestGetHistoFlavor.IntradayExtendedNTicks:
                    {
                        m_Connection.HistoIntradayExtendedNTicksHandler -= new FeedOSManaged.HistoIntradayExtendedNTicksEventHandler(m_Printer.HistoIntradayExtended);
                        break;
                    }

                case RequestGetHistoFlavor.IntradayBarsExt:
                    {
                        m_Connection.IntradayBarsHandler -= new FeedOSManaged.IntradayBarsEventHandler(PrinterIntradayBar.QuotationIntradayBar);
                        m_Connection.DeleteBarHandler -= new FeedOSManaged.DeleteBarEventHandler(PrinterIntradayBar.DeletedBar);
                        m_Connection.CorrectedBarHandler -= new FeedOSManaged.CorrectedBarEventHandler(PrinterIntradayBar.CorrectedBar);
                        break;
                    }
            }
        }
        #endregion

        #region Helpers
        private void GetTradeConditionsDictionnary(Connection connection)
        {
            Console.WriteLine("Getting referential data...");
            RequestReferentialGetTradeConditionsDictionary downloadDictionaryRequest = new RequestReferentialGetTradeConditionsDictionary(m_DumpTradeConditons);
            if (downloadDictionaryRequest.Start(connection, Request.NextId))
            {
                downloadDictionaryRequest.Wait();
                downloadDictionaryRequest.Stop();
            }
            else
            {
                API.LogCritical("RequestDownloadAndSubscribeIntradayExtendedGetHistoGetTradeConditionsDictionary failed to start.");
                throw new Exception("RequestDownloadAndSubscribeIntradayExtended GetTradeConditionsDictionary request cannot start");
            }
        }
        #endregion
    }
}
