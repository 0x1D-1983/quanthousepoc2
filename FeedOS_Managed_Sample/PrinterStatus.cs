///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterStatus
    {
        public static void MarketNewsHandler(uint requestId, MarketNews marketNews)
        {
            Console.WriteLine("MarketNews: FOSMarketId=" + marketNews.FOSMarketId + ", OrigUTCTime=" + marketNews.OrigUTCDateTime,
                "FIXMarketNewsUrgency=" + marketNews.Urgency + ", Headline=" + marketNews.Headline +
                "URLLink=" + marketNews.URLLink + ", Content=" + marketNews.Content);
            if ((marketNews.InstrumentCodeList != null) && (marketNews.InstrumentCodeList.Count > 0)) {
                Console.WriteLine("\tRelated Instruments:");
                foreach (UInt32 instrumentCode in marketNews.InstrumentCodeList) {
                    Console.WriteLine("\tRelated Instruments:" + instrumentCode);
                }
            }
        }

        public static void MarketStatusHandler(uint requestId, MarketStatus marketStatus)
        {
            Console.WriteLine("MarketStatus: FOSMarketId=" + marketStatus.FOSMarketId);
        }
    }
}
