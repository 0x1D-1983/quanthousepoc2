///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Performing a dump (all or part) of the markets
    /// </summary>
	//![snippet_refdump_handler]
    class RequestReferentialDumpStructure : Request
	//![snippet_refdump_handler]
    {
        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if ((args != null) && (args.Length == 0))
            {
                return true;
            }
            else
            {
                Console.WriteLine("Invalid argument number for \"ReferentialDumpStructure\" request");
                return false;
            }
        }

		/** \cond refguide
		 * The following sample in .NET shows you how to dump referential market structures.
		 * It forwards the events to the referential printer, which displays the markets characteristics.
		 *
		 * <ol><li>The handler inherits the %Request interface:</li></ol>
		 * \snippet RequestReferentialDumpStructure.cs snippet_refdump_handler
		 *
		 * <ol><li>Register the event handler:</li></ol>
		 * \snippet RequestReferentialDumpStructure.cs snippet_refdump_reg
		 *
		 * \endcond
		 */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
			//![snippet_refdump_reg]
            ConfigureRequestEvents(connection);
            m_Connection.MarketsHandler +=
            		new FeedOSManaged.MarketsEventHandler(
            				PrinterReferential.Markets);
            m_Connection.ReferentialDumpStructure(m_RequestID);
			//![snippet_refdump_reg]
            return true;
        }

        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.MarketsHandler -= new FeedOSManaged.MarketsEventHandler(PrinterReferential.Markets);
        }

        #endregion
    }
}
