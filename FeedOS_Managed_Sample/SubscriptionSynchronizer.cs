///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System.Collections.Generic;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;
using FeedOSManaged;
using FeedOS_Managed_Sample;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Allows subscription to wait for request id before issuing add/remove instruments
    /// </summary>
	public class SubscriptionSynchronizer
	{
		private RequestSubscribe	m_Subscriber;
		private Semaphore			m_Semaphore;
		private bool				m_Released;

		public SubscriptionSynchronizer(RequestSubscribe sub)
		{
			m_Subscriber = sub;
			m_Semaphore = new Semaphore (0, 1);
			m_Released = false;
		}

		public void Wait()
		{
			m_Semaphore.WaitOne();
		}

		public void RequestFailedCB (uint requestId, uint returnCode)
		{
			Release();
			m_Subscriber.RequestFailedCB (requestId, returnCode);
		}

		public void RequestAbortedCB (uint requestId, uint returnCode)
		{
			Release();
			m_Subscriber.RequestAbortedCB (requestId, returnCode);
		}

		protected void Release()
		{
			if (!m_Released)
			{
				m_Released = true;
				m_Semaphore.Release();
			}
		}

	}

    /// <summary>
    /// Allows subscription to wait for request id before issuing add/remove instruments
    /// </summary>
	public class SubMBLSynchronizer : SubscriptionSynchronizer
	{
		private PrinterMBL			m_PrinterMBL;

		public SubMBLSynchronizer(RequestSubscribe sub, PrinterMBL printer)
		: base (sub) {
			m_PrinterMBL = printer;
		}

        public void MBLAddedInstrumentsHandler(uint requestId, List<uint> addedInstruments)
        {
			m_PrinterMBL.MBLAddedInstrumentsHandler(requestId, addedInstruments);
			Release();
        }

	}

    /// <summary>
    /// Allows subscription to wait for request id before issuing add/remove instruments
    /// </summary>
	public class SubL1Synchronizer : SubscriptionSynchronizer
	{
		public SubL1Synchronizer(RequestSubscribe sub)
		: base (sub) {
		}

        public void SnapshotsHandler(uint requestId, List<InstrumentStatusL1> instrumentStatusL1)
        {
			PrinterL1.SnapshotsHandler(requestId, instrumentStatusL1);
			Release();
        }
	}
}