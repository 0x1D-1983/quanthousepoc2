///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSManaged;
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Searching for instruments based on a “string pattern” with optional filtering based on various criteria
    /// </summary>
	//![snippet_reflookup_handler]
    class RequestReferentialLookup : Request
	//![snippet_reflookup_handler]
    {
        private string m_Pattern;
        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args == null) {
                return false;
            }
            if (args.Length != 1) {
                return false;
            }
            m_Pattern = args[0];
            return true;
        }

		/** \cond refguide
		 * To perform a Referential Lookup %Request in FeedOS .NET API, you need
		 * to provide a string that acts as a filter when querying the referential
		 * server. If an instrument matches the query, it is returned along with
		 * the requested attributes, as described in the sample below
		 * (it forwards the events to the instrument printer):
		 *
		 * <ol><li>The handler inherits the %Request interface:</li></ol>
		 * \snippet RequestReferentialLookup.cs snippet_reflookup_handler
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestReferentialLookup.cs snippet_reflookup_reg
		 *
		 * <ol><li>Customize the optional attributes:</li></ol>
		 * \snippet RequestReferentialLookup.cs snippet_reflookup_custom
		 *
		 * \endcond
		 */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            List<TagNumAndValue> filter = null;
            List<ushort> format = null;
            #region Sample manual filtering
            /*
			//![snippet_reflookup_custom]
            filter = new List<TagNumAndValue>();
            //string px1 = "PX1";
            //filter.Add(new TagNumAndValue((ushort)TagsReferential.TAG_Symbol,
            //		px1));
            string xparName = "XPAR";
            ushort xparMicId = 0;
            API.FOSMarketID(xparName, ref xparMicId);
            filter.Add(new TagNumAndValue(
            		(ushort)TagsReferential.TAG_FOSMarketId, xparMicId));
            format = new List<ushort>();
            format.Add((ushort)TagsReferential.TAG_PriceCurrency);
            format.Add((ushort)TagsReferential.TAG_Symbol);
            format.Add((ushort)TagsReferential.TAG_Description);
            format.Add((ushort)TagsReferential.TAG_FOSMarketId);
            format.Add((ushort)TagsReferential.TAG_SecurityType);
            format.Add((ushort)TagsReferential.TAG_CFICode);
            format.Add((ushort)TagsReferential.TAG_LocalCodeStr);
            format.Add((ushort)TagsReferential.TAG_ISIN);
			//![snippet_reflookup_custom]
            */
            #endregion
            ConfigureRequestEvents(connection);
			//![snippet_reflookup_reg]
            m_Connection.LookupInstrumentsHandler +=
            		new FeedOSManaged.InstrumentsEventHandler(
            				PrinterInstrument.DisplayInstrumentWithFewTags);
            m_Connection.ReferentialLookup(m_Pattern, 5/*(ushort)format.Count*/,
            		FeedOSAPI.Types.LookupMode.LookupMode_Wide, filter, format,
					m_RequestID);
			//![snippet_reflookup_reg]
            return true;
        }

        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.LookupInstrumentsHandler -= new FeedOSManaged.InstrumentsEventHandler(PrinterInstrument.DisplayInstrumentWithFewTags);
        }
        #endregion IRequest Members
    }
}
