///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Subscription to market status : retrieve status of current trading session for all the markets and keep it continually updated
    /// </summary>
    public class RequestSubscribeMarketStatus : Subscription
    {
        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if ((args != null) && (args.Length == 0))
            {
                return true;
            }
            else
            {
                Console.WriteLine("Invalid argument number for \"SubscribeMarketStatus\" request");
                return false;
            }
        }

        public override bool Start(FeedOSManaged.Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);
            m_Connection.MarketNewsHandler += new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
            m_Connection.MarketStatusHandler += new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
            m_Connection.SubscribeStatus(m_RequestID);
            return true;
        }
        public override void Stop()
        {
            UnconfigureRequestEvents(m_Connection);
            m_Connection.RemoveRequest(m_RequestID);
            m_Connection.MarketNewsHandler -= new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
            m_Connection.MarketStatusHandler -= new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
        }
        #endregion IRequest Members
    }
}
