///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2012 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
// FeedOS
using FeedOSManaged;
using System.Threading;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Querying referential variable price increment tables
    /// </summary>
	//![snippet_refget_vpit]
    class RequestReferentialGetVariablePriceIncrementTables : Request
    //![snippet_refget_vpit]
    {
        public RequestReferentialGetVariablePriceIncrementTables()
        {
        }

        #region IRequest Members
        public override bool Parse(string[] args)
        {
            return true;
        }

        /** \cond refguide
         * The following sample in .NET retrieves variable price increment tables.
         *
         * It forwards the events to the variable price increment table printer.
         *
         * <ol><li>The handler inherits the %Request interface:</li></ol>
         * \snippet RequestReferentialGetVariablePriceIncrementTables.cs snippet_refget_vpit
         *
         * <ol><li>Register the event handlers:</li></ol>
         * \snippet RequestReferentialGetVariablePriceIncrementTables.cs snippet_refget_vpit_reg
         * 
         * * <ol><li>Unregister the event handlers:</li></ol>
         * \snippet RequestReferentialGetVariablePriceIncrementTables.cs snippet_refget_vpit_unreg
         *
         * \endcond
         */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            //![snippet_refget_vpit_reg]
            ConfigureRequestEvents(connection);
            m_Connection.GetVariablePriceIncrementTablesHandler +=
            		new FeedOSManaged.GetVariablePriceIncrementTablesEventHandler(
            				PrinterVariablePriceIncrementTables.DumpVariablePriceIncrementTables);
            m_Connection.ReferentialGetVariablePriceIncrementTables(m_RequestID);
            //![snippet_refget_vpit_reg]
            return true;
        }

        public override void Stop()
        {
            //![snippet_refget_vpit_unreg]
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.GetVariablePriceIncrementTablesHandler -=
            		new FeedOSManaged.GetVariablePriceIncrementTablesEventHandler(
            				PrinterVariablePriceIncrementTables.DumpVariablePriceIncrementTables);
            //![snippet_refget_vpit_unreg]
        }

        #endregion

    }
}
