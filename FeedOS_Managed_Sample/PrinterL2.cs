///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2010 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterL2
    {
        private bool m_Verbose;
        private SortedList<uint, OrderBook> g_OrderBookList = new SortedList<uint, OrderBook>();

        public bool Verbose
        {
            get
            {
                return m_Verbose;
            }
            set
            {
                m_Verbose = value;
            }
        }

        public static void Print(OrderBook orderBook)
        {
            CultureInfo ci = new CultureInfo("en-US");

            if (orderBook.BidLimits.Count > orderBook.AskLimits.Count) {
                int i = 0;
                for (; i < orderBook.AskLimits.Count; i++) {
                    Console.WriteLine("{0,10}      BID {1,9} x {2,6}({3,6})  ASK {4,9} x {5,6}({6,6})", i, orderBook.BidLimits[i].Price.ToString("F4", ci), orderBook.BidLimits[i].Size, orderBook.BidLimits[i].NbOrders, orderBook.AskLimits[i].Price.ToString("F4", ci), orderBook.AskLimits[i].Size, orderBook.AskLimits[i].NbOrders, ci);
                }
                for (; i < orderBook.BidLimits.Count; i++) {
                    Console.WriteLine("{0,10}      BID {1,9} x {2,6}({3,6})", i, orderBook.BidLimits[i].Price.ToString("F4", ci), orderBook.BidLimits[i].Size, orderBook.BidLimits[i].NbOrders, ci);
                }
            } else // bidList.Count <= askList.Count
            {
                int i = 0;
                for (; i < orderBook.BidLimits.Count; i++) {
                    Console.WriteLine("{0,10}      BID {1,9} x {2,6}({3,6})  ASK {4,9} x {5,6}({6,6})", i, orderBook.BidLimits[i].Price.ToString("F4", ci), orderBook.BidLimits[i].Size, orderBook.BidLimits[i].NbOrders, orderBook.AskLimits[i].Price.ToString("F4", ci), orderBook.AskLimits[i].Size, orderBook.AskLimits[i].NbOrders, ci);
                }
                for (; i < orderBook.AskLimits.Count; i++) {
                    Console.WriteLine("{0,10}                                      ASK {1,9} x {2,6}({3,6})", i, orderBook.AskLimits[i].Price.ToString("F4", ci), orderBook.AskLimits[i].Size, orderBook.AskLimits[i].NbOrders, ci);
                }
            }
        }

        public void OrderBookSnapshotsHandler(uint requestId, List<InstrumentStatusL2> instrumentStatusL2)
        {
            foreach (InstrumentStatusL2 statusL2 in instrumentStatusL2)
            {
                if (!g_OrderBookList.ContainsKey(statusL2.InstrumentCode))
                {
                    g_OrderBookList.Add(statusL2.InstrumentCode, statusL2.OrderBook);
                    Print(statusL2.OrderBook);
                }
                else
                {
                    g_OrderBookList[statusL2.InstrumentCode] = statusL2.OrderBook;
                }
                Console.WriteLine("OrderBookSnapshotsHandler: " + statusL2.InstrumentCode);
            }
        }
        
        public void OrderBookRefreshHandler(uint requestId, uint instrumentCode, ValueType serverUTCDateTime, OrderBookRefresh orderBookRefresh)
        {
            if (orderBookRefresh.BidLimits != null) {
                foreach (OrderBookEntryExt orderBookEntryExt in orderBookRefresh.BidLimits) {
                    if (orderBookEntryExt.Price == 0) {
                        Console.WriteLine("PrintOrderBookRefresh: orderBookEntryExt.Price == 0");
                    }
                }
            }
            if (orderBookRefresh.AskLimits != null) {
                foreach (OrderBookEntryExt orderBookEntryExt in orderBookRefresh.AskLimits) {
                    if (orderBookEntryExt.Price == 0) {
                        Console.WriteLine("PrintOrderBookRefresh: orderBookEntryExt.Price == 0");
                    }
                }
            }
            OrderBook orderBook = null;
            if (!g_OrderBookList.TryGetValue(instrumentCode, out orderBook)) // There is no Snapshot in replay mode
            { 
                orderBook = new OrderBook(DateTime.MinValue, new List<OrderBookEntryExt>(), new List<OrderBookEntryExt>());
                g_OrderBookList.Add(instrumentCode, orderBook);
            }
            orderBook.Update((DateTime)serverUTCDateTime, orderBookRefresh);
            if (m_Verbose) 
            {
                Print(orderBook);
            }
            Console.WriteLine("OrderBookRefreshHandler: " + instrumentCode);
        }

        public void OrderBookDeltaRefreshHandler(uint requestId, uint instrumentCode, ValueType serverUTCDateTime, OrderBookDeltaRefresh orderBookDeltaRefresh)
        {
            if (null != orderBookDeltaRefresh)
            {
                if (orderBookDeltaRefresh.Price == 0)
                {
                    if ((orderBookDeltaRefresh.Action != OrderBookDeltaAction.OrderBookDeltaAction_AskChangeQtyAtLevel) &&
                        (orderBookDeltaRefresh.Action != OrderBookDeltaAction.OrderBookDeltaAction_BidChangeQtyAtLevel) &&
                        (orderBookDeltaRefresh.Action != OrderBookDeltaAction.OrderBookDeltaAction_ALLClearFromLevel) &&
                        (orderBookDeltaRefresh.Action != OrderBookDeltaAction.OrderBookDeltaAction_AskClearFromLevel) &&
                        (orderBookDeltaRefresh.Action != OrderBookDeltaAction.OrderBookDeltaAction_BidClearFromLevel)) {
                        Console.WriteLine("PrintOrderBookRefresh: orderBookDeltaRefresh.Price == 0");
                    }
                }
                OrderBook orderBook = null;
                if (!g_OrderBookList.TryGetValue(instrumentCode, out orderBook)) { // There is no Snapshot in replay mode
                    orderBook = new OrderBook(DateTime.MinValue, new List<OrderBookEntryExt>(), new List<OrderBookEntryExt>());
                    g_OrderBookList.Add(instrumentCode, orderBook);
                }
                orderBook.Update((DateTime)serverUTCDateTime, orderBookDeltaRefresh);
                Console.WriteLine("OrderBookRefreshHandler: " + instrumentCode);
                if (m_Verbose) {
                    Print(orderBook);
                }
            }
        }

        public void OrderBookMaxVisibleDepthHandler(uint requestId, uint instrumentCode, byte maxVisibleDepth)
        {
            Console.WriteLine("OrderBookMaxVisibleDepthHandler: " + instrumentCode + ", maxVisibleDepth=" + maxVisibleDepth);
        }

    }
}
