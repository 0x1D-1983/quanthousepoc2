///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Adapter class Request embedding id generation as a static responsability.
    /// Base class for simple requests.
    /// </summary>
    public abstract class Request : IRequest
    {
        #region static member
        private static uint g_NextRequestID = 0;
        #endregion

        #region protected data members
        protected uint m_lastReturnCode = 0;
        protected AutoResetEvent m_AutoResetEvent = new AutoResetEvent(false);
        protected Connection m_Connection;
        protected uint m_RequestID;
        protected bool m_Verbose;
        #endregion

        protected Request() { }

        public static uint NextId
        {
            get { return g_NextRequestID++; }
        }

        #region Request Events 
        protected void ConfigureRequestEvents(Connection connection)
        {
            connection.RequestStarted += RequestStartedCB;
            connection.RequestSucceeded += RequestSucceededCB;
            connection.RequestAborted += RequestAbortedCB;
            connection.RequestFailed += RequestFailedCB;
        }

        protected void UnconfigureRequestEvents(Connection connection)
        {
            connection.RequestStarted -= RequestStartedCB;
            connection.RequestSucceeded -= RequestSucceededCB;
            connection.RequestAborted -= RequestAbortedCB;
            connection.RequestFailed -= RequestFailedCB;
        }

        protected void ReplaceFailedRequestEvents(Connection connection,
												  FeedOSManaged.RequestAbortedEventHandler abortedCB,
												  FeedOSManaged.RequestFailedEventHandler failedCB)
        {
			connection.RequestAborted -= RequestAbortedCB;
			connection.RequestAborted += abortedCB;
			connection.RequestFailed -= RequestFailedCB;
			connection.RequestFailed += failedCB;
		}

        protected void ResetFailedRequestEvents(Connection connection,
												FeedOSManaged.RequestAbortedEventHandler abortedCB,
												FeedOSManaged.RequestFailedEventHandler failedCB)
        {
			connection.RequestAborted -= abortedCB;
			connection.RequestAborted += RequestAbortedCB;
			connection.RequestFailed -= failedCB;
			connection.RequestFailed += RequestFailedCB;
		}

        private void RequestStartedCB(uint requestId)
        {
        }

        private void RequestSucceededCB(uint requestId)
        {
            m_lastReturnCode = 0;
            Console.Error.WriteLine("Request Succeeded");
            m_AutoResetEvent.Set();
        }

        public void RequestFailedCB(uint requestId, uint returnCode)
        {
            m_lastReturnCode = returnCode;
            Console.Error.WriteLine("Request Failed " + FeedOSManaged.API.ErrorString(returnCode));
            m_AutoResetEvent.Set();
        }

        public void RequestAbortedCB(uint requestId, uint returnCode)
        {
            m_lastReturnCode = returnCode;
            Console.Error.WriteLine("Request Aborted " + FeedOSManaged.API.ErrorString(returnCode));
            m_AutoResetEvent.Set();
        }
        #endregion Request Events

        #region IRequest Methods
        #region Properties
        public bool Verbose
        {
            get
            {
                return m_Verbose;
            }
            set
            {
                m_Verbose = value;
            }
        }

        public uint LastReturnCode
        {
            get
            {
                return m_lastReturnCode;
            }
        }

        public uint RequestID
        {
            get
            {
                return m_RequestID;
            }
        }
        #endregion Properties

        #region IRequest members
        public abstract bool Parse(string[] args);
        public abstract bool Start(Connection connection, uint requestID);

        /// <summary>
        /// Wait implementation for Requests
        /// </summary>
        /// <returns>Return code if failure/abortion occurs or 0</returns>
        public virtual uint Wait()
        {
            return NoWaitBehaviour();
        }

        public uint WaitBehaviour()
        {
            /*
            #region sample code to display total pending msgs
            while (true)
            {
                uint total_pending_msgs = m_Connection.GetTotalPendingMessages();
                Console.Out.WriteLine("Total pending msgs:" + total_pending_msgs);
                Thread.Sleep(500);
            }
            #endregion 
            */
            m_AutoResetEvent.WaitOne();
            if (0 != m_lastReturnCode)
            {
                Stop();
                // In order to be able to call a new Start with no resource leak after an unsuccessful Wait
                // Start(m_Connection, Request.NextId);
                // Wait();
                return m_lastReturnCode;
            }
            else
            {
                Console.ReadLine();
                return m_lastReturnCode;
            }
        }
        public uint NoWaitBehaviour()
        {
            m_AutoResetEvent.WaitOne();
            if (0 != m_lastReturnCode)
            {
                Stop();
                return m_lastReturnCode;
            }
            else
            {
                return m_lastReturnCode;
            }
        }
        public abstract void Stop();
        #endregion IRequest members
        #endregion IRequest Methods
    }

    /// <summary>
    /// Base class for subscriptions
    /// </summary>
    public abstract class Subscription : Request
    {
        #region mandatories
        public override abstract bool Parse(string[] args);
        public override abstract bool Start(Connection connection, uint requestID);
        public override abstract void Stop();
        #endregion

        #region Wait implementation
        /// <summary>
        /// Wait implementation for Subscription
        /// </summary>
        /// <returns></returns>
        public override uint Wait()
        {
            return WaitBehaviour();
        }
        #endregion
    }

    /// <summary>
    /// Request interface
    /// </summary>
    public interface IRequest
    {
        /// <summary>
        /// Attribute storing the verbosity of the request
        /// </summary>
        bool Verbose { get; set; }

        /// <summary>
        /// Last return code attribute storing the success or the failure of a request submission
        /// </summary>
        uint LastReturnCode { get; }

        /// <summary>
        /// RequestID propertie
        /// </summary>
        uint RequestID { get; }
        /// <summary>
        /// Parsing the command line arguments to grab input parameters used by the request.
        /// </summary>
        /// <param name="args">Command line arguments string array</param>
        /// <returns>true if the operation succeeds, false otherwise. </returns>
        bool Parse(string[] args);
        
        /// <summary>
        /// Start the request
        /// </summary>
        /// <param name="connection">Connection to be used to send the request</param>
        /// <param name="requestID">RequestId to identify myself, has to be generated through Request.NextId</param>
        /// <returns></returns>
        bool Start(Connection connection, uint requestID);
        
        /// <summary>
        /// Wait the request response if either a failure occurs.  
        /// </summary>
        /// <returns>Error code if a failure/abortion occurs or 0 if successful.</returns>
        uint Wait();

        /// <summary>
        /// Stopping the request 
        /// </summary>
        void Stop();
        
    }
}
