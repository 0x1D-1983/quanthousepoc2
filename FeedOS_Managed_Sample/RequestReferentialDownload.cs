///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;
using FeedOSManaged;

///
/// Summary description for token.
///
public class StringTokenizer
{
    private string m_data, m_delimeter;
    private string[] m_tokens;
    private int m_index;

    public StringTokenizer(string strdata, string delim)
    {
        m_data = strdata;
        m_delimeter = delim;
        m_tokens = m_data.Split(m_delimeter.ToCharArray());
        m_index = 0;
    }

    public bool hasElements()
    {
        return (m_index < (m_tokens.Length));
    }

    public string nextElement()
    {
        if (m_index < m_tokens.Length)
        {
            return m_tokens[m_index++];
        }
        else
        {
            return "";
        }
    }
}

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Performing a dump (all or part) of the instruments
    /// </summary>
	//![snippet_refdl_handler]
    public class RequestReferentialDownload : Request
	//![snippet_refdl_handler]
    {
        public RequestReferentialDownload(bool subscribe_to_events)
        {
            m_subscribe_to_events = subscribe_to_events;
        }

        bool                                    m_subscribe_to_events = false;

		//![snippet_refdl]
        private PrinterReferential              m_printer;
		//![snippet_refdl]

        private static int                      NB_DEFAULT_ITEMS_COUNT = 5;
        private List<string>                    m_cficode_filters;
        private Dictionary<string, ushort>      m_mic_filters;
        private List<string>                    m_sectype_filters;
        private List<ushort>                    m_ref_tags_format;

        bool                                    m_add_to_default_list = true;

        bool                                    m_opt_raw_format = false;
        bool                                    m_opt_csv_format = false;
        bool                                    m_opt_no_header = false;


        private void requestUsage()
        {
            string cmd = (m_subscribe_to_events ? "referentialdownloadandsubscribe" : "referentialdownload");
            Console.WriteLine("usage: (...) " + cmd + " [OPTIONS] [MARKETS [SECTYPES [CFICODES]]]");
            Console.WriteLine("");
            Console.WriteLine("purpose: Dump the referential data for a combination of Markets, security types and CFI codes");
            Console.WriteLine("");
            Console.WriteLine("MARKETS  is a comma-separated list of MIC to use as a filter, or \"all\"");
            Console.WriteLine("SECTYPES is a comma-separated list of SecurityType to use as a filter, or \"all\"");
            Console.WriteLine("CFICODES is a comma-separated list of CFICode to use as a filter, or \"all\"");
            Console.WriteLine("");
            Console.WriteLine("OPTIONS:");
            Console.WriteLine("\t-help              Displays help");
            Console.WriteLine("\t-raw               Displays in raw format, exclude -csv output");
            Console.WriteLine("\t-csv               Displays in csv format, exclude -raw output");
            Console.WriteLine("\t-no_header         Don't display header row for raw or csv output");
            Console.WriteLine("\t-f Tag1,Tag2...    (Format -- override the list of tags retrieved)");
            Console.WriteLine("\t+f Tag1,Tag2...    (Format -- add some tags to the default list)");
            Console.WriteLine("");
            Console.WriteLine("example1: (filtering with ISO Market IDs + SecurityType)");
            Console.WriteLine("\t"+cmd+" XEUR,XLIF FUT,OPT");
            Console.WriteLine("\t==> dump all FUTures and OPTions belonging to markets EUREX and LIFFE");
            Console.WriteLine("");
            Console.WriteLine("example2: (filtering with CFICode)");
            Console.WriteLine("\t"+cmd+" -raw -f FOSMarketId,LocalCodeStr all  all  E,MRI");
            Console.WriteLine("\t==> dump in raw mode all Equities and Indices belonging to all markets, only FOSMarketId and LocalCodeStr will be dumped");
            throw new Exception();
        }

        #region IRequest Members
        public override uint Wait()
        {
            if (m_subscribe_to_events)
            {
                return WaitBehaviour();
            }
            else
            {
                return NoWaitBehaviour();
            }
        }

        public override bool Parse(string[] args)
        {
            // Invalid arguments
            if (null == args || 0 == args.Length)
            {
                return true;
            }

            int argc = 0;
            int last_loop_argc = -1;
            bool has_specifed_ref_tags = false;
            while (last_loop_argc != argc)
            {
                last_loop_argc = argc;
                if ((args.Length - argc >= 1) && ("-help" == args[argc] || "-h" == args[argc]))
                {
                    requestUsage();
                    return false;
                }
                if ((args.Length - argc >= 1) && ("-raw" == args[argc]))
                {
                    if (m_opt_csv_format)
                    {
                        Console.WriteLine("-csv and _raw are exclusive options");
                        requestUsage();
                        return false;
                    }
                    m_opt_raw_format = true;
                    ++argc;
                }
                if ((args.Length - argc >= 1) && ("-csv" == args[argc]))
                {
                    if (m_opt_raw_format)
                    {
                        Console.WriteLine("-csv and _raw are exclusive options");
                        requestUsage();
                        return false;
                    }
                    m_opt_csv_format = true;
                    ++argc;
                }
                if ((args.Length - argc >= 1) && ("-no_header" == args[argc]))
                {
                    m_opt_no_header = true;
                    ++argc;
                }
                if ((args.Length - argc >= 1) && ("-f" == args[argc]))
                {
                    has_specifed_ref_tags = true;
                    m_add_to_default_list = false;
                    ++argc;
                }
                if ((args.Length - argc >= 1) && ("+f" == args[argc]))
                {
                    has_specifed_ref_tags = true;
                    m_add_to_default_list = true;
                    ++argc;
                }
                if ((args.Length - argc >= 1) && has_specifed_ref_tags)
                {
                    if (null == m_ref_tags_format) m_ref_tags_format = new List<ushort>(NB_DEFAULT_ITEMS_COUNT);
                    StringTokenizer refTagsTokenizer = new StringTokenizer(args[argc], ",");
                    if (refTagsTokenizer.hasElements())
                    {
                        string cur_tag_str = null;
                        do
                        {
                            cur_tag_str = refTagsTokenizer.nextElement();
                            if (0 != cur_tag_str.Length)
                            {
                                ushort tagId = 0;
                                if (API.TagString2Num(cur_tag_str, ref tagId))
                                {
                                    m_ref_tags_format.Add(tagId);
                                }
                                // May be a numerical tag, unknown from the API,
                                // trying to parse it
                                else
                                {
                                    try
                                    {
                                        tagId = Convert.ToUInt16(cur_tag_str);
                                        m_ref_tags_format.Add(tagId);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                        }
                        while ((null != cur_tag_str) && (0 != cur_tag_str.Length));
                    }
                    ++argc;
                    has_specifed_ref_tags = false;
                }
                else
                {
                    // -f/+f without anymore cmd line parameters
                    if (has_specifed_ref_tags)
                    {
                        return false;
                    }
                }
            }

            if ((args.Length - argc >= 1))
            {
                if ("all" == args[argc])
                {
                    ++argc;
                }
                else
                {
                    // Parsing MIC, CFICODE and SECTYPE filters
                    StringTokenizer micTokenizer = new StringTokenizer(args[argc], ",");
                    ushort current_mic_id = 0;
                    if (micTokenizer.hasElements())
                    {
                        string current_mic = null;
                        do
                        {
                            if (null == m_mic_filters) m_mic_filters = new Dictionary<string, ushort>(NB_DEFAULT_ITEMS_COUNT);
                            current_mic = micTokenizer.nextElement();
                            if (0 != current_mic.Length)
                            {
                                API.FOSMarketID(current_mic, ref current_mic_id);
                                m_mic_filters[current_mic] = current_mic_id;
                            }
                        }
                        while ((null == current_mic) ||
                                (null != current_mic) && (0 != current_mic.Length));
                        ++argc;
                    }
                }
            }
            if (args.Length - argc >= 1)
            {
                if ("all" == args[argc])
                {
                    ++argc;
                }
                else
                {
                    StringTokenizer sectypeTokenizer = new StringTokenizer(args[argc], ",");
                    if (sectypeTokenizer.hasElements())
                    {
                        string cur_sec_type_str = null;
                        do
                        {
                            cur_sec_type_str = sectypeTokenizer.nextElement();
                            if (0 != cur_sec_type_str.Length)
                            {
                                if (null == m_sectype_filters) m_sectype_filters = new List<string>(NB_DEFAULT_ITEMS_COUNT);
                                m_sectype_filters.Add(cur_sec_type_str);
                            }
                        }
                        while ((null == cur_sec_type_str) ||
                                (null != cur_sec_type_str) && (0 != cur_sec_type_str.Length));
                        ++argc;
                    }
                }
            }
            if ((args.Length - argc >= 1))
            {
                if ("all" == args[argc])
                {
                    ++argc;
                }
                else
                {
                    StringTokenizer cficodeTokenizer = new StringTokenizer(args[argc], ",");
                    if (cficodeTokenizer.hasElements())
                    {
                        string cur_cfi_code_str = null;
                        do
                        {
                            cur_cfi_code_str = cficodeTokenizer.nextElement();
                            if (0 != cur_cfi_code_str.Length)
                            {
                                if (null == m_cficode_filters) m_cficode_filters = new List<string>(NB_DEFAULT_ITEMS_COUNT);
                                m_cficode_filters.Add(cur_cfi_code_str);
                            }
                        }
                        while ((null == cur_cfi_code_str) ||
                               (null != cur_cfi_code_str) && (0 != cur_cfi_code_str.Length));
                        ++argc;
                    }
                }
            }
            // Assessing that all arguments are taken into account.
            if (argc == args.Length) return true;
            else
            {
                Console.WriteLine("Sorry, all arguments cannot be processed propertly");
                requestUsage();
                return false;
            }
        }

		/** \cond refguide
		 * To perform a Referential Download or Download and Subscribe %Request in FeedOS .NET API,
		 * you need to implement an interface, customize the attributes of the event handlers,
		 * and then filter the referential data, as described in the procedure below:
		 *
		 * <ol><li>Configure the referential printer to receive the events:</li></ol>
		 * \snippet RequestReferentialDownload.cs snippet_refdl
		 *
		 * <ol><li>The handler inherits the %Request interface:</li></ol>
		 * \snippet RequestReferentialDownload.cs snippet_refdl_handler
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestReferentialDownload.cs snippet_refdl_handler_reg
		 *
		 * <ol><li>Customize the attributes:</li></ol>
		 * \snippet RequestReferentialDownload.cs snippet_refdl_custom
		 *
		 * <ol><li>Filter on the MIC, Security Type and CFI code:</li></ol>
		 * \snippet RequestReferentialDownload.cs snippet_refdl_filter
		 *
		 * <ol><li>Choose between download only and download and subscribe:</li></ol>
		 * \snippet RequestReferentialDownload.cs snippet_refdl_sub
		 * \endcond
		 */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);

            PrinterReferentialLayout layout = PrinterReferentialLayout.Typed;
            if (m_opt_raw_format) layout = PrinterReferentialLayout.Raw;
            else if (m_opt_csv_format) layout = PrinterReferentialLayout.Csv;

            List<TagNumAndValue> filteringAttributes = null;

            ValueType lastUpdateTimestamp = null;
            bool sendCreated = true;
            bool sendModified = true;
            bool sendDeleted = true;
            bool sendOtherData = true; // for DownloadAndSubscribe

            // Creating List of MarketBranchID parameter from input values
            int branch_size = 1;
            if (null != m_mic_filters && 0 != m_mic_filters.Values.Count)  branch_size   *= m_mic_filters.Values.Count;
            if (null != m_sectype_filters && 0 != m_sectype_filters.Count) branch_size   *= m_sectype_filters.Count;
            if (null != m_cficode_filters && 0 != m_cficode_filters.Count) branch_size   *= m_cficode_filters.Count;
            List<MarketBranchId> targetBranches = null;
            string default_cfi_code = "";
            string default_sec_type = "";
            ushort default_mic_id = 0;
            if (null != m_mic_filters)
            {
                targetBranches = new List<MarketBranchId>(branch_size);
                foreach(ushort micid in m_mic_filters.Values)
                {
                    if (null != m_sectype_filters)
                    {
                         foreach(string cur_sec_type in m_sectype_filters)
                         {
                            if (null != m_cficode_filters)
                            {
                                foreach (string cur_cfi_code in m_cficode_filters)
                                 {
									 //![snippet_refdl_filter]
                                    targetBranches.Add(new MarketBranchId(micid,
                                    		cur_sec_type, cur_cfi_code));
									 //![snippet_refdl_filter]
                                 }
                            }
                            else
                            {
                                targetBranches.Add(new MarketBranchId(micid, cur_sec_type, default_cfi_code));
                            }
                         }
                    }
                    else
                    {
                        if (null != m_cficode_filters)
                        {
                            foreach (string cur_cfi_code in m_cficode_filters)
                             {
                                targetBranches.Add(new MarketBranchId(micid, default_sec_type, cur_cfi_code));
                             }
                        }
                        else
                        {
                            targetBranches.Add(new MarketBranchId(micid, default_sec_type, default_cfi_code));
                        }
                    }
                }
            }
            else 
            {
                targetBranches = new List<MarketBranchId>(branch_size);
                if (null != m_sectype_filters)
                {
                     foreach(string cur_sec_type in m_sectype_filters)
                     {
                        if (null != m_cficode_filters)
                        {
                            foreach (string cur_cfi_code in m_cficode_filters)
                             {
                                 targetBranches.Add(new MarketBranchId(default_mic_id, cur_sec_type, cur_cfi_code));
                             }
                        }
                        else
                        {
                            targetBranches.Add(new MarketBranchId(default_mic_id, cur_sec_type, default_cfi_code));
                        }
                     }
                }
                else
                {
                    if (null != m_cficode_filters)
                    {
                        foreach (string cur_cfi_code in m_cficode_filters)
                         {
                             targetBranches.Add(new MarketBranchId(default_mic_id, default_sec_type, cur_cfi_code));
                         }
                    }
                    else
                    {
                        targetBranches.Add(new MarketBranchId(default_mic_id, default_sec_type, default_cfi_code));
                    }
                }
            }
			//![snippet_refdl_custom]
            if (m_add_to_default_list)
            {
                if (null == m_ref_tags_format)
                	m_ref_tags_format = new List<ushort>(3);
                m_ref_tags_format.Add(TagsReferential.TAG_FOSMarketId);
                m_ref_tags_format.Add(TagsReferential.TAG_CFICode);
                m_ref_tags_format.Add(TagsReferential.TAG_SecurityType);

                m_ref_tags_format.Add(TagsReferential.TAG_LocalCodeStr);
                m_ref_tags_format.Add(TagsReferential.TAG_Symbol);
                m_ref_tags_format.Add(TagsReferential.TAG_Description);
                m_ref_tags_format.Add(TagsReferential.TAG_ISIN);

                m_ref_tags_format.Add(TagsReferential.TAG_MaturityYear);
                m_ref_tags_format.Add(TagsReferential.TAG_MaturityMonth);
                m_ref_tags_format.Add(TagsReferential.TAG_MaturityDay);
                m_ref_tags_format.Add(TagsReferential.TAG_StrikePrice);
            }
			//![snippet_refdl_custom]
            
            // Grab unique ref tags out of parsed/enriched List of tag numbers 
            Dictionary<ushort,bool> unique_ref_tags_dict = null;
            if (null != m_ref_tags_format)
            {
                unique_ref_tags_dict = new Dictionary<ushort, bool>(m_ref_tags_format.Count);
                foreach (ushort item in m_ref_tags_format)
                {
                    if (!(unique_ref_tags_dict.ContainsKey(item)))
                    {
                        unique_ref_tags_dict[item] = false;
                    }
                }
            }
            else
            {
                throw new Exception("Internal error, m_ref_tags_format is null");
            }
            List<ushort> unique_ref_tags = new List<ushort>(unique_ref_tags_dict.Keys);

            if (null == m_printer) m_printer = new PrinterReferential(layout, m_opt_no_header, unique_ref_tags);
			//![snippet_refdl_handler_reg]
            m_Connection.BranchBeginHandler +=
            		new FeedOSManaged.BranchBeginEventHandler(
            				m_printer.BranchBegin);
            m_Connection.InstrumentsCreatedHandler +=
            		new FeedOSManaged.InstrumentsCreatedEventHandler(
            				m_printer.InstrumentsCreated);
            m_Connection.InstrumentsModifiedHandler +=
            		new FeedOSManaged.InstrumentsModifiedEventHandler(
            				m_printer.InstrumentsModified);
            m_Connection.InstrumentsDeletedHandler +=
            		new FeedOSManaged.InstrumentsDeletedEventHandler(
            				m_printer.InstrumentsDeleted);
            if (m_subscribe_to_events)
            {
                m_Connection.MarkerTimestampHandler +=
                    new FeedOSManaged.MarkerTimestampEventHandler(
                            m_printer.MarkerTimestamp);
                m_Connection.VariablePriceIncrementTablesHandler +=
                new FeedOSManaged.VariablePriceIncrementTablesEventHandler(
                            m_printer.VariablePriceIncrementTables);
                m_Connection.TradeConditionsDictionaryHandler +=
                    new FeedOSManaged.TradeConditionsDictionaryEventHandler(
                            m_printer.TradeConditionsDictionary);
                m_Connection.MetaDataHandler +=
                    new FeedOSManaged.MetaDataEventHandler(
                            m_printer.MetaData);
                m_Connection.RealtimeBeginHandler +=
                    new FeedOSManaged.RealtimeBeginEventHandler(
                            m_printer.RealtimeBegin);
            }
			//![snippet_refdl_handler_reg]

			//![snippet_refdl_sub]
            if (m_subscribe_to_events)
            {
                m_Connection.ReferentialDownloadAndSubscribe(targetBranches,
                		filteringAttributes, unique_ref_tags,
                		lastUpdateTimestamp, sendCreated, sendModified,
                		sendDeleted, sendOtherData, m_RequestID);
            }
            else
            {
                m_Connection.ReferentialDownload(targetBranches,
                		filteringAttributes, unique_ref_tags,
                		lastUpdateTimestamp, sendCreated, sendModified,
                		sendDeleted, m_RequestID);
            }
			//![snippet_refdl_sub]
            return true;
        }

        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.BranchBeginHandler -= new FeedOSManaged.BranchBeginEventHandler(m_printer.BranchBegin);
            m_Connection.InstrumentsCreatedHandler -= new FeedOSManaged.InstrumentsCreatedEventHandler(m_printer.InstrumentsCreated);
            m_Connection.InstrumentsModifiedHandler -= new FeedOSManaged.InstrumentsModifiedEventHandler(m_printer.InstrumentsModified);
            m_Connection.InstrumentsDeletedHandler -= new FeedOSManaged.InstrumentsDeletedEventHandler(m_printer.InstrumentsDeleted);
            m_Connection.MarkerTimestampHandler -= new FeedOSManaged.MarkerTimestampEventHandler(m_printer.MarkerTimestamp);
            m_Connection.MetaDataHandler -= new FeedOSManaged.MetaDataEventHandler(m_printer.MetaData);

            if (m_subscribe_to_events)
            {
                m_Connection.RealtimeBeginHandler -= new FeedOSManaged.RealtimeBeginEventHandler(m_printer.RealtimeBegin);
            }
        }

        #endregion
    }
}
