///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSManaged;
using System.Threading;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Querying characteristics of instruments
    /// </summary>
	//![snippet_refget_handler]
    class RequestReferentialGetInstruments : Request
	//![snippet_refget_handler]
    {
        private List<string> m_PolymorphicInstrumentCodeList = new List<string>();
        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args.Length == 1) 
            {
                string polymorphicInstrumentCodes = args[0];
                string[] polymorphicInstrumentCodeTab = polymorphicInstrumentCodes.Split(new char[] { ',' });
                foreach (string polymorphicInstrumentCode in polymorphicInstrumentCodeTab) {
                    m_PolymorphicInstrumentCodeList.Add(polymorphicInstrumentCode);
                }
                return true;
            } 
            else 
            {
                Console.WriteLine("Invalid argument number for \"GetInstruments\" request");
            }
            return false;
        }

		/** \cond refguide
		 * The following sample in .NET shows you how to get the referential data of an instrument,
		 * in a synchronous request.
		 *
		 * It forwards the events to the instrument printer.
		 *
		 * <ol><li>The handler inherits the %Request interface:</li></ol>
		 * \snippet RequestReferentialGetInstruments.cs snippet_refget_handler
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestReferentialGetInstruments.cs snippet_refget_reg
		 *
		 * \endcond
		 */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
			//![snippet_refget_reg]
            ConfigureRequestEvents(connection);
            m_Connection.InstrumentsHandler +=
            		new FeedOSManaged.InstrumentsEventHandler(
            				PrinterInstrument.DisplayInstrumentWithAllTags);
            m_Connection.ReferentialGetInstruments(
            		m_PolymorphicInstrumentCodeList, null, false, requestID);
			//![snippet_refget_reg]
            return true;
        }

        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.InstrumentsHandler -= new FeedOSManaged.InstrumentsEventHandler(PrinterInstrument.DisplayInstrumentWithAllTags);
        }

        #endregion

    }
}
