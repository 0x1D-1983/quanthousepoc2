///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2012
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    public enum RequestSubscribeKind
    {
        L1,
        MBL,
        MBO
    }

    /// <summary>
    /// Subscribe to real time quotation data for one or more instruments.
    /// </summary>
    //![snippet_sub_class]
    public class RequestSubscribe : Subscription
    //![snippet_sub_class]
    {
        private RequestSubscribeKind m_Kind;
        private bool                m_Sequentially = false;
        private List<string>        m_PolymorphicInstrumentCodeList = new List<string>();
        private PrinterMBL          m_PrinterMBL;
        private List<uint>          m_LayerIds = new List<uint>();        
        private PrinterMBO          m_PrinterMBO;
		private SubL1Synchronizer	m_SubL1Sync;
		private SubMBLSynchronizer	m_SubMBLSync;
        private bool                m_DoMergeLayers = false;
        private uint                m_MBLMergeTargetLayerId = 0xFFFFFFFF;
        private PrinterMBL.MBLOutputMode m_MBLOutputMode = PrinterMBL.MBLOutputMode.Events;

        static RequestSubscribe()
        {
        }

        public RequestSubscribe(RequestSubscribeKind kind, bool sequentially)
        {
            m_Kind = kind;
            m_Sequentially = sequentially;
        }

        public RequestSubscribe(RequestSubscribeKind kind)
        {
            m_Kind = kind;
        }

        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args.Length < 1) {
                Console.WriteLine("Invalid argument number for \"Subscribe" + m_Kind + (m_Sequentially ? "Seq" : "") + " request");
                Console.WriteLine("Input a file containing instrument codes or Instrument code separated by commas");
                return false;
            }

            int current_arg = 0;
            // Extract L1 optional arguments
            if (m_Kind == RequestSubscribeKind.L1)
            {
                // Extract MBL optional arguments
                if (args.Length > 1 && args[0] == "-cache")
                {
                    ++current_arg;
                    PrinterL1.enablePrintCache();
                }
            }
            else if (m_Kind == RequestSubscribeKind.MBL)
            {
                // Extract MBL optional arguments
                for (; current_arg < args.Length; ++current_arg)
                {
                    if      (args[current_arg] == "-cache")             { m_MBLOutputMode = PrinterMBL.MBLOutputMode.Cache; }
                    else if (args[current_arg] == "-events_and_cache")  { m_MBLOutputMode = PrinterMBL.MBLOutputMode.EventsAndCache; }
                    else if (args[current_arg] == "-merge_layers_to")
                    {
                        m_DoMergeLayers = ((++current_arg < args.Length)
                                            && UInt32.TryParse(args[current_arg], out m_MBLMergeTargetLayerId));
                        if (!m_DoMergeLayers)
                        {
                            Console.WriteLine("-merge_layers_to command line switch was specified without a valid target layer id.");
                            return false;
                        }

                        if (m_MBLMergeTargetLayerId <= MBLLayer.FIRM_QUOTES_LAYER_ID)
                        {
                            Console.WriteLine("The specified merge target layer id is invalid (see reserved values in MBLSnapshot.cs).");
                            return false;
                        }
                    }
                    else if (args[current_arg] == "-layers") 
                    {
                        if (++current_arg <  args.Length)
                        {
                            StringTokenizer layersTokenizer = new StringTokenizer(args[current_arg], ",");
                            if (layersTokenizer.hasElements())
                            {
                                string cur_layer_str = null;
                                do
                                {
                                    cur_layer_str = layersTokenizer.nextElement();
                                    uint cur_mbllayer = 0xFFFFFFFF;
                                    ;
                                    if (UInt32.TryParse(cur_layer_str, out cur_mbllayer))
                                    {
                                        m_LayerIds.Add(cur_mbllayer);
                                    }
                                }
                                while ((null != cur_layer_str) && (0 != cur_layer_str.Length));
                            }
                            if (0 == m_LayerIds.Count)
                            {
                                Console.WriteLine("-layers command line parameters requires layers information");
                                return false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("-layers command line parameters requires layers information");
                            return false;
                        }
                    }
                    else
                    {	// At the first unknown argument, we exit this loop
                        break;
                    }
                }
            }
            // If no layers has been specified, then MBL subscription is done on Layer 0 only
            if (0 == m_LayerIds.Count)
            {
                m_LayerIds.Add(MBLLayer.DEFAULT_LAYER_ID);
            }
            if( (args.Length - current_arg) != 1)
            {
                Console.WriteLine("Invalid argument number for \"Subscribe" + m_Kind + (m_Sequentially ? "Seq" : "") + " request");
                Console.WriteLine("Input a file containing instrument codes or Instrument code separated by commas");
                return false;
            }

            // Get instrument codes
            string polymorphicInstrumentCodes = args[current_arg];
            if (System.IO.File.Exists(polymorphicInstrumentCodes))
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(polymorphicInstrumentCodes))
                {
                    while (sr.Peek() >= 0)
                    {
                        m_PolymorphicInstrumentCodeList.Add(sr.ReadLine());
                    }
                }
            }
            else
            {
                string[] polymorphicInstrumentCodeTab = polymorphicInstrumentCodes.Split(new char[] { ',' });
                foreach (string polymorphicInstrumentCode in polymorphicInstrumentCodeTab)
                {
                    m_PolymorphicInstrumentCodeList.Add(polymorphicInstrumentCode);
                }
            }
            
            return true;
        }

       	/** \cond refguide
		 * To perform a Level 1 instrument subscription in FeedOS .NET API, you need to implement an
		 * interface and several event handlers that handle the request and responses, as described
		 * in the procedure below (it forwards real-time updates to the L1 printer):
		 *
		 * <ol><li>Declare the handler that inherits the \c %Subscription interface:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_class
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_l1_reg
		 *
		 * <ol><li>Perform the request:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_l1_req
		 *
		 * **The following describes the use of generic tags** (:ref:`generic tags <GenericTags>`) **:**
		 *
		 * <ol><li>You can access the name and description the received values refer to:</li></ol>
		 * \snippet PrinterL1.cs snippet_sub_L1_generic_tags
		 *
		 * <ol><li>Here is how to access to the :ref:`entry id <GenericTags_Example_1>`, the name and description by :ref:`source id <GenericTags_Example_1>` of the received values:</li></ol>
		 * \snippet GetGenericTagBySourceId.cs snippet_sub_L1_generic_tags_entry_id_by_source_id
		 * \endcond
		 */
		private void StartL1 (uint requestID)
		{
			//![snippet_sub_l1_reg]
			m_Connection.TradeEventExtHandler +=
					new FeedOSManaged.TradeEventExtEventHandler(
							PrinterL1.TradeEventExtHandler);
			m_Connection.ValuesUpdateHandler +=
					new FeedOSManaged.ValuesUpdateEventHandler(
							PrinterL1.ValuesUpdateHandler);
			m_Connection.MarketNewsHandler +=
					new FeedOSManaged.MarketNewsEventHandler(
							PrinterStatus.MarketNewsHandler);
			m_Connection.MarketStatusHandler +=
					new FeedOSManaged.MarketStatusEventHandler(
							PrinterStatus.MarketStatusHandler);
			m_Connection.TradeCancelCorrectionHandler +=
					new FeedOSManaged.TradeCancelCorrectionEventHandler(
						PrinterL1.TradeCancelCorrectionHandler);
			//![snippet_sub_l1_reg]
			// Launches subscription in batch mode
			if (m_Sequentially)
			{
				m_SubL1Sync = new SubL1Synchronizer (this);
				ReplaceFailedRequestEvents (m_Connection,
											new FeedOSManaged.RequestAbortedEventHandler (m_SubL1Sync.RequestAbortedCB),
											new FeedOSManaged.RequestFailedEventHandler (m_SubL1Sync.RequestFailedCB));
				m_Connection.SnapshotsHandler += new FeedOSManaged.SnapshotsEventHandler(m_SubL1Sync.SnapshotsHandler);

				if (m_PolymorphicInstrumentCodeList.Count > 0)
				{
					string polymorphicInstrumentCode = m_PolymorphicInstrumentCodeList[0];
					List<string> polymorphicInstrumentCodeList = new List<string>();
					polymorphicInstrumentCodeList.Add(polymorphicInstrumentCode);
					m_Connection.SubscribeL1(polymorphicInstrumentCodeList, null, QuotationContent.Mask_EVERYTHING, false, requestID);
					m_SubL1Sync.Wait();
					for (int i = 1; i < m_PolymorphicInstrumentCodeList.Count; ++i)
					{
						polymorphicInstrumentCodeList = new List<string>();
						polymorphicInstrumentCodeList.Add(m_PolymorphicInstrumentCodeList[i]);
						m_Connection.SubscribeL1Add(polymorphicInstrumentCodeList, false, requestID);
					}
				}
			}
			else
			{
				m_Connection.SnapshotsHandler += new FeedOSManaged.SnapshotsEventHandler(PrinterL1.SnapshotsHandler);
				//![snippet_sub_l1_req]
				m_Connection.SubscribeL1(m_PolymorphicInstrumentCodeList, null,
						QuotationContent.Mask_EVERYTHING, false, m_RequestID);
				//![snippet_sub_l1_req]
			}
		}

        /** \cond refguide
		 * To perform an MBL instrument subscription in FeedOS .NET API, you need to implement an
		 * interface and several event handlers that handle the request and responses, as described
		 * in the procedure below (it forwards real-time updates to the MBL printer):
		 *
		 * <ol><li>Declare the handler that inherits the \c %Subscription interface:</li></ol>
	         * \snippet RequestSubscribe.cs snippet_sub_class
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_mbl_reg
		 *
		 * <ol><li>Perform the request:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_mbl_req
		 *
		 * \endcond
		 */
		private void StartMBL (uint requestID)
		{
			m_PrinterMBL = new PrinterMBL();
			m_PrinterMBL.Verbose = m_Verbose;
            m_PrinterMBL.OutputMode = m_MBLOutputMode;
            if ( m_DoMergeLayers )
            {
               m_PrinterMBL.DoMergeLayers = m_DoMergeLayers;
               m_PrinterMBL.MergeTargetLayerId = m_MBLMergeTargetLayerId;
            }
			
            //![snippet_sub_mbl_reg]
			m_Connection.MBLFullRefreshHandler +=
					new FeedOSManaged.MBLFullRefreshEventHandler(
							m_PrinterMBL.MBLFullRefreshHandler);
			m_Connection.MBLOverlapRefreshHandler +=
					new FeedOSManaged.MBLOverlapRefreshEventHandler(
							m_PrinterMBL.MBLOverlapRefreshHandler);
			m_Connection.MBLDeltaRefreshHandler +=
					new FeedOSManaged.MBLDeltaRefreshEventHandler(
							m_PrinterMBL.MBLDeltaRefreshHandler);
			m_Connection.MBLMaxVisibleDepthHandler +=
					new FeedOSManaged.MBLMaxVisibleDepthEventHandler(
							m_PrinterMBL.MBLMaxVisibleDepthHandler);
			//![snippet_sub_mbl_reg]
			// Launches subscription in batch mode
			if (m_Sequentially)
			{
				m_SubMBLSync = new SubMBLSynchronizer (this, m_PrinterMBL);
				ReplaceFailedRequestEvents (m_Connection,
											new FeedOSManaged.RequestAbortedEventHandler (m_SubMBLSync.RequestAbortedCB),
											new FeedOSManaged.RequestFailedEventHandler (m_SubMBLSync.RequestFailedCB));
				
				m_Connection.MBLAddedInstrumentsHandler += new FeedOSManaged.MBLAddedInstrumentsEventHandler(m_SubMBLSync.MBLAddedInstrumentsHandler);
				if (m_PolymorphicInstrumentCodeList.Count > 0)
				{
					string polymorphicInstrumentCode = m_PolymorphicInstrumentCodeList[0];
					List<string> polymorphicInstrumentCodeListOneByOne = new List<string>();
				polymorphicInstrumentCodeListOneByOne.Add(polymorphicInstrumentCode);
					m_Connection.SubscribeMBL(polymorphicInstrumentCodeListOneByOne, m_LayerIds, false, m_RequestID);
					m_SubMBLSync.Wait();
					for (int i = 1; i < m_PolymorphicInstrumentCodeList.Count; ++i)
					{
						polymorphicInstrumentCodeListOneByOne = new List<string>();
    					polymorphicInstrumentCodeListOneByOne.Add(m_PolymorphicInstrumentCodeList[i]);
						m_Connection.SubscribeMBLAdd(polymorphicInstrumentCodeListOneByOne, false, requestID);
					}
				}
			}
			else
			{
				m_Connection.MBLAddedInstrumentsHandler += new FeedOSManaged.MBLAddedInstrumentsEventHandler(m_PrinterMBL.MBLAddedInstrumentsHandler);
				//![snippet_sub_mbl_req]
				m_Connection.SubscribeMBL(m_PolymorphicInstrumentCodeList,
						m_LayerIds, false, m_RequestID);
				//![snippet_sub_mbl_req]
			}
		}

        /** \cond refguide
		 * To perform an MBO instrument subscription in FeedOS .NET API, you need to implement an
		 * interface and several event handlers that handle the request and responses, as described
		 * in the procedure below (it forwards real time updates to the MBO printer):
    	         *
		 * <ol><li>Declare the handler that inherits the \c %Subscription interface:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_class
		 *
		 * <ol><li>Register the event handlers:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_mbo_reg
		 *
		 * <ol><li>Perform the request:</li></ol>
		 * \snippet RequestSubscribe.cs snippet_sub_mbo_req
		 *
		 * \endcond
		 */
		private bool StartMBO (uint requestID)
		{
			m_PrinterMBO = new PrinterMBO();
			//![snippet_sub_mbo_reg]
			m_Connection.MarketSheetSnapshotHandler +=
					new FeedOSManaged.MarketSheetSnapshotEventHandler(
							m_PrinterMBO.MarketSheetSnapshotHandler);
			m_Connection.NewOrderHandler            +=
					new FeedOSManaged.NewOrderEventHandler(
							m_PrinterMBO.NewOrderHandler);
			m_Connection.ModifyOrderHandler         +=
					new FeedOSManaged.ModifyOrderEventHandler(
							m_PrinterMBO.ModifyOrderHandler);
			m_Connection.RemoveOneOrderHandler      +=
					new FeedOSManaged.RemoveOneOrderEventHandler(
							m_PrinterMBO.RemoveOneOrderHandler);
			m_Connection.RemoveAllPreviousOrdersHandler +=
					new FeedOSManaged.RemoveAllPreviousOrdersEventHandler(
							m_PrinterMBO.RemoveAllPreviousOrdersHandler);
			m_Connection.RemoveAllOrdersHandler     +=
					new FeedOSManaged.RemoveAllOrdersEventHandler(
							m_PrinterMBO.RemoveAllOrdersHandler);
			m_Connection.RetransmissionHandler      +=
					new FeedOSManaged.RetransmissionEventHandler(
							m_PrinterMBO.RetransmissionHandler);
			m_Connection.ValuesUpdateOneInstrumentHandler +=
					new FeedOSManaged.ValuesUpdateOneInstrumentEventHandler(
							m_PrinterMBO.ValuesUpdateOneInstrumentHandler);
			//![snippet_sub_mbo_reg]
                    
			if (1 == m_PolymorphicInstrumentCodeList.Count)
			{
				//![snippet_sub_mbo_req]
				m_Connection.SubscribeMBO(m_PolymorphicInstrumentCodeList[0],
						m_RequestID);
				//![snippet_sub_mbo_req]
			}
			else
			{
                //![snippet_sub_instruments_mbo_req]
                m_Connection.SubscribeInstrumentsMBO(
                		m_PolymorphicInstrumentCodeList, false, m_RequestID);
                //![snippet_sub_instruments_mbo_req]
			}
			return true;
		}

        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            ConfigureRequestEvents(connection);
            m_RequestID = requestID;
            switch (m_Kind) {
                case RequestSubscribeKind.L1:
                    StartL1 (requestID);
                    break;
                case RequestSubscribeKind.MBL:
                    StartMBL (requestID);
                    break;
                case RequestSubscribeKind.MBO:
                    StartMBO (requestID);
                    break;
                default:
                    return false;
            }
            return true;
        }

        private void StopL1()
        {
            if (m_Sequentially) 
            {
                m_Connection.SnapshotsHandler -= new FeedOSManaged.SnapshotsEventHandler(m_SubL1Sync.SnapshotsHandler);
						ResetFailedRequestEvents (m_Connection, 
												  new FeedOSManaged.RequestAbortedEventHandler (m_SubL1Sync.RequestAbortedCB),
												  new FeedOSManaged.RequestFailedEventHandler (m_SubL1Sync.RequestFailedCB));
                m_Connection.SubscribeL1Remove(m_PolymorphicInstrumentCodeList, m_RequestID);
            }
            UnconfigureRequestEvents(m_Connection);
            m_Connection.RemoveRequest(m_RequestID);
            if (!m_Sequentially)
            {
                m_Connection.SnapshotsHandler -= new FeedOSManaged.SnapshotsEventHandler(PrinterL1.SnapshotsHandler);
            }
            m_Connection.TradeEventExtHandler -= new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
            m_Connection.ValuesUpdateHandler -= new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
            m_Connection.MarketNewsHandler -= new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
            m_Connection.MarketStatusHandler -= new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
            m_Connection.TradeCancelCorrectionHandler -= new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
        }

        private void StopMBL()
        {
            if (m_Sequentially)
            {
                m_Connection.MBLAddedInstrumentsHandler -= new FeedOSManaged.MBLAddedInstrumentsEventHandler(m_SubMBLSync.MBLAddedInstrumentsHandler);
                ResetFailedRequestEvents(m_Connection,
                                          new FeedOSManaged.RequestAbortedEventHandler(m_SubMBLSync.RequestAbortedCB),
                                          new FeedOSManaged.RequestFailedEventHandler(m_SubMBLSync.RequestFailedCB));
                m_Connection.SubscribeMBLRemove(m_PolymorphicInstrumentCodeList, m_RequestID);
            }
            UnconfigureRequestEvents(m_Connection);
            m_Connection.RemoveRequest(m_RequestID);
            if (!m_Sequentially)
            {
                m_Connection.MBLAddedInstrumentsHandler -= new FeedOSManaged.MBLAddedInstrumentsEventHandler(m_PrinterMBL.MBLAddedInstrumentsHandler);
            }
            m_Connection.MBLFullRefreshHandler -= new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
            m_Connection.MBLOverlapRefreshHandler -= new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
            m_Connection.MBLDeltaRefreshHandler -= new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
            m_Connection.MBLMaxVisibleDepthHandler -= new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);
        }

        private void StopMBO()
        {
            UnconfigureRequestEvents(m_Connection);
            m_Connection.RemoveRequest(m_RequestID);

            m_Connection.MarketSheetSnapshotHandler -= new FeedOSManaged.MarketSheetSnapshotEventHandler(m_PrinterMBO.MarketSheetSnapshotHandler);
            m_Connection.NewOrderHandler -= new FeedOSManaged.NewOrderEventHandler(m_PrinterMBO.NewOrderHandler);
            m_Connection.ModifyOrderHandler -= new FeedOSManaged.ModifyOrderEventHandler(m_PrinterMBO.ModifyOrderHandler);
            m_Connection.RemoveOneOrderHandler -= new FeedOSManaged.RemoveOneOrderEventHandler(m_PrinterMBO.RemoveOneOrderHandler);
            m_Connection.RemoveAllPreviousOrdersHandler -= new FeedOSManaged.RemoveAllPreviousOrdersEventHandler(m_PrinterMBO.RemoveAllPreviousOrdersHandler);
            m_Connection.RemoveAllOrdersHandler -= new FeedOSManaged.RemoveAllOrdersEventHandler(m_PrinterMBO.RemoveAllOrdersHandler);
            m_Connection.RetransmissionHandler -= new FeedOSManaged.RetransmissionEventHandler(m_PrinterMBO.RetransmissionHandler);
            m_Connection.ValuesUpdateOneInstrumentHandler -= new FeedOSManaged.ValuesUpdateOneInstrumentEventHandler(m_PrinterMBO.ValuesUpdateOneInstrumentHandler);
        }

        public override void Stop()
        {
            switch (m_Kind) {
                case RequestSubscribeKind.L1:
                    StopL1();
                    break;
                case RequestSubscribeKind.MBL:
                    StopMBL();
                    break;
                case RequestSubscribeKind.MBO:
                    StopMBO();
                    break;
                default:
                    throw new Exception("Stop() unsupported RequestSubscribeKind:" + m_Kind);
            }
        }

        #endregion
    }
}
