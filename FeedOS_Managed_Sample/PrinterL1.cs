///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterL1 : PrinterBase
    {
        private static Dictionary<uint, InstrumentQuotationData> s_InstrumentQuotationDataCache;
        private static CultureInfo m_CultureInfo = null;
        private static ushort[] s_ManagedTags;
        private static bool s_PrintCache = false;

        /// <summary>
        /// get or create an OtherValues cache object per instrument
        /// </summary>
        /// <param name="internal_code">feedos internal code, identifying instrument in real-time stream</param>
        /// <returns></returns>
        static public InstrumentQuotationData getOrCreateInstrumentQuotationData(uint internal_code)
        {
            InstrumentQuotationData instr_quot_data;
            if (!s_InstrumentQuotationDataCache.TryGetValue(internal_code, out instr_quot_data))
            {
                s_InstrumentQuotationDataCache.Add(internal_code, new InstrumentQuotationData(internal_code));
                instr_quot_data = s_InstrumentQuotationDataCache[internal_code];
            }
            return instr_quot_data;
        }

        static public void enablePrintCache()
        {
            s_PrintCache = true;
        }

        static private void fillManagedTags()
        {
            s_ManagedTags = new ushort[] 
            {
                FeedOSAPI.Types.TagsQuotation.TAG_TradingStatus,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyOpeningPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyClosingPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyHighPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyLowPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyTotalVolumeTraded,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyTotalAssetTraded,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyTotalOffBookVolumeTraded,
                FeedOSAPI.Types.TagsQuotation.TAG_DailyTotalOffBookAssetTraded, 
                FeedOSAPI.Types.TagsQuotation.TAG_SessionOpeningPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionClosingPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionHighPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionLowPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_PreviousSessionClosingPrice,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionTotalVolumeTraded,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionTotalAssetTraded,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionTotalOffBookVolumeTraded,
                FeedOSAPI.Types.TagsQuotation.TAG_SessionTotalOffBookAssetTraded, 
                FeedOSAPI.Types.TagsQuotation.TAG_TradingSessionId,
                FeedOSAPI.Types.TagsQuotation.TAG_InternalDailyClosingPriceType,
                FeedOSAPI.Types.TagsQuotation.TAG_PreviousInternalDailyClosingPriceType,
                FeedOSAPI.Types.TagsQuotation.TAG_MARKET_TradingStatus,
                FeedOSAPI.Types.TagsQuotation.TAG_MARKET_HaltReason
            };
            Array.Sort(s_ManagedTags);
        }

        static public bool doManageTag(ushort tag_id)
        {
            // @TODO: implement BinarySearch here
            return (-1 != Array.IndexOf(s_ManagedTags, tag_id));
        }

        static private void initializeCache(InstrumentQuotationData cache, InstrumentStatusL1 statusL1)
        {
            cache.BestLimits = statusL1.OrderBookBestLimitsExt;

            if (statusL1.QuotationVariables != null)
            {
                foreach (TagNumAndValue tagNumAndValue in statusL1.QuotationVariables)
                {
                    if (doManageTag(tagNumAndValue.Num))
                    {
                        if (null == tagNumAndValue.Value)
                        {
                            cache.removeTagByNumber(tagNumAndValue.Num);
                        }
                        else
                        {
                            cache.setTag(tagNumAndValue);
                        }
                    }
                }
            }
        }

        static PrinterL1()
        {
            SetCultureInfo();
            s_InstrumentQuotationDataCache = new Dictionary<uint, InstrumentQuotationData>();
            fillManagedTags();
        }

        private static void SetCultureInfo()
        {
            if (m_CultureInfo == null)
            {
                m_CultureInfo = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            }
        }

        //![snippet_snap_l1_handler_impl]
        public static void SnapshotsHandler(uint requestId, List<InstrumentStatusL1> instrumentStatusL1)
        //![snippet_snap_l1_handler_impl]
        {
            Console.WriteLine("PrinterL1.SnaphotsHandler called");
            if (null != instrumentStatusL1)
            {
                foreach (InstrumentStatusL1 statusL1 in instrumentStatusL1)
                {
                    uint instrumentCode = statusL1.InstrumentCode;

                    InstrumentQuotationData cache = getOrCreateInstrumentQuotationData(instrumentCode);

                    initializeCache(cache, statusL1);

                    Console.WriteLine("-- " + FeedOSManaged.API.FOSMarketIDInstrument(instrumentCode) + "/" + FeedOSManaged.API.LocalCodeInstrument(instrumentCode));
                    Console.WriteLine(String.Format("\tBID: {0,7}\t{1}", statusL1.OrderBookBestLimitsExt.BestBid.Price, statusL1.OrderBookBestLimitsExt.BestBid.Size));
                    Console.WriteLine(String.Format("\tASK: {0,7}\t{1}", statusL1.OrderBookBestLimitsExt.BestAsk.Price, statusL1.OrderBookBestLimitsExt.BestAsk.Size));
                    if (null != statusL1.QuotationVariables)
                    {
                        /* use this to print TagNumAndValue based on tag definitions (defined in this API)
                        foreach (TagNumAndValue tagNumAndValue in statusL1.QuotationVariables)
                        {
                            string tagName = FeedOSManaged.API.TagNum2String(tagNumAndValue.Num);
                            if (tagName != null)
                            {
                                if (FeedOSManaged.API.IsDictionaryTag(tagNumAndValue.Num) && (null != tagNumAndValue.Value))
                                {
                                    Console.WriteLine("\t" + tagName + "=" + FeedOSManaged.API.DictionaryEntryToShortName(tagNumAndValue.Num, (uint)tagNumAndValue.Value)
                                                     + " (" + FeedOSManaged.API.DictionaryEntryToLongName(tagNumAndValue.Num, (uint)tagNumAndValue.Value) + ")");
                                }
                                else
                                {
                                    Console.WriteLine("\t" + tagName + "=" + tagNumAndValue.Value);
                                }
                            }
                            else
                            {
                                Console.WriteLine("\t\t" + tagNumAndValue.Num + ":\t\t" + tagNumAndValue.Value);
                            }
                        }
                        */

                        // Caution: this method is not optimal, as we have to marshall back TagNumAndValues back to unmanaged code, to be able to
                        // call the c++ pretty printer method.
                        // use this to print TagNumAndValue based on metadata definitions
                        String prettyPrint = FeedOSManaged.API.PrettyPrintTagNumAndValues(statusL1.QuotationVariables, "\n\t", false);
                        if (null != prettyPrint)
                        {
                            Console.WriteLine("\t" + prettyPrint);
                        }
                    }
                }
            }
        }

        public static void TradeEventExtHandler(uint requestId, uint instrumentCode, ValueType serverUTCDateTime, ValueType marketUTCDateTime, QuotationTradeEventExt quotationTradeEventExt)
        {
            #region Filtering LSE TradeCondition
            /*
            Predicate<TagNumAndValue> FirstTradeTypeIndicator = delegate(TagNumAndValue tagNumAndValue)
            {
                   return FeedOSAPI.Types.TagsContext.TAG_MARKET_LSE_TradeTypeIndicator == tagNumAndValue.Num;
            };
            TagNumAndValue tagNumAndValueTradeTypeIndicator = quotationTradeEventExt.Values.Find(FirstTradeTypeIndicator);
            if (tagNumAndValueTradeTypeIndicator != null && ("3" == (string)tagNumAndValueTradeTypeIndicator.Value || "4" == (string)tagNumAndValueTradeTypeIndicator.Value))
            {
                return;
            }
            */
            #endregion
            Console.WriteLine("EV " + DumpInstrument(instrumentCode) + " " +
                                      DumpTimestamp((DateTime)marketUTCDateTime, false) + " /ServerTime: " +
                                      DumpTimestamp((DateTime)serverUTCDateTime, true));

            // Print whole content mask in human readable format
            Console.Write("Content:");
            QuotationContentMaskHelper.PrintContentMask(quotationTradeEventExt.ContentMask);
            Console.Write("\n");

            InstrumentQuotationData cache = getOrCreateInstrumentQuotationData(instrumentCode);
            cache.update_with_TradeEventExt( (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime, quotationTradeEventExt);

            if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Bid))
            {
                Console.WriteLine(quotationTradeEventExt.BestBid.DumpOneSideLimit("\tBestBid", m_CultureInfo));
            }
            if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Ask))
            {
                Console.WriteLine(quotationTradeEventExt.BestAsk.DumpOneSideLimit("\tBestAsk", m_CultureInfo));
            }
            if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice))
            {
                Console.WriteLine("\tLastPrice=" + quotationTradeEventExt.Price);
            }
            if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastTradeQty))
            {
                Console.WriteLine("\tLastTradeQty=" + quotationTradeEventExt.LastTradeQty);
            }

            // CONTEXT
            #region displaying all context tags'content
            if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Context))
            {
                dumpListOfTagAndValue("CONTEXT:", quotationTradeEventExt.Context);
            }
            #endregion

            // VALUES
            #region displaying all value tags'content
            if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OtherValues) &&
                quotationTradeEventExt.Values != null)
            {
                Console.WriteLine("VALUES:");
                Predicate<TagNumAndValue> FirstTradingStatus = delegate(TagNumAndValue tagNumAndValue)
                {
                    return FeedOSAPI.Types.TagsQuotation.TAG_TradingStatus == tagNumAndValue.Num;
                };
                TagNumAndValue tagNumAndValueTradingStatus = quotationTradeEventExt.Values.Find(FirstTradingStatus);
                if (tagNumAndValueTradingStatus != null)
                {
                    if (tagNumAndValueTradingStatus.Value != null)
                    {
                        Console.WriteLine("\tTradingStatus=" + FeedOSManaged.API.FIXSecurityTradingStatusString((uint)tagNumAndValueTradingStatus.Value));
                    }
                    else
                    {
                        Console.WriteLine("\tTradingStatus=reset");
                    }
                }
                
                List<TagNumAndValue> valuesTags = quotationTradeEventExt.Values;
                foreach (TagNumAndValue tagNumAndVal in valuesTags)
                {
                    // Exclude Trading Status
                    if (FeedOSAPI.Types.TagsQuotation.TAG_TradingStatus != tagNumAndVal.Num && doManageTag(tagNumAndVal.Num))
                    {
                        string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                        // "Known" tag case
                        if (null != tagName)
                        {
                            if (FeedOSManaged.API.IsDictionaryTag(tagNumAndVal.Num) && (null != tagNumAndVal.Value))
                            {
                                Console.WriteLine("\t" + tagName + "=" + FeedOSManaged.API.DictionaryEntryToShortName(tagNumAndVal.Num, (uint)tagNumAndVal.Value)
                                                 + " (" + FeedOSManaged.API.DictionaryEntryToLongName(tagNumAndVal.Num, (uint)tagNumAndVal.Value) + ")");
                            }
                            else if (FeedOSAPI.Types.TagsQuotation.TAG_OrderEntryStatus == tagNumAndVal.Num)
                            {
                                if (tagNumAndVal.Value != null)
                                {
                                    Console.WriteLine("\tOrderEntryStatus=" + OrderEntryStatusHelper.PrintOrderEntryStatus(Convert.ToSByte(tagNumAndVal.Value)));
                                }
                                else
                                {
                                    Console.WriteLine("\tOrderEntryStatus=reset");
                                }
                            }
                            else
                            {
                                dumpTagNameAndValue(tagName, tagNumAndVal.Value);
                            }
                        }
                        // "Unknown" tag case
                        else
                        {
                            Console.WriteLine("\t" + (uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value);
                        }
                    }
                }
            }
            #endregion

            if (s_PrintCache)
            {
                Console.WriteLine("CACHE:");
                dumpCache(cache);
            }
        }

        public static void dumpListOfTagAndValue(string name, List<TagNumAndValue> values)
        {
            if (values != null)
            {
                Console.WriteLine(name);
                foreach (TagNumAndValue tagNumAndVal in values)
                {
                    string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                    // "Known" tag case
                    if (null != tagName)
                    {
                        switch (tagNumAndVal.Num)
                        {
                            case TagsContext.TAG_TradeImpactIndicator:
                            {
                                Console.Write("\t" + tagName + "=");
                                TradeImpactIndicatorHelper.PrintTradeImpactIndicator((uint)tagNumAndVal.Value, true);
                                Console.Write("\n");
                                break;
                            }
                            case TagsQuotation.TAG_OrderEntryStatus:
                            {
                                if (tagNumAndVal.Value != null)
                                {
                                    Console.WriteLine("\tOrderEntryStatus=" + OrderEntryStatusHelper.PrintOrderEntryStatus(Convert.ToSByte(tagNumAndVal.Value)) + "\n");
                                }
                                else
                                {
                                    Console.WriteLine("\tOrderEntryStatus=reset\n");
                                }
                                break;
                            }
                            default:
                            {
                                if (FeedOSManaged.API.IsDictionaryTag(tagNumAndVal.Num) && (null != tagNumAndVal.Value))
                                {
                                    Console.WriteLine("\t" + tagName + "=" + FeedOSManaged.API.DictionaryEntryToShortName(tagNumAndVal.Num, (uint)tagNumAndVal.Value)
                                                     + " (" + FeedOSManaged.API.DictionaryEntryToLongName(tagNumAndVal.Num, (uint)tagNumAndVal.Value) + ")");
                                }
                                else
                                {
                                    dumpTagNameAndValue(tagName, tagNumAndVal.Value);
                                }
                            }
                            break;
                        }
                    }
                    // "Unknown" tag case
                    else
                    {
                        Console.WriteLine("\t" + (uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value);
                    }
                }
            }
        }

        public static void dumpTagNameAndValue(string tagName, object tagValue)
        {
            if (tagValue is DateTime)
            {
                Console.WriteLine("\t" + tagName + "=" + DumpTimestamp((DateTime)tagValue, true));
            }
            else
            {
                Console.WriteLine("\t" + tagName + "=" + tagValue);
            }
        }

        public static void dumpTradeData(TradeData trade)
        {
            Console.WriteLine("\tid=" + trade.TradeId);
            Console.WriteLine("\tprice=" + trade.Price);
            Console.WriteLine("\tquantity=" + trade.Quantity);
            Console.Write("\timpact_indicator=");
            TradeImpactIndicatorHelper.PrintTradeImpactIndicator(trade.TradeImpactIndicator, true);
            Console.Write("\n");
            Console.WriteLine("\tmarket_ts=" + (DateTime)trade.MarketTimestamp);
            dumpListOfTagAndValue("\tcorrected_values=", trade.TradeProperties);
        }

        public static void TradeCancelCorrectionHandler(uint requestId, uint instrumentCode, ValueType serverUTCDateTime, QuotationTradeCancelCorrection quotationTradeCancelCorrection)
        {
            #region dumping event
            if (TradeCancelCorrectionContentMaskHelper.contains(quotationTradeCancelCorrection.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_IsCorrection))
            {
                Console.Write("TradeCorrection");
            }
            else
            {
                Console.Write("TradeCancel");
            }
            Console.Write( " " + DumpInstrument(instrumentCode) + " ServerTime: " + DumpTimestamp((DateTime)serverUTCDateTime, true));
            Console.Write("\n");

            Console.Write("Content:");
            TradeCancelCorrectionContentMaskHelper.PrintContentMask(quotationTradeCancelCorrection.ContentMask, true);
            Console.Write("\n");

            Console.WriteLine("Original Trade: ");
            dumpTradeData(quotationTradeCancelCorrection.OriginalTrade);

            Console.WriteLine("Session: " + quotationTradeCancelCorrection.TradingSessionId);

            if (TradeCancelCorrectionContentMaskHelper.contains(quotationTradeCancelCorrection.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_IsCorrection))
            {
                Console.WriteLine("Corrected Trade: ");
                dumpTradeData(quotationTradeCancelCorrection.CorrectedTrade);
            }

            if (TradeCancelCorrectionContentMaskHelper.contains(quotationTradeCancelCorrection.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_CorrectedValues))
            {
                dumpListOfTagAndValue("Corrected Values:", quotationTradeCancelCorrection.CorrectedValues);
            }
            Console.Write("\n");
            #endregion

            #region updating/displaying cache
            InstrumentQuotationData cache = getOrCreateInstrumentQuotationData(instrumentCode);
            cache.update_with_TradeCancelCorrection((DateTime)serverUTCDateTime, quotationTradeCancelCorrection);

            if (s_PrintCache)
            {
                Console.WriteLine("CACHE:");
                dumpCache(cache);
            }
            #endregion
        }

        public static void dumpCache(InstrumentQuotationData cache)
        {
            Console.Write(cache.BestLimits.BestBid.DumpOneSideLimit("\tBestBid", m_CultureInfo));
            Console.Write(cache.BestLimits.BestAsk.DumpOneSideLimit("\tBestAsk", m_CultureInfo));
            Console.WriteLine("");

            SortedDictionary<ushort, object> quotation_values = cache.QuotationValues;
            foreach (KeyValuePair<ushort, object> value in quotation_values)
            {
                string tagName = FeedOSManaged.API.TagNum2String(value.Key);
                // "Known" tag case
                if (null != tagName)
                {
                    //![snippet_sub_L1_generic_tags]
                    if (FeedOSManaged.API.IsDictionaryTag(value.Key) && (null != value.Value))
                    {
                        Console.WriteLine("\t" + tagName + "=" + FeedOSManaged.API.DictionaryEntryToShortName(value.Key, (uint)value.Value)
                                         + " (" + FeedOSManaged.API.DictionaryEntryToLongName(value.Key, (uint)value.Value) + ")");
                    }
                    else
                    {
                        Console.WriteLine("\t" + tagName + "=" + value.Value);
                    }
                    //![snippet_sub_L1_generic_tags]
                }
                // "Unknown" tag case
                else
                {
                    Console.WriteLine("\t" + (uint)value.Key + "=" + value.Value);
                }
            }
        }

        public static void ValuesUpdateHandler(uint requestId, uint instrumentCode, ValueType marketUTCDateTime, QuotationValuesUpdate quotationValuesUpdate)
        {
            Console.WriteLine("ValuesUpdateHandler: instrumentCode=" + instrumentCode);
            // UPDATE THE DAILY_TOTAL_VOLUME_TRADED
            if ((quotationValuesUpdate.Values != null) && (quotationValuesUpdate.Values.Count > 0))
            {

                Predicate<TagNumAndValue> FirstTradingStatus = delegate(TagNumAndValue tagNumAndValue)
                {
                    return tagNumAndValue.Num == FeedOSAPI.Types.TagsQuotation.TAG_TradingStatus;
                };
                TagNumAndValue tagNumAndValueTradingStatus = quotationValuesUpdate.Values.Find(FirstTradingStatus);
                if (tagNumAndValueTradingStatus != null)
                {
                    Console.WriteLine("New Trading Status for instrument " + FeedOSManaged.API.FOSMarketIDInstrument(instrumentCode) + "/" +
                                       FeedOSManaged.API.LocalCodeInstrument(instrumentCode) + ": " + (uint)tagNumAndValueTradingStatus.Value + "/" +
                                       FeedOSManaged.API.FIXSecurityTradingStatusString((uint)tagNumAndValueTradingStatus.Value));
                }

                Predicate<TagNumAndValue> FirstOrderEntryStatus = delegate(TagNumAndValue tagNumAndValue)
                {
                    return tagNumAndValue.Num == FeedOSAPI.Types.TagsQuotation.TAG_OrderEntryStatus;
                };
                TagNumAndValue tagNumAndValueOrderEntryStatus = quotationValuesUpdate.Values.Find(FirstOrderEntryStatus);
                if (tagNumAndValueOrderEntryStatus != null)
                {
                    Console.Write("New Order Entry Status for instrument " + FeedOSManaged.API.FOSMarketIDInstrument(instrumentCode) + "/" +
                                   FeedOSManaged.API.LocalCodeInstrument(instrumentCode) + ": " + Convert.ToSByte(tagNumAndValueOrderEntryStatus.Value) + "/" +
                                   OrderEntryStatusHelper.PrintOrderEntryStatus(Convert.ToSByte(tagNumAndValueOrderEntryStatus.Value)));
                }
            }
        }
    }
}

