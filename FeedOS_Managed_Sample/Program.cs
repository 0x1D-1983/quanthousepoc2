///-------------------------------
/// FeedOS C# Client API
/// copyright QuantHouse 2012
///-------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// /Main entry point demonstrating FeedOS C# API
    /// </summary>
    class Program
    {
        #region Helper Method
        public static T[] ExtractSubTable<T>(T[] table, int index, int length)
        {
            if ((index >= 0) && (length > 0)) {
                int lengthReal = table.Length - index;
                if (lengthReal > length) {
                    lengthReal = length;
                }
                T[] subtable = new T[lengthReal];
                for (int i = 0; i < lengthReal; ++i) {
                    subtable[i] = table[i + index];
                }
                return subtable;
            }
            return new T[0];
        }
        #endregion

        #region CommandsDefinition
        static private Dictionary<string, IRequest> commandMap;
        static Program()
        {
           commandMap = new Dictionary<string, IRequest>(16);     
           commandMap["subscribefeedstatus"]                        = new RequestSubscribeFeedStatus();
           commandMap["referentialdumpstructure"]                   = new RequestReferentialDumpStructure();
           commandMap["referentialgetinstruments"]                  = new RequestReferentialGetInstruments();
           commandMap["referentiallookup"]                          = new RequestReferentialLookup();
           commandMap["referentialdownload"]                        = new RequestReferentialDownload(false);
           commandMap["referentialdownloadandsubscribe"]            = new RequestReferentialDownload(true);
           commandMap["referentialgettradeconditionsdictionary"]    = new RequestReferentialGetTradeConditionsDictionary(true);
           commandMap["referentialgetvariablepriceincrementtables"] = new RequestReferentialGetVariablePriceIncrementTables();
           commandMap["histodailyext"]                              = new RequestQuotationGetHistoDaily();
           commandMap["histointradayextendednticks"]                = new RequestQuotationGetHisto(RequestGetHistoFlavor.IntradayExtendedNTicks);
           commandMap["downloadandsubscribeintradayextended"]       = new RequestQuotationGetHisto(RequestGetHistoFlavor.DownloadAndSubscribe);
           commandMap["downloadintradaybars"]                       = new RequestQuotationGetHisto(RequestGetHistoFlavor.IntradayBarsExt);
           commandMap["replayl1"]                                   = new RequestReplay(RequestReplayKind.L1);
           commandMap["replayl2"]                                   = new RequestReplay(RequestReplayKind.L2);
           commandMap["replaymbo"]                                  = new RequestReplay(RequestReplayKind.MBO);
           commandMap["replayfeedstatus"]                           = new RequestReplay(RequestReplayKind.FeedStatus);
           commandMap["replayl1l2"]                                 = new RequestReplay(RequestReplayKind.L1L2);
           commandMap["replayibar"]                                 = new RequestReplay(RequestReplayKind.IBar);
           commandMap["replaymuxed"]                                = new RequestReplay(RequestReplayKind.MUXED);
           commandMap["snapshotl1"]                                 = new RequestSnapshot(RequestSnapshotKind.L1);
           commandMap["subscribel1"]                                = new RequestSubscribe(RequestSubscribeKind.L1,false);
           commandMap["subscribel1seq"]                             = new RequestSubscribe(RequestSubscribeKind.L1, true);
           commandMap["subscribembl"]                               = new RequestSubscribe(RequestSubscribeKind.MBL,false);
           commandMap["subscribemblseq"]                            = new RequestSubscribe(RequestSubscribeKind.MBL, true);
           commandMap["subscribembo"]                               = new RequestSubscribe(RequestSubscribeKind.MBO);
           commandMap["subscribemarketstatus"] /* UNAUTHORIZED */   = new RequestSubscribeMarketStatus();
        }
        #endregion

        [STAThread]
        static void Main(string[] args)
        {
            int argc = 0;
            if (args.Length > 1) 
            {
                bool verbose = false;
                // Check the "Verbose" option presence
                while (argc < args.Length) 
                {
                    if ((args[argc] == "-v") ||
                        (args[argc].ToLower() == "--verbose")) {
                        verbose = true;
                    } else {
                        break;
                    }
                    argc++;
                }
                // Check both the FeedOS connection parameters and request name presence
                if (args.Length < (5 + argc)) {
                    Console.Error.WriteLine("Invalid argument number (at least one!)");
                    Usage();
                }
                // Parse both the FeedOS connection parameters and request name
                string feedos_hostname = args[argc++];
                uint feedos_port;
                if (!UInt32.TryParse(args[argc++], out feedos_port)) 
                {
                    Console.Error.WriteLine("Invalid \"port\" argument");
                    Usage();
                }
                string feedos_username = args[argc++];
                string feedos_password = args[argc++];
                string request = args[argc++].ToLower();
                // Initialize the FeedOS API
                API.Init("FeedOSSample(C#)");
                API.InitTraceAllAreas("FeedOSSample_" + DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss") + ".log",
                    true,  // append_mode
                    true,   // flush_every_line
                    true,   // enable_error_level
                    true,   // enable_warning_level
                    true,   // enable_info_level
                    true,   // enable_debug_level 
                    false    // enable_scope_level 
                );
                // Display FeedOS connection parameters and request name
                API.LogInfo("FeedOSSample(C#): common arguments:");
                API.LogInfo("    feedos_hostname=" + feedos_hostname);
                API.LogInfo("    feedos_port=" + feedos_port);
                API.LogInfo("    feedos_username=" + feedos_username);
                API.LogInfo("    feedos_password=" + feedos_password);
                API.LogInfo("    request=" + request);

                // Initialize the FeedOS connection and register the related callback methods
                Connection connection = new Connection();
                connection.Disconnected += PrinterConnection.Disconnected;
                connection.AdminMessage += PrinterConnection.AdminMessage;

                #region Monitoring HeartBeat
                connection.HeartBeat += PrinterConnection.HeartBeat;
                #endregion

                connection.MsgDispatchingHookBegin += PrinterConnection.MsgDispatchingHookBegin;
                connection.MsgDispatchingHookEnd += PrinterConnection.MsgDispatchingHookEnd;

                // Try to connect
                uint returnCode = connection.Connect(feedos_hostname, feedos_port, feedos_username, feedos_password);
                if (returnCode == 0)
                {
                    IRequest requestInterface = null;
                    string[] requestArgs = ExtractSubTable(args, argc, args.Length - argc);

                    if (commandMap.ContainsKey(request))
                    {
                        requestInterface = commandMap[request];
                    }
                    else if ("help" == request)
                    {
                        Usage();
                    }
                    else if (request == "getgenerictagbysourceid")
                    {
                        GetGenericTagBySourceId getGenericTagBySourceId = new GetGenericTagBySourceId();

                        getGenericTagBySourceId.Parse(requestArgs);
                    }
                    else
                    {
                        Console.Error.WriteLine("Invalid request: " + request);
                        Usage();
                    }

                    uint rc = 0;
                    if (null != requestInterface)
                    {
                        requestInterface.Verbose = verbose;
                        bool cmd_line_parsed = false;
                        
                        // Allowing request to throw exception to explain how they can be called
                        try
                        {
                            cmd_line_parsed = requestInterface.Parse(requestArgs);
                        }
                        catch
                        {

                        }

                        if (cmd_line_parsed)
                        {
                            if (requestInterface.Start(connection, Request.NextId))
                            {
                                rc = requestInterface.Wait();
                                requestInterface.Stop();
                            }
                            else
                            {
                                Console.Error.WriteLine("Request failed to start.");
                            }
                        }
                        else
                        {
                            Console.Error.WriteLine("Request failed to parse arguments.");
                            Usage();
                        }
                    }
                    // Finalize the connection
                    connection.Disconnect();
                    connection.Disconnected -= PrinterConnection.Disconnected;
                    connection.AdminMessage -= PrinterConnection.AdminMessage;
                    #region Monitoring HeartBeat
                    connection.HeartBeat -= PrinterConnection.HeartBeat;
                    #endregion
                    connection.MsgDispatchingHookBegin -= PrinterConnection.MsgDispatchingHookBegin;
                    connection.MsgDispatchingHookEnd -= PrinterConnection.MsgDispatchingHookEnd;
                    if (0 != rc) Console.Error.WriteLine("Request failed with error:" + FeedOSManaged.API.ErrorString(rc));
                }
                else
                {
                    Console.Error.WriteLine("Connection failed: " + FeedOSManaged.API.ErrorString(returnCode));
                    Thread.Sleep(2000);
                }
                FeedOSManaged.API.Shutdown();
            }
            else
            {
                Console.Error.WriteLine("Invalid argument number (at least five!)");
                Usage();
            }

            Console.ReadKey();
        }

        #region Private functions
        private static void Usage()
        {
            Console.WriteLine("SYNTAX: [-v] [-s] <hostname> <port> <username> <password> <request>");
            Console.WriteLine("supported requests:");
            Console.WriteLine(" Service REFERENTIAL:");
            Console.WriteLine("    ReferentialDumpStructure");
            Console.WriteLine("        => download the markets and branches");
            Console.WriteLine("    ReferentialGetInstruments <instrument_list>");
            Console.WriteLine("        => get the characteritics");
            Console.WriteLine("    ReferentialLookup <pattern>");
            Console.WriteLine("        => lookup using a pattern");
            Console.WriteLine("    ReferentialDownload <options> <markets <sectypes <cficodes>>>");
            Console.WriteLine("        => download the instrument referential");
            Console.WriteLine("    ReferentialDownloadAndSubscribe <options> <markets <sectypes <cficodes>>>");
            Console.WriteLine("        => download the instrument referential and subscribe to events");
            Console.WriteLine("    ReferentialGetTradeConditionsDictionary");
            Console.WriteLine("        => download the trade conditions dictionary");
            Console.WriteLine("    ReferentialGetVariablePriceIncrementTables");
            Console.WriteLine("        => download the variable price increment tables");
            Console.WriteLine("    GetGenericTagBySourceId <tag_number> <source_id> [<short_name>]");
            Console.WriteLine("        => get generic tag by source id");
            Console.WriteLine(" Service QUOTATION:");
            Console.WriteLine("    ReplayL1 <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay the L1 level");
            Console.WriteLine("    ReplayL2 <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay the L2 level");
            Console.WriteLine("    ReplayMBO <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay the Market Sheet");
            Console.WriteLine("    HistoDailyExt <instrument> [<point_type> <begin_day> <end_day> <adjustment_factors>]");
            Console.WriteLine("        => query extended points of type <point_type(=0:Daily|1:Session|2:DailyAndSession)> between <begin_day> and <end_day> on <instrument>");
            Console.WriteLine("    HistoIntradayExtendedNTicks <instrument> <nb_points> [<begin_date>] ");
            Console.WriteLine("        => query <nb_points> trades after or before <begin_date> according to nb_points (positive or negative) ");
            Console.WriteLine("    DownloadAndSubscribeIntradayExtended <instrument> [<nb_points> <with_trade_cancel_correction> <begin_date> <end_date>] ");
            Console.WriteLine("        => query all trades between <begin_date> and <end_date> on <instrument> with or without cancels/corrections then subscribe to events");
            Console.WriteLine("    DownloadIntradayBars <instrument> [<duration> <begin_date> <end_date> <nb_bars_before_begin>] ");
            Console.WriteLine("        => download intraday bars of <instrument> having the specified <duration> between <begin_date> and <end_date>");
            Console.WriteLine("    ReplayFeedStatus <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay the FeedStatus");
            Console.WriteLine("    ReplayL1L2 <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay both L1 and L2 levels");
            Console.WriteLine("    ReplayMUXED <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay all MUXED levels");
            Console.WriteLine("    ReplayIBar <source_id> <begin_date> <end_date>");
            Console.WriteLine("        => replay intraday bars");
            Console.WriteLine("    SnapshotL1 <instrument_list>");
            Console.WriteLine("        => display the \"level 1\" snapshot");
            Console.WriteLine("    SubscribeL1 <instrument_list>");
            Console.WriteLine("        => subscribe to the level 1");
            Console.WriteLine("    SubscribeL1Seq <instrument_list>");
            Console.WriteLine("        => subscribe sequentially to the level 1 of the list of instrument");
            Console.WriteLine("    SubscribeMBL [-layers <layer_ids>] [-merge_layers_to <layer_id>] [-cache | -events_and_cache] <instrument_list_file>");
            Console.WriteLine("    SubscribeMBL [-layers <layer_ids>] [-merge_layers_to <layer_id>] [-cache | -events_and_cache] <instrument_1,...,instrument_N>");
            Console.WriteLine("        => subscribe to MBL (order book aggregated by limit/price) and optionnaly merge MBL layers to target");
            Console.WriteLine("    SubscribeMBLSeq [-layers <layer_ids>] [-merge_layers_to <layer_id>] [-cache | -events_and_cache] <instrument_list_file>");
            Console.WriteLine("    SubscribeMBLSeq [-layers <layer_ids>] [-merge_layers_to <layer_id>] [-cache | -events_and_cache] <instrument_1,...,instrument_N>");
            Console.WriteLine("        => subscribe to MBL sequentially ");
            Console.WriteLine("    SubscribeMBO <instrument>");
            Console.WriteLine("        => subscribe to Market Sheet (order book without price aggregation)");
            Console.WriteLine("    SubscribeMarketStatus");
            Console.WriteLine("        => subscribe to the market status data");
            Console.WriteLine(" Service CONNECTION:");
            Console.WriteLine("    SubscribeFeedStatus");
            Console.WriteLine("        => subscribe to the FeedStatus");
        }
        #endregion
    }
}
