///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterVariablePriceIncrementTables
    {
        public static void DumpVariablePriceIncrementTable(VariablePriceIncrementTable variablePriceIncrementTable)
        {
            Console.WriteLine("TABLE # " + variablePriceIncrementTable.Id + " / " + variablePriceIncrementTable.Description);

            if (variablePriceIncrementTable.PriceBands != null)
            {
                foreach (VariableIncrementPriceBand priceBand in variablePriceIncrementTable.PriceBands)
                {
                    if (priceBand.Inclusive)
                    {
                        Console.Write(">= ");
                    }
                    else
                    {
                        Console.Write("> ");
                    }
                    Console.WriteLine(priceBand.LowerBoundary + "\t: " + priceBand.PriceIncrement);
                }
            }
            else
            {
                Console.WriteLine("<No price band>");
            }
        }

        public static void DumpVariablePriceIncrementTables(uint requestId, List<VariablePriceIncrementTable> variablePriceIncrementTables)
        {
            foreach( VariablePriceIncrementTable table in variablePriceIncrementTables )
		    {
                DumpVariablePriceIncrementTable(table);
		    }
        }
    }
}
