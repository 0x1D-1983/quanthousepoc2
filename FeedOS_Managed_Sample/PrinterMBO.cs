///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2012 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterMBO : PrinterBase
    {
        private static int s_HalfBookCharSize = 39;
        private static char[] s_EmptyLine;
        private static String s_EmptyString;

        static PrinterMBO()
        {
            SetCultureInfo();
            s_EmptyLine = new char[s_HalfBookCharSize];
            for (int i = 0; i < s_HalfBookCharSize; i++)
            {
                s_EmptyLine[i] = ' ';
            }
            s_EmptyString = new String(s_EmptyLine);
        }
        private static CultureInfo m_CultureInfo = null;

        private static void SetCultureInfo()
        {
            if (m_CultureInfo == null) m_CultureInfo = (CultureInfo)CultureInfo.InvariantCulture.Clone();
        }

        protected string DumpInstrumentAndTimestamps(uint instrumentCode, System.DateTime serverTimestamp, System.DateTime marketTimestamp)
        {
            return DumpInstrument(instrumentCode) + "\t" + DumpTimestamp(marketTimestamp, false) + " /ServerTime: " +
                                DumpTimestamp(serverTimestamp, true);
        }

        public static string DumpListTagNumAndValue(string prefix, List<TagNumAndValue> tagNValues)
        {
            StringBuilder builder = new StringBuilder();
            if (null != tagNValues && (0 != tagNValues.Count))
            {
                builder.Append(prefix);
                foreach (TagNumAndValue tagNumAndVal in tagNValues)
                {
                    string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                    // "Known" tag case
                    if (null != tagName)
                    {
                        if (null != tagNumAndVal.Value)
                        {
                            builder.Append("\t" + tagName + " : " + tagNumAndVal.Value);
                        }
                        else
                        {
                            // Means the tag value will be erased after update on MBLLayer 
                            builder.Append("\t*" + tagName);
                        }
                    }
                    // "Unknown" tag case
                    else
                    {
                        if (null != tagNumAndVal.Value)
                        {
                            builder.Append("\t" + (uint)tagNumAndVal.Num + " : " + tagNumAndVal.Value);
                        }
                        else
                        {
                            // Means the tag value will be erased after update on MBLLayer 
                            builder.Append("\t*" + (uint)tagNumAndVal.Num);
                        }
                    }
                }
            }
            return builder.ToString();
        }

        protected static string DumpMarketSheetEntry(string sideStr, int depth, MarketSheetEntry entry, bool compact)
        {
            if (null != entry)
            {

                string price_str = null;
                if (!OrderBookEntryExt.isASpecialValuePrice(entry.Price, ref price_str))
                {
                    price_str = entry.Price.ToString("F4", m_CultureInfo);
                }
                if (!compact)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendFormat("{0} {1, 3} {2,9} x {3,9} ({4})", depth < 0 ? "" : depth.ToString(), sideStr, price_str, entry.Qty.ToString("F4", m_CultureInfo), entry.OrderID);
                    return builder.ToString();
                }
                else
                {
                    return (depth < 0 ? "" : depth.ToString()) + " " + sideStr + " " + price_str + " x " + entry.Qty.ToString("F4", m_CultureInfo) + " (" + entry.OrderID + ")";
                }
            }
            else
            {
                return (depth < 0 ? "" : depth.ToString()) + " " + sideStr + " " + s_EmptyString;
            }
        }

        protected static string DumpSide(byte side)
        {
            // '1' for Buy, '2' for Sell
            return ('1' == side) ? "BID" : "ASK";
        }

        protected static string DumpMarketSheet(MarketSheet marketSheet)
        {
            StringBuilder builder = new StringBuilder();
            if (null != marketSheet)
            {
                List<MarketSheetEntryAndContext> bidSide = marketSheet.BidSide;
                List<MarketSheetEntryAndContext> askSide = marketSheet.AskSide;
                int bidSize = 0;
                if (null != bidSide)
                {
                    bidSize = bidSide.Count;
                }
                int askSize = 0;
                if (null != askSide)
                {
                    askSize = askSide.Count;
                }
                int maxSize = System.Math.Max(bidSize,askSize);
                MarketSheetEntry curBid = null;
                List<TagNumAndValue> contextBid = null;
                MarketSheetEntry curAsk = null;
                List<TagNumAndValue> contextAsk = null;
                for (int depth = 0; depth < maxSize - 1; depth++)
                {
                    if (depth > bidSize-1)
                    {
                        curBid = null;
                        contextBid = null;
                    }
                    else
                    {
                        curBid = bidSide[depth].Order;
                        contextBid = bidSide[depth].Context;
                    }
                    string bidEntryStr = DumpMarketSheetEntry("BID", depth, curBid, false);
                    string bidContextStr = null;
                    /* Uncomment to get context dumped as well, but with hugly output. 
                    if (null != contextBid && contextBid.Count > 0)
                    {
                        bidContextStr = DumpListTagNumAndValue(" ", contextBid);
                    }
                    */
                    if (depth > askSize-1)
                    {
                        curAsk = null;
                        contextAsk = null;
                    }
                    else
                    {
                        curAsk = askSide[depth].Order;
                        contextAsk = askSide[depth].Context;
                    }
                    // -1 to not print the depth for the ASK side
                    string askEntryStr = DumpMarketSheetEntry("ASK", -1, curAsk, false);
                    string askContextStr = null;
                    /*
                    if (null != contextAsk && contextAsk.Count > 0)
                    {
                        askContextStr = DumpListTagNumAndValue(" ", contextAsk);
                    }
                    */
                    builder.Append("\n\t" + bidEntryStr + ((null != bidContextStr) ?  bidContextStr : "" ) + "\t\t" + askEntryStr + ((null != askContextStr) ? askContextStr : ""));
                }
            }
            return builder.ToString();
        }

	    public void MarketSheetSnapshotHandler( 
            uint requestId, 
            uint instrumentCode, 
            ValueType lastServerUTCDateTime,
            ValueType lastMarketUTCDateTime, 
            MarketSheet marketSheet)
        {
            Console.WriteLine("notif_MarketSheetSnapshot: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)lastServerUTCDateTime, (DateTime)lastMarketUTCDateTime)
                + DumpMarketSheet(marketSheet));
        }
	    
        public void NewOrderHandler(
            uint requestId, 
            uint instrumentCode, 
            ValueType serverUTCDateTime, 
            ValueType marketUTCDateTime, 
            List<TagNumAndValue> context, 
            byte inSide, 
            MarketSheetEntry marketSheetEntry, 
            ushort	inLevel )
        {
            Console.WriteLine("notif_NewOrder: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                  + DumpListTagNumAndValue(" Context: ", context)
                  + " " + DumpMarketSheetEntry(DumpSide(inSide), inLevel, marketSheetEntry,true));
        }   

	    public void ModifyOrderHandler( 
            uint requestId, 
            uint instrumentCode, 
            ValueType serverUTCDateTime, 
            ValueType marketUTCDateTime, 
            List<TagNumAndValue> context,
            byte inSide, 
            MarketSheetEntry marketSheetEntry, 
            ushort inOldLevel, 
            ushort inNewLevel )
        {
            Console.WriteLine("notif_ModifyOrder: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                  + DumpListTagNumAndValue("Context", context)
                  + " " + DumpMarketSheetEntry(DumpSide(inSide), inOldLevel, marketSheetEntry, true) + " moved to level: " + inNewLevel);
        }

	    public void RemoveOneOrderHandler( 
            uint requestID, 
            uint instrumentCode, 
            ValueType serverUTCDateTime, 
            ValueType marketUTCDateTime, 
            List<TagNumAndValue> context,
            byte inSide,
            string orderID,
            ushort inLevel )
        {
            Console.WriteLine("notif_RemoveOneOrder: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                    + DumpListTagNumAndValue(" Context: ", context)
                    + " " + DumpSide(inSide) + " OrderID: " + orderID + " at level: " + inLevel); 
        }

	    public void RemoveAllPreviousOrdersHandler(
            uint requestID,
            uint instrumentCode, 
            ValueType serverUTCDateTime, 
            ValueType marketUTCDateTime, 
            List<TagNumAndValue> context, 
            byte inSide, 
            string orderID, 
            ushort inLevel	)
        {
            Console.WriteLine("notif_RemoveAllPreviousOrders: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                    + DumpListTagNumAndValue("Context", context)
                    + " " + DumpSide(inSide) + " orderID: " + orderID + " at: " + inLevel); 
        }

	    public void RemoveAllOrdersHandler(
            uint requestID,
            uint instrumentCode, 
            ValueType serverUTCDateTime, 
            ValueType marketUTCDateTime, 
            List<TagNumAndValue> context,
            byte inSide )
        {
            Console.WriteLine("notif_RemoveAllOrders: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                        + DumpListTagNumAndValue(" Context: ", context) + " " + DumpSide(inSide));
        }

	    public void RetransmissionHandler(
            uint requestID,
            uint instrumentCode, 
            ValueType serverUTCDateTime, 
            ValueType marketUTCDateTime, 
            MarketSheet marketSheet )
        {
            Console.WriteLine("notif_Retransmission: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                + DumpMarketSheet(marketSheet));
        }

        public void ValuesUpdateOneInstrumentHandler(
            uint requestID,
            uint instrumentCode, 
            ValueType serverUTCDateTime,
            ValueType marketUTCDateTime,
            List<TagNumAndValue> values)
        {
            Console.WriteLine("notif_ValuesUpdateOneInstrument: " + DumpInstrumentAndTimestamps(instrumentCode, (DateTime)serverUTCDateTime, (DateTime)marketUTCDateTime)
                + DumpListTagNumAndValue(" Values: ",values));
        }
    }
}
