///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2015
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
// FeedOS
using FeedOSManaged;
using System.Threading;
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    class RequestQuotationGetHistoDaily : Request
    {
        #region Members
        private string m_Instrument;
        private DateTime m_DateTimeBegin = new DateTime();
        private DateTime m_DateTimeEnd = new DateTime();
        private DailyExtPointType m_PointType = DailyExtPointType.DailyExtPointType_DailyAndSession;
        private UInt32 m_AdjustmentFactors = AdjustmentFactor.None;
        private PrinterHistoricalData m_Printer = new PrinterHistoricalData();
        private bool m_DumpTradeConditons = false;
        #endregion

        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args.Length == 0)
            {
                return false;
            }

            m_Instrument = args[0];

            if (args.Length > 1)
            {
                m_PointType = (DailyExtPointType)Convert.ToUInt32(args[1]);
            }
            if (args.Length > 2)
            {
                if (!DateTime.TryParseExact(args[2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeBegin))
                {
                    Console.WriteLine("Invalid begin date: " + args[2] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                    return false;
                }
            }
            if (args.Length > 3)
            {
                if (!DateTime.TryParseExact(args[3], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeEnd))
                {
                    Console.WriteLine("Invalid end date: " + args[3] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                    return false;
                }
            }
            if (args.Length > 4)
            {
                m_AdjustmentFactors = Convert.ToUInt32(args[2]);
            }

            return true;
        }
        /** \cond refguide
         * This sample demonstrates how to use the \c GetHistoryDailyExt request to
         * download daily and session historical data. This request also lets users
         * choose to apply corporate action adjustment factors if necessary.
         *
         * Implement and register handlers for common events (\c Started,
         * \c Failed, \c Succeeded).
         * 
         * Implement the event handler specific to this request:
         * \snippet PrinterHistoricalData.cs snippet_dext_event_handler
         * 
         * And register it:
         * \snippet RequestQuotationGetHistoDaily.cs snippet_dext_event_handler_reg
         * 
         * Set input parameters as needed: list of instrument codes,
         * begin and end date, whether they want daily, session or both points, and
         * which adjustment factor to apply. Refer to the request documentation for a
         * comprehensive explanation of these parameters.
         * 
         * Send the request:
         * \snippet RequestQuotationGetHistoDaily.cs snippet_dext_request
         * 
         * Once you have received the requested data, don't forget to unregister
         * your handlers. 
         * \endcond
         */
        public override bool Start(Connection connection, uint requestID)
        {
            GetTradeConditionsDictionnary(connection);

            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);

            //![snippet_dext_event_handler_reg]
            m_Connection.HistoDailyExtendedHandler +=
            		new FeedOSManaged.HistoDailyExtendedEventHandler(
            				m_Printer.HistoDailyExtended);
            //![snippet_dext_event_handler_reg]

            Console.WriteLine("Getting quotation data...");
            //![snippet_dext_request]
            m_Connection.GetHistoDailyExtended(m_Instrument, m_DateTimeBegin,
            		m_DateTimeEnd, m_PointType, m_AdjustmentFactors,
					m_RequestID);
            //![snippet_dext_request]

            return true;
        }
        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.HistoDailyExtendedHandler -= new FeedOSManaged.HistoDailyExtendedEventHandler(m_Printer.HistoDailyExtended);
        }
        #endregion

        #region Helpers
        private void GetTradeConditionsDictionnary(Connection connection)
        {
            Console.WriteLine("Getting referential data...");
            RequestReferentialGetTradeConditionsDictionary downloadDictionaryRequest = new RequestReferentialGetTradeConditionsDictionary(m_DumpTradeConditons);
            if (downloadDictionaryRequest.Start(connection, Request.NextId))
            {
                downloadDictionaryRequest.Wait();
                downloadDictionaryRequest.Stop();
            }
            else
            {
                API.LogCritical("RequestQuotationGetHistoDaily GetTradeConditionsDictionary failed to start.");
                throw new Exception("RequestQuotationGetHistoDaily GetTradeConditionsDictionary request cannot start");
            }
        }
        #endregion
    }
}
