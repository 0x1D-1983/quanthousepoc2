///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterHistoricalData
    {
        static Dictionary<uint, TradeConditionsDictionaryEntry> m_Dictionary;

        private void PrintPrice(double price, char suffix)
        {
            Console.Write("{0,9}{1}", price.ToString("F3", CultureInfo.InvariantCulture),suffix);
        }

        private void PrintTradeConditionsValue(uint index, bool humanReadable)
        {
		    if (0 != index)
		    {
			    if (humanReadable && null != m_Dictionary)
			    {
                    TradeConditionsDictionaryEntry entry;
                    if (m_Dictionary.TryGetValue(index, out entry))
                    {
                        List<TagNumAndValue> contextTags = entry.Value;
                        foreach (TagNumAndValue tagNumAndVal in contextTags)
                        {
                            string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                            // "Known" tag case
                            if (null != tagName)
                            {
                                Console.Write(tagName + "=" + tagNumAndVal.Value + " ");
                            }
                            // "Unknown" tag case
                            else
                            {
                                Console.Write((uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value+ " ");
                            }
                        }
				    }
				    else
				    {
					    Console.Write("UNKNOWN_INDEX");
				    }
			    }
			    else
			    {
				    Console.Write(index);
			    }
		    }
        }

        public static Dictionary<uint, TradeConditionsDictionaryEntry> Dictionary
        {
            get { if (null == m_Dictionary) m_Dictionary = new  Dictionary<uint, TradeConditionsDictionaryEntry>(); return m_Dictionary; }
        }

        public static void PrintTradeConditionsDictionary(uint requestId, List<TradeConditionsDictionaryEntry> downloadedTradeConditions)
        {
             Console.WriteLine("KEY\tTRADE_CONDITIONS_VALUE");
            if (null == downloadedTradeConditions) return;
            foreach (TradeConditionsDictionaryEntry entry in downloadedTradeConditions)
            {
                Console.Write(entry.Index + "\t");
                List<TagNumAndValue> contextTags = entry.Value;
                foreach (TagNumAndValue tagNumAndVal in contextTags)
                {
                    string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                    // "Known" tag case
                    if (null != tagName)
                    {
                        Console.Write(tagName + "=" + tagNumAndVal.Value + " ");
                    }
                    // "Unknown" tag case
                    else
                    {
                        Console.Write((uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value + " ");
                    }
                }
                Console.WriteLine("");
            }
        }

        public static void GetTradeConditionsDictionary(uint requestId, List<TradeConditionsDictionaryEntry> downloadedTradeConditions)
        {
            if (null == downloadedTradeConditions) return;

            Dictionary<uint, TradeConditionsDictionaryEntry> dictionary = Dictionary;
            foreach (TradeConditionsDictionaryEntry entry in downloadedTradeConditions)
            {
                dictionary.Add(entry.Index, entry);
            }
        }

        //![snippet_dext_event_handler]
        public void HistoDailyExtended(
        		uint requestId,
				uint instrumentCode,
				List<HistoDailyPointExtended> histoDailyPointsExtended)
        //![snippet_dext_event_handler]
        {
            Console.WriteLine("DATE\tSESSION\tOPEN\tHIGH\tLOW\tCLOSE\tVOLUME\tASSET\tOTHER_VALUES");
            if (null == histoDailyPointsExtended) return;
            foreach (HistoDailyPointExtended point in histoDailyPointsExtended)
            {
                Console.Write(point.Date.ToString("yyyy-MM-dd"));
                Console.Write('\t');
                if (SessionId.CoreSessionId == point.TradingSessionId)
                {
                    Console.Write("CORE" + "\t");
                }
                else
                {
                    Console.Write(point.TradingSessionId + "\t");
                }
                string specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.OpenPrice, ref specialPrice))
                {
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.OpenPrice, '\t');
                }
                specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.HighPrice, ref specialPrice))
                {
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.HighPrice, '\t');
                }
                specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.LowPrice, ref specialPrice))
                {
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.LowPrice, '\t');
                }
                specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.ClosePrice, ref specialPrice))
                {
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.ClosePrice, '\t');
                }
                specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.TotalVolumeTraded, ref specialPrice))
                {
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.TotalVolumeTraded, '\t');
                }
                specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.TotalAssetTraded, ref specialPrice))
                {
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.TotalAssetTraded, '\t');
                }
                if (point.OtherValues != null)
                {
                    bool print_comma = false;
                    foreach (TagNumAndValue tagNumAndVal in point.OtherValues)
                    {
                        if (print_comma)
                        {
                            Console.Write(", ");
                        }
                        print_comma = true;

                        string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                        // "Known" tag case
                        if (null != tagName)
                        {
                            Console.Write(tagName + "=" + tagNumAndVal.Value);
                        }
                        // "Unknown" tag case
                        else
                        {
                            Console.Write((uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value);
                        }
                    }
                }
                Console.Write('\n');
            }
        }

        public void HistoIntradayExtended(uint requestId, uint instrumentCode, List<HistoIntradayPointExtended> histoIntradayPointExtended)
        {
            Console.WriteLine("SERVER_UTC_TIME\tMARKET_OFFICIAL_TIME\tPRICE\tQTY\tCONTENT_MASK\tTRADE_CONDITIONS_KEY\tEXTENDED_VALUES");
            if (null == histoIntradayPointExtended) return;

            foreach (HistoIntradayPointExtended point in histoIntradayPointExtended)
            {
                Console.Write(((DateTime)point.ServerUTCTime).ToString("yyyy-MM-dd HH:mm:ss:ffffff") + '\t');
                Console.Write(((DateTime)point.MarketUTCTime).ToString("yyyy-MM-dd HH:mm:ss:ffffff") + '\t');
                string specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.Price, ref specialPrice))
                {
                    Console.Write(specialPrice);
                }
                else
                {
                    PrintPrice(point.Price, '\t');
                }

                string specialQty = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.LastTradeQty, ref specialQty))
                {
                    Console.Write(specialQty);
                }
                else
                {
                    PrintPrice(point.LastTradeQty, '\t');
                }

                QuotationContentMaskHelper.PrintContentMask(point.ContentMask, true);
                Console.Write('\t');
                PrintTradeConditionsValue(point.TradeConditionIndex, (null != m_Dictionary));
                PrintIntradayExtendedValues(point.Properties);
            }
        }

        public void PrintIntradayExtendedValues(List<TagNumAndValue> properties)
        {
            if (null == properties) return;

            foreach (TagNumAndValue tagNumAndVal in properties)
            {
                string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                // "Known" tag case
                if (null != tagName)
                {
                    switch (tagNumAndVal.Num)
                    {
                        case TagsContext.TAG_TradeImpactIndicator:
                            {
                                Console.Write("\t" + tagName + "=");
                                TradeImpactIndicatorHelper.PrintTradeImpactIndicator((uint)tagNumAndVal.Value, true);
                                break;
                            }

                        default:
                            Console.Write("\t" + tagName + "=" + tagNumAndVal.Value);
                            break;
                    }
                }
                // "Unknown" tag case
                else
                {
                    Console.Write("\t" + (uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value);
                }
            }
            Console.WriteLine("");
        }

        //![snippet_iext_data_handler]
        public void HistoIntradayDataExtended(
        		uint requestId,
				List<HistoIntradayDataExtended> histoIntradayDataExtended)
        {
            Console.WriteLine("INSTR_CODE\tTRADE_ID\tSERVER_UTC_TIME\t" +
            		"MARKET_OFFICIAL_TIME\tPRICE\tQTY\tCONTENT_MASK\t" +
            		"TRADE_CONDITIONS_KEY\tEXTENDED_VALUES");
            if (null == histoIntradayDataExtended) return;

            foreach (HistoIntradayDataExtended point in histoIntradayDataExtended)
            {
            	//![snippet_iext_data_handler]
                Console.Write(point.InstrCode + "\t");
                Console.Write(point.TradeId + "\t");
                Console.Write(((DateTime)point.Point.ServerUTCTime).ToString("yyyy-MM-dd HH:mm:ss:ffffff") + '\t');
                Console.Write(((DateTime)point.Point.MarketUTCTime).ToString("yyyy-MM-dd HH:mm:ss:ffffff") + '\t');
                string specialPrice = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.Point.Price, ref specialPrice))
                {
                    Console.Write(specialPrice);
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.Point.Price, '\t');
                }

                string specialQty = null;
                if (OrderBookEntryExt.isASpecialValuePrice(point.Point.LastTradeQty, ref specialQty))
                {
                    Console.Write(specialQty);
                    Console.Write('\t');
                }
                else
                {
                    PrintPrice(point.Point.LastTradeQty, '\t');
                }

                QuotationContentMaskHelper.PrintContentMask(point.Point.ContentMask, true);
                Console.Write('\t');
                PrintTradeConditionsValue(point.Point.TradeConditionIndex, (null != m_Dictionary));
                Console.Write('\t');
                PrintIntradayExtendedValues(point.Point.Properties);
            }
        }

        //![snippet_iext_cancel_handler]
        public void HistoIntradayCancel(
        		uint requestId, HistoIntradayCancel histoIntradayCancel)
        {
            Console.WriteLine("INSTR_CODE\tTRADE_ID\tORIGINAL_MARKET_TIMESTAMP\t"
            		+ "OFF_BOOK\tFROM_VENUE");

            Console.Write(histoIntradayCancel.InstrCode + "\t");
            Console.Write(histoIntradayCancel.TradeId + "\t");
            //![snippet_iext_cancel_handler]
            Console.Write(((DateTime)histoIntradayCancel.OriginalMarketTimestamp).ToString("yyyy-MM-dd HH:mm:ss:ffffff") + '\t');
            Console.Write(histoIntradayCancel.OffBook.ToString() + "\t");
            Console.Write(histoIntradayCancel.FromVenue.ToString());
            Console.Write('\n');
        }

        //![snippet_iext_correction_handler]
        public void HistoIntradayCorrection(
        		uint requestId, HistoIntradayCorrection histoIntradayCorrection)
        {
            Console.WriteLine("INSTR_CODE\tTRADE_ID\tORIGINAL_MARKET_TIMESTAMP\t"
            		+ "OFF_BOOK\tFROM_VENUE\tTRADING_SESSION_ID\tNEW_TRADE_ID\t"
					+ "NEW_PRICE\tNEW_QUANTITY\tNEW_IMPACT_INDICATOR\t"
					+ "NEW_TRADE_TIMESTAMP\tNEW_PROPERTIES");

            Console.Write(histoIntradayCorrection.InstrCode + "\t");
            Console.Write(histoIntradayCorrection.TradeId + "\t");
            //![snippet_iext_correction_handler]
            Console.Write(((DateTime)histoIntradayCorrection.OriginalMarketTimestamp).ToString("yyyy-MM-dd HH:mm:ss:ffffff") + '\t');
            Console.Write(histoIntradayCorrection.OffBook.ToString() + "\t");
            Console.Write(histoIntradayCorrection.FromVenue.ToString() + "\t");

            Console.Write(histoIntradayCorrection.TradingSessionId.ToString() + "\t");

            Console.Write(histoIntradayCorrection.Trade.TradeId + "\t");
            Console.Write(histoIntradayCorrection.Trade.Price + "\t");
            Console.Write(histoIntradayCorrection.Trade.Quantity + "\t");
            TradeImpactIndicatorHelper.PrintTradeImpactIndicator(histoIntradayCorrection.Trade.TradeImpactIndicator, true);
            Console.Write('\t');
            Console.Write(((DateTime)histoIntradayCorrection.Trade.MarketTimestamp).ToString("yyyy-MM-dd HH:mm:ss:ffffff"));
            if (histoIntradayCorrection.Trade.TradeProperties != null)
            {
                foreach (TagNumAndValue tagNumAndVal in histoIntradayCorrection.Trade.TradeProperties)
                {
                    string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                    // "Known" tag case
                    if (null != tagName)
                    {
                        switch (tagNumAndVal.Num)
                        {
                            case TagsContext.TAG_TradeImpactIndicator:
                                {
                                    Console.Write("\t" + tagName + "=");
                                    TradeImpactIndicatorHelper.PrintTradeImpactIndicator((uint)tagNumAndVal.Value, true);
                                    break;
                                }

                            default:
                                Console.Write("\t" + tagName + "=" + tagNumAndVal.Value);
                                break;
                        }
                    }
                    // "Unknown" tag case
                    else
                    {
                        Console.Write("\t" + (uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value);
                    }
                }
            }
            Console.Write('\n');
        }
    }
}
