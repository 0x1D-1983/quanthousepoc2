///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterInstrument : PrinterBase
    {
        public static void Print(InstrumentCharacteristics instrument)
        {
            Console.WriteLine("instr # " + DumpInstrument(instrument.InstrumentCode) + " = " + instrument.InstrumentCode);
            if (instrument.PriceCurrency != null)
                Console.WriteLine("    PriceCurrency               string{" + instrument.PriceCurrency + "}");
            if (instrument.Symbol != null)
                Console.WriteLine("    Symbol                      string{" + instrument.Symbol + "}");
            if (instrument.Description != null)
                Console.WriteLine("    Description                 string{" + instrument.Description + "}");
            if (instrument.SecurityType != null)
                Console.WriteLine("    SecurityType                string{" + instrument.SecurityType + "}");
            if (instrument.FOSMarketId != 0)
                Console.WriteLine("    FOSMarketId                 " + FeedOSManaged.API.FOSMarketIDString((ushort)instrument.FOSMarketId));
            if (instrument.ContractMultiplier != null)
                Console.WriteLine("    ContractMultiplier          float64{" + (double)instrument.ContractMultiplier + "}");
            if (instrument.CFICode != null)
                Console.WriteLine("    CFICode                     string{" + instrument.CFICode + "}");
            if (instrument.SecuritySubType != null)
                Console.WriteLine("    SecuritySubType             string{" + instrument.SecuritySubType + "}");
            if (instrument.InternalCreationDate != null)
                Console.WriteLine("    InternalCreationDate        TimeStamp{" + DumpTimestamp((DateTime)instrument.InternalCreationDate, true) + "}");
            if (instrument.InternalModificationDate != null)
                Console.WriteLine("    InternalModificationDate    TimeStamp{" + DumpTimestamp((DateTime)instrument.InternalModificationDate,true) + "}");
            if (instrument.InternalSourceId != null)
                Console.WriteLine("    InternalSourceId            uint16{" + (ushort)instrument.InternalSourceId + "}");
            if (instrument.LocalCodeStr != null)
                Console.WriteLine("    LocalCodeStr                string{" + instrument.LocalCodeStr + "}");
            if (instrument.ISIN != null)
                Console.WriteLine("    ISIN                        string{" + instrument.ISIN + "}");
            if (instrument.PriceIncrement_static != null)
                Console.WriteLine("    PriceIncrement_static       float64{" + (double)instrument.PriceIncrement_static + "}");
            if (instrument.MaturityYear != null)
                Console.WriteLine("    MaturityYear                uint16{" + instrument.MaturityYear + "}");
            if (instrument.MaturityMonth != null)
                Console.WriteLine("    MaturityMonth               uint8{" + instrument.MaturityMonth + "}");
            if (instrument.MaturityDay != null)
                Console.WriteLine("    MaturityDay                 uint8{" + instrument.MaturityDay + "}");
        }

        public static void DisplayInstrumentWithFewTags(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        {
            if (instrumentCharacteristicsList != null)
            {
                foreach (InstrumentCharacteristics instrumentCharacteristics in instrumentCharacteristicsList)
                {
                    Print(instrumentCharacteristics);
                }
            }
            else
            {
                Console.WriteLine("No instrument found ");
            }
        }

        public static void DisplayInstrumentWithAllTags(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        {
            if (instrumentCharacteristicsList != null)
            {
                foreach (InstrumentCharacteristics instrumentCharacteristics in instrumentCharacteristicsList)
                {
                    Console.WriteLine("instr # " + DumpInstrument(instrumentCharacteristics.InstrumentCode) + " = " + instrumentCharacteristics.InstrumentCode);
                    // Caution: this method is not optimal, as we have to marshall back TagNumAndValues back to unmanaged code, to be able to 
                    // call the c++ pretty print
                    // use this to print TagNumAndValue based on metadata definitions
                    String prettyPrint = FeedOSManaged.API.PrettyPrintTagNumAndValues(instrumentCharacteristics.ReferentialAttributes,"\n\t",false);
                    if (null != prettyPrint)
                    {
                    	Console.WriteLine('\t' + prettyPrint);	
                    }
                }
            }
            else
            {
                Console.WriteLine("No instrument found ");
            }
        }
    }
}
