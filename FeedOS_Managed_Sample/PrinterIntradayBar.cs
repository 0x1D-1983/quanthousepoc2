///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterIntradayBar : PrinterBase
    {
        public static void Dump(String evt, QuotationIntradayBar bar, bool dumpHeader)
        {
            if (dumpHeader)
            {
                Console.WriteLine(evt + "\tINSTR\tMARKET_TIMESTAMP\tSERVER_TIMESTAMP\tDURATION\tNB_POINTS\tOPEN\tHIGH\tLOW\tCLOSE\tVOLUME\tVALUES");
            }
            Console.Write(evt + "\t" + bar.InstrumentCode + "\t");
            Console.Write(((DateTime)bar.MarketUTCDateTimeBegin).ToString("yyyy-MM-dd HH:mm:ss:fff") + "\t");
            Console.Write(((DateTime)bar.ServerUTCDateTimeBegin).ToString("yyyy-MM-dd HH:mm:ss:fff") + "\t");
            Console.Write(bar.ServerDurationSeconds + "\t");
            Console.Write(bar.NbPoints + "\t");
            Console.Write(bar.Open + "\t");
            Console.Write(bar.High + "\t");
            Console.Write(bar.Low + "\t");
            Console.Write(bar.Close + "\t");
            Console.Write(bar.VolumeTraded);
            if (bar.OtherValues != null)
            {
                foreach (TagNumAndValue tagNumAndVal in bar.OtherValues)
                {
                    string tagName = FeedOSManaged.API.TagNum2String(tagNumAndVal.Num);
                    if (null != tagName)
                    {
                        Console.Write("\t" + tagName + "=" + tagNumAndVal.Value);
                    }
                    else
                    {
                        Console.Write("\t" + (uint)tagNumAndVal.Num + "=" + tagNumAndVal.Value);
                    }
                }
            }
            Console.Write("\n");
        }

        //![snippet_ibars_bars_handler]
        public static void QuotationIntradayBar(
        		uint requestId,
				List<QuotationIntradayBar> bars)
        {
            if ((bars != null) && (bars.Count > 0))
            {
                bool dumpHeader = true;
                foreach (QuotationIntradayBar bar in bars)
                {
                    Dump("IB", bar, dumpHeader);
                    dumpHeader = false;
                }
            }
        }
        //![snippet_ibars_bars_handler]

        //![snippet_ibars_delete_handler]
        public static void DeletedBar(
        		uint requestId, uint instrumentCode, ushort durationSeconds,
				ValueType timestampBeginUTC)
        {
            Console.WriteLine("DB\tINSTR\tSERVER_TIMESTAMP\tDURATION");
            Console.Write("DB\t" + instrumentCode + "\t");
            Console.Write(((DateTime)timestampBeginUTC)
            		.ToString("yyyy-MM-dd HH:mm:ss:fff") + "\t");
            Console.Write(durationSeconds);
            Console.Write("\n");
        }
        //![snippet_ibars_delete_handler]

        //![snippet_ibars_corrected_handler]
        public static void CorrectedBar(uint requestId, QuotationIntradayBar bar)
        {
            Dump("CB", bar, true);
        }
        //![snippet_ibars_corrected_handler]
    }
}
