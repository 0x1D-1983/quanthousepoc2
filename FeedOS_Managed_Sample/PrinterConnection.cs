///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using FeedOSManaged;

using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    public class PrinterConnection : PrinterBase
    {    
        public static void Disconnected(uint returnCode)
        {
            API.LogInfo("Disconnected: returnCode=" + API.ErrorString(returnCode));
        }
        public static void AdminMessage(bool isUrgent, string origin, string headline, string content)
        {
            API.LogInfo("AdminMessage: " + (isUrgent ? "Urgent" : "NotUrgent") + ": origin=" + origin + ", headline=" + headline + ", content=" + content);
        }
        public static void MsgDispatchingHookBegin(uint nbPendingMsg)
        {
            API.LogInfo("MsgDispatchingHookBegin: nbPendingMsg=" + nbPendingMsg);
        }
        public static void MsgDispatchingHookEnd()
        {
            API.LogInfo("MsgDispatchingHookEnd");
        }
        public static void HeartBeat(ValueType serverUTCDateTime, uint heartbeatPeriodSec )
        {
            API.LogInfo("HeartBeat: " + DumpTimestamp((DateTime)serverUTCDateTime,true) + " , heartbeatPeriodSec: " + heartbeatPeriodSec);
        }
        //![snippet_feed_status_snapshot_handler]
        public static void FeedStatusSnapshot( uint requestId, List<FeedStatus> snapshot )
        //![snippet_feed_status_snapshot_handler]
        {
            Console.WriteLine("FeedStatusSnapshot: ");
            if (null != snapshot)
            {
                foreach (FeedStatus fs in snapshot)
                {
                    Console.Write(fs.ToString());
                }
            }
            else
            {
                Console.WriteLine("<empty>");
            }
        }
        //![snippet_feed_status_update_handler]
        public static void FeedStatusUpdate(uint requestId, String sender, ValueType serverUTCDateTime, FeedStatus status)
        //![snippet_feed_status_update_handler]
        {
            Console.WriteLine("FeedStatusUpdate from : " + sender + " at time: " + DumpTimestamp((DateTime)serverUTCDateTime, false));
            Console.WriteLine(status.ToString());
        }
        //![snippet_feed_status_modified_handler]
        public static void FeedStatusModified( uint requestId, String sender, ValueType serverUTCDateTime, FeedStatusEvent statusEvent  )
        //![snippet_feed_status_modified_handler]
        {
            Console.WriteLine("FeedStatusModified from : " + sender + " at time: " + DumpTimestamp((DateTime)serverUTCDateTime, false));
            Console.WriteLine(statusEvent.ToString());
        }
        public static void ReplayCursor(uint requestId, ValueType replayCursor)
        {
            Console.WriteLine("FRAME\t" + DumpTimestamp((DateTime)replayCursor, true));
        }
    }
}
