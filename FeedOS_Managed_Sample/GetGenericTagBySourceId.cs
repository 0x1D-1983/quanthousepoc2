///-------------------------------
/// FeedOS C# Client API
/// copyright QuantHouse 2003-2017
///-------------------------------
using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
// FeedOS
using FeedOSManaged;
using FeedOSAPI;
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Getter class of generic tag by source id
    /// </summary>
	public class GetGenericTagBySourceId
	{
		public GetGenericTagBySourceId()
		{
		}

		static public void Print(DictionaryEntryWithId dictionaryEntryWithId)
		{
			Console.WriteLine("\n\t\tvalue=" + dictionaryEntryWithId.m_EntryId +
							  "\n\t\t\tname=" + dictionaryEntryWithId.m_DictEntry.m_ShortName +
							  "\n\t\t\tdescription=" + dictionaryEntryWithId.m_DictEntry.m_LongName +
							  "\n");
		}

		/** \cond refguide
		 *
		 * This sample demonstrates how to get generic tags by source id
		 * See RequestSubscribe.cs for documentation
		 *
		 * \endcond
		 */
		public void Parse(string[] args)
		{
			if (args.Length < 2 || args.Length > 3)
			{
				Console.WriteLine("GetGenericTagBySourceId <tag_number> <source_id> [<short_name>]");
				return;
			}
			//![snippet_sub_L1_generic_tags_entry_id_by_source_id]
			UInt16 tagNumber = Convert.ToUInt16(args[0]);
			UInt32 sourceId = Convert.ToUInt32(args[1]);

			Console.WriteLine("\trelated_tags=" + FeedOSManaged.API.TagNum2String(tagNumber) +
				 "\n\tsource_id=" + sourceId);

			if (args.Length > 2)
			{
				string shortName = args[2];
				// Get the entry_id, short_name and long_name of the tag associated to a short name, tag number and source id provided
				DictionaryEntryWithId dictionaryEntryWithId = FeedOSManaged.API.GetDictionaryEntryWithId(tagNumber, sourceId, shortName);

				if (dictionaryEntryWithId != null)
				{
					Print(dictionaryEntryWithId);
				}
			}
			else
			{
				// Get a list of the entry_id, short_name and long_name of a tag associated to the tag number and the source id provided
				List< DictionaryEntryWithId > dictionaryEntryWithIdList = FeedOSManaged.API.GetDictionaryEntryWithIdList(tagNumber, sourceId);

				if (dictionaryEntryWithIdList != null)
				{
					for (int i = 0; i < dictionaryEntryWithIdList.Count; ++i)
					{
						Print(dictionaryEntryWithIdList[i]);
					}
				}
			}
			//![snippet_sub_L1_generic_tags_entry_id_by_source_id]
		}
	}
}