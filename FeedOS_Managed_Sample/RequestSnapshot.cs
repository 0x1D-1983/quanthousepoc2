///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    public enum RequestSnapshotKind
    {
        L1,
    }

    /// <summary>
    /// Snapshot of instruments : retrieve current values (trading status, last price, volumes, best ask / bid limits, etc)
    /// </summary>
    class RequestSnapshot : Request
    {
        private RequestSnapshotKind m_Kind;
        private List<string> m_StringList = new List<string>();

        public RequestSnapshot(RequestSnapshotKind kind)
        {
            m_Kind = kind;
        }

        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args == null) return false;
            switch (m_Kind) 
            {
                case RequestSnapshotKind.L1:
                    if (args.Length != 1) {
                        Console.WriteLine("Invalid argument number for \"Snapshot" + m_Kind + "\" request");
                        return false;
                    }
                    break;
                default:
                    return false;
            }
            string polymorphicInstrumentCodes = args[0];
            string[] polymorphicInstrumentCodeTab = polymorphicInstrumentCodes.Split(new char[] { ',' });
            foreach (string polymorphicInstrumentCode in polymorphicInstrumentCodeTab) {
                m_StringList.Add(polymorphicInstrumentCode);
            }
            return true;
        }

        /** \cond refguide
         * The following sample demonstrates how to get a L1 snapshot for some
         * instruments.
         *
         * <ol><li>Fill a list of strings with the \c PolymorphicInstrumentCode
         * of the instruments you are interested in. Optionnally fill a list
         * of quotation tag numbers; here null is passed to retrieve all
         * available tags.</li></ol>
         * 
         * <ol><li>Implement and register handlers for common events (\c Started,
         * \c Failed, \c Succeeded).</li></ol>
         * 
         * <ol><li>Implement a \c SnapshotsEventHandler to handle the responses:
         * </li></ol>
         * \snippet PrinterL1.cs snippet_snap_l1_handler_impl
         * 
         * <ol><li>Register your handler:</li></ol>
         * \snippet RequestSnapshot.cs snippet_snap_l1_handler
         * 
         * <ol><li>Then use an established connection to perform the request:</li></ol>
         * \snippet RequestSnapshot.cs snippet_snap_l1_request
         *
         * <ol><li>Once the request is complete, unregister your handler:
         * </li></ol>
         * \snippet RequestSnapshot.cs snippet_snap_l1_handler_unplug
         *
         * \endcond
         */
        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);
            switch (m_Kind) {
                case RequestSnapshotKind.L1:
				    //![snippet_snap_l1_handler]
                    m_Connection.SnapshotsHandler += new FeedOSManaged.SnapshotsEventHandler(PrinterL1.SnapshotsHandler);
                    //![snippet_snap_l1_handler]
                    //![snippet_snap_l1_request]
                    m_Connection.SnapshotL1(m_StringList, null, false, m_RequestID);
                    //![snippet_snap_l1_request]
                    break;
                default:
                    return false;
            }
            return true;
        }

        public override void Stop()
        {
            UnconfigureRequestEvents(m_Connection);
            m_Connection.RemoveRequest(m_RequestID);
            switch (m_Kind) {
                case RequestSnapshotKind.L1:
				    //![snippet_snap_l1_handler_unplug]
                    m_Connection.SnapshotsHandler -= new FeedOSManaged.SnapshotsEventHandler(PrinterL1.SnapshotsHandler);
                    //![snippet_snap_l1_handler_unplug]
                    break;
            }
        }
        #endregion
    }
}

