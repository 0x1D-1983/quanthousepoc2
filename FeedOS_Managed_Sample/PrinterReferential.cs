///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOS_Managed_Sample
{
    /// <summary>
    /// Enum of all kind of Layout available
    /// </summary>
    public enum PrinterReferentialLayout
    {
        Typed,
        Raw,
        Csv
    }
   
    /// <summary>
    /// Class dedicated to display referential data.
    /// </summary>
    public class PrinterReferential : PrinterBase
    {
        private PrinterReferentialLayout m_layout;
        private List<ushort> m_tags;
        private bool m_header_printed = false;  
       
        public PrinterReferential(PrinterReferentialLayout layout, bool opt_no_header, List<ushort> tags)
        {
            m_layout = layout;
            m_tags = tags;
            // Setting the internal variable to know if we have done the header display
            m_header_printed = opt_no_header;
        }

        public static void Print(MarketCharacteristics market)
        {
            Console.WriteLine("market # " + market.FOSMarketId + "     " + market.MIC);
            Console.WriteLine("    MIC = " + market.MIC);
            Console.WriteLine("    TimeZone = " + market.TimeZone);
            Console.WriteLine("    Country = " + market.Country);
            Console.WriteLine("    NbMaxInstruments = " + market.NbMaxInstruments);
        }

        public static void Print(MarketContent branch)
        {
            foreach (MarketBranchContent branchContent in branch.MarketBranchContentList) {
                Console.WriteLine("    { " + FeedOSManaged.API.FOSMarketIDString(branch.FOSMarketId) + " " + branchContent.MarketBranchId.SecurityType + "  " + branchContent.MarketBranchId.CFICode + " } qty: " + branchContent.Quantity);
            }
        }

        public static void Markets(uint requestId, List<MarketCharacteristics> marketCharacteristicsList, List<MarketContent> marketContentList, ValueType LastUpdateDateTime)
        {
            if (marketCharacteristicsList != null) {
                Console.WriteLine("MARKETS");
                marketCharacteristicsList.ForEach(Print);
            }
            if (marketContentList != null) {
                Console.WriteLine("BRANCHES");
                marketContentList.ForEach(Print);
            }
            if (LastUpdateDateTime != null) {
                Console.WriteLine("update_timestamp:" + DumpTimestamp((DateTime)LastUpdateDateTime,true));
            }
        }

        public void BranchBegin(uint requestId, MarketBranchId marketBranchId, uint currentQuantity, uint deletedQuantity, uint createdQuantity, uint modifiedQuantity)
        {
            if (PrinterReferentialLayout.Typed  != m_layout )
            {
                // Print header of the data
                if (false == m_header_printed)
                {
                    m_header_printed = true;
                    char sep = '0';
                    if (PrinterReferentialLayout.Raw == m_layout)
                    {
                      sep = '\t';
                    }
                    else if (PrinterReferentialLayout.Csv == m_layout)
                    {
                        sep = ';';
                    }
                    if (null != m_tags)
                    {
                        StringBuilder headerBuilder = new StringBuilder();
                        headerBuilder.Append("INTERNAL");
                        foreach (ushort tag_id in m_tags)
                        {
                            headerBuilder.Append(sep);
                            headerBuilder.Append(FeedOSManaged.API.TagNum2String(tag_id));
                        }
                        Console.WriteLine(headerBuilder.ToString());
                    }
                }
            }
        }

        private void DumpInstruments(string eventType, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        {
            if (instrumentCharacteristicsList != null)
            {
                if (PrinterReferentialLayout.Typed == m_layout)
                {
                    foreach (InstrumentCharacteristics instrumentCharacteristics in instrumentCharacteristicsList) {
                        Console.WriteLine(eventType + "\t" + FeedOSManaged.API.FOSMarketIDInstrument(instrumentCharacteristics.InstrumentCode) + "/" + FeedOSManaged.API.LocalCodeInstrument(instrumentCharacteristics.InstrumentCode) + " FOSMarketId=(" + FeedOSManaged.API.FOSMarketIDString(FeedOSManaged.API.FOSMarketIDInstrument(instrumentCharacteristics.InstrumentCode)) + ")	" +
                            "CFICode=(" + instrumentCharacteristics.CFICode + ")	SecurityType=(" + instrumentCharacteristics.SecurityType +
                            ")	LocalCodeStr=(" + instrumentCharacteristics.LocalCodeStr + ")	Symbol=(" + instrumentCharacteristics.Symbol +
                            ")	Description=(" + instrumentCharacteristics.Description + ")	ISIN=(" + instrumentCharacteristics.ISIN +
                            ")	Maturity=(" + instrumentCharacteristics.MaturityYear + "-" + instrumentCharacteristics.MaturityMonth + "-" + instrumentCharacteristics.MaturityDay + ")");
                    }
                }
                else
                {
                    char sep = '0';
                    if (PrinterReferentialLayout.Raw == m_layout)
                    {
                      sep = '\t';
                    }
                    else if (PrinterReferentialLayout.Csv == m_layout)
                    {
                        sep = ';';
                    }
                    StringBuilder rowOutputer = new StringBuilder();
                    foreach (InstrumentCharacteristics instrumentCharacteristics in instrumentCharacteristicsList)
                    {
                        if (null != instrumentCharacteristics.ReferentialAttributes)
                        {
                            List<TagNumAndValue> cur_attributes = instrumentCharacteristics.ReferentialAttributes;
                            rowOutputer.Append(eventType).Append(sep);
                            rowOutputer.Append(instrumentCharacteristics.InstrumentCode);
                            foreach(ushort tag_id in m_tags)
                            {
                                FeedOSAPI.TagFinder finder = new FeedOSAPI.TagFinder(tag_id);
                                TagNumAndValue cur_tag_n_value = cur_attributes.Find(finder.FindTagWithNum);
                                if (null != cur_tag_n_value)
                                {
                                    rowOutputer.Append(sep);
                                    rowOutputer.Append(cur_tag_n_value.Value.ToString());
                                }
                            }
                            Console.WriteLine(rowOutputer.ToString());
                            rowOutputer.Remove(0, rowOutputer.Length);
                        }
                    }
                }
            }
        }

        public void InstrumentsCreated(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        {
            DumpInstruments("CREATED", instrumentCharacteristicsList);
        }

        public void InstrumentsModified(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        {
            DumpInstruments("MODIFIED", instrumentCharacteristicsList);
        }

        public void InstrumentsDeleted(uint requestId, List<uint> FOSInstrumentCodeList)
        {
            if (FOSInstrumentCodeList != null)
            {
                if (PrinterReferentialLayout.Typed == m_layout)
                {
                    foreach (uint code in FOSInstrumentCodeList)
                    {
                        Console.WriteLine("DELETED\t"
                            + FeedOSManaged.API.FOSMarketIDInstrument(code)
                            + "/" + FeedOSManaged.API.LocalCodeInstrument(code)
                            + " FOSMarketId=("
                            + FeedOSManaged.API.FOSMarketIDString(FeedOSManaged.API.FOSMarketIDInstrument(code)) + ")");
                    }
                }
                else
                {
                    foreach (uint code in FOSInstrumentCodeList)
                    {
                        Console.WriteLine("DELETED\t" + code);
                    }
                }
            }
        }

        public void MarkerTimestamp(uint requestId, ValueType markerDateTime)
        {
            if (markerDateTime != null)
            {
                Console.WriteLine("marker timestamp:" + DumpTimestamp((DateTime)markerDateTime, true));
            }
        }

        public void VariablePriceIncrementTables(uint requestId, List<VariablePriceIncrementTable> tables)
        {
            if (tables != null)
            {
                if (PrinterReferentialLayout.Typed == m_layout)
                {
                    foreach (VariablePriceIncrementTable table in tables)
                    {
                        Console.Write("PRICE_BAND_TABLE\tId=(" + table.Id + ")\tDescription=(" + table.Description + ")\tPriceBands={");
                        if (table.PriceBands != null)
                        {
                            bool printComma = false;
                            foreach (VariableIncrementPriceBand priceBand in table.PriceBands)
                            {
                                if (printComma)
                                {
                                    Console.Write(",");
                                }
                                Console.Write( " [IsInclusive=(" + (priceBand.Inclusive ? "Y" : "N") + ")"
                                             + ", LowerBoundary=(" + priceBand.LowerBoundary + ")"
                                             + ", PriceIncrement=(" + priceBand.PriceIncrement + ")]");
                                printComma = true;
                            }
                        }
                        Console.Write(" }\n");
                    }
                }
                else
                {
                    string separator = PrinterReferentialLayout.Csv == m_layout ? ";" : "\t";

                    Console.WriteLine("PriceBandTableId" 
                                     + separator + "Description"
                                     + separator + "Inclusive/Exclusive"
                                     + separator + "LowerBoundary"
                                     + separator + "PriceIncrement");

                    foreach (VariablePriceIncrementTable table in tables)
                    {
                        if (table.PriceBands != null)
                        {
                            foreach (VariableIncrementPriceBand priceBand in table.PriceBands)
                            {
                                Console.WriteLine(table.Id
                                                + separator + table.Description
                                                + separator + (priceBand.Inclusive ? "Inclusive" : "Exclusive")
                                                + separator + priceBand.LowerBoundary
                                                + separator + priceBand.PriceIncrement);
                            }
                        }
                        else
                        {
                            Console.WriteLine(table.Id + separator + table.Description + separator + "No price band");
                        }
                    }
                }
            }
        }

        public void TradeConditionsDictionary(uint requestId, List<TradeConditionsDictionaryEntry> entries)
        {
            if (entries != null)
            {
                if (PrinterReferentialLayout.Typed == m_layout)
                {
                    foreach (TradeConditionsDictionaryEntry entry in entries)
                    {
                        Console.Write("TRADE_COND\tIndex=(" + entry.Index + ")\tTags={");
                        if (entry.Value != null)
                        {
                            bool printComma = false;
                            foreach (TagNumAndValue tag in entry.Value)
                            {
                                if (printComma)
                                {
                                    Console.Write(",");
                                }
                                string tagName = FeedOSManaged.API.TagNum2String(tag.Num);
                                if (null != tagName)
                                {
                                    Console.Write(tagName);
                                }
                                else
                                {
                                    Console.Write((uint)tag.Num);
                                }
                                Console.Write("=" + tag.Value);
                                printComma = true;
                            }
                        }
                        Console.Write("}\n");
                    }
                }
                else
                {
                    string separator = PrinterReferentialLayout.Csv == m_layout ? ";" : "\t";

                    Console.WriteLine("TradeConditionIndex" + separator + "Tags");
                    foreach (TradeConditionsDictionaryEntry entry in entries)
                    {
                        Console.Write(entry.Index);
                        if (entry.Value != null)
                        {
                            Console.Write(separator);
                            bool printComma = false;
                            foreach (TagNumAndValue tag in entry.Value)
                            {
                                if (printComma)
                                {
                                    Console.Write(",");
                                }
                                string tagName = FeedOSManaged.API.TagNum2String(tag.Num);
                                if (null != tagName)
                                {
                                    Console.Write(tagName);
                                }
                                else
                                {
                                    Console.Write((uint)tag.Num);
                                }
                                Console.Write("=" + tag.Value);
                                printComma = true;
                            }
                        }
                        Console.Write("\n");
                    }
                }
            }
        }

        public void MetaData(uint requestId, List<TagDeclaration> tags, List<EnumTypeDeclaration> enums, List<string> providers)
        {
            if (null != tags)
            {
                foreach (TagDeclaration tag in tags)
                {
                    Console.WriteLine("METADATA_TAG"
                        + "\tUsage=(" + tag.TagUsage + ")"
                        + "\tSyntax=(" + FeedOSManaged.API.TagSyntaxString(tag.TagSyntax) + ")"
                        + "\tNumericId=(" + tag.TagNumericId + ")"
                        + "\tName=(" + tag.TagName + ")"
                        + "\tDescription=(" + tag.TagDescription + ")"
                        );
                }
            }
            if (null != enums)
            {
                foreach (EnumTypeDeclaration e in enums)
                {
                    if (null != e.RelatedTags)
                    {
                        foreach (ushort t in e.RelatedTags)
                        {
                            Console.WriteLine("METADATA_ENUM_TAG"
                                + "\tEnumName=(" + e.EnumName + ")"
                                + "\tEnumDescription=(" + e.EnumDescription + ")"
                                + "\tRelatedTag=(" + t + ")"
                                );
                        }
                    }

                    if (null != e.EnumValues)
                    {
                        foreach (EnumValueDeclaration value in e.EnumValues)
                        {
                            Console.WriteLine("METADATA_ENUM_VALUE"
                                + "\tEnumName=(" + e.EnumName + ")"
                                + "\tValue=(" + value.Value + ")"
                                + "\tValueName=(" + value.ValueName + ")"
                                + "\tValueDescription=(" + value.ValueDescription + ")"
                                );
                        }
                    }
                }
            }
            if (null != providers)
            {
                foreach (String provider in providers)
                {
                    Console.WriteLine("METADATA_PROVIDER"
                        + "\tProviderName=(" + provider + ")"
                        );
                }
            }
        }

        public void RealtimeBegin(uint requestId)
        {
            Console.WriteLine("realtime referential events starts");
        }
    }
}
