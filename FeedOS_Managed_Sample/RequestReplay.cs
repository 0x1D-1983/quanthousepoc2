///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
// FeedOS
using FeedOSAPI.Types;
using FeedOSManaged;

namespace FeedOS_Managed_Sample
{
    public enum RequestReplayKind
    {
        L1,
        L2,
        L1L2,
        IBar,
        MBO,
        MUXED,
        FeedStatus
    }

    /// <summary>
    /// Replaying one feed (usually one feed = one market) over a given period of time. 
    /// L1 and L2 data are supported, along with intraday bars and �mixed L1/L2� when this has been enabled on the replay server.
    /// </summary>
    public class RequestReplay : Subscription
    {
        private RequestReplayKind m_Kind;
        private UInt32 m_SourceId;
        private DateTime m_DateTimeBegin;
        private DateTime m_DateTimeEnd;
        private PrinterL2 m_PrinterL2;
        private PrinterMBL m_PrinterMBL;
        private PrinterMBO m_PrinterMBO;

        public RequestReplay(RequestReplayKind kind)
        {
            m_Kind = kind;
        }
        #region IRequest Members
        public override bool Parse(string[] args)
        {
            if (args.Length != 3) {
                return false;
            }
            if (!UInt32.TryParse(args[0], out m_SourceId)) {
                Console.WriteLine("Invalid source Id: " + args[0]);
                return false;
            }
            if (!DateTime.TryParseExact(args[1], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeBegin)) {
                Console.WriteLine("Invalid timestamp to start from: " + args[1] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                return false;
            }
            if (!DateTime.TryParseExact(args[2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out m_DateTimeEnd)) {
                Console.WriteLine("Invalid timestamp to finish: " + args[2] + " (format must match \"yyyy-MM-dd HH:mm:ss\")");
                return false;
            }
            if (m_Verbose) {
                Console.WriteLine("    sourceId=" + m_SourceId);
                Console.WriteLine("    dateTimeBegin=" + m_DateTimeBegin);
                Console.WriteLine("    dateTimeEnd=" + m_DateTimeEnd);
            }
            return true;
        }

        public override bool Start(Connection connection, uint requestID)
        {
            m_Connection = connection;
            m_RequestID = requestID;
            ConfigureRequestEvents(connection);
            m_Connection.ReplayCursorHandler += PrinterConnection.ReplayCursor;
            switch (m_Kind) {
                case RequestReplayKind.L1:
                    m_Connection.TradeEventExtHandler += new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
                    m_Connection.ValuesUpdateHandler += new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
                    m_Connection.MarketNewsHandler += new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
                    m_Connection.MarketStatusHandler += new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
                    m_Connection.TradeCancelCorrectionHandler += new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
                    m_Connection.ReplayL1(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                case RequestReplayKind.L2:
                    // Keep this to be able to dump legacy L2 replayed feed
                    m_PrinterL2 = new PrinterL2();
                    m_PrinterL2.Verbose = m_Verbose;

                    m_Connection.OrderBookRefreshHandler += new FeedOSManaged.OrderBookRefreshEventHandler(m_PrinterL2.OrderBookRefreshHandler);
                    m_Connection.OrderBookDeltaRefreshHandler += new FeedOSManaged.OrderBookDeltaRefreshEventHandler(m_PrinterL2.OrderBookDeltaRefreshHandler);
                    m_Connection.OrderBookMaxVisibleDepthHandler += new FeedOSManaged.OrderBookMaxVisibleDepthEventHandler(m_PrinterL2.OrderBookMaxVisibleDepthHandler);

                    // New MBL replay
                    m_PrinterMBL = new PrinterMBL();
                    m_PrinterMBL.Verbose = m_Verbose;
                    m_Connection.MBLFullRefreshHandler += new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
                    m_Connection.MBLOverlapRefreshHandler += new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
                    m_Connection.MBLDeltaRefreshHandler += new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
                    m_Connection.MBLMaxVisibleDepthHandler += new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);
      
        
                    m_Connection.ReplayL2(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                case RequestReplayKind.MBO:
                    m_PrinterMBO = new PrinterMBO();

                    m_Connection.MarketSheetSnapshotHandler += new FeedOSManaged.MarketSheetSnapshotEventHandler(m_PrinterMBO.MarketSheetSnapshotHandler);
                    m_Connection.NewOrderHandler += new FeedOSManaged.NewOrderEventHandler(m_PrinterMBO.NewOrderHandler);
                    m_Connection.ModifyOrderHandler += new FeedOSManaged.ModifyOrderEventHandler(m_PrinterMBO.ModifyOrderHandler);
                    m_Connection.RemoveOneOrderHandler += new FeedOSManaged.RemoveOneOrderEventHandler(m_PrinterMBO.RemoveOneOrderHandler);
                    m_Connection.RemoveAllPreviousOrdersHandler += new FeedOSManaged.RemoveAllPreviousOrdersEventHandler(m_PrinterMBO.RemoveAllPreviousOrdersHandler);
                    m_Connection.RemoveAllOrdersHandler += new FeedOSManaged.RemoveAllOrdersEventHandler(m_PrinterMBO.RemoveAllOrdersHandler);
                    m_Connection.RetransmissionHandler += new FeedOSManaged.RetransmissionEventHandler(m_PrinterMBO.RetransmissionHandler);
                    m_Connection.ValuesUpdateOneInstrumentHandler += new FeedOSManaged.ValuesUpdateOneInstrumentEventHandler(m_PrinterMBO.ValuesUpdateOneInstrumentHandler);

                    m_Connection.ReplayMBO(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                case RequestReplayKind.FeedStatus:
                    // FeedStatus
                    m_Connection.FeedStatusUpdate += new FeedOSManaged.FeedStatusUpdateEventHandler(PrinterConnection.FeedStatusUpdate);
                    m_Connection.FeedStatusModified += new FeedOSManaged.FeedStatusModifiedEventHandler(PrinterConnection.FeedStatusModified);

                    m_Connection.ReplayFeedStatus(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                case RequestReplayKind.L1L2:
                    // L1
                    m_Connection.TradeEventExtHandler += new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
                    m_Connection.ValuesUpdateHandler += new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
                    m_Connection.MarketNewsHandler += new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
                    m_Connection.MarketStatusHandler += new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
                    m_Connection.TradeCancelCorrectionHandler += new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
                    // Keep this to be able to dump legacy L2 replayed feed
                    m_PrinterL2 = new PrinterL2();
                    m_PrinterL2.Verbose = m_Verbose;
                    m_Connection.OrderBookRefreshHandler += new FeedOSManaged.OrderBookRefreshEventHandler(m_PrinterL2.OrderBookRefreshHandler);
                    m_Connection.OrderBookDeltaRefreshHandler += new FeedOSManaged.OrderBookDeltaRefreshEventHandler(m_PrinterL2.OrderBookDeltaRefreshHandler);
                    m_Connection.OrderBookMaxVisibleDepthHandler += new FeedOSManaged.OrderBookMaxVisibleDepthEventHandler(m_PrinterL2.OrderBookMaxVisibleDepthHandler);

                    // New MBL replay
                    m_PrinterMBL = new PrinterMBL();
                    m_PrinterMBL.Verbose = m_Verbose;
                    m_Connection.MBLFullRefreshHandler += new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
                    m_Connection.MBLOverlapRefreshHandler += new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
                    m_Connection.MBLDeltaRefreshHandler += new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
                    m_Connection.MBLMaxVisibleDepthHandler += new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);
      
                            
                    m_Connection.ReplayL1L2(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                case RequestReplayKind.MUXED:
                    // L1
                    m_Connection.TradeEventExtHandler += new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
                    m_Connection.ValuesUpdateHandler += new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
                    m_Connection.MarketNewsHandler += new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
                    m_Connection.MarketStatusHandler += new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
                    m_Connection.TradeCancelCorrectionHandler += new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
                    // Keep this to be able to dump legacy L2 replayed feed
                    m_PrinterL2 = new PrinterL2();
                    m_PrinterL2.Verbose = m_Verbose;
                    m_Connection.OrderBookRefreshHandler += new FeedOSManaged.OrderBookRefreshEventHandler(m_PrinterL2.OrderBookRefreshHandler);
                    m_Connection.OrderBookDeltaRefreshHandler += new FeedOSManaged.OrderBookDeltaRefreshEventHandler(m_PrinterL2.OrderBookDeltaRefreshHandler);
                    m_Connection.OrderBookMaxVisibleDepthHandler += new FeedOSManaged.OrderBookMaxVisibleDepthEventHandler(m_PrinterL2.OrderBookMaxVisibleDepthHandler);

                    // New MBL replay
                    m_PrinterMBL = new PrinterMBL();
                    m_PrinterMBL.Verbose = m_Verbose;
                    m_Connection.MBLFullRefreshHandler += new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
                    m_Connection.MBLOverlapRefreshHandler += new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
                    m_Connection.MBLDeltaRefreshHandler += new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
                    m_Connection.MBLMaxVisibleDepthHandler += new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);
      
                       
                    m_PrinterMBO = new PrinterMBO();

                    m_Connection.MarketSheetSnapshotHandler += new FeedOSManaged.MarketSheetSnapshotEventHandler(m_PrinterMBO.MarketSheetSnapshotHandler);
                    m_Connection.NewOrderHandler += new FeedOSManaged.NewOrderEventHandler(m_PrinterMBO.NewOrderHandler);
                    m_Connection.ModifyOrderHandler += new FeedOSManaged.ModifyOrderEventHandler(m_PrinterMBO.ModifyOrderHandler);
                    m_Connection.RemoveOneOrderHandler += new FeedOSManaged.RemoveOneOrderEventHandler(m_PrinterMBO.RemoveOneOrderHandler);
                    m_Connection.RemoveAllPreviousOrdersHandler += new FeedOSManaged.RemoveAllPreviousOrdersEventHandler(m_PrinterMBO.RemoveAllPreviousOrdersHandler);
                    m_Connection.RemoveAllOrdersHandler += new FeedOSManaged.RemoveAllOrdersEventHandler(m_PrinterMBO.RemoveAllOrdersHandler);
                    m_Connection.RetransmissionHandler += new FeedOSManaged.RetransmissionEventHandler(m_PrinterMBO.RetransmissionHandler);
                    m_Connection.ValuesUpdateOneInstrumentHandler += new FeedOSManaged.ValuesUpdateOneInstrumentEventHandler(m_PrinterMBO.ValuesUpdateOneInstrumentHandler);

                    // FeedStatus
                    m_Connection.FeedStatusUpdate += new FeedOSManaged.FeedStatusUpdateEventHandler(PrinterConnection.FeedStatusUpdate);
                    m_Connection.FeedStatusModified += new FeedOSManaged.FeedStatusModifiedEventHandler(PrinterConnection.FeedStatusModified);

                    m_Connection.ReplayMUXED(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                case RequestReplayKind.IBar:
                    m_Connection.IntradayBarsHandler += new FeedOSManaged.IntradayBarsEventHandler(PrinterIntradayBar.QuotationIntradayBar);
                    m_Connection.DeleteBarHandler += new FeedOSManaged.DeleteBarEventHandler(PrinterIntradayBar.DeletedBar);
                    m_Connection.CorrectedBarHandler += new FeedOSManaged.CorrectedBarEventHandler(PrinterIntradayBar.CorrectedBar);
                    m_Connection.ReplayIBar(m_SourceId, m_DateTimeBegin, m_DateTimeEnd, m_RequestID);
                    break;
                default:
                    return false;
            }
            return true;
        }

        public override void Stop()
        {
            m_Connection.RemoveRequest(m_RequestID);
            UnconfigureRequestEvents(m_Connection);
            m_Connection.ReplayCursorHandler -= PrinterConnection.ReplayCursor;
            switch (m_Kind) {
                case RequestReplayKind.L1:
                    m_Connection.TradeEventExtHandler -= new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
                    m_Connection.ValuesUpdateHandler -= new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
                    m_Connection.MarketNewsHandler -= new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
                    m_Connection.MarketStatusHandler -= new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
                    m_Connection.TradeCancelCorrectionHandler -= new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
                    break;
                case RequestReplayKind.L2:
                    // Legacy L2
                    m_Connection.OrderBookRefreshHandler -= new FeedOSManaged.OrderBookRefreshEventHandler(m_PrinterL2.OrderBookRefreshHandler);
                    m_Connection.OrderBookDeltaRefreshHandler -= new FeedOSManaged.OrderBookDeltaRefreshEventHandler(m_PrinterL2.OrderBookDeltaRefreshHandler);
                    m_Connection.OrderBookMaxVisibleDepthHandler -= new FeedOSManaged.OrderBookMaxVisibleDepthEventHandler(m_PrinterL2.OrderBookMaxVisibleDepthHandler);
        
                    // New MBL
                    m_Connection.MBLFullRefreshHandler -= new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
                    m_Connection.MBLOverlapRefreshHandler -= new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
                    m_Connection.MBLDeltaRefreshHandler -= new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
                    m_Connection.MBLMaxVisibleDepthHandler -= new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);
                       
                    break;
                case RequestReplayKind.L1L2:
                    m_Connection.TradeEventExtHandler -= new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
                    m_Connection.ValuesUpdateHandler -= new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
                    m_Connection.MarketNewsHandler -= new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
                    m_Connection.MarketStatusHandler -= new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
                    m_Connection.TradeCancelCorrectionHandler -= new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
                    // Legacy L2
                    m_Connection.OrderBookRefreshHandler -= new FeedOSManaged.OrderBookRefreshEventHandler(m_PrinterL2.OrderBookRefreshHandler);
                    m_Connection.OrderBookDeltaRefreshHandler -= new FeedOSManaged.OrderBookDeltaRefreshEventHandler(m_PrinterL2.OrderBookDeltaRefreshHandler);
                    m_Connection.OrderBookMaxVisibleDepthHandler -= new FeedOSManaged.OrderBookMaxVisibleDepthEventHandler(m_PrinterL2.OrderBookMaxVisibleDepthHandler);

                    // New MBL
                    m_Connection.MBLFullRefreshHandler -= new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
                    m_Connection.MBLOverlapRefreshHandler -= new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
                    m_Connection.MBLDeltaRefreshHandler -= new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
                    m_Connection.MBLMaxVisibleDepthHandler -= new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);
                                           
                    break;
                case RequestReplayKind.MBO:

                    m_Connection.MarketSheetSnapshotHandler -= new FeedOSManaged.MarketSheetSnapshotEventHandler(m_PrinterMBO.MarketSheetSnapshotHandler);
                    m_Connection.NewOrderHandler -= new FeedOSManaged.NewOrderEventHandler(m_PrinterMBO.NewOrderHandler);
                    m_Connection.ModifyOrderHandler -= new FeedOSManaged.ModifyOrderEventHandler(m_PrinterMBO.ModifyOrderHandler);
                    m_Connection.RemoveOneOrderHandler -= new FeedOSManaged.RemoveOneOrderEventHandler(m_PrinterMBO.RemoveOneOrderHandler);
                    m_Connection.RemoveAllPreviousOrdersHandler -= new FeedOSManaged.RemoveAllPreviousOrdersEventHandler(m_PrinterMBO.RemoveAllPreviousOrdersHandler);
                    m_Connection.RemoveAllOrdersHandler -= new FeedOSManaged.RemoveAllOrdersEventHandler(m_PrinterMBO.RemoveAllOrdersHandler);
                    m_Connection.RetransmissionHandler -= new FeedOSManaged.RetransmissionEventHandler(m_PrinterMBO.RetransmissionHandler);
                    m_Connection.ValuesUpdateOneInstrumentHandler -= new FeedOSManaged.ValuesUpdateOneInstrumentEventHandler(m_PrinterMBO.ValuesUpdateOneInstrumentHandler);

                    break;
                case RequestReplayKind.FeedStatus:
                    // FeedStatus
                    m_Connection.FeedStatusUpdate -= new FeedOSManaged.FeedStatusUpdateEventHandler(PrinterConnection.FeedStatusUpdate);
                    m_Connection.FeedStatusModified -= new FeedOSManaged.FeedStatusModifiedEventHandler(PrinterConnection.FeedStatusModified);
                    break;
                case RequestReplayKind.MUXED:
                    // L1
                    m_Connection.TradeEventExtHandler -= new FeedOSManaged.TradeEventExtEventHandler(PrinterL1.TradeEventExtHandler);
                    m_Connection.ValuesUpdateHandler -= new FeedOSManaged.ValuesUpdateEventHandler(PrinterL1.ValuesUpdateHandler);
                    m_Connection.MarketNewsHandler -= new FeedOSManaged.MarketNewsEventHandler(PrinterStatus.MarketNewsHandler);
                    m_Connection.MarketStatusHandler -= new FeedOSManaged.MarketStatusEventHandler(PrinterStatus.MarketStatusHandler);
                    m_Connection.TradeCancelCorrectionHandler -= new FeedOSManaged.TradeCancelCorrectionEventHandler(PrinterL1.TradeCancelCorrectionHandler);
                    // Legacy L2
                    m_Connection.OrderBookRefreshHandler -= new FeedOSManaged.OrderBookRefreshEventHandler(m_PrinterL2.OrderBookRefreshHandler);
                    m_Connection.OrderBookDeltaRefreshHandler -= new FeedOSManaged.OrderBookDeltaRefreshEventHandler(m_PrinterL2.OrderBookDeltaRefreshHandler);
                    m_Connection.OrderBookMaxVisibleDepthHandler -= new FeedOSManaged.OrderBookMaxVisibleDepthEventHandler(m_PrinterL2.OrderBookMaxVisibleDepthHandler);

                    // New MBL 
                    m_Connection.MBLFullRefreshHandler -= new FeedOSManaged.MBLFullRefreshEventHandler(m_PrinterMBL.MBLFullRefreshHandler);
                    m_Connection.MBLOverlapRefreshHandler -= new FeedOSManaged.MBLOverlapRefreshEventHandler(m_PrinterMBL.MBLOverlapRefreshHandler);
                    m_Connection.MBLDeltaRefreshHandler -= new FeedOSManaged.MBLDeltaRefreshEventHandler(m_PrinterMBL.MBLDeltaRefreshHandler);
                    m_Connection.MBLMaxVisibleDepthHandler -= new FeedOSManaged.MBLMaxVisibleDepthEventHandler(m_PrinterMBL.MBLMaxVisibleDepthHandler);

                    m_Connection.MarketSheetSnapshotHandler -= new FeedOSManaged.MarketSheetSnapshotEventHandler(m_PrinterMBO.MarketSheetSnapshotHandler);
                    m_Connection.NewOrderHandler -= new FeedOSManaged.NewOrderEventHandler(m_PrinterMBO.NewOrderHandler);
                    m_Connection.ModifyOrderHandler -= new FeedOSManaged.ModifyOrderEventHandler(m_PrinterMBO.ModifyOrderHandler);
                    m_Connection.RemoveOneOrderHandler -= new FeedOSManaged.RemoveOneOrderEventHandler(m_PrinterMBO.RemoveOneOrderHandler);
                    m_Connection.RemoveAllPreviousOrdersHandler -= new FeedOSManaged.RemoveAllPreviousOrdersEventHandler(m_PrinterMBO.RemoveAllPreviousOrdersHandler);
                    m_Connection.RemoveAllOrdersHandler -= new FeedOSManaged.RemoveAllOrdersEventHandler(m_PrinterMBO.RemoveAllOrdersHandler);
                    m_Connection.RetransmissionHandler -= new FeedOSManaged.RetransmissionEventHandler(m_PrinterMBO.RetransmissionHandler);
                    m_Connection.ValuesUpdateOneInstrumentHandler -= new FeedOSManaged.ValuesUpdateOneInstrumentEventHandler(m_PrinterMBO.ValuesUpdateOneInstrumentHandler);

                    // FeedStatus
                    m_Connection.FeedStatusUpdate -= new FeedOSManaged.FeedStatusUpdateEventHandler(PrinterConnection.FeedStatusUpdate);
                    m_Connection.FeedStatusModified -= new FeedOSManaged.FeedStatusModifiedEventHandler(PrinterConnection.FeedStatusModified);

                    break;
                case RequestReplayKind.IBar:
                    m_Connection.IntradayBarsHandler -= new FeedOSManaged.IntradayBarsEventHandler(PrinterIntradayBar.QuotationIntradayBar);
                    m_Connection.DeleteBarHandler -= new FeedOSManaged.DeleteBarEventHandler(PrinterIntradayBar.DeletedBar);
                    m_Connection.CorrectedBarHandler -= new FeedOSManaged.CorrectedBarEventHandler(PrinterIntradayBar.CorrectedBar);
                    break;
            }
        }
        #endregion IRequest Members
    }
}
