﻿// Crockford's supplant method (poor man's templating)
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {

    var stockTable = document.getElementById('stockTable');
    var stockTableBody = stockTable.getElementsByTagName('tbody')[0];

    var rowTemplate = `<td>{Description}</td>
                    <td class="{DirectionAsk}">{AskPrice}</td>
                    <td class="{DirectionBid}">{BidPrice}</td>
                    <td>{LastPrice}</td>
                    <td>{LastTradePrice}</td>
                    <td>{DailyClosingAskPrice}</td>
                    <td>{DailyClosingBidPrice}</td>
                    <td>{DailyOpeningPrice}</td>
                    <td>{PreviousDailyClosingPrice}</td>`;

    var up = '▲';
    var down = '▼';

    function Stock(internalCode, description) {
        this.InternalCode = internalCode;
        this.Description = description;
        this.AskPrice = null;
        this.AskPricePrev = null;
        this.BidPrice = null;
        this.BidPricePrev = null;
        this.LastPrice = null;
        this.LastTradePrice = null;
        this.LastTradeTimestamp = null;
        this.CurrentBusinessDay = null;
        this.DailyClosingAskPrice = null;
        this.DailyClosingAskTimestamp = null;
        this.DailyClosingBidPrice = null;
        this.DailyClosingBidTimestamp = null;
        this.DailyOpeningPrice = null;
        this.DailyOpeningPriceTimestamp = null;
        this.PreviousDailyClosingPrice = null;
        this.PreviousDailyClosingPriceTimestamp = null;
    }

    var store = {
        768315389: new Stock(768315389, 'Vodafone Group PLC'),
        940275063: new Stock(940275063, 'Vodafone Group PLC'),
        940282090: new Stock(940282090, 'Invesco FTSE RAFI All World 3000 UCITS ETF'),
        940279505: new Stock(940279505, 'Vanguard FTSE Developed Europe ex UK UCITS ETF'),
        940277943: new Stock(940277943, 'HSBC FTSE 100 UCITS ETF')
    };

    initialShowAllStocks();

    var conn = $.hubConnection();
    var proxy = conn.createHubProxy('stockTickerHub');
    proxy.hubName = 'stockTickerHub';
    conn.url = 'http://127.0.0.1:8088/signalr/';


    proxy.on('updateStock', function (stock) {
        store[stock.InternalCode] = stock;
        //if (!store[stock.InternalCode]) {
        //    store[stock.InternalCode] = stock;
        //}
        //else {
        //    if (stock.AskPrice) {
        //        store[stock.InternalCode].AskPricePrev = store[stock.InternalCode].AskPrice;
        //        store[stock.InternalCode].AskPrice = stock.AskPrice;
        //    }

        //    if (stock.BidPrice) {
        //        store[stock.InternalCode].BidPricePrev = store[stock.InternalCode].BidPrice;
        //        store[stock.InternalCode].BidPrice = stock.BidPrice;
        //    }

        //    if (stock.LastPrice) store[stock.InternalCode].LastPrice = stock.LastPrice;
        //    if (stock.LastTradePrice) store[stock.InternalCode].LastTradePrice = stock.LastTradePrice;
        //    if (stock.CurrentBusinessDay) store[stock.InternalCode].CurrentBusinessDay = stock.CurrentBusinessDay;
        //    if (stock.DailyClosingAskPrice) store[stock.InternalCode].DailyClosingAskPrice = stock.DailyClosingAskPrice;
        //    if (stock.DailyClosingAskTimestamp) store[stock.InternalCode].DailyClosingAskTimestamp = stock.DailyClosingAskTimestamp;
        //    if (stock.DailyClosingBidPrice) store[stock.InternalCode].DailyClosingBidPrice = stock.DailyClosingBidPrice;
        //    if (stock.DailyClosingBidTimestamp) store[stock.InternalCode].DailyClosingBidTimestamp = stock.DailyClosingBidTimestamp;
        //    if (stock.DailyOpeningPrice) store[stock.InternalCode].DailyOpeningPrice = stock.DailyOpeningPrice;
        //    if (stock.DailyOpeningPriceTimestamp) store[stock.InternalCode].DailyOpeningPriceTimestamp = stock.DailyOpeningPriceTimestamp;
        //    if (stock.PreviousDailyClosingPrice) store[stock.InternalCode].PreviousDailyClosingPrice = stock.PreviousDailyClosingPrice;
        //    if (stock.PreviousDailyClosingPriceTimestamp) store[stock.InternalCode].PreviousDailyClosingPriceTimestamp = stock.PreviousDailyClosingPriceTimestamp;
        //}

        displayStock(store[stock.InternalCode]);
    });

    conn.start().done(function () {

        document.getElementById('open').onclick = function () {
            proxy.invoke("subscribeL1");
            connected();
        }

        document.getElementById('close').onclick = function () {
            proxy.invoke("unsubscribeL1");
            disconnected();
        }
    }).fail(function (err) {
        console.log(err);
    });

    function connected() {
        document.getElementById('open').setAttribute("disabled", "disabled");
        document.getElementById('close').removeAttribute("disabled");
    }

    function disconnected() {
        document.getElementById('open').removeAttribute("disabled");
        document.getElementById('close').setAttribute("disabled", "disabled");
    }

    function initialShowAllStocks() {
        for (var key in store) {
            displayStock(store[key]);
        }
    }

    function displayStock(stock) {
        var displayStock = formatStock(stock);
        addOrReplaceStock(stockTableBody, displayStock, 'tr', rowTemplate);
    }

    function addOrReplaceStock(table, stock, type, template) {

        var child = createStockNode(stock, type, template);

        var stockNode = document.querySelector(type + "[data-symbol='" + stock.InternalCode + "']");
        if (stockNode) {
            table.replaceChild(child, stockNode);
        } else {
            table.appendChild(child);
        }
    }

    function formatStock(stock) {
        var changeAsk = stock.AskPrice - stock.AskPricePrev;
        var changeBid = stock.BidPrice - stock.BidPricePrev;

        stock.DirectionAsk = changeAsk == 0 ? '' : changeAsk >= 0 ? 'increase' : 'decrease';
        stock.DirectionBid = changeBid == 0 ? '' : changeBid >= 0 ? 'increase' : 'decrease';

        if (stock.AskPrice == null) stock.AskPrice = 'N/A';
        if (stock.AskPricePrev == null) stock.AskPricePrev = 'N/A';
        if (stock.BidPrice == null) stock.BidPrice = 'N/A';
        if (stock.BidPricePrev == null) stock.BidPricePrev = 'N/A';
        if (stock.LastPrice == null) stock.LastPrice = 'N/A';
        if (stock.LastTradePrice == null) stock.LastTradePrice = 'N/A';
        if (stock.DailyClosingAskPrice == null) stock.DailyClosingAskPrice = 'N/A';
        if (stock.DailyClosingBidPrice == null) stock.DailyClosingBidPrice = 'N/A';
        if (stock.DailyOpeningPrice == null) stock.DailyOpeningPrice = 'N/A';
        if (stock.PreviousDailyClosingPrice == null) stock.PreviousDailyClosingPrice = 'N/A';

        return stock;
    }

    function createStockNode(stock, type, template) {
        var child = document.createElement(type);
        child.setAttribute('data-symbol', stock.InternalCode);
        child.setAttribute('class', stock.InternalCode);
        child.innerHTML = template.supplant(stock);
        return child;
    }
});

