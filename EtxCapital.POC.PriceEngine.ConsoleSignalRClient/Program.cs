﻿using EtxCapital.POC.PriceEngine.Business.Models;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalRClient
{
    class Program
    {
        static StockStore store = new StockStore();

        private static async Task Main(string[] args)
        {
            var connection = new HubConnection("http://127.0.0.1:8088/");
            var proxy = connection.CreateHubProxy("StockTickerHub");
            

            proxy.On<Stock>("UpdateStock", stock => UpdateStock(stock));

            proxy.On<StockDetails>("UpdateStockDetails", stockDetails => UpdateStockDetails(stockDetails));

            await connection.Start();
            //var details = proxy.Invoke<List<StockDetails>>("RequestStockDetails");
            await proxy.Invoke("SubscribeL1");

            Console.ReadKey();
        }

        private static void UpdateStock(Stock stockToUpdate)
        {
            // update stock
            store.UpdateStock(stockToUpdate);

            // render screen
            Console.Clear();
            Console.WriteLine("{0, 20}{1, 10}{2, 10}{3, 10}{4, 10}{5, 15}{6, 20}{7, 10}{8, 20}{9, 10}{10, 20}{11, 10}{12, 10}{13, 10}{14, 20}", "Symbol", "ASK", "BID", "Last", "Last Trade", "Last Trade TS", "Current Business Day", "Day Close ASK", "Day Close ASK TS", "Day Close BID", "Day Close BID TS", "Day Open", "Day Open TS", "Prev Day Close", "Prev Day Close TS");

            var stocks = from sq in store.GetAllStocks()
                         from sd in store.GetAllStocksDetails()
                         where sq.InternalCode == sd.InternalCode
                         select (StockDetail: sd, Stock: sq);

            foreach (var stock in stocks)
            {
                Console.WriteLine($"{stock.StockDetail.InternalCode, 20}{stock.Stock.AskPrice, 10}{stock.Stock.BidPrice,10}{stock.Stock.LastPrice,10}{stock.Stock.LastTradePrice,10}{stock.Stock.LastTradeTimestamp,10}{stock.Stock.CurrentBusinessDay,10}{stock.Stock.DailyClosingAskPrice,10}{stock.Stock.DailyClosingAskTimestamp,10}{stock.Stock.DailyClosingBidPrice,10}{stock.Stock.DailyClosingBidTimestamp,10}{stock.Stock.DailyOpeningPrice,10}{stock.Stock.DailyOpeningPriceTimestamp,10}{stock.Stock.PreviousDailyClosingPrice,10}{stock.Stock.PreviousDailyClosingPriceTimestamp,10}");
            }
        }

        private static void UpdateStockDetails(StockDetails details)
        {
            store.AddStockDetails(details);
        }
    }
}
