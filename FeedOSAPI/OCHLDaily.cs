///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class OCHLDaily
        {
            #region Constructor
            public OCHLDaily(bool Open, bool Close, bool High, bool Low)
		    {
			    m_Open = Open;
			    m_Close = Close;
			    m_High = High;
			    m_Low = Low;
		    }
            #endregion

            #region Properties
		    public bool Open
		    {
			    get { return m_Open; }
		    }
		    public bool Close
		    {
			    get { return m_Close; }
		    }
		    public bool High
		    {
			    get { return m_High; }
		    }
		    public bool Low
		    {
			    get { return m_Low; }
		    }
            #endregion

            #region Members
		    protected bool m_Open;
            protected bool m_Close;
            protected bool m_High;
            protected bool m_Low;
            #endregion
        }
    }
}
