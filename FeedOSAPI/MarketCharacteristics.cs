///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketCharacteristics
        {
            #region Constructor
            public MarketCharacteristics(ushort FOSLowLevelMarketId, string MIC, string TimeZone, string Country, uint NbMaxInstruments)
            {
                m_FOSLowLevelMarketId = FOSLowLevelMarketId;
                m_MIC = MIC;
                m_TimeZone = TimeZone;
                m_Country = Country;
                m_NbMaxInstruments = NbMaxInstruments;
		    }
            #endregion

            #region Properties
            public ushort FOSMarketId
            {
                get { return m_FOSLowLevelMarketId; }
            }
            public string MIC
            {
                get { return m_MIC; }
            }
            public string TimeZone
            {
                get { return m_TimeZone; }
            }
            public string Country
            {
                get { return m_Country; }
            }
            public uint NbMaxInstruments
            {
                get { return m_NbMaxInstruments; }
            }
            #endregion

            #region Members
            protected ushort m_FOSLowLevelMarketId;
		    protected string m_MIC;
		    protected string m_TimeZone;
            protected string m_Country;
		    protected uint m_NbMaxInstruments;
            #endregion
        }
    }
}
