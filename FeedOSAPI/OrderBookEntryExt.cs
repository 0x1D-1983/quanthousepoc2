///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;

namespace FeedOSAPI
{
    namespace Types
    {
        public class OrderBookEntryExt
        {
           
            public const int ORDERBOOK_NB_ORDERS_UNKNOWN    = -1;
            // Constant to get the information that there is no more Bid/Ask provdied
            public const int ORDERBOOK_NB_ORDERS_EMPTY     = 0;

            // Special price values
            // decimal flavor
            public const decimal UNQUOTED =                      666666.666m;
            public const decimal ORDERBOOK_MAGIC_PRICE_AT_BEST = 999999.999m;
            public const decimal ORDERBOOK_MAGIC_PRICE_AT_OPEN = 999999.989m;
            public const decimal ORDERBOOK_MAGIC_PRICE_AT_CLOSE= 999999.988m;
            public const decimal ORDERBOOK_MAGIC_PRICE_PEG     = 999999.979m;
            // double flavor
            public const double UNQUOTEDd                      = 666666.666;
            public const double ORDERBOOK_MAGIC_PRICE_AT_BESTd = 999999.999;
            public const double ORDERBOOK_MAGIC_PRICE_AT_OPENd = 999999.989;
            public const double ORDERBOOK_MAGIC_PRICE_AT_CLOSEd= 999999.988;
            public const double ORDERBOOK_MAGIC_PRICE_PEGd     = 999999.979;	

            #region Constructor
            public OrderBookEntryExt(double Price, double Size, int NbOrders)
            {
                m_Price = Price;
                m_Size = Size;
                m_NbOrders = NbOrders;
            }
            #endregion

            #region Properties
            public double Price
            {
                get { return m_Price; } 
                set { m_Price = value; }
            }
            public double Size
            {
                get { return m_Size; }
                set { m_Size = value; }
            }
            public int NbOrders
            {
                get { return m_NbOrders; }
                set { m_NbOrders = value; }
            }
            #endregion

            public bool Equals(OrderBookEntryExt other)
            {
                return this.m_Price == other.Price && this.m_Size == other.Size && this.m_NbOrders == other.NbOrders;
            }

            #region Dump One Side limit
            // Manage special values in case of PreOpen or PreClose 
            public static bool isASpecialValuePrice(double price, ref string price_str)
            {
                if (OrderBookEntryExt.UNQUOTEDd == price)
                {
                    price_str = "UNQUOTED";
                    return true;
                }
                else if (OrderBookEntryExt.ORDERBOOK_MAGIC_PRICE_AT_BESTd == price)
                {
                    price_str = "AT_BEST";
                    return true;
                }
                else if (OrderBookEntryExt.ORDERBOOK_MAGIC_PRICE_AT_OPENd == price)
                {
                    price_str = "AT_OPEN";
                    return true;
                }
                else if (OrderBookEntryExt.ORDERBOOK_MAGIC_PRICE_AT_CLOSEd == price)
                {
                    price_str = "AT_CLOSE";
                    return true;
                }
                else if (OrderBookEntryExt.ORDERBOOK_MAGIC_PRICE_PEGd == price)
                {
                    price_str = "PEG";
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public string DumpOneSideLimit(string BidOrAsk, CultureInfo culture)
            {
                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append(BidOrAsk);
                strBuilder.Append("\tprice=");
                string special_price = null;
                if (!isASpecialValuePrice(m_Price, ref special_price)) 
                {
                    strBuilder.Append(m_Price.ToString("F3", culture));
                }
                else
                {
                    strBuilder.Append(special_price);
                }
 
                strBuilder.Append("\tqty=" + m_Size);
                switch (m_NbOrders) 
                {
                    case ORDERBOOK_NB_ORDERS_UNKNOWN:
                        strBuilder.Append("\tnb_orders=?");
                        break;
                    case ORDERBOOK_NB_ORDERS_EMPTY:
                        // number of orders at this price: NONE (the price/qty are the latest valid values)
                        strBuilder.Append("\t(NO ORDER)");
                        break;
                    default:
                        strBuilder.Append("\tnb_orders=" + m_NbOrders);
                        break;
                }
                strBuilder.Append('\n');
                return strBuilder.ToString();
            }
            #endregion Dump One Side limit

            #region Members
            protected double m_Price;
            protected double m_Size;
            protected int m_NbOrders;
            #endregion
        }
    }
}
