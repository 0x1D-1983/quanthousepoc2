///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketIds
        {
            #region Market Ids  Definitions
            /// ***GENERATOR-begin-MARKET_IDS-
		/* TIRANA STOCK EXCHANGE
		* <a href="http://www.tse.com.al">www.tse.com.al</a>
		*/
		public const ushort FOS_MARKET_ID_XTIR = 10;

		/* ALGIERS STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XALG = 11;

		/* EUREX DEUTSCHLAND
		* <a href="http://www.eurexchange.com">www.eurexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_XEUR = 12;

		/* MERCADO ABIERTO ELECTRONICO S.A.
		* <a href="http://www.mae.com.ar">www.mae.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XMAB = 13;

		/* MERCADO DE VALORES DE BUENOS AIRES S.A.
		* <a href="http://www.merval.sba.com.ar">www.merval.sba.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XMEV = 14;

		/* MERCADO A TERMINO DE BUENOS AIRES S.A.
		* <a href="http://www.matba.com.ar">www.matba.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XMTB = 15;

		/* BOLSA DE COMMERCIO DE MENDOZA S.A.
		* <a href="http://www.bolsamza.com.ar">www.bolsamza.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XBCM = 16;

		/* BOLSA DE COMERCIO ROSARIO
		* <a href="http://www.bcr.com.ar">www.bcr.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XROS = 17;

		/* ARMENIAN STOCK EXCHANGE
		* <a href="http://www.armex.am">www.armex.am</a>
		*/
		public const ushort FOS_MARKET_ID_XARM = 18;

		/* STOCK EXCHANGE OF NEWCASTLE LTD
		* <a href="http://www.newsx.com.au">www.newsx.com.au</a>
		*/
		public const ushort FOS_MARKET_ID_XNEC = 19;

		/* ASX OPERATIONS PTY LIMITED
		* <a href="http://www.asx.com.au">www.asx.com.au</a>
		*/
		public const ushort FOS_MARKET_ID_XASX = 20;

		/* SYDNEY FUTURES EXCHANGE LIMITED
		* <a href="http://www.sfe.com.au">www.sfe.com.au</a>
		*/
		public const ushort FOS_MARKET_ID_XSFE = 21;

		/* YIELDBROKER PTY LTD
		* <a href="http://www.yieldbroker.com">www.yieldbroker.com</a>
		*/
		public const ushort FOS_MARKET_ID_XYIE = 22;

		/* SUN TRADING INTERNATIONAL - SYSTEMATIC INTERNALISER
		* WWW.SUNTRADINGINTERNATIONAL.CO.UK
		*/
		public const ushort FOS_MARKET_ID_STSI = 23;

		/* VIRTU FINANCIAL IRELAND LIMITED - SYSTEMATIC INTERNALISER
		* WWW.VIRTU.COM
		*/
		public const ushort FOS_MARKET_ID_VFSI = 24;

		/* WIENER BOERSE AG
		* <a href="http://www.wienerboerse.at">www.wienerboerse.at</a>
		*/
		public const ushort FOS_MARKET_ID_XWBO = 25;

		/* BAKU INTERBANK CURRENCY EXCHANGE
		* <a href="http://www.bbvb.org">www.bbvb.org</a>
		*/
		public const ushort FOS_MARKET_ID_XIBE = 26;

		/* BAHAMAS INTERNATIONAL SECURITIES EXCHANGE
		* <a href="http://www.bisxbahamas.com">www.bisxbahamas.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBAA = 27;

		/* BAHRAIN STOCK EXCHANGE
		* <a href="http://www.bahrainstock.com">www.bahrainstock.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBAH = 28;

		/* CHITTAGONG STOCK EXCHANGE LTD.
		* <a href="http://www.csebd.com">www.csebd.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCHG = 29;

		/* DHAKA STOCK EXCHANGE LTD
		* <a href="http://www.dsebd.org">www.dsebd.org</a>
		*/
		public const ushort FOS_MARKET_ID_XDHA = 30;

		/* BARBADOS STOCK EXCHANGE
		* <a href="http://www.bse.com.bb">www.bse.com.bb</a>
		*/
		public const ushort FOS_MARKET_ID_XBAB = 31;

		/* EURONEXT BRUSSELS
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBRU = 32;

		/* BERMUDA STOCK EXCHANGE LTD; THE
		* <a href="http://www.bsx.com">www.bsx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBDA = 33;

		/* BOLSA BOLIVIANA DE VALORES S.A.
		* <a href="http://www.bolsa-valores-bolivia.com">www.bolsa-valores-bolivia.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBOL = 34;

		/* CBOE STOCK EXCHANGE
		* <a href="http://www.cbsx.com">www.cbsx.com</a>
		*/
		public const ushort FOS_MARKET_ID_CBSX = 35;

		/* ELECTRONIC BOND MARKET
		* <a href="http://www.borsaitaliana.it">www.borsaitaliana.it</a>
		*/
		public const ushort FOS_MARKET_ID_MOTX = 36;

		/* MIAX PEARL LLC
		* <a href="http://www.miaxoptions.com">www.miaxoptions.com</a>
		*/
		public const ushort FOS_MARKET_ID_MPRL = 37;

		/* NASDAQ STOCKHOLM AB - APA SERVICE
		* <a href="http://www.nasdaqomxnordic.com">www.nasdaqomxnordic.com</a>
		*/
		public const ushort FOS_MARKET_ID_NAPA = 38;

		/* EURONEXT CASH APA
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_ecpa = 39;

		/* EURONEXT DERIVATIVES APA
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_edpa = 40;

		/* BOLSA DE VALORES DE SAO PAULO
		* <a href="http://www.bovespa.com.br">www.bovespa.com.br</a>
		*/
		public const ushort FOS_MARKET_ID_XBSP = 41;

		/* BULGARIAN STOCK EXCHANGE
		* <a href="http://www.bse-sofia.bg">www.bse-sofia.bg</a>
		*/
		public const ushort FOS_MARKET_ID_XBUL = 42;

		/* MONTREAL EXCHANGE THE / BOURSE DE MONTREAL (OTIONS AND OTHER DERIVATIVES)
		* <a href="http://www.m-x.ca">www.m-x.ca</a>
		*/
		public const ushort FOS_MARKET_ID_XMOD = 43;

		/* ASX - PUREMATCH
		* <a href="http://www.asx.com.au/trading_services/asx-trade.htm">www.asx.com.au/trading_services/asx-trade.htm</a>
		*/
		public const ushort FOS_MARKET_ID_ASXP = 44;

		/* CANADA-S NEW STOCK EXCHANGE (CANADIAN TRADING AND QUOTING SYSTEM INC.)
		* <a href="http://www.cnq.ca">www.cnq.ca</a>
		*/
		public const ushort FOS_MARKET_ID_XCNQ = 45;

		/* SECURITISED DERIVATIVES MARKET
		* <a href="http://www.borsaitaliana.it">www.borsaitaliana.it</a>
		*/
		public const ushort FOS_MARKET_ID_SEDX = 46;

		/* ELECTRONIC OPEN-END FUNDS AND ETC MARKET
		* <a href="http://www.borsaitaliana.it">www.borsaitaliana.it</a>
		*/
		public const ushort FOS_MARKET_ID_ETFP = 47;

		/* TORONTO STOCK EXCHANGE
		* <a href="http://www.tse.com">www.tse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XTSE = 48;

		/* TSX VENTURE EXCHANGE
		* <a href="http://www.tsx.com">www.tsx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XTSX = 49;

		/* TSX VENTURE EXCHANGE - NEX
		* <a href="http://www.tsx.com/en/nex/index.html">www.tsx.com/en/nex/index.html</a>
		*/
		public const ushort FOS_MARKET_ID_XTNX = 50;

		/* WINNIPEG COMMODITY EXCHANGE; THE
		* <a href="http://www.wce.ca">www.wce.ca</a>
		*/
		public const ushort FOS_MARKET_ID_XWCE = 51;

		/* CAYMAN ISLANDS STOCK EXCHANGE
		* <a href="http://www.csx.com.ky">www.csx.com.ky</a>
		*/
		public const ushort FOS_MARKET_ID_XCAY = 52;

		/* LA BOLSA ELECTRONICA DE CHILE
		* <a href="http://www.bolchile.cl">www.bolchile.cl</a>
		*/
		public const ushort FOS_MARKET_ID_XBCL = 53;

		/* SANTIAGO STOCK EXCHANGE
		* <a href="http://www.bolsadesantiago.com">www.bolsadesantiago.com</a>
		*/
		public const ushort FOS_MARKET_ID_XSGO = 54;

		/* DALIAN COMMODITY EXCHANGE
		* <a href="http://www.dce.com.cn">www.dce.com.cn</a>
		*/
		public const ushort FOS_MARKET_ID_XDCE = 55;

		/* CHINA FOREIGN EXCHANGE TRADE SYSTEM 
		* <a href="http://www.chinamoney.com.cn">www.chinamoney.com.cn</a>
		*/
		public const ushort FOS_MARKET_ID_XCFE = 56;

		/* SHANGHAI FUTURES EXCHANGE
		* <a href="http://www.shfe.com.cn">www.shfe.com.cn</a>
		*/
		public const ushort FOS_MARKET_ID_XSGE = 57;

		/* SHANGHAI STOCK EXCHANGE
		* <a href="http://www.sse.com.cn">www.sse.com.cn</a>
		*/
		public const ushort FOS_MARKET_ID_XSHG = 58;

		/* SHENZHEN STOCK EXCHANGE
		* <a href="http://www.szse.cn">www.szse.cn</a>
		*/
		public const ushort FOS_MARKET_ID_XSHE = 59;

		/* SHENZHEN MERCANTILE EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XSME = 60;

		/* ZHENGZHOU COMMODITY EXCHANGE
		* <a href="http://www.czce.com.cn">www.czce.com.cn</a>
		*/
		public const ushort FOS_MARKET_ID_XZCE = 61;

		/* BOLSA DE VALORES DE COLOMBIA
		* <a href="http://www.bvc.com.co">www.bvc.com.co</a>
		*/
		public const ushort FOS_MARKET_ID_XBOG = 62;

		/* BOLSA NACIONAL DE VALORES; S.A.
		* <a href="http://www.bnv.co.cr">www.bnv.co.cr</a>
		*/
		public const ushort FOS_MARKET_ID_XBNV = 63;

		/* ZAGREB STOCK EXCHANGE; THE
		* <a href="http://www.zse.hr">www.zse.hr</a>
		*/
		public const ushort FOS_MARKET_ID_XZAG = 64;

		/* CYPRUS STOCK EXCHANGE
		* <a href="http://www.cse.com.cy">www.cse.com.cy</a>
		*/
		public const ushort FOS_MARKET_ID_XCYS = 65;

		/* STOCK EXCHANGE PRAGUE CO. LTD; THE
		* <a href="http://www.pse.cz">www.pse.cz</a>
		*/
		public const ushort FOS_MARKET_ID_XPRA = 66;

		/* COPENHAGEN STOCK EXCHANGE
		* <a href="http://www.cse.dk">www.cse.dk</a>
		*/
		public const ushort FOS_MARKET_ID_XCSE = 67;

		/* BOLSA DE VALORES DE LA REPUBLICA DOMINICANA SA.
		* <a href="http://www.bolsard.com">www.bolsard.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBVR = 68;

		/* GUAYAQUIL STOCK EXCHANGE
		* <a href="http://www.mundobvg.com">www.mundobvg.com</a>
		*/
		public const ushort FOS_MARKET_ID_XGUA = 69;

		/* QUITO STOCK EXCHANGE
		* <a href="http://www.ccbvq.com">www.ccbvq.com</a>
		*/
		public const ushort FOS_MARKET_ID_XQUI = 70;

		/* CAIRO AND ALEXANDRIA STOCK EXCHANGE
		* <a href="http://www.egyptse.com">www.egyptse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCAI = 71;

		/* EL SALVADOR STOCK EXCHANGE
		* <a href="http://www.bves.com.sv">www.bves.com.sv</a>
		*/
		public const ushort FOS_MARKET_ID_XSVA = 72;

		/* TALLINN STOCK EXCHANGE
		* <a href="http://www.hex.ee">www.hex.ee</a>
		*/
		public const ushort FOS_MARKET_ID_XTAL = 73;

		/* SOUTH PACIFIC STOCK EXCHANGE
		* <a href="http://www.spse.com.fj">www.spse.com.fj</a>
		*/
		public const ushort FOS_MARKET_ID_XSPS = 74;

		/* FINNISH OPTIONS MARKET
		* <a href="http://www.som.fi">www.som.fi</a>
		*/
		public const ushort FOS_MARKET_ID_XFOM = 75;

		/* THE HELSINKI STOCK EXCHANGE
		* <a href="http://www.hex.com">www.hex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XHEL = 76;

		/* SOCIETE DU NOUVEAU MARCHE
		* <a href="http://www.nouveau-marche.fr">www.nouveau-marche.fr</a>
		*/
		public const ushort FOS_MARKET_ID_XFMN = 77;

		/* EURONEXT PARIS - MATIF
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMAT = 78;

		/* EURONEXT PARIS S.A. - MARCHE LIBRE
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMLI = 79;

		/* EURONEXT PARIS - MONEP
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMON = 80;

		/* EURONEXT PARIS S.A.
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPAR = 81;

		/* POWERNEXT
		* <a href="http://www.powernext.fr">www.powernext.fr</a>
		*/
		public const ushort FOS_MARKET_ID_XPOW = 82;

		/* GEORGIA STOCK EXCHANGE
		* <a href="http://www.gse.ge">www.gse.ge</a>
		*/
		public const ushort FOS_MARKET_ID_XGSE = 83;

		/* BERLINER WERTPAPIERBOERSE
		* <a href="http://www.berlinerboerse.de">www.berlinerboerse.de</a>
		*/
		public const ushort FOS_MARKET_ID_XBER = 84;

		/* ZOBEX
		* <a href="http://www.zobex.de">www.zobex.de</a>
		*/
		public const ushort FOS_MARKET_ID_ZOBX = 85;

		/* BREMER WERTPAPIERBOERSE
		* <a href="http://www.boerse-bremen.de">www.boerse-bremen.de</a>
		*/
		public const ushort FOS_MARKET_ID_XBRE = 86;

		/* RHEINISCHE-WESTFAELISCHE BOERSE ZU DUESSELDORF
		* <a href="http://www.rwb.de">www.rwb.de</a>
		*/
		public const ushort FOS_MARKET_ID_XDUS = 87;

		/* EUROPEAN ENERGY EXCHANGE AG
		* <a href="http://www.eex.de">www.eex.de</a>
		*/
		public const ushort FOS_MARKET_ID_XEEE = 88;

		/* DEUTSCHER KASSENVEREIN AG GRUPPE DEUTSCHE BOERSE
		* <a href="http://www.deutsche-boerse.com">www.deutsche-boerse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XETR = 89;

		/* EUREX BONDS
		* <a href="http://www.eurex-bonds.com">www.eurex-bonds.com</a>
		*/
		public const ushort FOS_MARKET_ID_XEUB = 90;

		/* BUENOS AIRES STOCK EXCHANGE
		* <a href="http://www.bcba.sba.com.ar">www.bcba.sba.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XBUE = 91;

		/* DEUTSCHE BOERSE AG
		* <a href="http://www.deutsche-boerse.com">www.deutsche-boerse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XFRA = 92;

		/* RTR (REUTERS-REALTIME-DATEN)
		* 
		*/
		public const ushort FOS_MARKET_ID_XRTR = 93;

		/* HANSEATISCHE WERTPAPIERBOERSE HAMBURG
		* <a href="http://www.boerse-hamburg.de">www.boerse-hamburg.de</a>
		*/
		public const ushort FOS_MARKET_ID_XHAM = 94;

		/* NIEDERSAECHSISCHE BOERSE ZU HANNOVER
		* <a href="http://www.boerse-hannover.de">www.boerse-hannover.de</a>
		*/
		public const ushort FOS_MARKET_ID_XHAN = 95;

		/* WARENTERMINBOERSE HANNOVER
		* <a href="http://www.wtb-hannover.de">www.wtb-hannover.de</a>
		*/
		public const ushort FOS_MARKET_ID_XHCE = 96;

		/* BAYERISCHE BOERSE
		* <a href="http://www.bayerischeboerse.de">www.bayerischeboerse.de</a>
		*/
		public const ushort FOS_MARKET_ID_XMUN = 97;

		/* FINRA/NASDAQ TRF (TRADE REPORTING FACILITY)
		* <a href="http://www.finra.org">www.finra.org</a>
		*/
		public const ushort FOS_MARKET_ID_FINN = 98;

		/* BADEN-WUERTTEMBERGISCHE WERTPAPIERBOERSE ZU STUTTGART
		* <a href="http://www.boerse-stuttgart.de">www.boerse-stuttgart.de</a>
		*/
		public const ushort FOS_MARKET_ID_XSTU = 99;

		/* GHANA STOCK EXCHANGE
		* <a href="http://www.gse.com.gh">www.gse.com.gh</a>
		*/
		public const ushort FOS_MARKET_ID_XGHA = 100;

		/* BORSA ISTANBUL - EQUITY MARKET
		* <a href="http://www.borsaistanbul.com">www.borsaistanbul.com</a>
		*/
		public const ushort FOS_MARKET_ID_XEQY = 101;

		/* ATHENS DERIVATIVES EXCHANGE S.A.; THE
		* <a href="http://www.adex.ase.gr/AdexHomeEN">www.adex.ase.gr/AdexHomeEN</a>
		*/
		public const ushort FOS_MARKET_ID_XADE = 102;

		/* ATHENS STOCK EXCHANGE
		* <a href="http://www.ase.gr">www.ase.gr</a>
		*/
		public const ushort FOS_MARKET_ID_XATH = 103;

		/* BOLSA DE VALORES NACIONAL SA
		* <a href="http://www.bvnsa.com.gt">www.bvnsa.com.gt</a>
		*/
		public const ushort FOS_MARKET_ID_XGTG = 104;

		/* THE CHANNEL ISLANDS STOCK EXCHANGE
		* <a href="http://www.cisx.com">www.cisx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCIE = 105;

		/* HONDURIAN STOCK EXCHANGE; THE
		* <a href="http://www.bhv.hn">www.bhv.hn</a>
		*/
		public const ushort FOS_MARKET_ID_XHON = 106;

		/* BOLSA CENTROAMERICANA DE VALORES S.A.
		* <a href="http://www.bcv.hn">www.bcv.hn</a>
		*/
		public const ushort FOS_MARKET_ID_XBCV = 107;

		/* HONG KONG GROWTH ENTERPRISES MARKET
		* <a href="http://www.hkgem.com">www.hkgem.com</a>
		*/
		public const ushort FOS_MARKET_ID_XGEM = 108;

		/* HONG KONG FUTURES EXCHANGE LTD.
		* <a href="http://www.hkfe.com">www.hkfe.com</a>
		*/
		public const ushort FOS_MARKET_ID_XHKF = 109;

		/* STOCK EXCHANGE OF HONG KONG LTD; THE
		* <a href="http://www.sehk.com.hk">www.sehk.com.hk</a>
		*/
		public const ushort FOS_MARKET_ID_XHKG = 110;

		/* NASDAQ OMX EUROPE
		* <a href="http://www.nasdaqomxeurope.com">www.nasdaqomxeurope.com</a>
		*/
		public const ushort FOS_MARKET_ID_NURO = 111;

		/* BUDAPEST STOCK EXCHANGE
		* <a href="http://www.fornax.hu">www.fornax.hu</a>
		*/
		public const ushort FOS_MARKET_ID_XBUD = 112;

		/* ICELAND STOCK EXCHANGE
		* <a href="http://www.icex.is">www.icex.is</a>
		*/
		public const ushort FOS_MARKET_ID_XICE = 113;

		/* BANGALORE STOCK EXCHANGE LTD
		* <a href="http://www.kanataka.com/stock/bgse.shtml">www.kanataka.com/stock/bgse.shtml</a>
		*/
		public const ushort FOS_MARKET_ID_XBAN = 114;

		/* CALCUTTA STOCK EXCHANGE
		* <a href="http://www.cse-india.com">www.cse-india.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCAL = 115;

		/* DELHI STOCK EXCHANGE
		* <a href="http://www.business.vsnl.com/dse">www.business.vsnl.com/dse</a>
		*/
		public const ushort FOS_MARKET_ID_XDES = 116;

		/* MADRAS STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XMDS = 117;

		/* MUMBAI STOCK EXCHANGE
		* <a href="http://www.bseindia.com/index_op.htm">www.bseindia.com/index_op.htm</a>
		*/
		public const ushort FOS_MARKET_ID_XBOM = 118;

		/* NATIONAL STOCK EXCHANGE OF INDIA
		* <a href="http://www.nseindia.com">www.nseindia.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNSE = 119;

		/* JAKARTA FUTURES EXCHANGE (BURSA BERJANGKA JAKARTA)
		* <a href="http://www.bbj-jfx.com">www.bbj-jfx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBBJ = 120;

		/* JAKARTA STOCK EXCHANGE
		* <a href="http://www.jsx.co.id">www.jsx.co.id</a>
		*/
		public const ushort FOS_MARKET_ID_XIDX = 121;

		/* JAKARTA NEGOTIATED BOARD
		* 
		*/
		public const ushort FOS_MARKET_ID_XJNB = 122;

		/* SURABAYA STOCK EXCHANGE
		* <a href="http://www.bes.co.id">www.bes.co.id</a>
		*/
		public const ushort FOS_MARKET_ID_XSUR = 123;

		/* TEHERAN STOCK EXCHANGE
		* <a href="http://www.tse.or.ir">www.tse.or.ir</a>
		*/
		public const ushort FOS_MARKET_ID_XTEH = 124;

		/* IRISH STOCK EXCHANGE
		* <a href="http://www.ise.ie">www.ise.ie</a>
		*/
		public const ushort FOS_MARKET_ID_XDUB = 125;

		/* FINEX (NEW YORK AND DUBLIN)
		* <a href="http://www.finex.ie">www.finex.ie</a>
		*/
		public const ushort FOS_MARKET_ID_XFNX = 126;

		/* TEL AVIV STOCK EXCHANGE
		* <a href="http://www.tase.co.il">www.tase.co.il</a>
		*/
		public const ushort FOS_MARKET_ID_XTAE = 127;

		/* TRADINGLAB TLX
		* <a href="http://www.tradinglab.it">www.tradinglab.it</a>
		*/
		public const ushort FOS_MARKET_ID_TLAB = 128;

		/* ITALIAN DERIVATIVES MARKET
		* <a href="http://www.borsaitalia.it">www.borsaitalia.it</a>
		*/
		public const ushort FOS_MARKET_ID_XDMI = 129;

		/* MERCATO ITALIANO FUTURES EXCHANGE
		* <a href="http://www.tesoro.it">www.tesoro.it</a>
		*/
		public const ushort FOS_MARKET_ID_XMIF = 130;

		/* BORSA ITALIANA S.P.A.
		* <a href="http://www.borsaitalia.it">www.borsaitalia.it</a>
		*/
		public const ushort FOS_MARKET_ID_XMIL = 131;

		/* BOURSE REGIONALE DES VALEURS MOBILIERES
		* <a href="http://www.brvm.org/">www.brvm.org/</a>
		*/
		public const ushort FOS_MARKET_ID_XBRV = 132;

		/* JAMAICA STOCK EXCHANGE; THE
		* <a href="http://www.jamstockex.com/">www.jamstockex.com/</a>
		*/
		public const ushort FOS_MARKET_ID_XJAM = 133;

		/* FUKUOKA FUTURES EXCHANGE
		* <a href="http://www.ffe.or.jp/">www.ffe.or.jp/</a>
		*/
		public const ushort FOS_MARKET_ID_XFFE = 134;

		/* FUKUOKA STOCK EXCHANGE
		* <a href="http://www.fse.or.jp">www.fse.or.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XFKA = 135;

		/* NAGOYA STOCK EXCHANGE
		* <a href="http://www.nse.or.jp/e/index.html">www.nse.or.jp/e/index.html</a>
		*/
		public const ushort FOS_MARKET_ID_XNGO = 136;

		/* CENTRAL JAPAN COMMODITIES EXCHANGE
		* <a href="http://www.c-com.or.jp">www.c-com.or.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XNKS = 137;

		/* NIPPON NEW MARKET - HERCULES
		* <a href="http://www.hercules.ose.or.jp/e">www.hercules.ose.or.jp/e</a>
		*/
		public const ushort FOS_MARKET_ID_XHER = 138;

		/* KANSAI COMMODITIES EXCHANGE
		* <a href="http://www.kanex.or.jp/14english/index-eng.htm">www.kanex.or.jp/14english/index-eng.htm</a>
		*/
		public const ushort FOS_MARKET_ID_XKAC = 139;

		/* OSAKA SECURITIES EXCHANGE
		* <a href="http://www.ose.or.jp">www.ose.or.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XOSE = 140;

		/* OSAKA MERCANTILE EXCHANGE
		* <a href="http://www.soamex.com">www.soamex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XOSM = 141;

		/* SAPPORO SECURITIES EXCHANGE
		* <a href="http://www.tokeidai.co.jp/sse">www.tokeidai.co.jp/sse</a>
		*/
		public const ushort FOS_MARKET_ID_XSAP = 142;

		/* BORSA ITALIANA TRADING AFTER HOURS
		* <a href="http://www.borsaitaliana.it">www.borsaitaliana.it</a>
		*/
		public const ushort FOS_MARKET_ID_MTAH = 143;

		/* JASDAQ SECURITIES EXCHANGE
		* <a href="http://www.jasdaq.co.jp">www.jasdaq.co.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XJAS = 144;

		/* TOKYO INTERNATIONAL FINANCIAL FUTURES EXCHANGE; THE 
		* <a href="http://www.tiffe.or.jp">www.tiffe.or.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XTFF = 145;

		/* TOKYO KOKUMOTSU SHOHIN TORIHIKIJO (GRAIN EXCHANGE)
		* <a href="http://www.tge.or.jp">www.tge.or.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XTKO = 146;

		/* TOKYO STOCK EXCHANGE
		* <a href="http://www.tse.or.jp">www.tse.or.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XTKS = 147;

		/* TOKYO KOGYOIN TORIHIKIJO (COMMODITY EXCHANGE)
		* <a href="http://www.tocom.or.jp/">www.tocom.or.jp/</a>
		*/
		public const ushort FOS_MARKET_ID_XTKT = 148;

		/* DIRECT EDGE A
		* <a href="http://www.directedge.com">www.directedge.com</a>
		*/
		public const ushort FOS_MARKET_ID_EDGA = 149;

		/* AMMAN STOCK EXCHANGE
		* <a href="http://www.ammanstockex.com">www.ammanstockex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XAMM = 150;

		/* KAZAKHSTAN STOCK EXCHANGE
		* <a href="http://www.kaze.kz">www.kaze.kz</a>
		*/
		public const ushort FOS_MARKET_ID_XKAZ = 151;

		/* NAIROBI STOCK EXCHANGE
		* <a href="http://www.arcc.or.ke/nse/index.htm">www.arcc.or.ke/nse/index.htm</a>
		*/
		public const ushort FOS_MARKET_ID_XNAI = 152;

		/* KOREA FUTURES EXCHANGE
		* <a href="http://www.kofex.com">www.kofex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XKFE = 153;

		/* TRADEGATE EXCHANGE
		* <a href="http://www.tradegate.de">www.tradegate.de</a>
		*/
		public const ushort FOS_MARKET_ID_TGAT = 154;

		/* KOREA EXCHANGE -- STOCK MARKET
		* sm.krx.co.kr/webeng/
		*/
		public const ushort FOS_MARKET_ID_XKRX = 155;

		/* XKOR
		* an alias for XKRX
		*/
		public const ushort   FOS_MARKET_ID_XKOR = FOS_MARKET_ID_XKRX;

		/* KOSDAQ
		* <a href="http://www.kosdaq.or.kr">www.kosdaq.or.kr</a>
		*/
		public const ushort FOS_MARKET_ID_XKOS = 156;

		/* DIRECT EDGE X
		* <a href="http://www.directedge.com">www.directedge.com</a>
		*/
		public const ushort FOS_MARKET_ID_EDGX = 157;

		/* BATS Y-Exchange
		* <a href="http://www.batstrading.com">www.batstrading.com</a>
		*/
		public const ushort FOS_MARKET_ID_BATY = 158;

		/* RIGA STOCK EXCHANGE;THE
		* <a href="http://www.rfb.lv">www.rfb.lv</a>
		*/
		public const ushort FOS_MARKET_ID_XRIS = 159;

		/* BOURSE DE BEYROUTH
		* <a href="http://www.bse.com.lb">www.bse.com.lb</a>
		*/
		public const ushort FOS_MARKET_ID_XBEY = 160;

		/* NATIONAL STOCK EXCHANGE OF LITHUANIA
		* <a href="http://www.nse.lt">www.nse.lt</a>
		*/
		public const ushort FOS_MARKET_ID_XLIT = 161;

		/* CENTRALE DE COMMUNICATIONS LUXEMBOURG S.A.
		* <a href="http://www.cclux.lu">www.cclux.lu</a>
		*/
		public const ushort FOS_MARKET_ID_CCLX = 162;

		/* LUXEMBOURG STOCK EXCHANGE
		* <a href="http://www.bourse.lu">www.bourse.lu</a>
		*/
		public const ushort FOS_MARKET_ID_XLUX = 163;

		/* VESTIMA+
		* <a href="http://www.vestima.com">www.vestima.com</a>
		*/
		public const ushort FOS_MARKET_ID_XVES = 164;

		/* MACEDONIAN STOCK EXCHANGE
		* <a href="http://www.mse.org.mk">www.mse.org.mk</a>
		*/
		public const ushort FOS_MARKET_ID_XMAE = 165;

		/* MARCHE INTERBANCAIRE DES DEVISES M.I.D.
		* 
		*/
		public const ushort FOS_MARKET_ID_XMDG = 166;

		/* MALAWI STOCK EXCHANGE
		* <a href="http://www.mse.co.mw">www.mse.co.mw</a>
		*/
		public const ushort FOS_MARKET_ID_XMSW = 167;

		/* BURSA MALAYSIA
		* <a href="http://www.klse.com.my">www.klse.com.my</a>
		*/
		public const ushort FOS_MARKET_ID_XKLS = 168;

		/* MALAYSIA DERIVATIVES EXCHANGE BHD
		* <a href="http://www.mdex.com.my">www.mdex.com.my</a>
		*/
		public const ushort FOS_MARKET_ID_XLOF = 169;

		/* RINGGIT BOND MARKET
		* <a href="http://www.rmbond.bnm.gov.my">www.rmbond.bnm.gov.my</a>
		*/
		public const ushort FOS_MARKET_ID_XRBM = 170;

		/* LABUAN INTERNATIONAL FINANCIAL EXCHANGE
		* <a href="http://www.lfx.com.my">www.lfx.com.my</a>
		*/
		public const ushort FOS_MARKET_ID_XLFX = 171;

		/* MALTA STOCK EXCHANGE
		* <a href="http://www.borzamalta.com.mt">www.borzamalta.com.mt</a>
		*/
		public const ushort FOS_MARKET_ID_XMAL = 172;

		/* STOCK EXCHANGE OF MAURITIUS LTD; THE
		* <a href="http://www.semdex.com">www.semdex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMAU = 173;

		/* MERCADO MEXICANO DE DERIVADOS
		* <a href="http://www.mexder.com.mx">www.mexder.com.mx</a>
		*/
		public const ushort FOS_MARKET_ID_XEMD = 174;

		/* BOLSA MEXICANA DE VALORES (MEXICAN STOCK EXCHANGE)
		* <a href="http://www.bmv.com.mx">www.bmv.com.mx</a>
		*/
		public const ushort FOS_MARKET_ID_XMEX = 175;

		/* MOLDOVA STOCK EXCHANGE
		* <a href="http://www.moldse.md">www.moldse.md</a>
		*/
		public const ushort FOS_MARKET_ID_XMOL = 176;

		/* MONGOLIAN STOCK EXCHANGE
		* <a href="http://www.mse.mn">www.mse.mn</a>
		*/
		public const ushort FOS_MARKET_ID_XULA = 177;

		/* CASABLANCA STOCK EXCHANGE
		* <a href="http://www.casablanca-bourse.com">www.casablanca-bourse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCAS = 178;

		/* MAPUTO STOCK  EXCHANGE
		* <a href="http://www.mbendi.co.za/exmz.htm">www.mbendi.co.za/exmz.htm</a>
		*/
		public const ushort FOS_MARKET_ID_XMAP = 179;

		/* NAMIBIAN STOCK EXCHANGE
		* <a href="http://www.nsx.com.na">www.nsx.com.na</a>
		*/
		public const ushort FOS_MARKET_ID_XNAM = 180;

		/* NEPAL STOCK EXCHANGE
		* <a href="http://www.nepalstock.com">www.nepalstock.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNEP = 181;

		/* NEW ZEALAND FUTURES AND OPTIONS EXCHANGE
		* <a href="http://www.nzfoe.co.nz">www.nzfoe.co.nz</a>
		*/
		public const ushort FOS_MARKET_ID_XNEE = 182;

		/* NEW ZEALAND STOCK EXCHANGE
		* <a href="http://www.nzx.com">www.nzx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNZE = 183;

		/* BOLSA DE VALORES DE NICARAGUA
		* <a href="http://www.bolsanic.com">www.bolsanic.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMAN = 184;

		/* NIGERIAN STOCK EXCHANGE;THE
		* <a href="http://www.nse.com.ng">www.nse.com.ng</a>
		*/
		public const ushort FOS_MARKET_ID_XNSA = 185;

		/* THE NORWEGIAN OVER THE COUNTER MARKET
		* <a href="http://www.nfmf.no/NOTC/Default_en.htm">www.nfmf.no/NOTC/Default_en.htm</a>
		*/
		public const ushort FOS_MARKET_ID_NOTC = 186;

		/* OSLO BORS
		* <a href="http://www.ose.no">www.ose.no</a>
		*/
		public const ushort FOS_MARKET_ID_XOSL = 187;

		/* MUSCAT SECURITIES MARKET
		* <a href="http://www.msm.gov.om">www.msm.gov.om</a>
		*/
		public const ushort FOS_MARKET_ID_XMUS = 188;

		/* ISLAMABAD STOCK EXCHANGE
		* <a href="http://www.ise.com.pk">www.ise.com.pk</a>
		*/
		public const ushort FOS_MARKET_ID_XISL = 189;

		/* KARACHI STOCK EXCHANGE (GUARANTEE) LIMITED; THE
		* <a href="http://www.kse.com.pk">www.kse.com.pk</a>
		*/
		public const ushort FOS_MARKET_ID_XKAR = 190;

		/* LAHORE STOCK EXCHANGE
		* <a href="http://www.lse.brain.net.pk">www.lse.brain.net.pk</a>
		*/
		public const ushort FOS_MARKET_ID_XLAH = 191;

		/* PALESTINE SECURITIES EXCHANGE
		* <a href="http://www.p-s-e.com">www.p-s-e.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPAE = 192;

		/* BOLSA DE VALORES DE PANAMA; S.A.
		* <a href="http://www.panabolsa.com">www.panabolsa.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPTY = 193;

		/* PORT MORESBY STOCK EXCHANGE
		* <a href="http://www.pomsox.com.pg">www.pomsox.com.pg</a>
		*/
		public const ushort FOS_MARKET_ID_XPOM = 194;

		/* BOLSA DE VALORES Y PRODUCTOS DE ASUNCION SA
		* <a href="http://www.bvpasa.com.py">www.bvpasa.com.py</a>
		*/
		public const ushort FOS_MARKET_ID_XVPA = 195;

		/* BOLSA DE VALORES DE LIMA
		* <a href="http://www.bvl.com.pe">www.bvl.com.pe</a>
		*/
		public const ushort FOS_MARKET_ID_XLIM = 196;

		/* PHILIPPINE STOCK EXCHANGE; INC.
		* <a href="http://www.pse.org.ph">www.pse.org.ph</a>
		*/
		public const ushort FOS_MARKET_ID_XPHS = 197;

		/* WARSAW STOCK EXCHANGE
		* <a href="http://www.wse.com.pl">www.wse.com.pl</a>
		*/
		public const ushort FOS_MARKET_ID_XWAR = 198;

		/* BATS CHI-X EUROPE - OFF EXCHANGE REPORTS
		* <a href="http://www.batstrading.co.uk">www.batstrading.co.uk</a>
		*/
		public const ushort FOS_MARKET_ID_BOTC = 199;

		/* MEDIP
		* 
		*/
		public const ushort FOS_MARKET_ID_MDIP = 200;

		/* PEX-PRIVATE EXCHANGE
		* <a href="http://www.opex.pt/en/pex">www.opex.pt/en/pex</a>
		*/
		public const ushort FOS_MARKET_ID_OPEX = 201;

		/* EURONEXT LISBOA
		* <a href="http://www.euronext.pt">www.euronext.pt</a>
		*/
		public const ushort FOS_MARKET_ID_XLIS = 202;

		/* DOHA SECURITIES MARKET
		* <a href="http://www.dsm.com.qa">www.dsm.com.qa</a>
		*/
		public const ushort FOS_MARKET_ID_DSMD = 203;

		/* ROMANIAN  COMMODITIES EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XBRM = 204;

		/* BUCHAREST STOCK EXCHANGE
		* <a href="http://www.bvb.ro">www.bvb.ro</a>
		*/
		public const ushort FOS_MARKET_ID_XBSE = 205;

		/* RASDAQ
		* <a href="http://www.rasd.ro">www.rasd.ro</a>
		*/
		public const ushort FOS_MARKET_ID_XRAS = 206;

		/* CAN-ATS
		* <a href="http://www.bvb.ro">www.bvb.ro</a>
		*/
		public const ushort FOS_MARKET_ID_XCAN = 207;

		/* URALS REGIONAL CURRENCY EXCHANGE 
		* <a href="http://www.urvb.ru">www.urvb.ru</a>
		*/
		public const ushort FOS_MARKET_ID_URCE = 208;

		/* RTS STOCK EXCHANGE
		* <a href="http://www.rtsnet.ru">www.rtsnet.ru</a>
		*/
		public const ushort FOS_MARKET_ID_RTSX = 209;

		/* MOSCOW INTERBANK CURRENCY EXCHANGE
		* <a href="http://www.micex.com">www.micex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMIC = 210;

		/* MOSCOW CENTRAL STOCK EXCHANGE
		* <a href="http://www.mcse.ru/FORA/birggame.htm">www.mcse.ru/FORA/birggame.htm</a>
		*/
		public const ushort FOS_MARKET_ID_XMOS = 211;

		/* RUSSIAN EXCHANGE; THE
		* <a href="http://www.indx.ru">www.indx.ru</a>
		*/
		public const ushort FOS_MARKET_ID_XRUS = 212;

		/* NIZHNY NOVGOROD CURRENCY AND STOCKEXCHANGE CLOSE JOINT STOCK COMPANY
		* <a href="http://www.nnx.ru/eng/deystv.htm">www.nnx.ru/eng/deystv.htm</a>
		*/
		public const ushort FOS_MARKET_ID_NNCS = 213;

		/* SIBERIAN STOCK EXCHANGE
		* <a href="http://www.sse.nsk.su">www.sse.nsk.su</a>
		*/
		public const ushort FOS_MARKET_ID_XSIB = 214;

		/* SIBERIAN INTERBANK CURRENCY EXCHANGE
		* <a href="http://www.sice.ru">www.sice.ru</a>
		*/
		public const ushort FOS_MARKET_ID_XSIC = 215;

		/* ROSTOV CURRENCY AND STOCK EXCHANGE
		* <a href="http://www.rndex.ru">www.rndex.ru</a>
		*/
		public const ushort FOS_MARKET_ID_XROV = 216;

		/* ST. PETERSBURG STOCK EXCHANGE
		* <a href="http://www.spbex.ru">www.spbex.ru</a>
		*/
		public const ushort FOS_MARKET_ID_XPET = 217;

		/* SAINT-PETERSBURG CURRENCY EXCHANGE
		* <a href="http://www.spcex.ru">www.spcex.ru</a>
		*/
		public const ushort FOS_MARKET_ID_XPIC = 218;

		/* SAMARA CURRENCY INTERBANK EXCHANGE
		* <a href="http://www.sciex.ru">www.sciex.ru</a>
		*/
		public const ushort FOS_MARKET_ID_XSAM = 219;

		/* ASIA-PACIFIC INTERBANK CURRENCY EXCHANGE; THE
		* 
		*/
		public const ushort FOS_MARKET_ID_XAPI = 220;

		/* VLADIVOSTOK (RUSSIA) STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XVLA = 221;

		/* EASTERN CARIBBEAN SECURITIES EXCHANGE
		* <a href="http://www.ecseonline.com">www.ecseonline.com</a>
		*/
		public const ushort FOS_MARKET_ID_XECS = 222;

		/* SAUDI ARABIA STOCK EXCHANGE
		* <a href="http://www.tadawul.com.sa">www.tadawul.com.sa</a>
		*/
		public const ushort FOS_MARKET_ID_XSAU = 223;

		/* BELGRADE STOCK EXCHANGE
		* <a href="http://www.belex.co.yu">www.belex.co.yu</a>
		*/
		public const ushort FOS_MARKET_ID_XBEL = 224;

		/* SINGAPORE COMMODITY EXCHANGE
		* <a href="http://www.sicom.com.sg">www.sicom.com.sg</a>
		*/
		public const ushort FOS_MARKET_ID_XSCE = 225;

		/* SINGAPORE EXCHANGE
		* <a href="http://www.sgx.com">www.sgx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XSES = 226;

		/* SINGAPORE EXCHANGE DERIVATIVES CLEARING LIMITED
		* <a href="http://www.sgx.com">www.sgx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XSIM = 227;

		/* BRATISLAVA STOCK EXCHANGE; THE
		* <a href="http://www.bsse.sk">www.bsse.sk</a>
		*/
		public const ushort FOS_MARKET_ID_XBRA = 228;

		/* SK RM-S (SLOVAK STOCK EXCHANGE)
		* <a href="http://www.rms.sk">www.rms.sk</a>
		*/
		public const ushort FOS_MARKET_ID_XRMS = 229;

		/* LJUBLJANA STOCK EXCHANGE; INC.
		* <a href="http://www.ljse.si">www.ljse.si</a>
		*/
		public const ushort FOS_MARKET_ID_XLJU = 230;

		/* BOND EXCHANGE OF SOUTH AFRICA
		* <a href="http://www.besa.za.com">www.besa.za.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBES = 231;

		/* JSE SECURITIES EXCHANGE
		* <a href="http://www.jse.co.za">www.jse.co.za</a>
		*/
		public const ushort FOS_MARKET_ID_XJSE = 232;

		/* SOUTH AFRICAN FUTURES EXCHANGE
		* <a href="http://www.safex.co.za">www.safex.co.za</a>
		*/
		public const ushort FOS_MARKET_ID_XSAF = 233;

		/* SOUTH AFRICAN FUTURES EXCHANGE - AGRICULTURAL MARKET DIVISION
		* <a href="http://www.safex.co.za">www.safex.co.za</a>
		*/
		public const ushort FOS_MARKET_ID_XSFA = 234;

		/* YIELD-X
		* <a href="http://www.yieldx.co.za">www.yieldx.co.za</a>
		*/
		public const ushort FOS_MARKET_ID_YLDX = 235;

		/* BARCELONA STOCK EXCHANGE
		* <a href="http://www.borsabcn.es">www.borsabcn.es</a>
		*/
		public const ushort FOS_MARKET_ID_XBAR = 236;

		/* MERCHBOLSA AGENCIA DE VALORES; S.A.
		* 
		*/
		public const ushort FOS_MARKET_ID_XBAV = 237;

		/* MERCATO CONTINUO ESPANOL
		* <a href="http://www.sbolsas.es">www.sbolsas.es</a>
		*/
		public const ushort FOS_MARKET_ID_XMCE = 238;

		/* MEFF RENTA FIJA
		* <a href="http://www.meff.es">www.meff.es</a>
		*/
		public const ushort FOS_MARKET_ID_XMEF = 239;

		/* MEFF RENTA VARIABLE
		* <a href="http://www.meffrv.es">www.meffrv.es</a>
		*/
		public const ushort FOS_MARKET_ID_XMRV = 240;

		/* BOLSA DE VALORES DE BILBAO
		* <a href="http://www.bolsabilbao.es">www.bolsabilbao.es</a>
		*/
		public const ushort FOS_MARKET_ID_XBIL = 241;

		/* MERCADO DE FUTUROS DE ACEITE DE OLIVA; S.A.
		* <a href="http://www.mfao.es">www.mfao.es</a>
		*/
		public const ushort FOS_MARKET_ID_XSRM = 242;

		/* CADE - MERCADO DE DEUDA PUBLICA ANOTADA
		* 
		*/
		public const ushort FOS_MARKET_ID_XDPA = 243;

		/* AIAF - MERCADO DE RENTA FIJA
		* <a href="http://www.aiaf.es">www.aiaf.es</a>
		*/
		public const ushort FOS_MARKET_ID_XDRF = 244;

		/* LATIBEX
		* <a href="http://www.latibex.com">www.latibex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XLAT = 245;

		/* BOLSA DE MADRID
		* <a href="http://www.bolsamadrid.es">www.bolsamadrid.es</a>
		*/
		public const ushort FOS_MARKET_ID_XMAD = 246;

		/* MERCADO DE FUTUROS Y OPCIONES SOBRE CITRICOS
		* <a href="http://www.fcym.com">www.fcym.com</a>
		*/
		public const ushort FOS_MARKET_ID_XFCM = 247;

		/* BOLSA DE VALENCIA
		* <a href="http://www.bolsavalencia.es">www.bolsavalencia.es</a>
		*/
		public const ushort FOS_MARKET_ID_XVAL = 248;

		/* COLOMBO STOCK EXCHANGE
		* <a href="http://www.cse.lk">www.cse.lk</a>
		*/
		public const ushort FOS_MARKET_ID_XCOL = 249;

		/* KHARTOUM STOCL EXCHANGE
		* <a href="http://www.ksesudan.com">www.ksesudan.com</a>
		*/
		public const ushort FOS_MARKET_ID_XKHA = 250;

		/* SWAZILAND STOCK EXCHANGE
		* <a href="http://www.ssx.org.sz">www.ssx.org.sz</a>
		*/
		public const ushort FOS_MARKET_ID_XSWA = 251;

		/* NORDIC GROWTH MARKET
		* <a href="http://www.ngm.se">www.ngm.se</a>
		*/
		public const ushort FOS_MARKET_ID_XNGM = 252;

		/* OM STOCKHOLM EXCHANGE
		* <a href="http://www.omhex.com">www.omhex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XOME = 253;

		/* OM FIXED INTEREST EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XOMF = 254;

		/* BERNE STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XBRN = 255;

		/* SWISS EXCHANGE
		* <a href="http://www.swx.com">www.swx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XSWX = 256;

		/* GRETAI SECURITIES MARKET
		* <a href="http://www.gretai.org.tw/e_index.htm">www.gretai.org.tw/e_index.htm</a>
		*/
		public const ushort FOS_MARKET_ID_ROCO = 257;

		/* TAIWAN INTERNATIONAL MERCANTILE EXCHANGE
		* <a href="http://www.taimex.com.tw">www.taimex.com.tw</a>
		*/
		public const ushort FOS_MARKET_ID_XIME = 258;

		/* XETA - SCOACH_EU TECHNICAL FOSMARKET 1
		* <a href="http://www.scoach.de">www.scoach.de</a>
		*/
		public const ushort FOS_MARKET_ID_xet1 = 259;

		/* TAIWAN FUTURES EXCHANGE
		* <a href="http://www.taifex.com.tw">www.taifex.com.tw</a>
		*/
		public const ushort FOS_MARKET_ID_XTAF = 260;

		/* TAIWAN STOCK EXCHANGE
		* <a href="http://www.tse.com.tw">www.tse.com.tw</a>
		*/
		public const ushort FOS_MARKET_ID_XTAI = 261;

		/* DAR ES  SALAAM STOCK EXCHANGE
		* <a href="http://www.darstockexchange.com">www.darstockexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_XDAR = 262;

		/* STOCK EXCHANGE OF THAILAND - FOREIGN BOARD
		* 
		*/
		public const ushort FOS_MARKET_ID_XBKF = 263;

		/* STOCK EXCHANGE OF THAILAND
		* <a href="http://www.set.or.th">www.set.or.th</a>
		*/
		public const ushort FOS_MARKET_ID_XBKK = 264;

		/* EURONEXT AMSTERDAM
		* <a href="http://www.aex.nl">www.aex.nl</a>
		*/
		public const ushort FOS_MARKET_ID_XAMS = 265;

		/* EURONEXT COM; COMMODITIES FUTURES AND OPTIONS
		* 
		*/
		public const ushort FOS_MARKET_ID_XEUC = 266;

		/* EURONEXT EQF; EQUITIES AND INDICES DERIVATIVES
		* 
		*/
		public const ushort FOS_MARKET_ID_XEUE = 267;

		/* EURONEXT IRF; INTEREST RATE FUTURE AND OPTIONS
		* 
		*/
		public const ushort FOS_MARKET_ID_XEUI = 268;

		/* TRINIDAD AND TOBAGO STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XTRN = 269;

		/* BOURSE DES VALEURS MOBILIERES
		* 
		*/
		public const ushort FOS_MARKET_ID_XTUN = 270;

		/* ISTANBUL STOCK EXCHANGE
		* <a href="http://www.ise.org">www.ise.org</a>
		*/
		public const ushort FOS_MARKET_ID_XIST = 271;

		/* UGANDA SECURITIES EXCHANGE
		* <a href="http://www.use.or.ug/home.asp">www.use.or.ug/home.asp</a>
		*/
		public const ushort FOS_MARKET_ID_XUGA = 272;

		/* KHARKOV COMMODITY EXCHANGE
		* <a href="http://www.xtb.com.ua">www.xtb.com.ua</a>
		*/
		public const ushort FOS_MARKET_ID_XKHR = 273;

		/* FIRST SECURITIES TRADING SYSTEM - UKRAINIAN OTC
		* <a href="http://www.pfts.com/ukr">www.pfts.com/ukr</a>
		*/
		public const ushort FOS_MARKET_ID_PFTS = 274;

		/* KIEV UNIVERSAL EXCHANGE
		* <a href="http://www.kue.kiev.ua">www.kue.kiev.ua</a>
		*/
		public const ushort FOS_MARKET_ID_XKIE = 275;

		/* UKRAINIAN UNIVERSAL COMMODITY EXCHANGE
		* <a href="http://www.uutb.com.ua">www.uutb.com.ua</a>
		*/
		public const ushort FOS_MARKET_ID_XUKR = 276;

		/* ODESSA COMMODITY EXCHANGE
		* <a href="http://www.otb.odessa.ua">www.otb.odessa.ua</a>
		*/
		public const ushort FOS_MARKET_ID_XODE = 277;

		/* PRIDNEPROVSK COMMODITY EXCHANGE
		* <a href="http://www.pce.dp.ua">www.pce.dp.ua</a>
		*/
		public const ushort FOS_MARKET_ID_XPRI = 278;

		/* ABU DHABI SECURITIES MARKET
		* <a href="http://www.adsm.co.ae">www.adsm.co.ae</a>
		*/
		public const ushort FOS_MARKET_ID_XADS = 279;

		/* DUBAI FINANCIAL MARKET
		* <a href="http://www.dfm.co.ae">www.dfm.co.ae</a>
		*/
		public const ushort FOS_MARKET_ID_XDFM = 280;

		/* NYSE AMEX OPTIONS
		* <a href="http://www.nyse.com">www.nyse.com</a>
		*/
		public const ushort FOS_MARKET_ID_AMXO = 281;

		/* NYSE Consolidated BBO
		* <a href="http://www.nyxdata.com">www.nyxdata.com</a>
		*/
		public const ushort FOS_MARKET_ID_Xbqt = 282;

		/* EUROMTS
		* <a href="http://www.euromts-ltd.com">www.euromts-ltd.com</a>
		*/
		public const ushort FOS_MARKET_ID_EMTS = 283;

		/* GEMMA (Gilt Edged Market Makers Association)
		* 
		*/
		public const ushort FOS_MARKET_ID_GEMX = 284;

		/* ELECTRONIC SHARE MARKET
		* <a href="http://www.borsaitaliana.it">www.borsaitaliana.it</a>
		*/
		public const ushort FOS_MARKET_ID_MTAA = 285;

		/* ASX BOOKBUILD
		* <a href="http://www.asx.com.au">www.asx.com.au</a>
		*/
		public const ushort FOS_MARKET_ID_ASXB = 286;

		/* TRADEWEB EUROPE LIMITED 
		* <a href="http://www.tradeweb.com">www.tradeweb.com</a>
		*/
		public const ushort FOS_MARKET_ID_TREU = 287;

		/* ISMA TRADING SERVICE
		* <a href="http://www.isma.co.uk">www.isma.co.uk</a>
		*/
		public const ushort FOS_MARKET_ID_XCOR = 288;

		/* EDX LONDON LIMITED
		* 
		*/
		public const ushort FOS_MARKET_ID_XEDX = 289;

		/* INTERCONTINENTAL EXCHANGE LTD. 
		* <a href="http://www.intcx.com">www.intcx.com</a>
		*/
		public const ushort FOS_MARKET_ID_IEPA = 290;

		/* XIPE
		* an alias for IEPA
		*/
		public const ushort   FOS_MARKET_ID_XIPE = FOS_MARKET_ID_IEPA;

		/* LONDON BULLION MARKET; THE
		* <a href="http://www.lbma.org.uk">www.lbma.org.uk</a>
		*/
		public const ushort FOS_MARKET_ID_XLBM = 291;

		/* THE LONDON COMMODITY EXCHANGE
		* <a href="http://www.lcomex.com">www.lcomex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XLCE = 292;

		/* LIFFE
		* <a href="http://www.liffe.com">www.liffe.com</a>
		*/
		public const ushort FOS_MARKET_ID_XLIF = 293;

		/* LONDON METAL EXCHANGE
		* <a href="http://www.lme.co.uk">www.lme.co.uk</a>
		*/
		public const ushort FOS_MARKET_ID_XLME = 294;

		/* LONDON STOCK EXCHANGE; THE
		* <a href="http://www.londonstockexchange.com">www.londonstockexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_XLON = 295;

		/* LSE INTERNATIONAL MARKETS
		* <a href="http://www.londonstockexchange.com">www.londonstockexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_Xloi = 296;

		/* SINGAPORE MERCANTILE EXCHANGE PTE LTD
		* <a href="http://www.smx.com.sg">www.smx.com.sg</a>
		*/
		public const ushort FOS_MARKET_ID_SMEX = 297;

		/* VIRT-X
		* <a href="http://www.virt-x.com">www.virt-x.com</a>
		*/
		public const ushort FOS_MARKET_ID_XVTX = 298;

		/* BOSTON STOCK EXCHANGE
		* <a href="http://www.bostonstock.com">www.bostonstock.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBOS = 299;

		/* BOSTON OPTIONS EXCHANGE
		* <a href="http://www.bostonoptions.com">www.bostonoptions.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBOX = 300;

		/* THE THIRD MARKET CORPORATION
		* <a href="http://www.ETHRD.COM">www.ETHRD.COM</a>
		*/
		public const ushort FOS_MARKET_ID_THRD = 301;

		/* CBOE FUTURES EXCHANGE
		* <a href="http://www.cboe.com/cfe/index.asp">www.cboe.com/cfe/index.asp</a>
		*/
		public const ushort FOS_MARKET_ID_XCBF = 303;

		/* CHICAGO BOARD OPTIONS EXCHANGE
		* <a href="http://www.cboe.com">www.cboe.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCBO = 304;

		/* CHICAGO BOARD OF TRADE
		* <a href="http://www.cbot.com">www.cbot.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCBT = 305;

		/* CHICAGO CLIMATE EXCHANGE; INC
		* <a href="http://www.chicagoclimateexchange.com">www.chicagoclimateexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCCX = 306;

		/* CHICAGO STOCK EXCHANGE; INC.
		* <a href="http://www.chx.com">www.chx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCHI = 307;

		/* NATIONAL STOCK EXCHANGE
		* <a href="http://www.cincinnatistock.com">www.cincinnatistock.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCIS = 308;

		/* CHICAGO MERCANTILE EXCHANGE
		* <a href="http://www.cme.com">www.cme.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCME = 309;

		/* CHICAGO RICE AND COTTON EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XCRC = 310;

		/* EUREX US
		* <a href="http://www.eurexus.com">www.eurexus.com</a>
		*/
		public const ushort FOS_MARKET_ID_XEUS = 311;

		/* INTERNATIONAL MONETARY MARKET
		* <a href="http://www.cme.com">www.cme.com</a>
		*/
		public const ushort FOS_MARKET_ID_XIMM = 312;

		/* INDEX AND OPTIONS MARKET
		* <a href="http://www.cme.com">www.cme.com</a>
		*/
		public const ushort FOS_MARKET_ID_XIOM = 313;

		/* MID AMERICA COMMODITY EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XMAC = 314;

		/* MERCHANTS- EXCHANGE
		* <a href="http://www.merchants-exchange.com">www.merchants-exchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMER = 315;

		/* ONECHICAGO; LLC.
		* <a href="http://www.onechicago.com">www.onechicago.com</a>
		*/
		public const ushort FOS_MARKET_ID_XOCH = 316;

		/* BROKERTEC USA; LLC
		* <a href="http://www.btec.com">www.btec.com</a>
		*/
		public const ushort FOS_MARKET_ID_BTEC = 317;

		/* TRADEWEB LLC
		* <a href="http://www.tradeweb.com">www.tradeweb.com</a>
		*/
		public const ushort FOS_MARKET_ID_TRWB = 318;

		/* NASDAQ UTP Data Feed National BBO
		* <a href="http://www.nasdaq.com">www.nasdaq.com</a>
		*/
		public const ushort FOS_MARKET_ID_Xudf = 319;

		/* KANSAS CITY BOARD OF TRADE
		* <a href="http://www.kcbt.com">www.kcbt.com</a>
		*/
		public const ushort FOS_MARKET_ID_XKBT = 320;

		/* MINNEAPOLIS GRAIN EXCHANGE
		* <a href="http://www.mgex.com">www.mgex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMGE = 321;

		/* BORSA ISTANBUL - FUTURES AND OPTIONS MARKET
		* <a href="http://www.borsaistanbul.com">www.borsaistanbul.com</a>
		*/
		public const ushort FOS_MARKET_ID_XFNO = 322;

		/* OTC MARKETS
		* <a href="http://www.otcmarkets.com">www.otcmarkets.com</a>
		*/
		public const ushort FOS_MARKET_ID_OTCM = 323;

		/* AMERICAN STOCK EXCHANGE
		* <a href="http://www.amex.com">www.amex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XASE = 324;

		/* THE BRUT ECN
		* <a href="http://www.nasdaqtrader.com/eBrut/home.shtm">www.nasdaqtrader.com/eBrut/home.shtm</a>
		*/
		public const ushort FOS_MARKET_ID_XBRT = 325;

		/* COMMODITIES EXCHANGE CENTER
		* <a href="http://www.nymex.com">www.nymex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCEC = 326;

		/* CANTOR FINANCIAL FURTURES EXCHANGE
		* <a href="http://www.cantor.com">www.cantor.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCFF = 327;

		/* NYSE EURONEXT - EASY NEXT
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_ENXB = 328;

		/* INTERNATIONAL SECURITIES EXCHANGE; LLC.
		* <a href="http://www.iseoptions.com">www.iseoptions.com</a>
		*/
		public const ushort FOS_MARKET_ID_XISX = 329;

		/* NASDAQ
		* <a href="http://www.nasdaq.com">www.nasdaq.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNAS = 330;

		/* NASDAQ/NMS (NATIONAL MARKET SYSTEM)
		* <a href="http://www.nasdaq.com">www.nasdaq.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNMS = 331;

		/* NQLX (NASDAQ LIFFE)
		* <a href="http://www.nqlx.com">www.nqlx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNQL = 332;

		/* MARKIT BOAT
		*  <a href="http://www.markit.com">www.markit.com</a>
		*/
		public const ushort FOS_MARKET_ID_BOAT = 333;

		/* NEW YORK BOARD OF TRADE
		* <a href="http://www.nybot.com">www.nybot.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNYF = 334;

		/* NEW YORK MERCANTILE EXCHANGE
		* <a href="http://www.nymex.com">www.nymex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNYM = 335;

		/* NEW YORK STOCK EXCHANGE; INC.
		* <a href="http://www.nyse.com">www.nyse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNYS = 336;

		/* PHILADELPHIA BOARD OF TRADE
		* <a href="http://www.phlx.com">www.phlx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPBT = 337;

		/* PHILADELPHIA STOCK EXCHANGE
		* <a href="http://www.phlx.com">www.phlx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPHL = 338;

		/* PHILADELPHIA OPTIONS EXCHANGE
		* <a href="http://www.phlx.com">www.phlx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPHO = 339;

		/* ARIZONA STOCK EXCHANGE
		* <a href="http://www.azx.com">www.azx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XAZX = 340;

		/* XETRA - REGULIERTER MARKT
		* <a href="http://www.scoach.de">www.scoach.de</a>
		*/
		public const ushort FOS_MARKET_ID_XETA = 341;

		/* OTC BULLETIN BOARD
		* <a href="http://www.otcbb.com">www.otcbb.com</a>
		*/
		public const ushort FOS_MARKET_ID_XOTC = 342;

		/* PORTAL
		* 
		*/
		public const ushort FOS_MARKET_ID_XPOR = 343;

		/* BOLSA DE VALORES DE MONTEVIDEO
		* <a href="http://www.bvm.com.uy">www.bvm.com.uy</a>
		*/
		public const ushort FOS_MARKET_ID_XMNT = 344;

		/* SARAJEVO STOCK EXCHANGE
		* <a href="http://www.sase.ba">www.sase.ba</a>
		*/
		public const ushort FOS_MARKET_ID_XSSE = 345;

		/* DERIVATIVE REGULATED MARKET - BVB
		* <a href="http://www.bvb.ro">www.bvb.ro</a>
		*/
		public const ushort FOS_MARKET_ID_XBSD = 346;

		/* DEUTSCHE BOERSE AG - VOLATILITY INDICES
		* <a href="http://www.deutsche-boerse.com">www.deutsche-boerse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XDBV = 347;

		/* UZBEKISTAN STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XSTE = 348;

		/* UNIVERSAL BROKER-S EXCHANGE -TASHKENT-
		* 
		*/
		public const ushort FOS_MARKET_ID_XUNI = 349;

		/* CARACAS STOCK EXCHANGE
		* <a href="http://www.caracasstock.com">www.caracasstock.com</a>
		*/
		public const ushort FOS_MARKET_ID_XCAR = 350;

		/* VIETNAM STOCK EXCHANGE
		* 
		*/
		public const ushort FOS_MARKET_ID_XSTC = 351;

		/* LUSAKA STOCK EXCHANGE
		* <a href="http://www.luse.co.zm">www.luse.co.zm</a>
		*/
		public const ushort FOS_MARKET_ID_XLUS = 352;

		/* ZIMBABWE STOCK EXCHANGE
		* <a href="http://www.zse.co.zw">www.zse.co.zw</a>
		*/
		public const ushort FOS_MARKET_ID_XZIM = 353;

		/* NYSE EURONEXT - MARKET WITHOUT QUOTATIONS LISBON
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_WQXL = 354;

		/* NYSE EURONEXT - EASYNEXT LISBON
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_ENXL = 355;

		/* NYSE ARCA OPTIONS
		* <a href="http://www.nyse.com">www.nyse.com</a>
		*/
		public const ushort FOS_MARKET_ID_ARCO = 356;

		/* NASDAQ OMX BX OPTIONS
		* <a href="http://www.nasdaqomxtrader.com">www.nasdaqomxtrader.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBXO = 357;

		/* MTS AMSTERDAM N.V.
		* <a href="http://www.mtsamsterdam.com">www.mtsamsterdam.com</a>
		*/
		public const ushort FOS_MARKET_ID_AMTS = 358;

		/* NYSE ARCA
		* <a href="http://www.archipelago.com">www.archipelago.com</a>
		*/
		public const ushort FOS_MARKET_ID_ARCX = 359;

		/* ISE GEMINI EXCHANGE
		* <a href="http://www.ise.com">www.ise.com</a>
		*/
		public const ushort FOS_MARKET_ID_GMNI = 360;

		/* MIAMI INTERNATIONAL SECURITIES EXCHANGE - OPTIONS
		* <a href="http://www.miaxoptions.com">www.miaxoptions.com</a>
		*/
		public const ushort FOS_MARKET_ID_XMIO = 361;

		/* C2 OPTIONS EXCHANGE INC.
		* <a href="http://www.c2exchange.com">www.c2exchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_C2OX = 362;

		/* BATS EXCHANGE OPTIONS MARKET
		* <a href="http://www.batstrading.com">www.batstrading.com</a>
		*/
		public const ushort FOS_MARKET_ID_BATO = 363;

		/* MTS BELGIUM
		* <a href="http://www.mtsbelgium.com">www.mtsbelgium.com</a>
		*/
		public const ushort FOS_MARKET_ID_BMTS = 364;

		/* WARSAW STOCK EXCHANGE/FINANCIAL DERIVATIVES
		* <a href="http://www.pochodne.gpw.pl">www.pochodne.gpw.pl</a>
		*/
		public const ushort FOS_MARKET_ID_WDER = 365;

		/* INSTINET CHI-X LTD.
		* <a href="http://www.instinet.com">www.instinet.com</a>
		*/
		public const ushort FOS_MARKET_ID_CHIX = 366;

		/* EUROCREDIT
		* <a href="http://www.euromts.com">www.euromts.com</a>
		*/
		public const ushort FOS_MARKET_ID_CMTS = 367;

		/* COREDEAL MTS
		* <a href="http://www.mtsgroup.org">www.mtsgroup.org</a>
		*/
		public const ushort FOS_MARKET_ID_CRDL = 368;

		/* NEW ZEALAND FUTURES & OPTIONS
		* WWW.NZFOX.NZX.COM
		*/
		public const ushort FOS_MARKET_ID_NZFX = 369;

		/* DUBAI GOLD & COMMODITIES EXCHANGE DMCC
		* <a href="http://www.dgcx.ae">www.dgcx.ae</a>
		*/
		public const ushort FOS_MARKET_ID_DGCX = 370;

		/* DUBAI INTERNATIONAL FINANCIAL EXCHANGE LTD.
		* <a href="http://www.difx.ae">www.difx.ae</a>
		*/
		public const ushort FOS_MARKET_ID_DIFX = 371;

		/* UKRAINIAN EXCHANGE
		* <a href="http://www.ux.ua">www.ux.ua</a>
		*/
		public const ushort FOS_MARKET_ID_UKEX = 372;

		/* MTS FRANCE SAS
		* <a href="http://www.mtsfrance.com">www.mtsfrance.com</a>
		*/
		public const ushort FOS_MARKET_ID_FMTS = 373;

		/* EUROTLX
		* <a href="http://www.eurotlx.com">www.eurotlx.com</a>
		*/
		public const ushort FOS_MARKET_ID_ETLX = 374;

		/* MTS DEUTSCHLAND AG
		* <a href="http://www.mtsgermany.com">www.mtsgermany.com</a>
		*/
		public const ushort FOS_MARKET_ID_GMTS = 375;

		/* BORSA ISTANBUL - DEBT SECURITIES MARKET
		* <a href="http://www.borsaistanbul.com">www.borsaistanbul.com</a>
		*/
		public const ushort FOS_MARKET_ID_XDSM = 376;

		/* HANOI SECURITIES TRADING CENTER
		* 
		*/
		public const ushort FOS_MARKET_ID_HSTC = 377;

		/* MTS IRELAND
		* <a href="http://www.mtsireland.com">www.mtsireland.com</a>
		*/
		public const ushort FOS_MARKET_ID_IMTS = 378;

		/* ISEC (ICEX SECOND MARKET)
		* <a href="http://www.isec.is">www.isec.is</a>
		*/
		public const ushort FOS_MARKET_ID_ISEC = 379;

		/* BORSA ISTANBUL - PRECIOUS METALS AND DIAMONDS MARKETS
		* <a href="http://www.borsaistanbul.com">www.borsaistanbul.com</a>
		*/
		public const ushort FOS_MARKET_ID_XPMS = 380;

		/* SBI JAPANNEXT - U - MARKET
		* <a href="http://www.japannext.co.jp">www.japannext.co.jp</a>
		*/
		public const ushort FOS_MARKET_ID_SBIU = 381;

		/* OTHER OTC
		* <a href="http://www.otcbb.com">www.otcbb.com</a>
		*/
		public const ushort FOS_MARKET_ID_OOTC = 382;

		/* MTS AUSTRIAN MARKET
		* <a href="http://www.mtsaustria.com">www.mtsaustria.com</a>
		*/
		public const ushort FOS_MARKET_ID_MTSA = 383;

		/* IEX MARKET
		* <a href="http://www.iextrading.com">www.iextrading.com</a>
		*/
		public const ushort FOS_MARKET_ID_IEXG = 384;

		/* MTS DENMARK
		* <a href="http://www.mtsdenmark.com">www.mtsdenmark.com</a>
		*/
		public const ushort FOS_MARKET_ID_MTSD = 385;

		/* MTS FINLAND
		* <a href="http://www.mtsfinland.com">www.mtsfinland.com</a>
		*/
		public const ushort FOS_MARKET_ID_MTSF = 386;

		/* MTS GREEK MARKET
		* <a href="http://www.mtsgreece.com">www.mtsgreece.com</a>
		*/
		public const ushort FOS_MARKET_ID_MTSG = 387;

		/* FIRST NORTH DENMARK
		* <a href="http://www.nasdaqomxnordic.com/firstnorth">www.nasdaqomxnordic.com/firstnorth</a>
		*/
		public const ushort FOS_MARKET_ID_FNDK = 388;

		/* ISE MERCURY LLC
		* <a href="http://www.ise.com/mercury">www.ise.com/mercury</a>
		*/
		public const ushort FOS_MARKET_ID_MCRY = 389;

		/* AQUIS EXCHANGE
		* <a href="http://www.aquis.eu">www.aquis.eu</a>
		*/
		public const ushort FOS_MARKET_ID_AQXE = 390;

		/* NORD POOL
		* <a href="http://www.nordpool.com">www.nordpool.com</a>
		*/
		public const ushort FOS_MARKET_ID_NORX = 391;

		/* NYMEX EUROPE LTD.
		* <a href="http://www.nymexeurope.co.uk">www.nymexeurope.co.uk</a>
		*/
		public const ushort FOS_MARKET_ID_NYMX = 392;

		/* OTC EXCHANGE OF INDIA
		* <a href="http://www.otcei.net">www.otcei.net</a>
		*/
		public const ushort FOS_MARKET_ID_OTCX = 393;

		/* PIPELINE
		* <a href="http://www.pipelinetrading.com">www.pipelinetrading.com</a>
		*/
		public const ushort FOS_MARKET_ID_PIPE = 394;

		/* MTS PORTUGAL SGMR SA
		* <a href="http://www.mtsportugal.com">www.mtsportugal.com</a>
		*/
		public const ushort FOS_MARKET_ID_PMTS = 395;

		/* MTS QUASI GOVERNMENT
		* <a href="http://www.euromts.com">www.euromts.com</a>
		*/
		public const ushort FOS_MARKET_ID_QMTS = 396;

		/* SHANGHAI GOLD EXCHANGE
		* <a href="http://www.sge.sh">www.sge.sh</a>
		*/
		public const ushort FOS_MARKET_ID_SGEX = 397;

		/* MTS SPAIN S.A.
		* <a href="http://www.mtsspain.com">www.mtsspain.com</a>
		*/
		public const ushort FOS_MARKET_ID_SMTS = 398;

		/* THAILAND FUTURES EXCHANGE
		* <a href="http://www.tfex.co.th">www.tfex.co.th</a>
		*/
		public const ushort FOS_MARKET_ID_TFEX = 399;

		/* EUROBENCHMARK TRES. BILLS
		* <a href="http://www.euromts.com">www.euromts.com</a>
		*/
		public const ushort FOS_MARKET_ID_TMTS = 400;

		/* APX POWER UK
		* <a href="http://www.apxgroup.com">www.apxgroup.com</a>
		*/
		public const ushort FOS_MARKET_ID_UKPX = 401;

		/* MTS CEDULAS
		* <a href="http://www.euromts.com">www.euromts.com</a>
		*/
		public const ushort FOS_MARKET_ID_UMTS = 402;

		/* A1
		* a1.primary.com.ar
		*/
		public const ushort FOS_MARKET_ID_XA1X = 403;

		/* BOLSA DE COMERCIO DE CORBODA
		* <a href="http://www.bolsacba.com.ar">www.bolsacba.com.ar</a>
		*/
		public const ushort FOS_MARKET_ID_XBCC = 404;

		/* BANJA LUKA STOCK EXCHANGE
		* <a href="http://www.blberza.com">www.blberza.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBLB = 405;

		/* CAPE VERDE STOCK EXCHANGE
		* <a href="http://www.bvc.cv">www.bvc.cv</a>
		*/
		public const ushort FOS_MARKET_ID_XBVC = 406;

		/* THE CHINESE GOLD & SILVER EXCHANGE SOCIETY
		* <a href="http://www.cgse.com.hk">www.cgse.com.hk</a>
		*/
		public const ushort FOS_MARKET_ID_XCGS = 407;

		/* DONETSK STOCK EXCHANGE
		* <a href="http://www.dfb.donbass.com">www.dfb.donbass.com</a>
		*/
		public const ushort FOS_MARKET_ID_XDFB = 408;

		/* EASDAQ
		* <a href="http://www.easdaq.be">www.easdaq.be</a>
		*/
		public const ushort FOS_MARKET_ID_XEAS = 409;

		/* EUREX REPO GMBH
		* <a href="http://www.eurexchange.com">www.eurexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_XEUP = 410;

		/* ISTANBUL GOLD EXCHANGE
		* <a href="http://www.iab.gov.tr">www.iab.gov.tr</a>
		*/
		public const ushort FOS_MARKET_ID_XIAB = 411;

		/* INTERNATIONAL MARTIME EXCHANGE
		* <a href="http://www.imarex.com/home">www.imarex.com/home</a>
		*/
		public const ushort FOS_MARKET_ID_XIMA = 412;

		/* MULTI COMMODITY EXCHANGE OF INDIA LTD.
		* <a href="http://www.mcxindia.com">www.mcxindia.com</a>
		*/
		public const ushort FOS_MARKET_ID_XIMC = 413;

		/* THE IRAK STOCK EXCHANGE
		* <a href="http://www.isx-iq.net">www.isx-iq.net</a>
		*/
		public const ushort FOS_MARKET_ID_XIQS = 414;

		/* KIEV INTERNATIONAL STOCK EXCHANGE
		* <a href="http://www.kise-ua.com">www.kise-ua.com</a>
		*/
		public const ushort FOS_MARKET_ID_XKIS = 415;

		/* NYSE EURONEXT - TRADING FACILITY BRUSSELS
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_TNLB = 416;

		/* NATIONAL COMMODITY & DERIVATIVES EXCHANGE LTD.
		* <a href="http://www.ncdex.com">www.ncdex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNCD = 417;

		/* NASDAQ/NGS -- GLOBAL SELECT MARKET
		* <a href="http://www.nasdaq.com">www.nasdaq.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNGS = 418;

		/* OSAKA SECURITIES EXCHANGE J-NET
		* <a href="http://www.ose.or.jp/e/stocks/st-jn.html">www.ose.or.jp/e/stocks/st-jn.html</a>
		*/
		public const ushort FOS_MARKET_ID_XOSJ = 419;

		/* PLUS MARKETS GROUP
		* <a href="http://www.plus-trading.co.uk">www.plus-trading.co.uk</a>
		*/
		public const ushort FOS_MARKET_ID_XPLU = 420;

		/* RM-SYSTEM A.S.
		* <a href="http://www.rmsystem.cz">www.rmsystem.cz</a>
		*/
		public const ushort FOS_MARKET_ID_XRMZ = 421;

		/* TOKYO STOCK EXCHANGE - TOSTNET-1
		* <a href="http://www.tse.or.jp/english/cash/tostnet/index.html">www.tse.or.jp/english/cash/tostnet/index.html</a>
		*/
		public const ushort FOS_MARKET_ID_XTK1 = 422;

		/* TOKYO STOCK EXCHANGE - TOSTNET-2
		* <a href="http://www.tse.or.jp/english/cash/tostnet/index.html">www.tse.or.jp/english/cash/tostnet/index.html</a>
		*/
		public const ushort FOS_MARKET_ID_XTK2 = 423;

		/* XETA - SCOACH_EU TECHNICAL FOSMARKET 2
		* <a href="http://www.scoach.de">www.scoach.de</a>
		*/
		public const ushort FOS_MARKET_ID_xet2 = 424;

		/* TURKISH DERIVATIVES EXCHANGE
		* <a href="http://www.turkdex.org.tr">www.turkdex.org.tr</a>
		*/
		public const ushort FOS_MARKET_ID_XTUR = 425;

		/* UKRAINIAN STOCK EXCHANGE
		* <a href="http://www.ukrse.kiev.ua">www.ukrse.kiev.ua</a>
		*/
		public const ushort FOS_MARKET_ID_XUAX = 426;

		/* VARAZDIN STOCK EXCHANGE
		* <a href="http://www.vse.hr">www.vse.hr</a>
		*/
		public const ushort FOS_MARKET_ID_XVAR = 427;

		/* TURQUOISE
		* <a href="http://www.tradeturquoise.com/">www.tradeturquoise.com/</a>
		*/
		public const ushort FOS_MARKET_ID_TRQX = 428;

		/* NYSE EURONEXT - MARCHE LIBRE BRUSSELS
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_MLXB = 429;

		/* OMX NORDIC EXCHANGE STOCKHOLM AB
		* 
		*/
		public const ushort FOS_MARKET_ID_XSTO = 430;

		/* DUBAI MERCANTILE EXCHANGE
		* <a href="http://www.dubaimerc.com">www.dubaimerc.com</a>
		*/
		public const ushort FOS_MARKET_ID_DUMX = 431;

		/* INTERCONTINENTAL EXCHANGE - ICE FUTURES EUROPE
		* <a href="http://www.theice.com">www.theice.com</a>
		*/
		public const ushort FOS_MARKET_ID_ICEU = 432;

		/* NORDIC DERIVATIVES EXCHANGES
		* <a href="http://www.ndx.se">www.ndx.se</a>
		*/
		public const ushort FOS_MARKET_ID_XNDX = 433;

		/* NYSE EURONEXT - ALTERNEXT PARIS
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_ALXP = 434;

		/* NYSE EURONEXT - ALTERNEXT BRUSSELS
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_ALXB = 435;

		/* NYSE EURONEXT - ALTERNEXT AMSTERDAM
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_ALXA = 436;

		/* OSLO BORS ALTERNATIVE BOND MARKET
		* <a href="http://www.abmportal.no">www.abmportal.no</a>
		*/
		public const ushort FOS_MARKET_ID_XOAM = 437;

		/* NYSE EURONEXT - TRADED BUT NOT LISTED AMSTERDAM
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_TNLA = 438;

		/* OSLO AXESS
		* <a href="http://www.osloaxess.no">www.osloaxess.no</a>
		*/
		public const ushort FOS_MARKET_ID_XOAS = 439;

		/* OMX OTC PUBLICATION VENUE
		* <a href="http://www.omxgroup.com">www.omxgroup.com</a>
		*/
		public const ushort FOS_MARKET_ID_XOPV = 440;

		/* FIRST NORTH DENMARK
		* <a href="http://www.omxgroup.com/firstnorth">www.omxgroup.com/firstnorth</a>
		*/
		public const ushort FOS_MARKET_ID_XFND = 441;

		/* AKTIETORGET
		* <a href="http://www.actietorget.se">www.actietorget.se</a>
		*/
		public const ushort FOS_MARKET_ID_XSAT = 442;

		/* FIRST NORTH STOCKHOLM
		* <a href="http://www.omxgroup.com/firstnorth">www.omxgroup.com/firstnorth</a>
		*/
		public const ushort FOS_MARKET_ID_FNSE = 443;

		/* BATS EXCHANGE
		* <a href="http://www.batstrading.com">www.batstrading.com</a>
		*/
		public const ushort FOS_MARKET_ID_BATS = 444;

		/* NASDAQ OPTION EXCHANGE
		* <a href="http://www.nasdaq.com">www.nasdaq.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNDQ = 445;

		/* NYSE ARCA EUROPE
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XHFT = 446;

		/* SMARTPOOL
		* <a href="http://www.tradeonsmartpool.com">www.tradeonsmartpool.com</a>
		*/
		public const ushort FOS_MARKET_ID_XSMP = 447;

		/* BATS EUROPE
		* <a href="http://www.batstrading.co.uk">www.batstrading.co.uk</a>
		*/
		public const ushort FOS_MARKET_ID_BATE = 448;

		/* brussels derivatives
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_XBRD = 449;

		/* NYSE EURONEXT - MERCADO DE FUTUROS E OPCOES
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_MFOX = 450;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_1 = 451;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_2 = 452;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_3 = 453;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_4 = 454;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_5 = 455;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_6 = 456;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_7 = 457;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_8 = 458;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_9 = 459;

		/* 
		* <a href="http://www.quanthouse.com">www.quanthouse.com</a>
		*/
		public const ushort FOS_MARKET_ID_RESERVED_10 = 460;

		/* LONDON STOCK EXCHANGE - APA
		* <a href="http://www.londonstockexchange.com">www.londonstockexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_ECHO = 468;

		/* SIX SWISS EXCHANGE Sweep SwissAtMid
		* <a href="http://www.swx.com">www.swx.com</a>
		*/
		public const ushort FOS_MARKET_ID_XSWM = 469;

		/* MUTUAL FUND QUOTATION SERVICE
		* <a href="http://www.nasdaq.com">www.nasdaq.com</a>
		*/
		public const ushort FOS_MARKET_ID_mfqs = 470;

		/* ICE FUTURES CANADA
		* <a href="http://www.theice.com">www.theice.com</a>
		*/
		public const ushort FOS_MARKET_ID_IFCA = 471;

		/* CHI-X CANADA ATS
		* <a href="http://www.chi-xcanada.com">www.chi-xcanada.com</a>
		*/
		public const ushort FOS_MARKET_ID_CHIC = 472;

		/* CMA Datavision
		* <a href="http://www.cmavision.com">www.cmavision.com</a>
		*/
		public const ushort FOS_MARKET_ID_Xcma = 473;

		/* PURE TRADING
		* <a href="http://www.puretrading.ca">www.puretrading.ca</a>
		*/
		public const ushort FOS_MARKET_ID_PURE = 474;

		/* XETA - SCOACH_EU TECHNICAL FOSMARKET 3
		* <a href="http://www.scoach.de">www.scoach.de</a>
		*/
		public const ushort FOS_MARKET_ID_xet3 = 475;

		/* ALPHA EXCHANGE
		* <a href="http://www.alphatradingsystems.ca">www.alphatradingsystems.ca</a>
		*/
		public const ushort FOS_MARKET_ID_XATS = 476;

		/* OMEGA ATS
		* <a href="http://www.omegaats.com">www.omegaats.com</a>
		*/
		public const ushort FOS_MARKET_ID_OMGA = 477;

		/* CHI-X AUSTRALIA
		* <a href="http://www.chi-x.com/apac/">www.chi-x.com/apac/</a>
		*/
		public const ushort FOS_MARKET_ID_CHIA = 478;

		/* CHI-X JAPAN
		* <a href="http://www.chi-x.asia/apac/jp/eng">www.chi-x.asia/apac/jp/eng</a>
		*/
		public const ushort FOS_MARKET_ID_CHIJ = 479;

		/* SBI JAPANNEXT-J-MARKET
		* <a href="http://www.japannext.co.jp">www.japannext.co.jp</a>
		*/
		public const ushort FOS_MARKET_ID_SBIJ = 480;

		/* SBI JAPANNEXT-X-MARKET
		* <a href="http://www.japannext.co.jp">www.japannext.co.jp</a>
		*/
		public const ushort FOS_MARKET_ID_XSBI = 481;

		/* INTERCONTINENTAL EXCHANGE - ICE FUTURES LIMITED
		* <a href="http://www.theice.com">www.theice.com</a>
		*/
		public const ushort FOS_MARKET_ID_IFEU = 482;

		/* ICE FUTURES U.S.
		* <a href="http://www.theice.com">www.theice.com</a>
		*/
		public const ushort FOS_MARKET_ID_IFUS = 483;

		/* EPEX SPOT SE
		* <a href="http://www.epexspot.fr">www.epexspot.fr</a>
		*/
		public const ushort FOS_MARKET_ID_EPEX = 484;

		/* HOTSPOT FX
		* <a href="http://www.hotspotfx.com">www.hotspotfx.com</a>
		*/
		public const ushort FOS_MARKET_ID_HSFX = 485;

		/* MICEX STOCK EXCHANGE
		* <a href="http://www.micex.com">www.micex.com</a>
		*/
		public const ushort FOS_MARKET_ID_MISX = 486;

		/* NORDIC MTF
		* <a href="http://www.nordicmtf.se">www.nordicmtf.se</a>
		*/
		public const ushort FOS_MARKET_ID_NMTF = 487;

		/* TULLETT PREBON PLC
		* <a href="http://www.tullettprebon.com">www.tullettprebon.com</a>
		*/
		public const ushort FOS_MARKET_ID_XTUP = 488;

		/* ETS EURASIAN TRADING SYSTEM COMMODITY EXCHANGE
		* <a href="http://www.ets.kz/en/">www.ets.kz/en/</a>
		*/
		public const ushort FOS_MARKET_ID_ETSC = 489;

		/* CANADIAN CONSOLIDATED EQUITIES
		* unknown
		*/
		public const ushort FOS_MARKET_ID_Xcce = 490;

		/* LIQUIDNET SYSTEM
		* <a href="http://www.liquidnet.com">www.liquidnet.com</a>
		*/
		public const ushort FOS_MARKET_ID_LIQU = 491;

		/* FIRST NORTH FINLAND
		* <a href="http://www.omxnordicexchange.com/firstnorth">www.omxnordicexchange.com/firstnorth</a>
		*/
		public const ushort FOS_MARKET_ID_FNFI = 492;

		/* FIRST NORTH LATVIA
		* firstnorth.nasdaqomxbaltic.com
		*/
		public const ushort FOS_MARKET_ID_FNLV = 493;

		/* NYSE BONDMATCH
		* <a href="http://www.euronext.com">www.euronext.com</a>
		*/
		public const ushort FOS_MARKET_ID_MTCH = 494;

		/* NYSE LIFFE
		*  <a href="http://www.nyse.com">www.nyse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNLI = 495;

		/* BM&FBOVESPA S.A. - BOLSA DE VALORES -MERCADORIAS E FUTUROS
		*  <a href="http://www.bmfbovespa.com.br">www.bmfbovespa.com.br</a>
		*/
		public const ushort FOS_MARKET_ID_BVMF = 496;

		/* THE GREEN EXCHANGE
		* nymex.greenfutures.com
		*/
		public const ushort FOS_MARKET_ID_GREE = 497;

		/* SCOACH SWITZERLAND
		* <a href="http://www.scoach.com">www.scoach.com</a>
		*/
		public const ushort FOS_MARKET_ID_XQMH = 498;

		/* Baltic future
		* <a href="http://www.balticexchange.com">www.balticexchange.com</a>
		*/
		public const ushort FOS_MARKET_ID_Xbfe = 499;

		/* Alternext
		* <a href="http://www.alternext.com">www.alternext.com</a>
		*/
		public const ushort FOS_MARKET_ID_Xalt = 500;

		/* Miscellaneous
		* 
		*/
		public const ushort FOS_MARKET_ID_misc = 501;

		/* FOREX
		* 
		*/
		public const ushort FOS_MARKET_ID_Xfor = 502;

		/* EUREX Helsinki
		* 
		*/
		public const ushort FOS_MARKET_ID_Xhex = 503;

		/* OPRA
		* <a href="http://www.opradata.com/">www.opradata.com/</a>
		*/
		public const ushort FOS_MARKET_ID_opra = 504;

		/* CONSOLIDATED TAPE ASSOCIATION
		* <a href="http://www.nyxdata.com/cta">www.nyxdata.com/cta</a>
		*/
		public const ushort FOS_MARKET_ID_Xcta = 505;

		/* WIENER BOERSE AG AMTLICHER HANDEL (OFFICIAL MARKET)
		* <a href="http://www.wienerboerse.at">www.wienerboerse.at</a>
		*/
		public const ushort FOS_MARKET_ID_WBAH = 506;

		/* ICE FUTURES U.S. INC
		* <a href="http://www.theice.com">www.theice.com</a>
		*/
		public const ushort FOS_MARKET_ID_ICUS = 507;

		/* XETRA INTERNATIONAL MARKET
		* <a href="http://www.deutsche-boerse.com">www.deutsche-boerse.com</a>
		*/
		public const ushort FOS_MARKET_ID_XETI = 508;

		/* NEW YORK MERCANTILE EXCHANGE - OTC MARKETS NYMEX ECM
		* <a href="http://www.nymex.com">www.nymex.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNYE = 509;

		/* NEW YORK MERCANTILE EXCHANGE - ENERGY MARKETS NYMEX MTF LIMITED
		* <a href="http://www.nymexonlchclearnet.com">www.nymexonlchclearnet.com</a>
		*/
		public const ushort FOS_MARKET_ID_XNYL = 510;

		/* FINRA ALTERNATIVE DISPLAY FACILITY
		* <a href="http://www.finra.org">www.finra.org</a>
		*/
		public const ushort FOS_MARKET_ID_XADF = 511;

		/* Xnsd
		* an alias for XADF
		*/
		public const ushort   FOS_MARKET_ID_Xnsd = FOS_MARKET_ID_XADF;

            /// ***GENERATOR-end-MARKET_IDS-
            #endregion

            #region Constructor
            private MarketIds() { }
            #endregion
         }
    }
}
