using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class TagsReferential
        {
            #region Referential Tags Definitions
            /// ***GENERATOR-begin-REF_TAGS-

		/* TAG_FOSMarketId (syntax: uint16) */
		public const ushort TAG_FOSMarketId	=207;

		/* TAG_ExDestination (syntax: String) */
		public const ushort TAG_ExDestination	=100;

		/* TAG_CFICode (syntax: String) */
		public const ushort TAG_CFICode	=461;

		/* TAG_SecurityType (syntax: String) */
		public const ushort TAG_SecurityType	=167;

		/* TAG_SecuritySubType (syntax: String) */
		public const ushort TAG_SecuritySubType	=762;

		/* TAG_Symbol (syntax: String) */
		public const ushort TAG_Symbol	=55;

		/* TAG_Description (syntax: String) */
		public const ushort TAG_Description	=107;

		/* TAG_Product (syntax: int) */
		public const ushort TAG_Product	=460;

		/* TAG_CountryOfIssue (syntax: String) */
		public const ushort TAG_CountryOfIssue	=470;

		/* TAG_PriceCurrency (syntax: String) */
		public const ushort TAG_PriceCurrency	=15;

		/* TAG_ContractMultiplier (syntax: float64) */
		public const ushort TAG_ContractMultiplier	=231;

		/* TAG_StrikePrice (syntax: float64) */
		public const ushort TAG_StrikePrice	=202;

		/* TAG_StrikeCurrency (syntax: String) */
		public const ushort TAG_StrikeCurrency	=947;

		/* TAG_OptAttributeVersion (syntax: int) */
		public const ushort TAG_OptAttributeVersion	=206;

		/* TAG_NbLegs (syntax: int) */
		public const ushort TAG_NbLegs	=555;

		/* TAG_CouponRate (syntax: float64) */
		public const ushort TAG_CouponRate	=223;

		/* TAG_CouponPaymentDate (syntax: int) */
		public const ushort TAG_CouponPaymentDate	=224;

		/* TAG_RoundLot (syntax: float64) */
		public const ushort TAG_RoundLot	=561;

		/* TAG_MinTradeVol (syntax: float64) */
		public const ushort TAG_MinTradeVol	=562;

		/* TAG_CreditRating (syntax: String) */
		public const ushort TAG_CreditRating	=255;

		/* TAG_Issuer (syntax: String) */
		public const ushort TAG_Issuer	=106;

		/* TAG_IssueDate (syntax: Timestamp) */
		public const ushort TAG_IssueDate	=225;

		/* TAG_Factor (syntax: float64) */
		public const ushort TAG_Factor	=228;

		/* TAG_FinancialStatus (syntax: String) */
		public const ushort TAG_FinancialStatus	=291;

		/* TAG_PriceType (syntax: int) */
		public const ushort TAG_PriceType	=423;

		/* TAG_Account (syntax: String) */
		public const ushort TAG_Account	=1;

		/* TAG_DatedDate (syntax: Timestamp) */
		public const ushort TAG_DatedDate	=873;

		/* TAG_MarketSegmentDesc (syntax: String) */
		public const ushort TAG_MarketSegmentDesc	=1396;

		/* TAG_MarketSegmentID (syntax: String) */
		public const ushort TAG_MarketSegmentID	=1300;

		/* TAG_StdMaturity (syntax: String) */
		public const ushort TAG_StdMaturity	=200;

		/* TAG_MatchAlgorithm (syntax: String) */
		public const ushort TAG_MatchAlgorithm	=1142;

		/* TAG_UnitOfMeasure (syntax: String) */
		public const ushort TAG_UnitOfMeasure	=996;

		/* TAG_SecurityGroup (syntax: String) */
		public const ushort TAG_SecurityGroup	=1151;

		/* TAG_LotType (syntax: int) */
		public const ushort TAG_LotType	=1093;

		/* TAG_MaxTradeVol (syntax: float64) */
		public const ushort TAG_MaxTradeVol	=1140;

		/* TAG_MaxFloor (syntax: float64) */
		public const ushort TAG_MaxFloor	=111;

		/* TAG_ProductComplex (syntax: String) */
		public const ushort TAG_ProductComplex	=1227;

		/* TAG_SecurityStatus (syntax: int) */
		public const ushort TAG_SecurityStatus	=965;

		/* TAG_LocalCodeStr (syntax: String) */
		public const ushort TAG_LocalCodeStr	=9500;

		/* TAG_ForeignFOSMarketId (syntax: uint16) */
		public const ushort TAG_ForeignFOSMarketId	=9501;

		/* TAG_ForeignMarketId (syntax: String) */
		public const ushort TAG_ForeignMarketId	=9502;

		/* TAG_ISIN (syntax: String) */
		public const ushort TAG_ISIN	=9503;

		/* TAG_CUSIP (syntax: String) */
		public const ushort TAG_CUSIP	=9504;

		/* TAG_CINS (syntax: String) */
		public const ushort TAG_CINS	=9541;

		/* TAG_SEDOL (syntax: String) */
		public const ushort TAG_SEDOL	=9505;

		/* TAG_PriceIncrement_static (syntax: float64) */
		public const ushort TAG_PriceIncrement_static	=9506;

		/* TAG_PriceDisplayPrecision (syntax: int16) */
		public const ushort TAG_PriceDisplayPrecision	=9507;

		/* TAG_ReutersInstrumentCode (syntax: String) */
		public const ushort TAG_ReutersInstrumentCode	=9508;

		/* TAG_UnderlyingFOSMarketId (syntax: uint16) */
		public const ushort TAG_UnderlyingFOSMarketId	=9509;

		/* TAG_UnderlyingLocalCodeStr (syntax: String) */
		public const ushort TAG_UnderlyingLocalCodeStr	=9510;

		/* TAG_UnderlyingFOSInstrumentCode (syntax: uint32) */
		public const ushort TAG_UnderlyingFOSInstrumentCode	=9511;

		/* TAG_MaturityYear (syntax: int) */
		public const ushort TAG_MaturityYear	=9512;

		/* TAG_MaturityMonth (syntax: int) */
		public const ushort TAG_MaturityMonth	=9513;

		/* TAG_MaturityDay (syntax: int) */
		public const ushort TAG_MaturityDay	=9514;

		/* TAG_IsFrontMonth (syntax: bool) */
		public const ushort TAG_IsFrontMonth	=9582;

		/* TAG_ICB_IndustryCode (syntax: int) */
		public const ushort TAG_ICB_IndustryCode	=9515;

		/* TAG_ICB_SupersectorCode (syntax: int) */
		public const ushort TAG_ICB_SupersectorCode	=9516;

		/* TAG_ICB_SectorCode (syntax: int) */
		public const ushort TAG_ICB_SectorCode	=9517;

		/* TAG_ICB_SubsectorCode (syntax: int) */
		public const ushort TAG_ICB_SubsectorCode	=9518;

		/* TAG_BloombergTicker (syntax: String) */
		public const ushort TAG_BloombergTicker	=9519;

		/* TAG_WertpapierKennNummer (syntax: String) */
		public const ushort TAG_WertpapierKennNummer	=9520;

		/* TAG_Telekurs_Valor (syntax: String) */
		public const ushort TAG_Telekurs_Valor	=9521;

		/* TAG_PriceIncrement_dynamic_TableId (syntax: int) */
		public const ushort TAG_PriceIncrement_dynamic_TableId	=9522;

		/* TAG_PrimaryReutersInstrumentCode (syntax: String) */
		public const ushort TAG_PrimaryReutersInstrumentCode	=9523;

		/* TAG_PrimaryBloombergTicker (syntax: String) */
		public const ushort TAG_PrimaryBloombergTicker	=9524;

		/* TAG_SecurityTradingId (syntax: String) */
		public const ushort TAG_SecurityTradingId	=9525;

		/* TAG_AlternativeSecurityTradingId (syntax: String) */
		public const ushort TAG_AlternativeSecurityTradingId	=9576;

		/* TAG_UMTF (syntax: String) */
		public const ushort TAG_UMTF	=9526;

		/* TAG_NbIndexComponents (syntax: int) */
		public const ushort TAG_NbIndexComponents	=9527;

		/* TAG_IndexMemberships (syntax: String) */
		public const ushort TAG_IndexMemberships	=9528;

		/* TAG_InitialListingMarketId (syntax: String) */
		public const ushort TAG_InitialListingMarketId	=9529;

		/* TAG_GICS (syntax: String) */
		public const ushort TAG_GICS	=9530;

		/* TAG_ICB (syntax: String) */
		public const ushort TAG_ICB	=9531;

		/* TAG_SICC_SectorCode (syntax: String) */
		public const ushort TAG_SICC_SectorCode	=9580;

		/* TAG_MBLLayersDesc (syntax: String) */
		public const ushort TAG_MBLLayersDesc	=9532;

		/* TAG_OperatingMIC (syntax: String) */
		public const ushort TAG_OperatingMIC	=9533;

		/* TAG_SegmentMIC (syntax: String) */
		public const ushort TAG_SegmentMIC	=9534;

		/* TAG_AuctionOnDemand_MIC (syntax: String) */
		public const ushort TAG_AuctionOnDemand_MIC	=9583;

		/* TAG_IsCentralOrderBook (syntax: bool) */
		public const ushort TAG_IsCentralOrderBook	=9581;

		/* TAG_CIQ_ExchangeID (syntax: int64) */
		public const ushort TAG_CIQ_ExchangeID	=9535;

		/* TAG_CIQ_ExchangeName (syntax: String) */
		public const ushort TAG_CIQ_ExchangeName	=9536;

		/* TAG_CIQ_CompanyID (syntax: int64) */
		public const ushort TAG_CIQ_CompanyID	=9537;

		/* TAG_CIQ_CompanyName (syntax: String) */
		public const ushort TAG_CIQ_CompanyName	=9538;

		/* TAG_CIQ_SecurityID (syntax: int64) */
		public const ushort TAG_CIQ_SecurityID	=9539;

		/* TAG_CIQ_TradingItemID (syntax: int64) */
		public const ushort TAG_CIQ_TradingItemID	=9540;

		/* TAG_CapitalIQ_ExchangeID (syntax: int32) */
		public const ushort TAG_CapitalIQ_ExchangeID	=9542;

		/* TAG_CapitalIQ_CompanyID (syntax: int32) */
		public const ushort TAG_CapitalIQ_CompanyID	=9543;

		/* TAG_CapitalIQ_SecurityID (syntax: int32) */
		public const ushort TAG_CapitalIQ_SecurityID	=9544;

		/* TAG_CapitalIQ_TradingItemID (syntax: int32) */
		public const ushort TAG_CapitalIQ_TradingItemID	=9545;

		/* TAG_CouponPaymentDate2 (syntax: int32) */
		public const ushort TAG_CouponPaymentDate2	=9550;

		/* TAG_IssueAttributeInfo (syntax: char) */
		public const ushort TAG_IssueAttributeInfo	=9551;

		/* TAG_CCP_Eligible (syntax: bool) */
		public const ushort TAG_CCP_Eligible	=9552;

		/* TAG_IndustryCode (syntax: String) */
		public const ushort TAG_IndustryCode	=9546;

		/* TAG_EMCF_Eligible (syntax: bool) */
		public const ushort TAG_EMCF_Eligible	=9547;

		/* TAG_XCLEAR_Eligible (syntax: bool) */
		public const ushort TAG_XCLEAR_Eligible	=9548;

		/* TAG_LCH_Clearnet_Eligible (syntax: bool) */
		public const ushort TAG_LCH_Clearnet_Eligible	=9549;

		/* TAG_LegFOSInstrumentCode (syntax: [array of 100] uint32) */
		public const ushort TAG_LegFOSInstrumentCode_0	=9600; // x100
		public const ushort TAG_NB_MAX_LegFOSInstrumentCode=100;

		/* TAG_LegRatioQty (syntax: [array of 100] float64) */
		public const ushort TAG_LegRatioQty_0	=9700; // x100
		public const ushort TAG_NB_MAX_LegRatioQty=100;

		/* TAG_LegFIXSide (syntax: [array of 100] char) */
		public const ushort TAG_LegFIXSide_0	=9800; // x100
		public const ushort TAG_NB_MAX_LegFIXSide=100;

		/* TAG_DynamicVariationRange (syntax: float64) */
		public const ushort TAG_DynamicVariationRange	=9553;

		/* TAG_StaticVariationRange (syntax: float64) */
		public const ushort TAG_StaticVariationRange	=9554;

		/* TAG_ParValue (syntax: float64) */
		public const ushort TAG_ParValue	=9555;

		/* TAG_ShortSellEligibleFlag (syntax: bool) */
		public const ushort TAG_ShortSellEligibleFlag	=9556;

		/* TAG_OutstandingSharesBillions (syntax: int32) */
		public const ushort TAG_OutstandingSharesBillions	=9557;

		/* TAG_OutstandingShares (syntax: int32) */
		public const ushort TAG_OutstandingShares	=9558;

		/* TAG_MarginTradingEligibleFlag (syntax: bool) */
		public const ushort TAG_MarginTradingEligibleFlag	=9559;

		/* TAG_ShortSellRule (syntax: char) */
		public const ushort TAG_ShortSellRule	=9560;

		/* TAG_BookType (syntax: char) */
		public const ushort TAG_BookType	=9561;

		/* TAG_BuyBoardLot (syntax: uint32) */
		public const ushort TAG_BuyBoardLot	=9562;

		/* TAG_SellBoardLot (syntax: uint32) */
		public const ushort TAG_SellBoardLot	=9563;

		/* TAG_PriceEarningRatio (syntax: float64) */
		public const ushort TAG_PriceEarningRatio	=9564;

		/* TAG_FaceValue (syntax: float64) */
		public const ushort TAG_FaceValue	=9565;

		/* TAG_RateType (syntax: char) */
		public const ushort TAG_RateType	=9566;

		/* TAG_PaymentPeriod (syntax: uint16) */
		public const ushort TAG_PaymentPeriod	=9567;

		/* TAG_PrimeRate (syntax: float64) */
		public const ushort TAG_PrimeRate	=9568;

		/* TAG_YieldToMaturity (syntax: float64) */
		public const ushort TAG_YieldToMaturity	=9569;

		/* TAG_RedemptionValue (syntax: float64) */
		public const ushort TAG_RedemptionValue	=9570;

		/* TAG_BlockSize (syntax: float64) */
		public const ushort TAG_BlockSize	=9571;

		/* TAG_AutomaticExerciseLimit (syntax: float64) */
		public const ushort TAG_AutomaticExerciseLimit	=9572;

		/* TAG_IPO_Indicator (syntax: bool) */
		public const ushort TAG_IPO_Indicator	=9406;

		/* TAG_DelayedFeedMin (syntax: uint16) */
		public const ushort TAG_DelayedFeedMin	=9407;

		/* TAG_CompositeFIGI (syntax: String) */
		public const ushort TAG_CompositeFIGI	=9573;

		/* TAG_BloombergCode (syntax: String) */
		public const ushort TAG_BloombergCode	=9574;

		/* TAG_PrimaryBloombergCode (syntax: String) */
		public const ushort TAG_PrimaryBloombergCode	=9575;

		/* TAG_ContractVersion (syntax: uint8) */
		public const ushort TAG_ContractVersion	=9577;

		/* TAG_OriginalContractMultiplier (syntax: float64) */
		public const ushort TAG_OriginalContractMultiplier	=9578;

		/* TAG_OriginalStrikePrice (syntax: float64) */
		public const ushort TAG_OriginalStrikePrice	=9579;

		/* TAG_MiFID_Flags (syntax: uint16) */
		public const ushort TAG_MiFID_Flags	=9450;

		/* TAG_AverageDailyTurnover (syntax: float64) */
		public const ushort TAG_AverageDailyTurnover	=9451;

		/* TAG_AverageValueOfTransactions (syntax: float64) */
		public const ushort TAG_AverageValueOfTransactions	=9452;

		/* TAG_StandardMarketSize (syntax: uint32) */
		public const ushort TAG_StandardMarketSize	=9453;

		/* TAG_AverageDailyNumberOfTransactions (syntax: float64) */
		public const ushort TAG_AverageDailyNumberOfTransactions	=9454;

		/* TAG_ReportedFOSInstrumentCode (syntax: uint32) */
		public const ushort TAG_ReportedFOSInstrumentCode	=9455;

		/* TAG_InternalCreationDate (syntax: Timestamp) */
		public const ushort TAG_InternalCreationDate	=9400;

		/* TAG_InternalModificationDate (syntax: Timestamp) */
		public const ushort TAG_InternalModificationDate	=9401;

		/* TAG_InternalHideFromLookup (syntax: bool) */
		public const ushort TAG_InternalHideFromLookup	=9402;

		/* TAG_InternalSourceId (syntax: uint16) */
		public const ushort TAG_InternalSourceId	=9403;

		/* TAG_InternalAggregationId (syntax: uint16) */
		public const ushort TAG_InternalAggregationId	=9404;

		/* TAG_InternalEntitlementId (syntax: int32) */
		public const ushort TAG_InternalEntitlementId	=9405;

		/* TAG_DeliveryStartDate (syntax: Timestamp) */
		public const ushort TAG_DeliveryStartDate	=9408;

		/* TAG_DeliveryEndDate (syntax: Timestamp) */
		public const ushort TAG_DeliveryEndDate	=9409;

		/* TAG_LastTradingDate (syntax: Timestamp) */
		public const ushort TAG_LastTradingDate	=9421;

		/* TAG_UnderlyingSymbol (syntax: String) */
		public const ushort TAG_UnderlyingSymbol	=9422;

		/* TAG_UnderlyingISIN (syntax: String) */
		public const ushort TAG_UnderlyingISIN	=9423;

		/* TAG_FIGI (syntax: String) */
		public const ushort TAG_FIGI	=9424;

		/* TAG_FISN (syntax: String) */
		public const ushort TAG_FISN	=9442;

		/* TAG_PartitionID (syntax: uint32) */
		public const ushort TAG_PartitionID	=9443;

		/* TAG_SettlementPeriod (syntax: uint8) */
		public const ushort TAG_SettlementPeriod	=9425;

		/* TAG_ExchangeSymbol (syntax: String) */
		public const ushort TAG_ExchangeSymbol	=9426;

		/* TAG_PriceDisplayFormat (syntax: uint8) */
		public const ushort TAG_PriceDisplayFormat	=9427;

		/* TAG_ContractMultiplierUnit (syntax: String) */
		public const ushort TAG_ContractMultiplierUnit	=9428;

		/* TAG_ExpiryMonthCode (syntax: char) */
		public const ushort TAG_ExpiryMonthCode	=9429;

		/* TAG_FirstTradingDate (syntax: Timestamp) */
		public const ushort TAG_FirstTradingDate	=9430;

		/* TAG_SettlementDate (syntax: Timestamp) */
		public const ushort TAG_SettlementDate	=9431;

		/* TAG_SettlementCurrency (syntax: String) */
		public const ushort TAG_SettlementCurrency	=9432;

		/* TAG_InternalFeedQualifier (syntax: uint32) */
		public const ushort TAG_InternalFeedQualifier	=9433;

		/* TAG_HasClosingAuctionSession (syntax: bool) */
		public const ushort TAG_HasClosingAuctionSession	=11712;

		/* TAG_HasVolatilityControlMechanism (syntax: bool) */
		public const ushort TAG_HasVolatilityControlMechanism	=11713;

		/* TAG_ShortSellUptickRule (syntax: bool) */
		public const ushort TAG_ShortSellUptickRule	=9434;

		/* TAG_Parity (syntax: float64) */
		public const ushort TAG_Parity	=9435;

		/* TAG_DeactivationBarrierPrice (syntax: float64) */
		public const ushort TAG_DeactivationBarrierPrice	=9436;

		/* TAG_ActivationBarrierPrice (syntax: float64) */
		public const ushort TAG_ActivationBarrierPrice	=9437;

		/* TAG_OrderEntryPriceDecimals (syntax: uint8) */
		public const ushort TAG_OrderEntryPriceDecimals	=9438;

		/* TAG_OrderEntryQuantityDecimals (syntax: uint8) */
		public const ushort TAG_OrderEntryQuantityDecimals	=9439;

		/* TAG_OrderEntryAmountDecimals (syntax: uint8) */
		public const ushort TAG_OrderEntryAmountDecimals	=9440;

		/* TAG_OrderEntryRatioDecimals (syntax: uint8) */
		public const ushort TAG_OrderEntryRatioDecimals	=9441;

		/* TAG_InternalMagic (syntax: [array of 10] String) */
		public const ushort TAG_InternalMagic_0	=9410; // x10
		public const ushort TAG_NB_MAX_InternalMagic=10;

		/* TAG_IndexComponentFOSInstrumentCode (syntax: [array of 5000] uint32) */
		public const ushort TAG_IndexComponentFOSInstrumentCode_0	=20000; // x5000
		public const ushort TAG_NB_MAX_IndexComponentFOSInstrumentCode=5000;

		/* TAG_IndexComponentWeight (syntax: [array of 5000] float64) */
		public const ushort TAG_IndexComponentWeight_0	=40000; // x5000
		public const ushort TAG_NB_MAX_IndexComponentWeight=5000;

		/* TAG_REF_USER (syntax: [array of 1000] String) */
		public const ushort TAG_REF_USER_0	=60000; // x1000
		public const ushort TAG_NB_MAX_REF_USER=1000;

		/* TAG_MARKET_LSE_NormalMarketSize (syntax: float64) */
		public const ushort TAG_MARKET_LSE_NormalMarketSize	=11000;

		/* TAG_MARKET_LSE_SectorCode (syntax: String) */
		public const ushort TAG_MARKET_LSE_SectorCode	=11001;

		/* TAG_MARKET_LSE_SegmentCode (syntax: String) */
		public const ushort TAG_MARKET_LSE_SegmentCode	=11002;

		/* TAG_MARKET_EURONEXT_InstrumentGroupCode (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_InstrumentGroupCode	=11050;

		/* TAG_MARKET_EURONEXT_TypeOfUnitOfExpressionForInstrumentPrice (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_TypeOfUnitOfExpressionForInstrumentPrice	=11051;

		/* TAG_MARKET_EURONEXT_NominalMarketValueOfTheSecurity (syntax: float64) */
		public const ushort TAG_MARKET_EURONEXT_NominalMarketValueOfTheSecurity	=11052;

		/* TAG_MARKET_EURONEXT_QuantityNotation (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_QuantityNotation	=11053;

		/* TAG_MARKET_EURONEXT_IndicatorOfUnderlyingSecurityOnLending (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_IndicatorOfUnderlyingSecurityOnLending	=11054;

		/* TAG_MARKET_EURONEXT_EligibleToPEA (syntax: bool) */
		public const ushort TAG_MARKET_EURONEXT_EligibleToPEA	=11055;

		/* TAG_MARKET_EURONEXT_TypeOfMarketAdmission (syntax: char) */
		public const ushort TAG_MARKET_EURONEXT_TypeOfMarketAdmission	=11056;

		/* TAG_MARKET_EURONEXT_PartitionID (syntax: int32) */
		public const ushort TAG_MARKET_EURONEXT_PartitionID	=11057;

		/* TAG_MARKET_EURONEXT_AvailableMarketMechanisms (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_AvailableMarketMechanisms	=11058;

		/* TAG_MARKET_XETRA_SegmentCode (syntax: String) */
		public const ushort TAG_MARKET_XETRA_SegmentCode	=11100;

		/* TAG_MARKET_XETRA_ISIX (syntax: int) */
		public const ushort TAG_MARKET_XETRA_ISIX	=11101;

		/* TAG_MARKET_XETRA_OptimalGatewayLocation (syntax: String) */
		public const ushort TAG_MARKET_XETRA_OptimalGatewayLocation	=11102;

		/* TAG_MARKET_XETRA_CCP_Eligible (syntax: bool) */
		public const ushort TAG_MARKET_XETRA_CCP_Eligible	=11103;

		/* TAG_MARKET_XETRA_VolumeDiscoveryMinimumExecutedQuantity (syntax: float64) */
		public const ushort TAG_MARKET_XETRA_VolumeDiscoveryMinimumExecutedQuantity	=11104;

		/* TAG_MARKET_XETRA_OrderTypes (syntax: int) */
		public const ushort TAG_MARKET_XETRA_OrderTypes	=11105;

		/* TAG_MARKET_CHIX_LegacyInstrumentCode (syntax: String) */
		public const ushort TAG_MARKET_CHIX_LegacyInstrumentCode	=11150;

		/* TAG_MARKET_OMX_SubSegmentName (syntax: String) */
		public const ushort TAG_MARKET_OMX_SubSegmentName	=11200;

		/* TAG_MARKET_OMX_SegmentName (syntax: String) */
		public const ushort TAG_MARKET_OMX_SegmentName	=11201;

		/* TAG_BPIPE_BSID (syntax: String) */
		public const ushort TAG_BPIPE_BSID	=11250;

		/* TAG_BPIPE_EntitlementId (syntax: String) */
		public const ushort TAG_BPIPE_EntitlementId	=11251;

		/* TAG_BPIPE_SessionType (syntax: char) */
		public const ushort TAG_BPIPE_SessionType	=11252;

		/* TAG_MARKET_TURQUOISE_Ticker (syntax: String) */
		public const ushort TAG_MARKET_TURQUOISE_Ticker	=11300;

		/* TAG_MARKET_SWX_IssuerCountry (syntax: String) */
		public const ushort TAG_MARKET_SWX_IssuerCountry	=11350;

		/* TAG_MARKET_SWX_InstrumentPartitionCode (syntax: int) */
		public const ushort TAG_MARKET_SWX_InstrumentPartitionCode	=11351;

		/* TAG_MARKET_SWX_TradingSessionID (syntax: String) */
		public const ushort TAG_MARKET_SWX_TradingSessionID	=11353;

		/* TAG_MARKET_SWX_ListingStateCode (syntax: String) */
		public const ushort TAG_MARKET_SWX_ListingStateCode	=11354;

		/* TAG_MARKET_SWX_ListingStateDesc (syntax: String) */
		public const ushort TAG_MARKET_SWX_ListingStateDesc	=11355;

		/* TAG_MARKET_MICEX_Remarks (syntax: String) */
		public const ushort TAG_MARKET_MICEX_Remarks	=11400;

		/* TAG_MARKET_MICEX_FaceValue (syntax: float64) */
		public const ushort TAG_MARKET_MICEX_FaceValue	=11401;

		/* TAG_MARKET_MICEX_FaceValueCurrency (syntax: String) */
		public const ushort TAG_MARKET_MICEX_FaceValueCurrency	=11402;

		/* TAG_MARKET_MICEX_QuoteBasis (syntax: String) */
		public const ushort TAG_MARKET_MICEX_QuoteBasis	=11403;

		/* TAG_MARKET_CME_DisplayPricePrimaryDenominator (syntax: int) */
		public const ushort TAG_MARKET_CME_DisplayPricePrimaryDenominator	=11500;

		/* TAG_MARKET_CME_DisplayPriceSecondaryDenominator (syntax: int) */
		public const ushort TAG_MARKET_CME_DisplayPriceSecondaryDenominator	=11501;

		/* TAG_MARKET_CME_DisplayPriceNbOfDecimal (syntax: int) */
		public const ushort TAG_MARKET_CME_DisplayPriceNbOfDecimal	=11502;

		/* TAG_COMSTOCK_IdCode (syntax: int32) */
		public const ushort TAG_COMSTOCK_IdCode	=11550;

		/* TAG_COMSTOCK_MarketIdCode (syntax: int32) */
		public const ushort TAG_COMSTOCK_MarketIdCode	=11551;

		/* TAG_MARKET_ICE_ContractSymbol (syntax: String) */
		public const ushort TAG_MARKET_ICE_ContractSymbol	=11600;

		/* TAG_MARKET_ICE_OffExchangeIncrementQty (syntax: float64) */
		public const ushort TAG_MARKET_ICE_OffExchangeIncrementQty	=11601;

		/* TAG_MARKET_ICE_OffExchangeIncrementPrice (syntax: float64) */
		public const ushort TAG_MARKET_ICE_OffExchangeIncrementPrice	=11602;

		/* TAG_MARKET_ICE_SerialUnderlyingLocalCodeStr (syntax: String) */
		public const ushort TAG_MARKET_ICE_SerialUnderlyingLocalCodeStr	=11603;

		/* TAG_MARKET_ICE_IsBlockOnly (syntax: bool) */
		public const ushort TAG_MARKET_ICE_IsBlockOnly	=11604;

		/* TAG_MARKET_ICE_FlexAllowed (syntax: bool) */
		public const ushort TAG_MARKET_ICE_FlexAllowed	=11605;

		/* TAG_MARKET_RTS_Signs (syntax: int) */
		public const ushort TAG_MARKET_RTS_Signs	=11650;

		/* TAG_MARKET_EUREX_ULTRA_PLUS_ProductComplexType (syntax: int) */
		public const ushort TAG_MARKET_EUREX_ULTRA_PLUS_ProductComplexType	=11670;

		/* TAG_MARKET_EUREX_ULTRA_PLUS_DisseminatedByNTA (syntax: bool) */
		public const ushort TAG_MARKET_EUREX_ULTRA_PLUS_DisseminatedByNTA	=11671;

		/* TAG_MARKET_LIFFE_XDP_StrikePriceDenominator (syntax: int) */
		public const ushort TAG_MARKET_LIFFE_XDP_StrikePriceDenominator	=11700;

		/* TAG_MARKET_LIFFE_XDP_StrikePriceDecimalLocator (syntax: int) */
		public const ushort TAG_MARKET_LIFFE_XDP_StrikePriceDecimalLocator	=11701;

		/* TAG_MARKET_LIFFE_XDP_InstrumentDenominator (syntax: int) */
		public const ushort TAG_MARKET_LIFFE_XDP_InstrumentDenominator	=11702;

		/* TAG_MARKET_HK_ExchangeRate (syntax: float64) */
		public const ushort TAG_MARKET_HK_ExchangeRate	=11710;

		/* TAG_MARKET_HK_HasStampDutyFlag (syntax: bool) */
		public const ushort TAG_MARKET_HK_HasStampDutyFlag	=11711;

		/* TAG_MARKET_JSDA_IssueNameKana (syntax: String) */
		public const ushort TAG_MARKET_JSDA_IssueNameKana	=11720;

		/* TAG_MARKET_MFDS_FundType (syntax: char) */
		public const ushort TAG_MARKET_MFDS_FundType	=11730;

		/* TAG_MARKET_MFDS_FundCode (syntax: char) */
		public const ushort TAG_MARKET_MFDS_FundCode	=11731;

		/* TAG_MARKET_TOCOM_MinVisibleVolume (syntax: uint32) */
		public const ushort TAG_MARKET_TOCOM_MinVisibleVolume	=11740;

		/* TAG_MARKET_TOCOM_AdditionalHiddenVolumeAllowed (syntax: bool) */
		public const ushort TAG_MARKET_TOCOM_AdditionalHiddenVolumeAllowed	=11741;

		/* TAG_MARKET_OMX_NORDIC_NoteCodes (syntax: int32) */
		public const ushort TAG_MARKET_OMX_NORDIC_NoteCodes	=11750;
            /// ***GENERATOR-end-REF_TAGS-
            #endregion

            #region Constructor
            private TagsReferential() { }
            #endregion
        }
    }
}
