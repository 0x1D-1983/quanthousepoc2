///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketSheetEntry
        {
            #region Constructor
            public MarketSheetEntry(string orderID, double Price, double Qty)
            {
                m_OrderID = orderID;
                m_Price = Price;
                m_Qty = Qty;
            }
            #endregion

            #region Properties
            public string OrderID
            {
                get { return m_OrderID; }
                set { m_OrderID = value; }
            }
            public double Price
            {
                get { return m_Price; }
            }
            public double Qty
            {
                get { return m_Qty; }
                set { m_Qty = value; }
            }
            #endregion

            #region Members
            protected string m_OrderID;
            protected double m_Price;
            protected double m_Qty;
            #endregion
        }

        public class MarketSheetEntryAndContext
        {
            public MarketSheetEntryAndContext(MarketSheetEntry order, List<TagNumAndValue> context)
            {
                m_Order = order;
                m_Context = context;
            }

            #region Properties
            public MarketSheetEntry Order
            {
                get { return m_Order; }
                set { m_Order = value; }
            }
            public List<TagNumAndValue> Context
            {
                get { return m_Context; }
                set { m_Context = value; }
            }
            #endregion

            #region Members
            protected MarketSheetEntry m_Order;
            protected List<TagNumAndValue> m_Context;
            #endregion
        }


        public class MarketSheet
        {
            public MarketSheet(List<MarketSheetEntryAndContext> bidSide, List<MarketSheetEntryAndContext> askSide)
            {
                m_BidSide = bidSide;
                m_AskSide = askSide;
            }

            #region Properties
            public List<MarketSheetEntryAndContext> BidSide
            {
                get { return m_BidSide; }
                set { m_BidSide = value; }
            }
            public List<MarketSheetEntryAndContext> AskSide
            {
                get { return m_AskSide; }
                set { m_AskSide = value; }
            }
            #endregion

            #region Members
            protected List<MarketSheetEntryAndContext> m_BidSide;
            protected List<MarketSheetEntryAndContext> m_AskSide;
            #endregion
        }
    }
}
