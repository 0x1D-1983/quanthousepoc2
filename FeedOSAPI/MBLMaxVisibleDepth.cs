///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2010 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MBLMaxVisibleDepth
        {
            #region Constructor
            public MBLMaxVisibleDepth(uint instrumentCode, uint layerId, FOSOrderBookLevel maxVisibleDepth) 
            {
                m_InstrumentCode = instrumentCode;
                m_LayerId = layerId;
                m_MaxVisibleDepth = maxVisibleDepth;
            }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public uint LayerId
            {
                get { return m_LayerId; }
            }
            public FOSOrderBookLevel MaxVisibleDepth 
            {
                get { return m_MaxVisibleDepth; }
            }
            #endregion

            #region Members
            protected uint  m_InstrumentCode;
            protected uint  m_LayerId;
            protected FOSOrderBookLevel m_MaxVisibleDepth;
            #endregion
        }
    }
}
