///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------

namespace FeedOSAPI
{
    namespace Types
    {
        public class TagsContext
        {
            #region Context Tags definitions
            /// ***GENERATOR-begin-CONTEXT_TAGS-

		/* TAG_Text (syntax: String) */
		public const ushort TAG_Text	=58;

		/* TAG_TradeCondition (syntax: String) */
		public const ushort TAG_TradeCondition	=277;

		/* TAG_Buyer (syntax: String) */
		public const ushort TAG_Buyer	=288;

		/* TAG_Seller (syntax: String) */
		public const ushort TAG_Seller	=289;

		/* TAG_Scope (syntax: char) */
		public const ushort TAG_Scope	=546;

		/* TAG_TradeID (syntax: String) */
		public const ushort TAG_TradeID	=1003;

		/* TAG_OriginFOSMarketIdOf_LastPrice (syntax: uint16) */
		public const ushort TAG_OriginFOSMarketIdOf_LastPrice	=9350;

		/* TAG_OriginOf_LastPrice (syntax: String) */
		public const ushort TAG_OriginOf_LastPrice	=9351;

		/* TAG_OriginFOSMarketIdOf_BestBid (syntax: uint16) */
		public const ushort TAG_OriginFOSMarketIdOf_BestBid	=9352;

		/* TAG_OriginOf_BestBid (syntax: String) */
		public const ushort TAG_OriginOf_BestBid	=9353;

		/* TAG_OriginFOSMarketIdOf_BestAsk (syntax: uint16) */
		public const ushort TAG_OriginFOSMarketIdOf_BestAsk	=9354;

		/* TAG_OriginOf_BestAsk (syntax: String) */
		public const ushort TAG_OriginOf_BestAsk	=9355;

		/* TAG_OriginOf_Quote (syntax: String) */
		public const ushort TAG_OriginOf_Quote	=9906;

		/* TAG_AggressorSide (syntax: char) */
		public const ushort TAG_AggressorSide	=9356;

		/* TAG_OrderModificationReason (syntax: Enum) */
		public const ushort TAG_OrderModificationReason	=9357;

		/* TAG_TradeConditionsDictionaryKey (syntax: uint32) */
		public const ushort TAG_TradeConditionsDictionaryKey	=9358;

		/* TAG_NbOfBuyOrdersTraded (syntax: int32) */
		public const ushort TAG_NbOfBuyOrdersTraded	=9359;

		/* TAG_NbOfSellOrdersTraded (syntax: int32) */
		public const ushort TAG_NbOfSellOrdersTraded	=9360;

		/* TAG_PegOrderLimitPrice (syntax: float64) */
		public const ushort TAG_PegOrderLimitPrice	=9361;

		/* TAG_HiddenLiquidityFlag (syntax: bool) */
		public const ushort TAG_HiddenLiquidityFlag	=9362;

		/* TAG_TargetMBLLayerId (syntax: uint32) */
		public const ushort TAG_TargetMBLLayerId	=9363;

		/* TAG_RawSequenceNumber (syntax: uint32) */
		public const ushort TAG_RawSequenceNumber	=9365;

		/* TAG_MMTFlagsV2 (syntax: String) */
		public const ushort TAG_MMTFlagsV2	=9901;

		/* TAG_MMTFlagsV3 (syntax: String) */
		public const ushort TAG_MMTFlagsV3	=9904;

		/* TAG_MiFID_TradePrice (syntax: String) */
		public const ushort TAG_MiFID_TradePrice	=9907;

		/* TAG_MiFID_TradeQty (syntax: String) */
		public const ushort TAG_MiFID_TradeQty	=9908;

		/* TAG_TradeImpactIndicator (syntax: uint32) */
		public const ushort TAG_TradeImpactIndicator	=9902;

		/* TAG_TradingVenue (syntax: uint16) */
		public const ushort TAG_TradingVenue	=9903;

		/* TAG_TradeExecutionVenue (syntax: String) */
		public const ushort TAG_TradeExecutionVenue	=9909;

		/* TAG_TradePublicationVenue (syntax: String) */
		public const ushort TAG_TradePublicationVenue	=9910;

		/* TAG_TradeExecutionTimestamp (syntax: Timestamp) */
		public const ushort TAG_TradeExecutionTimestamp	=9905;

		/* TAG_ActualTradingSessionId (syntax: int8) */
		public const ushort TAG_ActualTradingSessionId	=9375;

		/* TAG_CancelledOrModifiedTradingSessionId (syntax: int8) */
		public const ushort TAG_CancelledOrModifiedTradingSessionId	=9376;

		/* TAG_MARKET_TradeType (syntax: uint32) */
		public const ushort TAG_MARKET_TradeType	=9261;

		/* TAG_AuctionOnDemand_TradingStatus (syntax: Enum) */
		public const ushort TAG_AuctionOnDemand_TradingStatus	=9911;

		/* TAG_AuctionOnDemand_Start (syntax: bool) */
		public const ushort TAG_AuctionOnDemand_Start	=9912;

		/* TAG_COMSTOCK_TOKENS_FIRST (syntax: String) */
		public const ushort TAG_COMSTOCK_TOKENS_FIRST	=30000;

		/* TAG_COMSTOCK_ENUM_SRC_ID (syntax: String) */
		public const ushort TAG_COMSTOCK_ENUM_SRC_ID	=30003;

		/* TAG_COMSTOCK_QUOTE_COND_1 (syntax: String) */
		public const ushort TAG_COMSTOCK_QUOTE_COND_1	=32000;

		/* TAG_COMSTOCK_f79 (syntax: String) */
		public const ushort TAG_COMSTOCK_f79	=32500;

		/* TAG_COMSTOCK_FUND_INDEX_MEMBERSHIP (syntax: String) */
		public const ushort TAG_COMSTOCK_FUND_INDEX_MEMBERSHIP	=33137;

		/* TAG_COMSTOCK_TOKENS_LAST (syntax: String) */
		public const ushort TAG_COMSTOCK_TOKENS_LAST	=39999;

		/* TAG_MARKET_LSE_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_TradeTypeIndicator	=15000;

		/* TAG_MARKET_LSE_UncrossingVolume (syntax: float64) */
		public const ushort TAG_MARKET_LSE_UncrossingVolume	=15001;

		/* TAG_MARKET_LSE_BargainConditionIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_BargainConditionIndicator	=15002;

		/* TAG_MARKET_LSE_TradeTimeIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_TradeTimeIndicator	=15003;

		/* TAG_MARKET_LSE_ConvertedPriceIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_ConvertedPriceIndicator	=15004;

		/* TAG_MARKET_EURONEXT_CrossOrderIndicator (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_CrossOrderIndicator	=15050;

		/* TAG_MARKET_EURONEXT_TheoreticalOpeningVolume (syntax: float64) */
		public const ushort TAG_MARKET_EURONEXT_TheoreticalOpeningVolume	=15051;

		/* TAG_MARKET_EURONEXT_OrderPriorityTimestamp (syntax: Timestamp) */
		public const ushort TAG_MARKET_EURONEXT_OrderPriorityTimestamp	=15052;

		/* TAG_MARKET_EURONEXT_TradeOffExchangeFlag (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_TradeOffExchangeFlag	=15053;

		/* TAG_MARKET_EURONEXT_TradingVenue (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_TradingVenue	=15054;

		/* TAG_MARKET_EURONEXT_OpeningTradeIndicator (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_OpeningTradeIndicator	=15055;

		/* TAG_MARKET_EURONEXT_TradeTypeOfOperation (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_TradeTypeOfOperation	=15056;

		/* TAG_MARKET_EURONEXT_MarketMechanism (syntax: uint32) */
		public const ushort TAG_MARKET_EURONEXT_MarketMechanism	=15057;

		/* TAG_MARKET_CME_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_CME_TradeTypeIndicator	=15100;

		/* TAG_MARKET_CME_MatchEventIndicator (syntax: String) */
		public const ushort TAG_MARKET_CME_MatchEventIndicator	=15101;

		/* TAG_MARKET_CME_MsgSeqNum (syntax: uint32) */
		public const ushort TAG_MARKET_CME_MsgSeqNum	=15102;

		/* TAG_MARKET_LIFFE_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_LIFFE_TradeTypeIndicator	=15200;

		/* TAG_MARKET_LIFFE_XDP_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_LIFFE_XDP_TradeTypeIndicator	=15201;

		/* TAG_BPIPE_TradeAction (syntax: String) */
		public const ushort TAG_BPIPE_TradeAction	=15250;

		/* TAG_BPIPE_TradeCondition (syntax: String) */
		public const ushort TAG_BPIPE_TradeCondition	=15251;

		/* TAG_BPIPE_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_BPIPE_TradeTypeIndicator	=15252;

		/* TAG_BPIPE_QuoteConditionBid (syntax: String) */
		public const ushort TAG_BPIPE_QuoteConditionBid	=15253;

		/* TAG_BPIPE_QuoteConditionAsk (syntax: String) */
		public const ushort TAG_BPIPE_QuoteConditionAsk	=15254;

		/* TAG_BPIPE_QuoteCondition (syntax: String) */
		public const ushort TAG_BPIPE_QuoteCondition	=15255;

		/* TAG_MARKET_TURQUOISE_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_TURQUOISE_TradeTypeIndicator	=15300;

		/* TAG_MARKET_TURQUOISE_LastAuctionQty (syntax: float64) */
		public const ushort TAG_MARKET_TURQUOISE_LastAuctionQty	=15301;

		/* TAG_MARKET_TURQUOISE_EndOfAuctionTime (syntax: Timestamp) */
		public const ushort TAG_MARKET_TURQUOISE_EndOfAuctionTime	=15302;

		/* TAG_MARKET_TURQUOISE_TradeId (syntax: String) */
		public const ushort TAG_MARKET_TURQUOISE_TradeId	=15303;

		/* TAG_MARKET_TURQUOISE_DealId (syntax: String) */
		public const ushort TAG_MARKET_TURQUOISE_DealId	=15304;

		/* TAG_MARKET_CEF_IndexTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_CEF_IndexTypeIndicator	=15150;

		/* TAG_MARKET_CEF_LastAuctionQty (syntax: float64) */
		public const ushort TAG_MARKET_CEF_LastAuctionQty	=15151;

		/* TAG_MARKET_CEF_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_CEF_TradeTypeIndicator	=15400;

		/* TAG_MARKET_SWX_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_SWX_TradeTypeIndicator	=15450;

		/* TAG_MARKET_SWX_LastAuctionQty (syntax: float64) */
		public const ushort TAG_MARKET_SWX_LastAuctionQty	=15451;

		/* TAG_MARKET_SWX_TradeOffExchangeFlag (syntax: String) */
		public const ushort TAG_MARKET_SWX_TradeOffExchangeFlag	=15452;

		/* TAG_MARKET_SWX_TradingPhase (syntax: String) */
		public const ushort TAG_MARKET_SWX_TradingPhase	=15453;

		/* TAG_MARKET_ICE_BlockTradeType (syntax: String) */
		public const ushort TAG_MARKET_ICE_BlockTradeType	=15501;

		/* TAG_MARKET_ICE_SystemPricedLegType (syntax: String) */
		public const ushort TAG_MARKET_ICE_SystemPricedLegType	=15502;

		/* TAG_MARKET_ICE_SequenceWithinMillis (syntax: int32) */
		public const ushort TAG_MARKET_ICE_SequenceWithinMillis	=15503;

		/* TAG_MARKET_ICE_IsImplied (syntax: bool) */
		public const ushort TAG_MARKET_ICE_IsImplied	=15504;

		/* TAG_MARKET_BOVESPA_OrderPriorityTimestamp (syntax: Timestamp) */
		public const ushort TAG_MARKET_BOVESPA_OrderPriorityTimestamp	=15550;

		/* TAG_MARKET_BOVESPA_OriginOfTrade (syntax: String) */
		public const ushort TAG_MARKET_BOVESPA_OriginOfTrade	=15551;

		/* TAG_MARKET_BME_IndexTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_BME_IndexTypeIndicator	=15600;

		/* TAG_MARKET_BME_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_BME_TradeTypeIndicator	=15602;

		/* TAG_MARKET_NASDAQ_UTP_SaleCondition (syntax: String) */
		public const ushort TAG_MARKET_NASDAQ_UTP_SaleCondition	=15650;

		/* TAG_MARKET_NASDAQ_UTP_QuoteCondition (syntax: char) */
		public const ushort TAG_MARKET_NASDAQ_UTP_QuoteCondition	=15651;

		/* TAG_MARKET_NASDAQ_UTP_SubMarketCenterId (syntax: char) */
		public const ushort TAG_MARKET_NASDAQ_UTP_SubMarketCenterId	=15652;

		/* TAG_MARKET_CTA_SaleCondition (syntax: String) */
		public const ushort TAG_MARKET_CTA_SaleCondition	=15700;

		/* TAG_MARKET_CTA_QuoteCondition (syntax: char) */
		public const ushort TAG_MARKET_CTA_QuoteCondition	=15701;

		/* TAG_MARKET_BURGUNDY_SubTypeOfTrade (syntax: String) */
		public const ushort TAG_MARKET_BURGUNDY_SubTypeOfTrade	=15750;

		/* TAG_MARKET_EUREX_ULTRA_PLUS_TradeType (syntax: String) */
		public const ushort TAG_MARKET_EUREX_ULTRA_PLUS_TradeType	=15800;

		/* TAG_MARKET_EUREX_ULTRA_PLUS_TradeIndicator (syntax: String) */
		public const ushort TAG_MARKET_EUREX_ULTRA_PLUS_TradeIndicator	=15801;

		/* TAG_MARKET_EUREX_ULTRA_PLUS_StrategyTradeIndicator (syntax: String) */
		public const ushort TAG_MARKET_EUREX_ULTRA_PLUS_StrategyTradeIndicator	=15802;

		/* TAG_MARKET_NASDAQ_OMX_EU_TradeType (syntax: String) */
		public const ushort TAG_MARKET_NASDAQ_OMX_EU_TradeType	=15850;

		/* TAG_MARKET_XETRA_ULTRA_PLUS_TradeType (syntax: String) */
		public const ushort TAG_MARKET_XETRA_ULTRA_PLUS_TradeType	=15900;

		/* TAG_MARKET_XETRA_ULTRA_PLUS_TradeTypeIndicator (syntax: char) */
		public const ushort TAG_MARKET_XETRA_ULTRA_PLUS_TradeTypeIndicator	=15901;

		/* TAG_MARKET_LSE_MIT_OffBookReportingTradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_MIT_OffBookReportingTradeTypeIndicator	=15950;

		/* TAG_MARKET_LSE_MIT_AuctionTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_MIT_AuctionTypeIndicator	=15951;

		/* TAG_MARKET_LSE_MIT_FirmQuote (syntax: bool) */
		public const ushort TAG_MARKET_LSE_MIT_FirmQuote	=15952;

		/* TAG_MARKET_LSE_MIT_CrossType (syntax: char) */
		public const ushort TAG_MARKET_LSE_MIT_CrossType	=15953;

		/* TAG_MARKET_BVMF_TradeTypeIndicator (syntax: String) */
		public const ushort TAG_MARKET_BVMF_TradeTypeIndicator	=16000;

		/* TAG_MARKET_BVMF_MsgSeqNum (syntax: uint32) */
		public const ushort TAG_MARKET_BVMF_MsgSeqNum	=16001;

		/* TAG_MARKET_OSAKA_TradeCondition (syntax: String) */
		public const ushort TAG_MARKET_OSAKA_TradeCondition	=16050;

		/* TAG_MARKET_OSAKA_TradeSource (syntax: String) */
		public const ushort TAG_MARKET_OSAKA_TradeSource	=16051;

		/* TAG_MARKET_OSAKA_JNetTradingType (syntax: String) */
		public const ushort TAG_MARKET_OSAKA_JNetTradingType	=16052;

		/* TAG_MARKET_OMX_NORDIC_OrderReferenceNumber (syntax: uint32) */
		public const ushort TAG_MARKET_OMX_NORDIC_OrderReferenceNumber	=16100;

		/* TAG_MARKET_OMX_NORDIC_TradeType (syntax: char) */
		public const ushort TAG_MARKET_OMX_NORDIC_TradeType	=16101;

		/* TAG_MARKET_OMX_NORDIC_NumberOfTrades (syntax: uint32) */
		public const ushort TAG_MARKET_OMX_NORDIC_NumberOfTrades	=16102;

		/* TAG_MARKET_OMX_NORDIC_CrossType (syntax: char) */
		public const ushort TAG_MARKET_OMX_NORDIC_CrossType	=16103;

		/* TAG_MARKET_OMX_NORDIC_ReportedTradeType (syntax: char) */
		public const ushort TAG_MARKET_OMX_NORDIC_ReportedTradeType	=16104;

		/* TAG_MARKET_BATS_ExecutionType (syntax: String) */
		public const ushort TAG_MARKET_BATS_ExecutionType	=16150;

		/* TAG_MARKET_BATS_TradeReportFlags (syntax: int) */
		public const ushort TAG_MARKET_BATS_TradeReportFlags	=16151;

		/* TAG_MARKET_TMX_CrossType (syntax: char) */
		public const ushort TAG_MARKET_TMX_CrossType	=16170;

		/* TAG_MARKET_TMX_StockState (syntax: String) */
		public const ushort TAG_MARKET_TMX_StockState	=16171;

		/* TAG_MARKET_TMX_SettlementTerms (syntax: char) */
		public const ushort TAG_MARKET_TMX_SettlementTerms	=16172;

		/* TAG_MARKET_TMX_GroupStatus (syntax: String) */
		public const ushort TAG_MARKET_TMX_GroupStatus	=16173;

		/* TAG_MARKET_ADH_TradeType (syntax: String) */
		public const ushort TAG_MARKET_ADH_TradeType	=16200;

		/* TAG_MARKET_DIRECTEDGE_HiddenLiquidityFlag (syntax: bool) */
		public const ushort TAG_MARKET_DIRECTEDGE_HiddenLiquidityFlag	=16220;

		/* TAG_MARKET_HOTSPOT_MinQty (syntax: float64) */
		public const ushort TAG_MARKET_HOTSPOT_MinQty	=16250;

		/* TAG_MARKET_HOTSPOT_LotSize (syntax: float64) */
		public const ushort TAG_MARKET_HOTSPOT_LotSize	=16251;

		/* TAG_MARKET_RTS_TradeStatusSellSide (syntax: uint32) */
		public const ushort TAG_MARKET_RTS_TradeStatusSellSide	=16270;

		/* TAG_MARKET_RTS_TradeStatusBuySide (syntax: uint32) */
		public const ushort TAG_MARKET_RTS_TradeStatusBuySide	=16271;

		/* TAG_MARKET_CHIX_ExecutionType (syntax: String) */
		public const ushort TAG_MARKET_CHIX_ExecutionType	=16300;

		/* TAG_MARKET_JSE_MIT_AuctionTypeIndicator (syntax: char) */
		public const ushort TAG_MARKET_JSE_MIT_AuctionTypeIndicator	=16320;

		/* TAG_MARKET_MILAN_MIT_AuctionTypeIndicator (syntax: char) */
		public const ushort TAG_MARKET_MILAN_MIT_AuctionTypeIndicator	=16350;

		/* TAG_MARKET_BPOD_PreviousBidPrice (syntax: float64) */
		public const ushort TAG_MARKET_BPOD_PreviousBidPrice	=16370;

		/* TAG_MARKET_BPOD_PreviousAskPrice (syntax: float64) */
		public const ushort TAG_MARKET_BPOD_PreviousAskPrice	=16371;

		/* TAG_MARKET_OPRA_QuodCondition (syntax: char) */
		public const ushort TAG_MARKET_OPRA_QuodCondition	=16380;

		/* TAG_MARKET_OPRA_TradeCondition (syntax: char) */
		public const ushort TAG_MARKET_OPRA_TradeCondition	=16381;

		/* TAG_MARKET_CMA_QuoteType (syntax: uint16) */
		public const ushort TAG_MARKET_CMA_QuoteType	=16280;

		/* TAG_MARKET_CMA_QuoteStatus (syntax: uint16) */
		public const ushort TAG_MARKET_CMA_QuoteStatus	=16281;

		/* TAG_MARKET_MONTREAL_TradeCondition (syntax: char) */
		public const ushort TAG_MARKET_MONTREAL_TradeCondition	=16290;

		/* TAG_MARKET_TSE_BidQuoteCondition (syntax: char) */
		public const ushort TAG_MARKET_TSE_BidQuoteCondition	=16390;

		/* TAG_MARKET_TSE_AskQuoteCondition (syntax: char) */
		public const ushort TAG_MARKET_TSE_AskQuoteCondition	=16391;

		/* TAG_MARKET_TSE_TostnetPriceCode (syntax: uint8) */
		public const ushort TAG_MARKET_TSE_TostnetPriceCode	=16392;

		/* TAG_MARKET_TSE_TostnetTransactionFlag (syntax: char) */
		public const ushort TAG_MARKET_TSE_TostnetTransactionFlag	=16393;

		/* TAG_MARKET_TOCOM_TradeCondition (syntax: uint16) */
		public const ushort TAG_MARKET_TOCOM_TradeCondition	=16400;

		/* TAG_MARKET_TOCOM_TradeSource (syntax: uint16) */
		public const ushort TAG_MARKET_TOCOM_TradeSource	=16401;

		/* TAG_MARKET_ULTRAFEED_QuoteCondition (syntax: uint8) */
		public const ushort TAG_MARKET_ULTRAFEED_QuoteCondition	=16410;

		/* TAG_MARKET_OMX_TradeCondition (syntax: uint8) */
		public const ushort TAG_MARKET_OMX_TradeCondition	=16420;

		/* TAG_MARKET_OMX_TradeSource (syntax: uint16) */
		public const ushort TAG_MARKET_OMX_TradeSource	=16421;

		/* TAG_MARKET_SET_MatchType (syntax: String) */
		public const ushort TAG_MARKET_SET_MatchType	=16422;
            /// ***GENERATOR-end-CONTEXT_TAGS-
            #endregion

            #region Constructor
            private TagsContext() { }
            #endregion
         }
    }
}
