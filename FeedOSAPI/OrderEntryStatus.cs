///-------------------------------
/// FeedOS C# Client API
/// copyright QuantHouse 2003-2017
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class OrderEntryStatus
        {
            public const sbyte OrderEntryDisabled	= 0;
            public const sbyte OrderModifyDisabled	= 1;
            public const sbyte OrderCancelDisabled	= 2;
        }

        public class OrderEntryStatusBit
        {
            public const sbyte OrderEntryDisabled	= 1 << OrderEntryStatus.OrderEntryDisabled;
            public const sbyte OrderModifyDisabled	= 1 << OrderEntryStatus.OrderModifyDisabled;
            public const sbyte OrderCancelDisabled	= 1 << OrderEntryStatus.OrderCancelDisabled;
        }

        public class OrderEntryStatusHelper
        {
            public static string PrintOrderEntryStatus(sbyte order_entry_status)
            {
                string os = "";

                bool previous_set = false;
                if ((order_entry_status & OrderEntryStatusBit.OrderEntryDisabled) == OrderEntryStatusBit.OrderEntryDisabled)
                {
                    os += "Order Entry Disabled";
                    previous_set = true;
                }
                if ((order_entry_status & OrderEntryStatusBit.OrderModifyDisabled) == OrderEntryStatusBit.OrderModifyDisabled)
                {
                    if (previous_set)
                        os += ", ";
                    os += "Order Modify Disabled";
                    previous_set = true;
                }
                if ((order_entry_status & OrderEntryStatusBit.OrderCancelDisabled) == OrderEntryStatusBit.OrderCancelDisabled)
                {
                    if (previous_set)
                        os += ", ";
                    os += "Order Cancel Disabled";
                }

                return os;
            }
        }
    }
}