///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class OrderBookBestLimitsExt
        {
            #region Constructor
            public OrderBookBestLimitsExt(OrderBookEntryExt BestBid, OrderBookEntryExt BestAsk)
            {
                m_BestBid = BestBid;
                m_BestAsk = BestAsk;
            }
            #endregion

            #region Properties
            public OrderBookEntryExt BestBid
            {
                get { return m_BestBid; }
                set { m_BestBid = value; }
            }
            public OrderBookEntryExt BestAsk
            {
                get { return m_BestAsk; }
                set { m_BestAsk = value; }
            }
            #endregion

            #region Members
            protected OrderBookEntryExt m_BestBid;
            protected OrderBookEntryExt m_BestAsk;
            #endregion
        }

    }
}
