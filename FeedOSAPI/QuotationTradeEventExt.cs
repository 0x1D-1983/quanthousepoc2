///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class QuotationContentMaskHelper
        {
            public static uint QuotationContentBit_TradingStatus = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_TradingStatus;
            public static uint QuotationContentBit_Bid = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Bid;
            public static uint QuotationContentBit_Ask = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Ask;
            public static uint QuotationContentBit_LastPrice = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_LastPrice;
            public static uint QuotationContentBit_LastTradeQty = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_LastTradeQty;
            public static uint QuotationContentBit_Open = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Open;
            public static uint QuotationContentBit_Close = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Close;
            public static uint QuotationContentBit_High = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_High;
            public static uint QuotationContentBit_Low = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Low;
            public static uint QuotationContentBit_OCHL_daily = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OCHL_daily;
            public static uint QuotationContentBit_OtherValues = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OtherValues;
            public static uint QuotationContentBit_OpeningNextCalendarDay
                                                                        = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OpeningNextCalendarDay;
            public static uint QuotationContentBit_Context = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Context;
            public static uint QuotationContentBit_OffBookTrade = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OffBookTrade;
            public static uint QuotationContentBit_ChangeBusinessDay = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_ChangeBusinessDay;
            public static uint QuotationContentBit_Session = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Session;
            public static uint QuotationContentBit_BidOrAsk = QuotationContentBit_Bid | QuotationContentBit_Ask;

            public static bool contains(uint contentMask, uint mask)
            {
                return (mask == (contentMask & mask));
            }

            public static void PrintContentMask(uint contentMask, bool humanReadable)
            {
                if (humanReadable)
                {
                    bool is_daily = contains(contentMask, QuotationContentBit_OCHL_daily);

                    if (contains(contentMask, QuotationContentBit_Open)) { if (is_daily) { Console.Write('O'); } else { Console.Write('o'); } }
                    if (contains(contentMask, QuotationContentBit_Close)) { if (is_daily) { Console.Write('C'); } else { Console.Write('c'); } }
                    if (contains(contentMask, QuotationContentBit_High)) { if (is_daily) { Console.Write('H'); } else { Console.Write('h'); } }
                    if (contains(contentMask, QuotationContentBit_Low)) { if (is_daily) { Console.Write('L'); } else { Console.Write('l'); } }
                    if (contains(contentMask, QuotationContentBit_TradingStatus)) { Console.Write('s'); }
                    if (contains(contentMask, QuotationContentBit_Bid)) { Console.Write('b'); }
                    if (contains(contentMask, QuotationContentBit_Ask)) { Console.Write('a'); }
                    if (contains(contentMask, QuotationContentBit_LastPrice)) { Console.Write('p'); }
                    if (contains(contentMask, QuotationContentBit_LastTradeQty)) { Console.Write('q'); }
                    if (contains(contentMask, QuotationContentBit_OCHL_daily)) { Console.Write('d'); }
                    if (contains(contentMask, QuotationContentBit_OtherValues)) { Console.Write('v'); }
                    if (contains(contentMask, QuotationContentBit_OpeningNextCalendarDay)) { Console.Write('n'); }
                    if (contains(contentMask, QuotationContentBit_Context)) { Console.Write('x'); }
                    if (contains(contentMask, QuotationContentBit_OffBookTrade)) { Console.Write('f'); }
                    if (contains(contentMask, QuotationContentBit_ChangeBusinessDay)) { Console.Write('y'); }
                    if (contains(contentMask, QuotationContentBit_Session)) { Console.Write('e'); }
                }
                else
                {
                    Console.Write(contentMask);
                }
            }

            public static void PrintContentMask(uint contentMask)
            {
                if (contains(contentMask, QuotationContentBit_Open)) { Console.Write(" Open"); }
                if (contains(contentMask, QuotationContentBit_Close)) { Console.Write(" Close"); }
                if (contains(contentMask, QuotationContentBit_High)) { Console.Write(" High"); }
                if (contains(contentMask, QuotationContentBit_Low)) { Console.Write(" Low"); }
                if (contains(contentMask, QuotationContentBit_TradingStatus)) { Console.Write(" TradingStatus"); }
                if (contains(contentMask, QuotationContentBit_Bid)) { Console.Write(" Bid"); }
                if (contains(contentMask, QuotationContentBit_Ask)) { Console.Write(" Ask"); }
                if (contains(contentMask, QuotationContentBit_LastPrice)) { Console.Write(" LastPrice"); }
                if (contains(contentMask, QuotationContentBit_LastTradeQty)) { Console.Write(" LastTradeQty"); }
                if (contains(contentMask, QuotationContentBit_OCHL_daily)) { Console.Write(" OCHL_daily"); }
                if (contains(contentMask, QuotationContentBit_OtherValues)) { Console.Write(" OtherValues"); }
                if (contains(contentMask, QuotationContentBit_OpeningNextCalendarDay)) { Console.Write(" OpeningNextCalendarDay"); }
                if (contains(contentMask, QuotationContentBit_Context)) { Console.Write(" Context"); }
                if (contains(contentMask, QuotationContentBit_OffBookTrade)) { Console.Write(" OffBookTrade"); }
                if (contains(contentMask, QuotationContentBit_ChangeBusinessDay)) { Console.Write(" ChangeBusinessDay"); }
                if (contains(contentMask, QuotationContentBit_Session)) { Console.Write(" Session"); }
            }
        }

        public class TradeImpactIndicatorHelper
        {
            public static uint TradeImpactIndicatorBit_HasOpen	= 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HasOpen;
            public static uint TradeImpactIndicatorBit_HasHighLow = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HasHighLow;
            public static uint TradeImpactIndicatorBit_HasLast = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HasLast;
            public static uint TradeImpactIndicatorBit_HasVolume = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HasVolume;
            public static uint TradeImpactIndicatorBit_HasOffBookVolume = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HasOffBookVolume;
            public static uint TradeImpactIndicatorBit_HasVWAP = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HasVWAP;
            public static uint TradeImpactIndicatorBit_Open = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_Open;
            public static uint TradeImpactIndicatorBit_HighLow = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_HighLow;
            public static uint TradeImpactIndicatorBit_Last = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_Last;
            public static uint TradeImpactIndicatorBit_Volume = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_Volume;
            public static uint TradeImpactIndicatorBit_OffBookVolume = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_OffBookVolume;
            public static uint TradeImpactIndicatorBit_VWAP = 1 << (int)QuotationTradeImpactIndicator.QuotationTradeImpactIndicator_VWAP;

            public static bool contains(uint contentMask, uint mask)
            {
                return (mask == (contentMask & mask));
            }

            public static void PrintTradeImpactIndicator(uint tii, bool humanReadable)
            {
                if (humanReadable)
                {
                    if (contains(tii, TradeImpactIndicatorBit_HasOpen)) { Console.Write(" HasOpen"); }
                    if (contains(tii, TradeImpactIndicatorBit_HasHighLow)) { Console.Write(" HasHighLow"); }
                    if (contains(tii, TradeImpactIndicatorBit_HasLast)) { Console.Write(" HasLast"); }
                    if (contains(tii, TradeImpactIndicatorBit_HasVolume)) { Console.Write(" HasVolume"); }
                    if (contains(tii, TradeImpactIndicatorBit_HasOffBookVolume)) { Console.Write(" HasOffBookVolume"); }
                    if (contains(tii, TradeImpactIndicatorBit_HasVWAP)) { Console.Write(" HasVWAP"); }
                    if (contains(tii, TradeImpactIndicatorBit_Open)) { Console.Write(" Open"); }
                    if (contains(tii, TradeImpactIndicatorBit_HighLow)) { Console.Write(" HighLow"); }
                    if (contains(tii, TradeImpactIndicatorBit_Last)) { Console.Write(" Last"); }
                    if (contains(tii, TradeImpactIndicatorBit_Volume)) { Console.Write(" Volume"); }
                    if (contains(tii, TradeImpactIndicatorBit_OffBookVolume)) { Console.Write(" OffBookVolume"); }
                    if (contains(tii, TradeImpactIndicatorBit_VWAP)) { Console.Write(" VWAP"); }
                }
                else
                {
                    Console.Write(tii);
                }
            }
        }

        public class QuotationTradeEventExt
        {
            #region Members
            protected uint m_ContentMask;
            protected OrderBookEntryExt m_BestBid;
            protected OrderBookEntryExt m_BestAsk;
            protected double m_LastTradeQty;
            protected double m_Price;
            protected List<TagNumAndValue> m_Context;
            protected List<TagNumAndValue> m_Values;
            #endregion

            #region Constructors
            public QuotationTradeEventExt(uint contentMask, OrderBookEntryExt BestBid, OrderBookEntryExt BestAsk,
                double LastTradeQty, double Price, List<TagNumAndValue> Context, List<TagNumAndValue> Values)
            {
                m_ContentMask = contentMask;
                m_BestBid = BestBid;
                m_BestAsk = BestAsk;
                m_LastTradeQty = LastTradeQty;
                m_Price = Price;
                m_Context = Context;
                m_Values = Values;
            }

            public QuotationTradeEventExt()
            {
                m_ContentMask = 0;
                m_BestBid = new OrderBookEntryExt(0.0, 0.0, 0);
                m_BestAsk = new OrderBookEntryExt(0.0, 0.0, 0);
                m_LastTradeQty = 0.0;
                m_Price = 0.0;
                m_Context = new List<TagNumAndValue>();
                m_Values = new List<TagNumAndValue>();
            }
            #endregion

            #region Properties
            public uint ContentMask
            {
                get { return m_ContentMask; }
                set { m_ContentMask = value; }
            }
            public OrderBookEntryExt BestBid
            {
                get { return m_BestBid; }
                set { m_BestBid = value; }
            }
            public OrderBookEntryExt BestAsk
            {
                get { return m_BestAsk; }
                set { m_BestAsk = value; }
            }
            public double LastTradeQty
            {
                get { return m_LastTradeQty; }
                set { m_LastTradeQty = value; }
            }
            public double Price
            {
                get { return m_Price; }
                set { m_Price = value; }
            }
            public List<TagNumAndValue> Context
            {
                get { return m_Context; }
                set { m_Context = value; }
            }
            public List<TagNumAndValue> Values
            {
                get { return m_Values; }
                set { m_Values = value; }
            }
            #endregion
        }
    }
}
