///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {

        public class OrderBookEntry
        {
            public const int Orderbook_Nb_Orders_Unknown = -1;

            #region Constructor
            public OrderBookEntry(double Price, double Size, int NbOrders)
            {
                m_Price = Price;
                m_Size = Size;
            }
            #endregion

            #region Properties
            public double Price
            {
                get { return m_Price; }
            }
            public double Size
            {
                get { return m_Size; }
                set { m_Size = value; }
            }
            #endregion

            #region Members
            protected double m_Price;
            protected double m_Size;
            #endregion
        }
    }

}
