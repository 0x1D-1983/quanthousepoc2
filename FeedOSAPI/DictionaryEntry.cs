///-------------------------------
/// FeedOS C# Client API
/// copyright QuantHouse 2003-2009
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
	/// <summary>
	/// Generic tag dictionary entry
	/// </summary>
	#region class DictionaryEntry
	public class DictionaryEntry
	{
		#region Constructor
		public DictionaryEntry(string shortName, string longName)
		{
			m_ShortName = shortName;
			m_LongName = longName;
		}
		#endregion

		#region Members
		public string	m_ShortName;
		public string	m_LongName;
		#endregion
	};
	#endregion

	/// <summary>
	/// Generic tag dictionary entry with the associated key
	/// </summary>
	#region class DictionaryEntryWithId
	public class DictionaryEntryWithId
	{
		#region Constructor
		public DictionaryEntryWithId(int entryId, DictionaryEntry dictEntry)
		{
			m_EntryId = entryId;
			m_DictEntry = dictEntry;
		}
		#endregion

		#region Members
		public int m_EntryId;
		public DictionaryEntry m_DictEntry;
		#endregion
	};
	#endregion
}
