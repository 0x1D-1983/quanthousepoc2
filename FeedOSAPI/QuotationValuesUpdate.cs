///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class QuotationValuesUpdate
        {
            #region Constructor
            public QuotationValuesUpdate(List<TagNumAndValue> values)
            {
                m_Values = values;
            }
            #endregion

            #region Properties
            public List<TagNumAndValue> Values
            {
                get { return m_Values; }
            }
            #endregion

            #region Members
            protected List<TagNumAndValue> m_Values;
            #endregion
        }
    }
}
