///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2015
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class TradeData
        {
            #region Members
            protected double m_Price;
            protected double m_Quantity;
            protected uint m_TradeImpactIndicator;
            protected ValueType m_MarketTimestamp;
            protected object m_TradeId;
            protected List<TagNumAndValue> m_TradeProperties;
            #endregion

            #region Constructors
            public TradeData(double price, double quantity, uint tradeImpactIndicator, ValueType marketTimestamp, object tradeid, List<TagNumAndValue> tradeProperties)
            {
                m_Price = price;
                m_Quantity = quantity;
                m_TradeImpactIndicator = tradeImpactIndicator;
                m_MarketTimestamp = marketTimestamp;
                m_TradeId = tradeid;
                m_TradeProperties = tradeProperties;
            }
            public TradeData()
            {
                m_Price = 0;
                m_Quantity = 0;
                m_TradeImpactIndicator = 0;
                m_MarketTimestamp = new DateTime();
                m_TradeId = 0;
                m_TradeProperties = new List<TagNumAndValue>();
            }
            #endregion

            #region Properties
            public double Price
            {
                get { return m_Price; }
                set { m_Price = value; }
            }
            public double Quantity
            {
                get { return m_Quantity; }
                set { m_Quantity = value; }
            }
            public uint TradeImpactIndicator
            {
                get { return m_TradeImpactIndicator; }
                set { m_TradeImpactIndicator = value; }
            }
            public ValueType MarketTimestamp
            {
                get { return m_MarketTimestamp; }
                set { m_MarketTimestamp = value; }
            }
            public object TradeId
            {
                get { return m_TradeId; }
                set { m_TradeId = value; }
            }
            public List<TagNumAndValue> TradeProperties
            {
                get { return m_TradeProperties; }
                set { m_TradeProperties = value; }
            }
            #endregion
        }
    }
}
