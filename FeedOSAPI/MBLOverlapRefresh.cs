///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2010 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MBLOverlapRefresh
        {
            
            #region Constructor
            public MBLOverlapRefresh(   
                uint instrumentCode,
                uint layerId,
                UTCTimestamps timestamps, 
                FOSInteger bidChangeIndicator,
                FOSInteger askChangeIndicator, 
                List<MBLOrderBookEntry> bidLimits,
                List<MBLOrderBookEntry> askLimits,
                List<TagNumAndValue> otherValues)
            {
                m_InstrumentCode = instrumentCode;
                m_LayerId = layerId;
                m_Timestamps = timestamps;
                m_BidChangeIndicator = bidChangeIndicator;
                m_AskChangeIndicator = askChangeIndicator;
                m_BidLimits = bidLimits;
                m_AskLimits = askLimits;
                m_OtherValues = otherValues;
            }
            #endregion

            static public void splitOrderBookChangeIndicator(FOSInteger indicator, ref bool is_complete, ref int start_level)
	        {
                if (indicator.Num < 0)
                {
			        is_complete=true;
			        start_level=(int)(-indicator.Num-1);
		        } else {
			        is_complete=false;
			        start_level=(int)(indicator.Num);
		        }
	        }

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }    
            }
            public uint LayerId
            {
                get { return m_LayerId; }
            }
            public UTCTimestamps Timestamps
            {
                get { return m_Timestamps; }
            }
            public FOSInteger BidChangeIndicator
            {
                get { return m_BidChangeIndicator; }
                set { m_BidChangeIndicator = value; }
            }
            public List<MBLOrderBookEntry> BidLimits
            {
                get { return m_BidLimits; }
                set { m_BidLimits = value; }
            }
            public FOSInteger AskChangeIndicator
            {
                get { return m_AskChangeIndicator; }
                set { m_AskChangeIndicator = value; }
            }
            public List<MBLOrderBookEntry> AskLimits
            {
                get { return m_AskLimits; }
                set { m_AskLimits = value; }
            }
            public List<TagNumAndValue> OtherValues
            {
                get { return m_OtherValues; }
                set { m_OtherValues = value; }
            }
            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected uint m_LayerId;
            protected UTCTimestamps m_Timestamps;		
            protected FOSInteger m_BidChangeIndicator;		
            protected List<MBLOrderBookEntry> m_BidLimits;
            protected FOSInteger m_AskChangeIndicator;
            protected List<MBLOrderBookEntry> m_AskLimits;
            protected List<TagNumAndValue> m_OtherValues;
            #endregion

        }
    }
}