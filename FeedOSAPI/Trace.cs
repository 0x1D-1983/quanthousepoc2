using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    public class Trace
    {
        public enum FEEDOS_TRACE_LEVEL : int {
            FEEDOS_TRACE_LEVEL_Fatal		= 3,
            FEEDOS_TRACE_LEVEL_Critical	    = 2,
            FEEDOS_TRACE_LEVEL_Warning      = 1,
            FEEDOS_TRACE_LEVEL_Info		    = 0,
            FEEDOS_TRACE_LEVEL_Debug		= -1,
            FEEDOS_TRACE_LEVEL_MoreDebug	= -2,
            FEEDOS_TRACE_LEVEL_FullDebug	= -3,
            FEEDOS_TRACE_LEVEL_Scope		= -16
        }

        #region Constructor
        private Trace() { }
        #endregion
        
    }
}
