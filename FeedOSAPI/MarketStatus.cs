///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketStatus
        {
            #region Constructor
            public MarketStatus(ushort FOSLowLevelMarketId, TradingSessionStatus TradingSessionStatus)
            {
                m_FOSLowLevelMarketId = FOSLowLevelMarketId;
                m_TradingSessionStatus = TradingSessionStatus;
            }
            #endregion

            #region Properties
            public ushort FOSMarketId
            {
                get { return m_FOSLowLevelMarketId; }
            }
            public TradingSessionStatus TradingSessionStatus
            {
                get { return m_TradingSessionStatus; }
            }
            #endregion

            #region Members
            protected ushort m_FOSLowLevelMarketId;
		    protected TradingSessionStatus m_TradingSessionStatus;
            #endregion
        }
    }
}
