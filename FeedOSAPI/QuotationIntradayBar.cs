///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class QuotationIntradayBar
        {
            #region Constructor
            public QuotationIntradayBar(ValueType serverUTCDateTimeBegin, ushort serverDurationSeconds, uint instrumentCode,
                double open, double high, double low, double close, double volumeTraded, uint nbPoints,
                ValueType marketUTCDateTimeBegin, List<TagNumAndValue> otherValues)
            {
                m_ServerUTCDateTimeBegin = serverUTCDateTimeBegin;
                m_ServerDurationSeconds = serverDurationSeconds;
                m_InstrumentCode = instrumentCode;
                m_Open = open;
                m_High = high;
                m_Low = low;
                m_Close = close;
                m_VolumeTraded = volumeTraded;
                m_NbPoints = nbPoints;
                m_MarketUTCDateTimeBegin = marketUTCDateTimeBegin;
                m_OtherValues = otherValues;
            }
            #endregion

            #region Properties
            public ValueType ServerUTCDateTimeBegin
            {
                get { return m_ServerUTCDateTimeBegin; }
            }
            public ushort ServerDurationSeconds
            {
                get { return m_ServerDurationSeconds; }
            }
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public double Open
            {
                get { return m_Open; }
            }
            public double High
            {
                get { return m_High; }
            }
            public double Low
            {
                get { return m_Low; }
            }
            public double Close
            {
                get { return m_Close; }
            }
            public double VolumeTraded
            {
                get { return m_VolumeTraded; }
            }
            public uint NbPoints
            {
                get { return m_NbPoints; }
            }
            public ValueType MarketUTCDateTimeBegin
            {
                get { return m_MarketUTCDateTimeBegin; }
            }
            public List<TagNumAndValue> OtherValues
            {
                get { return m_OtherValues; }
            }
            #endregion

            #region Members
            protected ValueType m_ServerUTCDateTimeBegin;
            protected ushort m_ServerDurationSeconds;
            protected uint m_InstrumentCode;
            protected double m_Open;
            protected double m_High;
            protected double m_Low;
            protected double m_Close;
            protected double m_VolumeTraded;
            protected uint m_NbPoints;
            protected ValueType m_MarketUTCDateTimeBegin;
            protected List<TagNumAndValue> m_OtherValues;
            #endregion
        }
    }
}
