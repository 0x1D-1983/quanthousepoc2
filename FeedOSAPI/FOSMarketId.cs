///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    public class FOSMarketId
    {
        public const ushort MARKET_ID_unknown = 0;

        #region Constructor
        private FOSMarketId() { }
        private FOSMarketId(FOSMarketId FOSMarketId) { }
        #endregion

        #region Properties
        public ushort internalCode
        {
            get { return m_internal; }
        }
        #endregion

        #region Members
        protected ushort m_internal;
        #endregion
    }
}
