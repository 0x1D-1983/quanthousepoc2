///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public enum LookupMode : uint
        {
	        LookupMode_Narrow /** Precise search */,
	        LookupMode_Wide /** Extended search */,
        }

        public enum ReturnCode
        {
            RC_OK,	// success
            RC_ERROR,	// general error
            RC_NOT_AVAILABLE,	// feature is not available
            RC_NOT_AUTHORISED,	// wrong permission
            RC_NORMAL_DISCONNECT,	// disconnected at user request
            RC_DISCONNECTED,	// disconnected (network problem)
            RC_TIMEOUT,	// timeout
            RC_SERVER_BUSY,	// server is busy
            RC_SERVER_ERROR,	// general server error
            RC_CLIENT_VERSION_TOO_OLD,	// API is too old
            RC_CLIENT_VERSION_TOO_RECENT,	// API is too recent (server is too old)
            RC_INVALID_INSTRUMENT_CODE,	// an invalid instrument code was provided
            RC_INVALID_MARKET_ID,	// an invalid market id was provided
            RC_INVALID_REFERENTIAL_TAG,	// an invalid referential tag was provided
            RC_INVALID_QUOTATION_TAG,	// an invalid quotation tag was provided
            RC_INVALID_ARGUMENT,	// an invalid argument was provided
            RC_MISSING_ARGUMENT,	// a mandatory argument was not provided
            RC_UA_INVALID_USER_ID,	// invalid user id (maybe it is already in use)
            RC_UA_INVALID_GROUP_ID,	// invalid or unknown group id
            RC_UA_INVALID_NAME_OR_PASSWORD,	// authentication failed
            RC_UA_TOO_MANY_CONNECTIONS,	// maximum number of simultaneous connections reached
            RC_UA_USER_EXPIRED,	// user account expired
        };

        public enum QuotationUpdateContent
        {
            QuotationUpdateContent_TradingStatus /**  */,
            QuotationUpdateContent_Bid /**  */,
            QuotationUpdateContent_Ask /**  */,
            QuotationUpdateContent_LastPrice /**  */,
            QuotationUpdateContent_LastTradeQty /**  */,
            QuotationUpdateContent_Open /**  */,
            QuotationUpdateContent_Close /**  */,
            QuotationUpdateContent_High /**  */,
            QuotationUpdateContent_Low /**  */,
            QuotationUpdateContent_OCHL_daily /**  */,
            QuotationUpdateContent_OtherValues /**  */,
            QuotationUpdateContent_OpeningNextCalendarDay /**  */,
            QuotationUpdateContent_Context /**  */,
            QuotationUpdateContent_OffBookTrade /** */,
            QuotationUpdateContent_ChangeBusinessDay /** */,
            QuotationUpdateContent_Session /**  */,
            QuotationUpdateContent_reserved_16 /**  */,
            QuotationUpdateContent_reserved_17 /**  */,
            QuotationUpdateContent_reserved_18 /**  */,
            QuotationUpdateContent_reserved_19 /**  */,
            QuotationUpdateContent_reserved_20 /**  */,
            QuotationUpdateContent_reserved_21 /**  */,
            QuotationUpdateContent_reserved_22 /**  */,
            QuotationUpdateContent_reserved_23 /**  */,
            QuotationUpdateContent_reserved_24 /**  */,
            QuotationUpdateContent_reserved_25 /**  */,
            QuotationUpdateContent_reserved_26 /**  */,
            QuotationUpdateContent_reserved_27 /**  */,
            QuotationUpdateContent_reserved_28 /**  */,
            QuotationUpdateContent_reserved_29 /**  */,
            QuotationUpdateContent_reserved_30 /**  */
        }

        /// <summary>
        /// TradingStatus enum to be used.
        /// </summary>
        public enum FIXTradSesStatus
        {
            FIXSecurityTradingStatus_UNKNOWN /**  */,
	        FIXSecurityTradingStatus_OpeningDelay /**  */,
	        FIXSecurityTradingStatus_TradingHalt /**  */,
	        FIXSecurityTradingStatus_Resume /**  */,
	        FIXSecurityTradingStatus_NoOpenNoResume /**  */,
	        FIXSecurityTradingStatus_PriceIndication /**  */,
	        FIXSecurityTradingStatus_TradingRangeIndication /**  */,
	        FIXSecurityTradingStatus_MarketImbalanceBuy /**  */,
	        FIXSecurityTradingStatus_MarketImbalanceSell /**  */,
	        FIXSecurityTradingStatus_MarketOnCloseImbalanceBuy /**  */,
	        FIXSecurityTradingStatus_MarketOnCloseImbalanceSell /**  */,
	        FIXSecurityTradingStatus_NOT_ASSIGNED_11 /**  */,
	        FIXSecurityTradingStatus_NoMarketImbalance /**  */,
	        FIXSecurityTradingStatus_NoMarketOnCloseImbalance /**  */,
	        FIXSecurityTradingStatus_ITSPreOpening /**  */,
	        FIXSecurityTradingStatus_NewPriceIndication /**  */,
	        FIXSecurityTradingStatus_TradeDisseminationTime /**  */,
	        FIXSecurityTradingStatus_ReadyToTrade /** start of session */,
	        FIXSecurityTradingStatus_NotAvailableForTrading /** end of session */,
	        FIXSecurityTradingStatus_NotTradedOnThisMarket /**  */,
	        FIXSecurityTradingStatus_UnknownOrInvalid /**  */,
	        FIXSecurityTradingStatus_PreOpen /**  */,
	        FIXSecurityTradingStatus_OpeningRotation /**  */,
	        FIXSecurityTradingStatus_FastMarket /**  */,
	        FIXSecurityTradingStatus_PreCross /**  */,
	        FIXSecurityTradingStatus_Cross /**  */,
        }

        public enum FeedState
        {
	        FeedState_Active /** normal with activity */,
	        FeedState_ProbablyNormal /** looks like normal */,
	        FeedState_ProbablyDisrupted /** looks like disrupted */,
	        FeedState_Disrupted_TechnicalLevel /** not usable (technical reasons) */,
	        FeedState_Disrupted_ExchangeLevel /** not usable (N/A at exchange level) */,
        }

        public class TagNumAndValue
        {
            #region Constructor
            public TagNumAndValue(ushort Num, object Value)
            {
                m_Num = Num;
                m_Value = Value;
            }
            #endregion

            #region Properties
            public ushort Num
            {
                get { return m_Num; }
            }
            public object Value
            {
                get { return m_Value; }
            }
            #endregion

            #region Members
            protected ushort m_Num;
            protected object m_Value;
            #endregion

        }

        public enum QuotationTradeImpactIndicator
        {
            QuotationTradeImpactIndicator_HasOpen,
            QuotationTradeImpactIndicator_HasHighLow,
            QuotationTradeImpactIndicator_HasLast,
            QuotationTradeImpactIndicator_HasVolume,
            QuotationTradeImpactIndicator_HasOffBookVolume,
            QuotationTradeImpactIndicator_HasVWAP,
            QuotationTradeImpactIndicator_reserved_6,
            QuotationTradeImpactIndicator_reserved_7,
            QuotationTradeImpactIndicator_reserved_8,
            QuotationTradeImpactIndicator_reserved_9,
            QuotationTradeImpactIndicator_reserved_10,
            QuotationTradeImpactIndicator_reserved_11,
            QuotationTradeImpactIndicator_reserved_12,
            QuotationTradeImpactIndicator_reserved_13,
            QuotationTradeImpactIndicator_reserved_14,
            QuotationTradeImpactIndicator_reserved_15,
            QuotationTradeImpactIndicator_Open,
            QuotationTradeImpactIndicator_HighLow,
            QuotationTradeImpactIndicator_Last,
            QuotationTradeImpactIndicator_Volume,
            QuotationTradeImpactIndicator_OffBookVolume,
            QuotationTradeImpactIndicator_VWAP,
            QuotationTradeImpactIndicator_reserved_22,
            QuotationTradeImpactIndicator_reserved_23,
            QuotationTradeImpactIndicator_reserved_24,
            QuotationTradeImpactIndicator_reserved_25,
            QuotationTradeImpactIndicator_reserved_26,
            QuotationTradeImpactIndicator_reserved_27,
            QuotationTradeImpactIndicator_reserved_28,
            QuotationTradeImpactIndicator_reserved_29,
            QuotationTradeImpactIndicator_reserved_30,
            QuotationTradeImpactIndicator_reserved_31
        };

        public enum TradeCancelCorrectionContent
        {
            TradeCancelCorrectionContent_IsCorrection,
            TradeCancelCorrectionContent_IsFromVenue,
            TradeCancelCorrectionContent_CorrectedValues,
            TradeCancelCorrectionContent_OffBookTrade,
            TradeCancelCorrectionContent_CurrentSession,
            TradeCancelCorrectionContent_reserved_5,
            TradeCancelCorrectionContent_reserved_6,
            TradeCancelCorrectionContent_reserved_7
        };

        public enum AdjustmentFactorEventType
        {
            AdjustmentFactorEventType_reserved_0,
            AdjustmentFactorEventType_RightsSameClass,
            AdjustmentFactorEventType_RightsDifferentClass,
            AdjustmentFactorEventType_EntitlementSameClass,
            AdjustmentFactorEventType_EntitlementDifferentClass,
            AdjustmentFactorEventType_Subdivision,
            AdjustmentFactorEventType_Consolidation,
            AdjustmentFactorEventType_reserved_7,
            AdjustmentFactorEventType_reserved_8,
            AdjustmentFactorEventType_Demerger,
            AdjustmentFactorEventType_CapitalReturn,
            AdjustmentFactorEventType_Distribution,
            AdjustmentFactorEventType_reserved_12,
            AdjustmentFactorEventType_BonusSameClass,
            AdjustmentFactorEventType_BonusDifferentClass,
            AdjustmentFactorEventType_reserved_15,
            AdjustmentFactorEventType_CapitalReduction,
            AdjustmentFactorEventType_CashDividend,
            AdjustmentFactorEventType_ScriptDividendSameClass,
            AdjustmentFactorEventType_reserved_19,
            AdjustmentFactorEventType_ScriptDividendDifferentClass,
            AdjustmentFactorEventType_CapitalCall,
            AdjustmentFactorEventType_reserved_22,
            AdjustmentFactorEventType_reserved_23,
            AdjustmentFactorEventType_reserved_24,
            AdjustmentFactorEventType_reserved_25,
            AdjustmentFactorEventType_reserved_26,
            AdjustmentFactorEventType_reserved_27,
            AdjustmentFactorEventType_reserved_28,
            AdjustmentFactorEventType_reserved_29,
            AdjustmentFactorEventType_reserved_30
        };

        public enum DailyExtPointType
        {
            DailyExtPointType_DailyOnly,
            DailyExtPointType_SessionOnly,
            DailyExtPointType_DailyAndSession
        };
    }
}
