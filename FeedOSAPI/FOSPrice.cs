///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    public class FOSPrice
    {
        #region Constructor
        public FOSPrice(double price)
        {
            m_Num = price;
        }
        #endregion

        #region Properties
        public double Num
        {
            get { return m_Num; }
        }
        #endregion

        #region Members
        protected double m_Num;
        #endregion
    }
}
