///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketNews
        {
            #region Constructor
            public MarketNews(ushort FOSLowLevelMarketId, ValueType origUTCDateTime, uint urgency, string headline, string urlLink,
                string content, List<uint> instrumentCodeList)
            {
                m_FOSLowLevelMarketId = FOSLowLevelMarketId;
                m_OrigUTCDateTime = origUTCDateTime;
                m_Urgency = urgency;
                m_Headline = headline;
                m_URLLink = urlLink;
                m_Content = content;
                m_InstrumentCodeList = instrumentCodeList;
            }
            #endregion

            #region Properties
            public ushort FOSMarketId
            {
                get { return m_FOSLowLevelMarketId; }
            }
            public ValueType OrigUTCDateTime
            {
                get { return m_OrigUTCDateTime; }
            }
            public uint Urgency
            {
                get { return m_Urgency; }
            }
            public string Headline
            {
                get { return m_Headline; }
            }
            public string URLLink
            {
                get { return m_URLLink; }
            }
            public string Content
            {
                get { return m_Content; }
            }
            public List<uint> InstrumentCodeList
            {
                get { return m_InstrumentCodeList; }
            }
            #endregion

            #region Members
            protected ushort m_FOSLowLevelMarketId;
            protected ValueType m_OrigUTCDateTime;
            protected uint m_Urgency;
            protected string m_Headline;
            protected string m_URLLink;
            protected string m_Content;
            protected List<uint> m_InstrumentCodeList;
            #endregion
         }
    }
}
