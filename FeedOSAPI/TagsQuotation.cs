///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class TagsQuotation
        {
            #region Quotation Tags  Definitions
            /// ***GENERATOR-begin-QUOT_TAGS-

		/* TAG_TradSesOpenTime (syntax: Timestamp) */
		public const ushort TAG_TradSesOpenTime	=342;

		/* TAG_MidPrice (syntax: float64) */
		public const ushort TAG_MidPrice	=631;

		/* TAG_SettlPriceType (syntax: int) */
		public const ushort TAG_SettlPriceType	=731;

		/* TAG_PriceDelta (syntax: float64) */
		public const ushort TAG_PriceDelta	=811;

		/* TAG_LowLimitPrice (syntax: float64) */
		public const ushort TAG_LowLimitPrice	=1148;

		/* TAG_HighLimitPrice (syntax: float64) */
		public const ushort TAG_HighLimitPrice	=1149;

		/* TAG_TradingStatus (syntax: Enum) */
		public const ushort TAG_TradingStatus	=9100;

		/* TAG_OrderEntryStatus (syntax: uint8) */
		public const ushort TAG_OrderEntryStatus	=9102;

		/* TAG_TradingSessionId (syntax: int8) */
		public const ushort TAG_TradingSessionId	=9101;

		/* TAG_LastPrice (syntax: float64) */
		public const ushort TAG_LastPrice	=9106;

		/* TAG_LastTradeQty (syntax: float64) */
		public const ushort TAG_LastTradeQty	=9107;

		/* TAG_LastTradeTimestamp (syntax: Timestamp) */
		public const ushort TAG_LastTradeTimestamp	=9108;

		/* TAG_LastTradePrice (syntax: float64) */
		public const ushort TAG_LastTradePrice	=9109;

		/* TAG_LastOffBookTradePrice (syntax: float64) */
		public const ushort TAG_LastOffBookTradePrice	=9110;

		/* TAG_LastOffBookTradeQty (syntax: float64) */
		public const ushort TAG_LastOffBookTradeQty	=9111;

		/* TAG_LastOffBookTradeTimestamp (syntax: Timestamp) */
		public const ushort TAG_LastOffBookTradeTimestamp	=9112;

		/* TAG_RegSHOAction (syntax: Enum) */
		public const ushort TAG_RegSHOAction	=9113;

		/* TAG_VarClose (syntax: float64) */
		public const ushort TAG_VarClose	=9140;

		/* TAG_VarClosePct (syntax: float64) */
		public const ushort TAG_VarClosePct	=9141;

		/* TAG_SessionOpeningPrice (syntax: float64) */
		public const ushort TAG_SessionOpeningPrice	=9121;

		/* TAG_SessionHighPrice (syntax: float64) */
		public const ushort TAG_SessionHighPrice	=9124;

		/* TAG_SessionLowPrice (syntax: float64) */
		public const ushort TAG_SessionLowPrice	=9125;

		/* TAG_SessionVWAPPrice (syntax: float64) */
		public const ushort TAG_SessionVWAPPrice	=9126;

		/* TAG_SessionTotalVolumeTraded (syntax: float64) */
		public const ushort TAG_SessionTotalVolumeTraded	=9120;

		/* TAG_SessionTotalAssetTraded (syntax: float64) */
		public const ushort TAG_SessionTotalAssetTraded	=9127;

		/* TAG_PreviousSessionSettlementPrice (syntax: float64) */
		public const ushort TAG_PreviousSessionSettlementPrice	=9123;

		/* TAG_PreviousSessionClosingPrice (syntax: float64) */
		public const ushort TAG_PreviousSessionClosingPrice	=9122;

		/* TAG_SessionTotalOffBookAssetTraded (syntax: float64) */
		public const ushort TAG_SessionTotalOffBookAssetTraded	=9114;

		/* TAG_SessionTotalOffBookVolumeTraded (syntax: float64) */
		public const ushort TAG_SessionTotalOffBookVolumeTraded	=9115;

		/* TAG_PriorSessionsTotalAssetTraded (syntax: float64) */
		public const ushort TAG_PriorSessionsTotalAssetTraded	=9116;

		/* TAG_PriorSessionsTotalVolumeTraded (syntax: float64) */
		public const ushort TAG_PriorSessionsTotalVolumeTraded	=9117;

		/* TAG_PriorSessionsTotalOffBookAssetTraded (syntax: float64) */
		public const ushort TAG_PriorSessionsTotalOffBookAssetTraded	=9118;

		/* TAG_PriorSessionsTotalOffBookVolumeTraded (syntax: float64) */
		public const ushort TAG_PriorSessionsTotalOffBookVolumeTraded	=9119;

		/* TAG_SessionClosingPrice (syntax: float64) */
		public const ushort TAG_SessionClosingPrice	=9128;

		/* TAG_DailyOpeningPrice (syntax: float64) */
		public const ushort TAG_DailyOpeningPrice	=9131;

		/* TAG_DailyClosingPrice (syntax: float64) */
		public const ushort TAG_DailyClosingPrice	=9132;

		/* TAG_DailySettlementPrice (syntax: float64) */
		public const ushort TAG_DailySettlementPrice	=9133;

		/* TAG_DailyHighPrice (syntax: float64) */
		public const ushort TAG_DailyHighPrice	=9134;

		/* TAG_DailyLowPrice (syntax: float64) */
		public const ushort TAG_DailyLowPrice	=9135;

		/* TAG_DailyTotalVolumeTraded (syntax: float64) */
		public const ushort TAG_DailyTotalVolumeTraded	=9130;

		/* TAG_DailyTotalAssetTraded (syntax: float64) */
		public const ushort TAG_DailyTotalAssetTraded	=9136;

		/* TAG_PreviousDailyTotalVolumeTraded (syntax: float64) */
		public const ushort TAG_PreviousDailyTotalVolumeTraded	=9137;

		/* TAG_PreviousDailyTotalAssetTraded (syntax: float64) */
		public const ushort TAG_PreviousDailyTotalAssetTraded	=9138;

		/* TAG_PreviousDailyClosingPrice (syntax: float64) */
		public const ushort TAG_PreviousDailyClosingPrice	=9142;

		/* TAG_PreviousBusinessDay (syntax: Timestamp) */
		public const ushort TAG_PreviousBusinessDay	=9143;

		/* TAG_CurrentBusinessDay (syntax: Timestamp) */
		public const ushort TAG_CurrentBusinessDay	=9144;

		/* TAG_PreviousDailySettlementPrice (syntax: float64) */
		public const ushort TAG_PreviousDailySettlementPrice	=9145;

		/* TAG_DailyTotalOffBookVolumeTraded (syntax: float64) */
		public const ushort TAG_DailyTotalOffBookVolumeTraded	=9148;

		/* TAG_DailyTotalOffBookAssetTraded (syntax: float64) */
		public const ushort TAG_DailyTotalOffBookAssetTraded	=9149;

		/* TAG_AverageDailyTotalVolumeTraded (syntax: float64) */
		public const ushort TAG_AverageDailyTotalVolumeTraded	=9154;

		/* TAG_InternalDailyClosingPriceType (syntax: char) */
		public const ushort TAG_InternalDailyClosingPriceType	=9155;

		/* TAG_PreviousInternalDailyClosingPriceType (syntax: char) */
		public const ushort TAG_PreviousInternalDailyClosingPriceType	=9156;

		/* TAG_OpenInterest (syntax: float64) */
		public const ushort TAG_OpenInterest	=9150;

		/* TAG_LastAuctionPrice (syntax: float64) */
		public const ushort TAG_LastAuctionPrice	=9146;

		/* TAG_LastAuctionVolume (syntax: float64) */
		public const ushort TAG_LastAuctionVolume	=9147;

		/* TAG_LastAuctionVolumeTraded (syntax: float64) */
		public const ushort TAG_LastAuctionVolumeTraded	=9183;

		/* TAG_LastAuctionImbalanceSide (syntax: char) */
		public const ushort TAG_LastAuctionImbalanceSide	=9151;

		/* TAG_QH_LastAuctionImbalanceSide (syntax: char) */
		public const ushort TAG_QH_LastAuctionImbalanceSide	=9157;

		/* TAG_LastAuctionImbalanceVolume (syntax: float64) */
		public const ushort TAG_LastAuctionImbalanceVolume	=9152;

		/* TAG_QH_LastAuctionImbalanceVolume (syntax: float64) */
		public const ushort TAG_QH_LastAuctionImbalanceVolume	=9158;

		/* TAG_AuctionOnDemand_Price (syntax: float64) */
		public const ushort TAG_AuctionOnDemand_Price	=9184;

		/* TAG_AuctionOnDemand_Volume (syntax: float64) */
		public const ushort TAG_AuctionOnDemand_Volume	=9185;

		/* TAG_PriceVega (syntax: float64) */
		public const ushort TAG_PriceVega	=9153;

		/* TAG_AveragePriceDaytoDayChange (syntax: float64) */
		public const ushort TAG_AveragePriceDaytoDayChange	=9160;

		/* TAG_AveragePriceSimpleYield (syntax: float64) */
		public const ushort TAG_AveragePriceSimpleYield	=9161;

		/* TAG_AveragePriceCompoundYield (syntax: float64) */
		public const ushort TAG_AveragePriceCompoundYield	=9162;

		/* TAG_HighestPriceDaytoDayChange (syntax: float64) */
		public const ushort TAG_HighestPriceDaytoDayChange	=9163;

		/* TAG_HighestPriceSimpleYield (syntax: float64) */
		public const ushort TAG_HighestPriceSimpleYield	=9164;

		/* TAG_HighestPriceCompoundYield (syntax: float64) */
		public const ushort TAG_HighestPriceCompoundYield	=9165;

		/* TAG_LowestPriceDaytoDayChange (syntax: float64) */
		public const ushort TAG_LowestPriceDaytoDayChange	=9166;

		/* TAG_LowestPriceSimpleYield (syntax: float64) */
		public const ushort TAG_LowestPriceSimpleYield	=9167;

		/* TAG_LowestPriceCompoundYield (syntax: float64) */
		public const ushort TAG_LowestPriceCompoundYield	=9168;

		/* TAG_MedianPrice (syntax: float64) */
		public const ushort TAG_MedianPrice	=9169;

		/* TAG_MedianPriceDaytoDayChange (syntax: float64) */
		public const ushort TAG_MedianPriceDaytoDayChange	=9170;

		/* TAG_MedianPriceSimpleYield (syntax: float64) */
		public const ushort TAG_MedianPriceSimpleYield	=9171;

		/* TAG_MedianPriceCompoundYield (syntax: float64) */
		public const ushort TAG_MedianPriceCompoundYield	=9172;

		/* TAG_NumberOfReportingCompanies (syntax: int32) */
		public const ushort TAG_NumberOfReportingCompanies	=9173;

		/* TAG_DailyHighBidPrice (syntax: float64) */
		public const ushort TAG_DailyHighBidPrice	=9174;

		/* TAG_DailyLowBidPrice (syntax: float64) */
		public const ushort TAG_DailyLowBidPrice	=9175;

		/* TAG_DailyHighAskPrice (syntax: float64) */
		public const ushort TAG_DailyHighAskPrice	=9176;

		/* TAG_DailyLowAskPrice (syntax: float64) */
		public const ushort TAG_DailyLowAskPrice	=9177;

		/* TAG_DailyHighMidPrice (syntax: float64) */
		public const ushort TAG_DailyHighMidPrice	=9178;

		/* TAG_DailyLowMidPrice (syntax: float64) */
		public const ushort TAG_DailyLowMidPrice	=9179;

		/* TAG_DailyNumberOfBlockTrades (syntax: int) */
		public const ushort TAG_DailyNumberOfBlockTrades	=9180;

		/* TAG_DailyTotalBlockVolumeTraded (syntax: float64) */
		public const ushort TAG_DailyTotalBlockVolumeTraded	=9181;

		/* TAG_MarketTradingStatusCode (syntax: uint32) */
		public const ushort TAG_MarketTradingStatusCode	=9182;

		/* TAG_InternalDailyOpenTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalDailyOpenTimestamp	=9300;

		/* TAG_InternalDailyCloseTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalDailyCloseTimestamp	=9301;

		/* TAG_InternalDailyHighTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalDailyHighTimestamp	=9302;

		/* TAG_InternalDailyLowTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalDailyLowTimestamp	=9303;

		/* TAG_InternalPriceActivityTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalPriceActivityTimestamp	=9304;

		/* TAG_InternalLastAuctionTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalLastAuctionTimestamp	=9305;

		/* TAG_InternalDailyBusinessDayTimestamp (syntax: Timestamp) */
		public const ushort TAG_InternalDailyBusinessDayTimestamp	=9310;

		/* TAG_InternalCrossIndicator (syntax: bool) */
		public const ushort TAG_InternalCrossIndicator	=9306;

		/* TAG_InternalBestBidNbOrders (syntax: int32) */
		public const ushort TAG_InternalBestBidNbOrders	=9307;

		/* TAG_InternalBestAskNbOrders (syntax: int32) */
		public const ushort TAG_InternalBestAskNbOrders	=9308;

		/* TAG_PriceActivityMarketTimestamp (syntax: Timestamp) */
		public const ushort TAG_PriceActivityMarketTimestamp	=9309;

		/* TAG_52WeekHighPrice (syntax: float64) */
		public const ushort TAG_52WeekHighPrice	=9200;

		/* TAG_52WeekHighPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_52WeekHighPriceTimestamp	=9201;

		/* TAG_52WeekLowPrice (syntax: float64) */
		public const ushort TAG_52WeekLowPrice	=9202;

		/* TAG_52WeekLowPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_52WeekLowPriceTimestamp	=9203;

		/* TAG_YearToDateHighPrice (syntax: float64) */
		public const ushort TAG_YearToDateHighPrice	=9204;

		/* TAG_YearToDateHighPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_YearToDateHighPriceTimestamp	=9205;

		/* TAG_YearToDateLowPrice (syntax: float64) */
		public const ushort TAG_YearToDateLowPrice	=9206;

		/* TAG_YearToDateLowPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_YearToDateLowPriceTimestamp	=9207;

		/* TAG_DailyNumberOfTrades (syntax: uint32) */
		public const ushort TAG_DailyNumberOfTrades	=9208;

		/* TAG_PreviousDailyHighPrice (syntax: float64) */
		public const ushort TAG_PreviousDailyHighPrice	=9209;

		/* TAG_PreviousDailyLowPrice (syntax: float64) */
		public const ushort TAG_PreviousDailyLowPrice	=9210;

		/* TAG_PreviousValidBidPrice (syntax: float64) */
		public const ushort TAG_PreviousValidBidPrice	=9211;

		/* TAG_PreviousValidBidTimestamp (syntax: Timestamp) */
		public const ushort TAG_PreviousValidBidTimestamp	=9212;

		/* TAG_PreviousValidAskPrice (syntax: float64) */
		public const ushort TAG_PreviousValidAskPrice	=9213;

		/* TAG_PreviousValidAskTimestamp (syntax: Timestamp) */
		public const ushort TAG_PreviousValidAskTimestamp	=9214;

		/* TAG_DailyClosingBidPrice (syntax: float64) */
		public const ushort TAG_DailyClosingBidPrice	=9215;

		/* TAG_DailyClosingAskPrice (syntax: float64) */
		public const ushort TAG_DailyClosingAskPrice	=9216;

		/* TAG_ReportingType (syntax: String) */
		public const ushort TAG_ReportingType	=9220;

		/* TAG_NetAssetValuePrice (syntax: float64) */
		public const ushort TAG_NetAssetValuePrice	=9221;

		/* TAG_TotalNetAssets (syntax: float64) */
		public const ushort TAG_TotalNetAssets	=9222;

		/* TAG_WrapPrice (syntax: float64) */
		public const ushort TAG_WrapPrice	=9223;

		/* TAG_EstimatedLongTimeReturn (syntax: float64) */
		public const ushort TAG_EstimatedLongTimeReturn	=9224;

		/* TAG_DailyDividend (syntax: float64) */
		public const ushort TAG_DailyDividend	=9225;

		/* TAG_DailyDividendIndicator (syntax: char) */
		public const ushort TAG_DailyDividendIndicator	=9226;

		/* TAG_Footnotes (syntax: String) */
		public const ushort TAG_Footnotes	=9227;

		/* TAG_AccruedInterest (syntax: float64) */
		public const ushort TAG_AccruedInterest	=9228;

		/* TAG_EntryTimestamp (syntax: Timestamp) */
		public const ushort TAG_EntryTimestamp	=9229;

		/* TAG_AvgMaturityDays (syntax: uint16) */
		public const ushort TAG_AvgMaturityDays	=9230;

		/* TAG_AvgLifeDays (syntax: uint16) */
		public const ushort TAG_AvgLifeDays	=9231;

		/* TAG_YieldSevenDayGross (syntax: float64) */
		public const ushort TAG_YieldSevenDayGross	=9232;

		/* TAG_YieldSevenDaySubsidized (syntax: float64) */
		public const ushort TAG_YieldSevenDaySubsidized	=9233;

		/* TAG_YieldSevenDayAnnualized (syntax: float64) */
		public const ushort TAG_YieldSevenDayAnnualized	=9234;

		/* TAG_YieldThirtyDay (syntax: float64) */
		public const ushort TAG_YieldThirtyDay	=9235;

		/* TAG_YieldThirtyDayTimestamp (syntax: Timestamp) */
		public const ushort TAG_YieldThirtyDayTimestamp	=9236;

		/* TAG_CorporateActionType (syntax: char) */
		public const ushort TAG_CorporateActionType	=9237;

		/* TAG_ShortTermGain (syntax: float64) */
		public const ushort TAG_ShortTermGain	=9238;

		/* TAG_LongTermGain (syntax: float64) */
		public const ushort TAG_LongTermGain	=9239;

		/* TAG_UnallocatedDistribution (syntax: float64) */
		public const ushort TAG_UnallocatedDistribution	=9240;

		/* TAG_ReturnOnCapital (syntax: float64) */
		public const ushort TAG_ReturnOnCapital	=9241;

		/* TAG_ExDistributionTimestamp (syntax: Timestamp) */
		public const ushort TAG_ExDistributionTimestamp	=9242;

		/* TAG_RecordTimestamp (syntax: Timestamp) */
		public const ushort TAG_RecordTimestamp	=9243;

		/* TAG_PaymentTimestamp (syntax: Timestamp) */
		public const ushort TAG_PaymentTimestamp	=9244;

		/* TAG_ReinvestTimestamp (syntax: Timestamp) */
		public const ushort TAG_ReinvestTimestamp	=9245;

		/* TAG_DistributionType (syntax: char) */
		public const ushort TAG_DistributionType	=9246;

		/* TAG_TotalCashDistribution (syntax: float64) */
		public const ushort TAG_TotalCashDistribution	=9247;

		/* TAG_NonQualifiedCashDistribution (syntax: float64) */
		public const ushort TAG_NonQualifiedCashDistribution	=9248;

		/* TAG_QualifiedCashDistribution (syntax: float64) */
		public const ushort TAG_QualifiedCashDistribution	=9249;

		/* TAG_TaxFreeCashDistribution (syntax: float64) */
		public const ushort TAG_TaxFreeCashDistribution	=9250;

		/* TAG_OrdinaryForeignTaxCredit (syntax: float64) */
		public const ushort TAG_OrdinaryForeignTaxCredit	=9252;

		/* TAG_QualifiedForeignTaxCredit (syntax: float64) */
		public const ushort TAG_QualifiedForeignTaxCredit	=9253;

		/* TAG_LastYield (syntax: float64) */
		public const ushort TAG_LastYield	=9254;

		/* TAG_LimitUpLimitDownIndicator (syntax: char) */
		public const ushort TAG_LimitUpLimitDownIndicator	=9255;

		/* TAG_MinimumNumberOfReportingCompaniesFlag (syntax: bool) */
		public const ushort TAG_MinimumNumberOfReportingCompaniesFlag	=9256;

		/* TAG_StockDividend (syntax: float64) */
		public const ushort TAG_StockDividend	=9257;

		/* TAG_CancelledOrModifiedTradePrice (syntax: float64) */
		public const ushort TAG_CancelledOrModifiedTradePrice	=9258;

		/* TAG_CancelledOrModifiedTradeQuantity (syntax: float64) */
		public const ushort TAG_CancelledOrModifiedTradeQuantity	=9259;

		/* TAG_RetailPriceImprovement (syntax: char) */
		public const ushort TAG_RetailPriceImprovement	=9364;

		/* TAG_DailyClosingBidTimestamp (syntax: Timestamp) */
		public const ushort TAG_DailyClosingBidTimestamp	=9366;

		/* TAG_DailyClosingAskTimestamp (syntax: Timestamp) */
		public const ushort TAG_DailyClosingAskTimestamp	=9367;

		/* TAG_AdjustedDailyClosingPrice (syntax: float64) */
		public const ushort TAG_AdjustedDailyClosingPrice	=9368;

		/* TAG_PreviousAdjustedDailyClosingPrice (syntax: float64) */
		public const ushort TAG_PreviousAdjustedDailyClosingPrice	=9369;

		/* TAG_TradingReferencePrice (syntax: float64) */
		public const ushort TAG_TradingReferencePrice	=9370;

		/* TAG_ExchangeLastComputedPrice (syntax: float64) */
		public const ushort TAG_ExchangeLastComputedPrice	=9371;

		/* TAG_ExchangeIndicativeBuyPrice (syntax: float64) */
		public const ushort TAG_ExchangeIndicativeBuyPrice	=9267;

		/* TAG_ExchangeIndicativeSellPrice (syntax: float64) */
		public const ushort TAG_ExchangeIndicativeSellPrice	=9268;

		/* TAG_UnadjustedDailyClosingPrice (syntax: float64) */
		public const ushort TAG_UnadjustedDailyClosingPrice	=9373;

		/* TAG_PreviousUnadjustedDailyClosingPrice (syntax: float64) */
		public const ushort TAG_PreviousUnadjustedDailyClosingPrice	=9374;

		/* TAG_EstimatedCashPerUnitCreation (syntax: float64) */
		public const ushort TAG_EstimatedCashPerUnitCreation	=9377;

		/* TAG_TotalCashPerUnitCreation (syntax: float64) */
		public const ushort TAG_TotalCashPerUnitCreation	=9378;

		/* TAG_ImpliedVolatility (syntax: float64) */
		public const ushort TAG_ImpliedVolatility	=9379;

		/* TAG_SettlementPriceDate (syntax: Timestamp) */
		public const ushort TAG_SettlementPriceDate	=9380;

		/* TAG_PreviousSettlementPriceDate (syntax: Timestamp) */
		public const ushort TAG_PreviousSettlementPriceDate	=9381;

		/* TAG_OpenInterestDate (syntax: Timestamp) */
		public const ushort TAG_OpenInterestDate	=9382;

		/* TAG_SettlementPriceType (syntax: char) */
		public const ushort TAG_SettlementPriceType	=9383;

		/* TAG_PreviousSettlementPriceType (syntax: char) */
		public const ushort TAG_PreviousSettlementPriceType	=9384;

		/* TAG_LastEligibleTradePrice (syntax: float64) */
		public const ushort TAG_LastEligibleTradePrice	=9385;

		/* TAG_LastEligibleTradeQty (syntax: float64) */
		public const ushort TAG_LastEligibleTradeQty	=9386;

		/* TAG_LastEligibleTradeTimestamp (syntax: Timestamp) */
		public const ushort TAG_LastEligibleTradeTimestamp	=9387;

		/* TAG_DailyOpeningPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_DailyOpeningPriceTimestamp	=9388;

		/* TAG_DailyClosingPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_DailyClosingPriceTimestamp	=9389;

		/* TAG_DailyHighPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_DailyHighPriceTimestamp	=9390;

		/* TAG_DailyLowPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_DailyLowPriceTimestamp	=9391;

		/* TAG_PreviousDailyClosingPriceTimestamp (syntax: Timestamp) */
		public const ushort TAG_PreviousDailyClosingPriceTimestamp	=9392;

		/* TAG_InternalDailyVWAP (syntax: float64) */
		public const ushort TAG_InternalDailyVWAP	=9393;

		/* TAG_MARKET_TradingStatus (syntax: uint32) */
		public const ushort TAG_MARKET_TradingStatus	=9394;

		/* TAG_MARKET_HaltReason (syntax: int) */
		public const ushort TAG_MARKET_HaltReason	=9395;

		/* TAG_StaticTradingReferencePrice (syntax: float64) */
		public const ushort TAG_StaticTradingReferencePrice	=9396;

		/* TAG_MARKET_GroupTradingStatus (syntax: uint32) */
		public const ushort TAG_MARKET_GroupTradingStatus	=9260;

		/* TAG_PreviousOpenInterest (syntax: float64) */
		public const ushort TAG_PreviousOpenInterest	=9262;

		/* TAG_PreviousOpenInterestDate (syntax: Timestamp) */
		public const ushort TAG_PreviousOpenInterestDate	=9263;

		/* TAG_SessionNumberOfTrades (syntax: uint32) */
		public const ushort TAG_SessionNumberOfTrades	=9264;

		/* TAG_HasContinuationFlag (syntax: bool) */
		public const ushort TAG_HasContinuationFlag	=9265;

		/* TAG_NationalConsolidatedVolume (syntax: float64) */
		public const ushort TAG_NationalConsolidatedVolume	=9266;

		/* TAG_Consolidation_LastEvent (syntax: uint8) */
		public const ushort TAG_Consolidation_LastEvent	=9397;

		/* TAG_Consolidation_LastEventTimestamp (syntax: Timestamp) */
		public const ushort TAG_Consolidation_LastEventTimestamp	=9398;

		/* TAG_Consolidation_LastEventLeg (syntax: uint32) */
		public const ushort TAG_Consolidation_LastEventLeg	=9399;

		/* TAG_LegConsolidationStatus (syntax: [array of 100] uint8) */
		public const ushort TAG_LegConsolidationStatus_0	=10000; // x100
		public const ushort TAG_NB_MAX_LegConsolidationStatus=100;

		/* TAG_QUOT_USER (syntax: [array of 1000] float64) */
		public const ushort TAG_QUOT_USER_0	=59000; // x1000
		public const ushort TAG_NB_MAX_QUOT_USER=1000;

		/* TAG_MARKET_SWX_BookCondition (syntax: int32) */
		public const ushort TAG_MARKET_SWX_BookCondition	=14452;

		/* TAG_MARKET_SWX_SecurityTradingStatus (syntax: int32) */
		public const ushort TAG_MARKET_SWX_SecurityTradingStatus	=14453;

		/* TAG_MARKET_SWX_TradingSessionSubID (syntax: String) */
		public const ushort TAG_MARKET_SWX_TradingSessionSubID	=14454;

		/* TAG_MARKET_BOVESPA_SecurityTradingStatus (syntax: String) */
		public const ushort TAG_MARKET_BOVESPA_SecurityTradingStatus	=14470;

		/* TAG_MARKET_XETRA_ULTRA_PLUS_InstrumentStatus (syntax: float64) */
		public const ushort TAG_MARKET_XETRA_ULTRA_PLUS_InstrumentStatus	=14480;

		/* TAG_MARKET_ICE_BlockVolume (syntax: float64) */
		public const ushort TAG_MARKET_ICE_BlockVolume	=14500;

		/* TAG_MARKET_ICE_EFSVolume (syntax: float64) */
		public const ushort TAG_MARKET_ICE_EFSVolume	=14501;

		/* TAG_MARKET_ICE_EFPVolume (syntax: float64) */
		public const ushort TAG_MARKET_ICE_EFPVolume	=14502;

		/* TAG_MARKET_ICE_IntervalPriceLimitsOnHold (syntax: bool) */
		public const ushort TAG_MARKET_ICE_IntervalPriceLimitsOnHold	=14503;

		/* TAG_MARKET_EURONEXT_HaltReason (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_HaltReason	=14550;

		/* TAG_MARKET_EURONEXT_ClassState (syntax: String) */
		public const ushort TAG_MARKET_EURONEXT_ClassState	=14551;

		/* TAG_MARKET_EURONEXT_OrderEntryRejection (syntax: char) */
		public const ushort TAG_MARKET_EURONEXT_OrderEntryRejection	=14552;

		/* TAG_MARKET_EURONEXT_InstrumentState (syntax: char) */
		public const ushort TAG_MARKET_EURONEXT_InstrumentState	=14553;

		/* TAG_MARKET_EURONEXT_PhaseQualifier (syntax: int32) */
		public const ushort TAG_MARKET_EURONEXT_PhaseQualifier	=14554;

		/* TAG_MARKET_EURONEXT_OrderPriority (syntax: int64) */
		public const ushort TAG_MARKET_EURONEXT_OrderPriority	=14555;

		/* TAG_MARKET_EURONEXT_TradingPeriod (syntax: uint8) */
		public const ushort TAG_MARKET_EURONEXT_TradingPeriod	=14556;

		/* TAG_MARKET_OMX_SAXESS_OrderbookState (syntax: String) */
		public const ushort TAG_MARKET_OMX_SAXESS_OrderbookState	=14590;

		/* TAG_MARKET_OMX_SAXESS_OrderbookStopCode (syntax: String) */
		public const ushort TAG_MARKET_OMX_SAXESS_OrderbookStopCode	=14591;

		/* TAG_MARKET_OMX_SAXESS_InstrumentStopCode (syntax: String) */
		public const ushort TAG_MARKET_OMX_SAXESS_InstrumentStopCode	=14592;

		/* TAG_MARKET_OMX_SAXESS_SubmarketStopCode (syntax: String) */
		public const ushort TAG_MARKET_OMX_SAXESS_SubmarketStopCode	=14593;

		/* TAG_MARKET_OMX_NORDIC_MarketSegmentState (syntax: String) */
		public const ushort TAG_MARKET_OMX_NORDIC_MarketSegmentState	=14594;

		/* TAG_MARKET_OMX_NORDIC_OrderBookTradingState (syntax: String) */
		public const ushort TAG_MARKET_OMX_NORDIC_OrderBookTradingState	=14595;

		/* TAG_MARKET_OMX_NORDIC_OrderBookHaltReason (syntax: String) */
		public const ushort TAG_MARKET_OMX_NORDIC_OrderBookHaltReason	=14596;

		/* TAG_MARKET_OMX_NORDIC_NoteCodes1 (syntax: int32) */
		public const ushort TAG_MARKET_OMX_NORDIC_NoteCodes1	=14597;

		/* TAG_MARKET_OMX_NORDIC_NoteCodes2 (syntax: int32) */
		public const ushort TAG_MARKET_OMX_NORDIC_NoteCodes2	=14598;

		/* TAG_MARKET_LSE_PeriodName (syntax: String) */
		public const ushort TAG_MARKET_LSE_PeriodName	=14600;

		/* TAG_MARKET_LSE_PeriodStatus (syntax: String) */
		public const ushort TAG_MARKET_LSE_PeriodStatus	=14601;

		/* TAG_MARKET_LSE_SuspendedIndicator (syntax: String) */
		public const ushort TAG_MARKET_LSE_SuspendedIndicator	=14602;

		/* TAG_MARKET_LSE_HaltReason (syntax: String) */
		public const ushort TAG_MARKET_LSE_HaltReason	=14603;

		/* TAG_MARKET_LSE_OffBookReportingTradingStatus (syntax: String) */
		public const ushort TAG_MARKET_LSE_OffBookReportingTradingStatus	=14604;

		/* TAG_MARKET_LIFFE_MarketMode (syntax: String) */
		public const ushort TAG_MARKET_LIFFE_MarketMode	=14650;

		/* TAG_MARKET_LIFFE_MarketStatuses (syntax: int) */
		public const ushort TAG_MARKET_LIFFE_MarketStatuses	=14651;

		/* TAG_MARKET_ADH_OrderbookStateCode (syntax: String) */
		public const ushort TAG_MARKET_ADH_OrderbookStateCode	=14700;

		/* TAG_MARKET_TURQUOISE_HaltReason (syntax: String) */
		public const ushort TAG_MARKET_TURQUOISE_HaltReason	=14720;

		/* TAG_MARKET_TURQUOISE_DarkBookTradingStatus (syntax: Enum) */
		public const ushort TAG_MARKET_TURQUOISE_DarkBookTradingStatus	=14721;

		/* TAG_MARKET_TURQUOISE_OffBookReportingTradingStatus (syntax: Enum) */
		public const ushort TAG_MARKET_TURQUOISE_OffBookReportingTradingStatus	=14722;

		/* TAG_MARKET_CME_PreliminarySettlementPrice (syntax: float64) */
		public const ushort TAG_MARKET_CME_PreliminarySettlementPrice	=14740;

		/* TAG_MARKET_LSE_MIT_TradingStatusDetails (syntax: String) */
		public const ushort TAG_MARKET_LSE_MIT_TradingStatusDetails	=14750;

		/* TAG_MARKET_LSE_MIT_HaltReason (syntax: String) */
		public const ushort TAG_MARKET_LSE_MIT_HaltReason	=14752;

		/* TAG_MARKET_LSE_MIT_TotalAuctionVolume (syntax: float64) */
		public const ushort TAG_MARKET_LSE_MIT_TotalAuctionVolume	=14756;

		/* TAG_MARKET_OSE_TradingStateName (syntax: String) */
		public const ushort TAG_MARKET_OSE_TradingStateName	=14755;

		/* TAG_MARKET_NASDAQ_TradingActionReason (syntax: String) */
		public const ushort TAG_MARKET_NASDAQ_TradingActionReason	=14770;

		/* TAG_MARKET_NASDAQ_FarPrice (syntax: float64) */
		public const ushort TAG_MARKET_NASDAQ_FarPrice	=14771;

		/* TAG_MARKET_NASDAQ_NearPrice (syntax: float64) */
		public const ushort TAG_MARKET_NASDAQ_NearPrice	=14772;

		/* TAG_MARKET_NYSE_TradeCond (syntax: String) */
		public const ushort TAG_MARKET_NYSE_TradeCond	=14790;

		/* TAG_MARKET_NYSE_SecurityStatus (syntax: char) */
		public const ushort TAG_MARKET_NYSE_SecurityStatus	=14791;

		/* TAG_MARKET_NYSE_HaltCondition (syntax: char) */
		public const ushort TAG_MARKET_NYSE_HaltCondition	=14792;

		/* TAG_MARKET_OMNET_OMX_TradingStateName (syntax: String) */
		public const ushort TAG_MARKET_OMNET_OMX_TradingStateName	=14800;

		/* TAG_MARKET_BATS_AuctionType (syntax: char) */
		public const ushort TAG_MARKET_BATS_AuctionType	=14850;

		/* TAG_MARKET_BPOD_SubscriptionErrorCode (syntax: uint8) */
		public const ushort TAG_MARKET_BPOD_SubscriptionErrorCode	=14870;

		/* TAG_MARKET_CEF_LastTradeTradingPhase (syntax: char) */
		public const ushort TAG_MARKET_CEF_LastTradeTradingPhase	=14900;

		/* TAG_MARKET_BME_DynamicVariationRange (syntax: float64) */
		public const ushort TAG_MARKET_BME_DynamicVariationRange	=14920;

		/* TAG_MARKET_BME_StaticVariationRange (syntax: float64) */
		public const ushort TAG_MARKET_BME_StaticVariationRange	=14921;

		/* TAG_MARKET_BME_HaltReason (syntax: uint8) */
		public const ushort TAG_MARKET_BME_HaltReason	=14922;

		/* TAG_MARKET_MILAN_MIT_TradingStatusDetails (syntax: char) */
		public const ushort TAG_MARKET_MILAN_MIT_TradingStatusDetails	=14950;

		/* TAG_MARKET_PINKSHEET_QuoteFlags (syntax: uint16) */
		public const ushort TAG_MARKET_PINKSHEET_QuoteFlags	=14960;

		/* TAG_MARKET_PINKSHEET_QuoteCondition (syntax: char) */
		public const ushort TAG_MARKET_PINKSHEET_QuoteCondition	=14961;

		/* TAG_MARKET_JSE_MIT_TradingStatusDetails (syntax: char) */
		public const ushort TAG_MARKET_JSE_MIT_TradingStatusDetails	=14970;

		/* TAG_MARKET_BPIPE_PreviousBidPrice (syntax: float64) */
		public const ushort TAG_MARKET_BPIPE_PreviousBidPrice	=14980;

		/* TAG_MARKET_BPIPE_PreviousAskPrice (syntax: float64) */
		public const ushort TAG_MARKET_BPIPE_PreviousAskPrice	=14981;

		/* TAG_MARKET_MFDS_Flags (syntax: uint16) */
		public const ushort TAG_MARKET_MFDS_Flags	=14990;

		/* TAG_MARKET_MFDS_CapitalDistributionFlags (syntax: uint16) */
		public const ushort TAG_MARKET_MFDS_CapitalDistributionFlags	=14991;

		/* TAG_MARKET_MFDS_DividendInterestFlags (syntax: uint16) */
		public const ushort TAG_MARKET_MFDS_DividendInterestFlags	=14992;

		/* TAG_MARKET_HK_TradingState (syntax: String) */
		public const ushort TAG_MARKET_HK_TradingState	=15010;

		/* TAG_MARKET_HK_ShortSellSharesTraded (syntax: float64) */
		public const ushort TAG_MARKET_HK_ShortSellSharesTraded	=15011;

		/* TAG_MARKET_HK_ShortSellTurnover (syntax: float64) */
		public const ushort TAG_MARKET_HK_ShortSellTurnover	=15012;

		/* TAG_MARKET_HK_CoolingOffStartTime (syntax: Timestamp) */
		public const ushort TAG_MARKET_HK_CoolingOffStartTime	=15013;

		/* TAG_MARKET_HK_CoolingOffEndTime (syntax: Timestamp) */
		public const ushort TAG_MARKET_HK_CoolingOffEndTime	=15014;

		/* TAG_MARKET_HK_DailyQuotaBalance (syntax: float64) */
		public const ushort TAG_MARKET_HK_DailyQuotaBalance	=15015;

		/* TAG_MARKET_HK_BuyTurnover (syntax: float64) */
		public const ushort TAG_MARKET_HK_BuyTurnover	=15016;

		/* TAG_MARKET_HK_SellTurnover (syntax: float64) */
		public const ushort TAG_MARKET_HK_SellTurnover	=15017;

		/* TAG_MARKET_HK_BuySellTurnover (syntax: float64) */
		public const ushort TAG_MARKET_HK_BuySellTurnover	=15018;

		/* TAG_MARKET_HK_NominalPrice (syntax: float64) */
		public const ushort TAG_MARKET_HK_NominalPrice	=15019;

		/* TAG_MARKET_QUANTUM_StockState (syntax: String) */
		public const ushort TAG_MARKET_QUANTUM_StockState	=15020;

		/* TAG_MARKET_QUANTUM_GroupStatus (syntax: char) */
		public const ushort TAG_MARKET_QUANTUM_GroupStatus	=15021;

		/* TAG_MARKET_JSDA_PrefixSequenceNumber (syntax: String) */
		public const ushort TAG_MARKET_JSDA_PrefixSequenceNumber	=15030;

		/* TAG_MARKET_JSDA_DataType (syntax: String) */
		public const ushort TAG_MARKET_JSDA_DataType	=15031;

		/* TAG_MARKET_NGM_KnockOutBuyback (syntax: char) */
		public const ushort TAG_MARKET_NGM_KnockOutBuyback	=15040;

		/* TAG_MARKET_NGM_FinancialStatus (syntax: String) */
		public const ushort TAG_MARKET_NGM_FinancialStatus	=15041;

		/* TAG_MARKET_TSE_BidMarketOrderVolume (syntax: float64) */
		public const ushort TAG_MARKET_TSE_BidMarketOrderVolume	=15060;

		/* TAG_MARKET_TSE_AskMarketOrderVolume (syntax: float64) */
		public const ushort TAG_MARKET_TSE_AskMarketOrderVolume	=15061;

		/* TAG_MARKET_TSE_BidSpecialQuotePrice (syntax: float64) */
		public const ushort TAG_MARKET_TSE_BidSpecialQuotePrice	=15062;

		/* TAG_MARKET_TSE_AskSpecialQuotePrice (syntax: float64) */
		public const ushort TAG_MARKET_TSE_AskSpecialQuotePrice	=15063;

		/* TAG_MARKET_MICEX_SecurityTradingStatus (syntax: uint16) */
		public const ushort TAG_MARKET_MICEX_SecurityTradingStatus	=15070;

		/* TAG_MARKET_ULTRAFEED_CorporateActionReportType (syntax: uint8) */
		public const ushort TAG_MARKET_ULTRAFEED_CorporateActionReportType	=15080;

		/* TAG_MARKET_KOR_LiquidityProviderHoldedQuantity (syntax: float64) */
		public const ushort TAG_MARKET_KOR_LiquidityProviderHoldedQuantity	=15090;
            /// ***GENERATOR-end-QUOT_TAGS-
            #endregion

            #region Constructor
            private TagsQuotation() { }
            #endregion
         }
    }
}
