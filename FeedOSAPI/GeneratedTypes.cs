///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class FOSOrderBookLevel : FOSInteger
        {
            public FOSOrderBookLevel(long level):base(level)
            {
            }

        }

        public class FOSQty : FOSInteger
        {
            public FOSQty(long qty):base(qty)
            {
            }
        }

        #region OrderBookChangeIndicator
        public class OrderBookChangeIndicator
        {
            public static void split_OrderBookChangeIndicator(int source_Indicator, out bool is_complete, out uint start_level)
            {
                if (source_Indicator < 0) {
                    is_complete = true;
                    start_level = (uint)((-source_Indicator) - 1);
                } else { // source_Indicator >= 0
                    is_complete = false;
                    start_level = (uint)source_Indicator;
                }
            }

            public static int build_OrderBookChangeIndicator(bool is_complete, uint start_level)
            {
                if (is_complete) {
                    return -(((int)start_level) + 1);
                } // return source_Indicator >= 0
                return (int)start_level;
            }
            #region Constructor
            private OrderBookChangeIndicator() { }
            #endregion
        }
        #endregion
        


        public class UTCTimestamps
        {
           #region Constructor
            public UTCTimestamps(ValueType serverTimestamp, ValueType marketTimestamp)
           {
               m_Server = serverTimestamp;
               m_Market = marketTimestamp;
           }
           #endregion

           #region Properties
            public ValueType Server
           {
             get { return m_Server; }
           }
            public ValueType Market
           {
             get { return m_Market; }
           }
           #endregion

           #region Members
            protected ValueType m_Server;
            protected ValueType m_Market; 
           #endregion 
        }

        public class MBLQty
        {
            #region Constructor
            public MBLQty(FOSQty cumulated_units, FOSQty nb_orders)
            {
                m_CumulatedUnits = cumulated_units;
                m_NbOrders = nb_orders;
            }
            #endregion

            #region Properties
            public FOSQty CumulatedUnits
            {
                get { return m_CumulatedUnits; }
            }
            public FOSQty NbOrders
            {
                get { return m_NbOrders; }
            }
            #endregion

            #region Members
            protected FOSQty m_CumulatedUnits;
            protected FOSQty m_NbOrders;
            #endregion
        }
    }

    /// Util class to find a tag by its number
    public class TagFinder
    {
        public ushort TagNum
        {
            set { m_TagNum = value; }
        }

        private ushort m_TagNum;
        public TagFinder(ushort tagNum)
        {
            m_TagNum = tagNum;
        }

        public bool FindTagWithNum(FeedOSAPI.Types.TagNumAndValue tagNumAndValue)
        {
            return (m_TagNum == tagNumAndValue.Num);
        }
    }
}
