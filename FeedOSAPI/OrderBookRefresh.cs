///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class OrderBookRefresh
        {
            
            #region Constructor
            public OrderBookRefresh(DateTime serverUTCDateTime, DateTime marketUTCDateTime, bool isBidEndReached, int bidStartLevel, List<OrderBookEntryExt> bidLimits,
                bool isAskEndReached, int askStartLevel, List<OrderBookEntryExt> askLimits)
            {
                m_ServerUTCDateTime = serverUTCDateTime;
                m_MarketUTCDateTime = marketUTCDateTime;
                m_IsBidEndReached = isBidEndReached;
                m_BidStartLevel = bidStartLevel;
                m_BidLimits = bidLimits;
                m_IsAskEndReached = isAskEndReached;
                m_AskStartLevel = askStartLevel;
                m_AskLimits = askLimits;
            }
            
            public OrderBookRefresh(DateTime serverUTCDateTime, DateTime marketUTCDateTime)
            {
                m_ServerUTCDateTime = serverUTCDateTime;
                m_MarketUTCDateTime = marketUTCDateTime;
                m_IsBidEndReached = false;
                m_BidStartLevel = 0;
                m_BidLimits = new List<OrderBookEntryExt>();
                m_IsAskEndReached = false;
                m_AskStartLevel = 0;
                m_AskLimits = new List<OrderBookEntryExt>();
            }
            #endregion

            #region Properties
            public DateTime ServerUTCDateTime
            {
                get { return m_ServerUTCDateTime; }
            }
            public DateTime MarketUTCDateTime
            {
                get { return m_MarketUTCDateTime; }
            }
            public int BidStartLevel
            {
                get { return m_BidStartLevel; }
                set { m_BidStartLevel = value; }
            }
            public bool IsBidEndReached
            {
                get { return m_IsBidEndReached; }
                set { m_IsBidEndReached = value; }
            }
            public List<OrderBookEntryExt> BidLimits
            {
                get { return m_BidLimits; }
                set { m_BidLimits = value; }
            }
            public int AskStartLevel
            {
                get { return m_AskStartLevel; }
                set { m_AskStartLevel = value; }
            }
            public bool IsAskEndReached
            {
                get { return m_IsAskEndReached; }
                set { m_IsAskEndReached = value; }
            }
            public List<OrderBookEntryExt> AskLimits
            {
                get { return m_AskLimits; }
                set { m_AskLimits = value; }
            }
            #endregion

            #region Members
            protected DateTime m_ServerUTCDateTime;		// depth at which the cached data should be overidden with inLimits 
            protected DateTime m_MarketUTCDateTime;		// depth at which the cached data should be overidden with inLimits 
            protected int m_BidStartLevel;		// depth at which the cached data should be overidden with inLimits 
            protected bool m_IsBidEndReached;		// true if the given inLimits spread up to the last available limit
            protected List<OrderBookEntryExt> m_BidLimits;
            protected int m_AskStartLevel;
            protected bool m_IsAskEndReached;
            protected List<OrderBookEntryExt> m_AskLimits;
            #endregion

        }
    }
}