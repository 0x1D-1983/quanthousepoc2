///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class InstrumentStatusL1
        {
            #region Constructor
            public InstrumentStatusL1(uint InstrumentCode, List<TagNumAndValue> QuotationVariables, OrderBookBestLimitsExt OrderBookBestLimitsExt)
            {
                m_InstrumentCode = InstrumentCode;
                m_QuotationVariables = QuotationVariables;
                m_OrderBookBestLimitsExt = OrderBookBestLimitsExt;
		    }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public OrderBookBestLimitsExt OrderBookBestLimitsExt
            {
                get { return m_OrderBookBestLimitsExt; }
            }
            public List<TagNumAndValue> QuotationVariables
            {
                get { return m_QuotationVariables; }
            }
            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected List<TagNumAndValue> m_QuotationVariables;
		    protected OrderBookBestLimitsExt m_OrderBookBestLimitsExt;
            #endregion
        }
    }
}
