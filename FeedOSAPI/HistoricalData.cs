///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class HistoDailyPoint
        {
            #region Members
            private ValueType m_LocalDate;
            private double m_OpenPrice;
            private double m_ClosePrice;
            private double m_HighPrice;
            private double m_LowPrice;
            private double m_VolumeTraded;
            private double m_AssetTraded;
            #endregion

            #region Constructor
            public HistoDailyPoint(ValueType localDate, double open, double close, double high, double low, double volumeTraded, double assetTraded)
            {
                m_LocalDate = localDate;
                m_OpenPrice = open;
                m_ClosePrice = close;
                m_HighPrice = high;
                m_LowPrice = low;
                m_VolumeTraded = volumeTraded;
                m_AssetTraded = assetTraded;
		    }
            #endregion

            #region Properties
            public DateTime LocalDate
            {
                get { return (DateTime)m_LocalDate; }
            }
            public double OpenPrice
            {
                get { return m_OpenPrice; }
            }
            public double ClosePrice
            {
                get { return m_ClosePrice; }
            }
            public double HighPrice
            {
                get { return m_HighPrice; }
            }
            public double LowPrice
            {
                get { return m_LowPrice; }
            }
            public double TotalVolumeTraded
            {
                get { return m_VolumeTraded; }
            }
            public double TotalAssetTraded
            {
                get { return m_AssetTraded; }
            }
            #endregion
        }

        public class HistoDailyPointExtended
        {
            #region Members
            private DateTime m_Date;
            private sbyte m_TradingSessionId;
            private double m_OpenPrice;
            private double m_HighPrice;
            private double m_LowPrice;
            private double m_ClosePrice;
            private double m_VolumeTraded;
            private double m_AssetTraded;
            private List<TagNumAndValue> m_OtherValues;
            #endregion

            #region Constructor
            public HistoDailyPointExtended(ValueType date, sbyte tradingSessionId, double open, double high, double low, double close, double volumeTraded, double assetTraded, List<TagNumAndValue> otherValues)
            {
                m_Date = (DateTime)date;
                m_TradingSessionId = tradingSessionId;
                m_OpenPrice = open;
                m_HighPrice = high;
                m_LowPrice = low;
                m_ClosePrice = close;
                m_VolumeTraded = volumeTraded;
                m_AssetTraded = assetTraded;
                m_OtherValues = otherValues;
            }
            #endregion

            #region Properties
            public DateTime Date
            {
                get { return m_Date; }
            }
            public sbyte TradingSessionId
            {
                get { return m_TradingSessionId; }
            }
            public double OpenPrice
            {
                get { return m_OpenPrice; }
            }
            public double HighPrice
            {
                get { return m_HighPrice; }
            }
            public double LowPrice
            {
                get { return m_LowPrice; }
            }
            public double ClosePrice
            {
                get { return m_ClosePrice; }
            }
            public double TotalVolumeTraded
            {
                get { return m_VolumeTraded; }
            }
            public double TotalAssetTraded
            {
                get { return m_AssetTraded; }
            }
            public List<TagNumAndValue> OtherValues
            {
                get { return m_OtherValues; }
            }
            #endregion
        }

        // Legacy History Intraday Point
        public class HistoIntradayPoint
        {
            #region Members
            private ValueType m_MarketUTCTime;
            private double m_Price;
            private double m_Qty;
            #endregion

            #region Constructor
            public HistoIntradayPoint(ValueType marketTS, double price, double qty)
            {
                m_Price = price;
                m_Qty = qty;
                m_MarketUTCTime = marketTS;
            }
            #endregion

            #region Properties
            public double Price
            {
                get { return m_Price; }
            }
            public double Qty
            {
                get { return m_Qty; }
            }
            public DateTime MarketUTCTime
            {
                get { return (DateTime)m_MarketUTCTime; }
            }
            #endregion
        }

        public class HistoIntradayPoint2
        {
            #region Members
            private double m_Price;
            private double m_LastTradeQty;
            private ValueType m_MarketUTCTime;
            private ValueType m_ServerUTCTime;
            private uint m_ContentMask;
            private uint m_TradeConditionIndex;
            #endregion

            #region Constructor
            public HistoIntradayPoint2(double price, double lastTradeQty, ValueType serverTS, ValueType marketTS, uint contentMask, uint tradeConditionIndex)
            {
                m_Price = price;
                m_LastTradeQty = lastTradeQty;
                m_MarketUTCTime = marketTS;
                m_ServerUTCTime = serverTS;
                m_ContentMask = contentMask;
                m_TradeConditionIndex = tradeConditionIndex;
		    }
            #endregion

            #region Properties
            public double Price
            {
                get { return m_Price; }
            }
            public double LastTradeQty
            {
                get { return m_LastTradeQty; }
            }
            public DateTime MarketUTCTime
            {
                get { return (DateTime)m_MarketUTCTime; }
            }
            public DateTime ServerUTCTime
            {
                get { return (DateTime)m_ServerUTCTime; }
            }
            public uint ContentMask
            {
                get { return m_ContentMask; }
            }
            public uint TradeConditionIndex
            {
                get { return m_TradeConditionIndex; }
            }
            #endregion
        }

        public class HistoIntradayPointExtended
        {
            #region Members
            private double m_Price;
            private double m_LastTradeQty;
            private ValueType m_MarketUTCTime;
            private ValueType m_ServerUTCTime;
            private uint m_ContentMask;
            private uint m_TradeConditionIndex;
            private List<TagNumAndValue> m_Properties;
            #endregion

            #region Constructor
            public HistoIntradayPointExtended(double price, double lastTradeQty, ValueType serverTS, ValueType marketTS, uint contentMask, uint tradeConditionIndex, List<TagNumAndValue> properties)
            {
                m_Price = price;
                m_LastTradeQty = lastTradeQty;
                m_MarketUTCTime = marketTS;
                m_ServerUTCTime = serverTS;
                m_ContentMask = contentMask;
                m_TradeConditionIndex = tradeConditionIndex;
                m_Properties = properties;
            }
            #endregion

            #region Properties
            public double Price
            {
                get { return m_Price; }
            }
            public double LastTradeQty
            {
                get { return m_LastTradeQty; }
            }
            public DateTime MarketUTCTime
            {
                get { return (DateTime)m_MarketUTCTime; }
            }
            public DateTime ServerUTCTime
            {
                get { return (DateTime)m_ServerUTCTime; }
            }
            public uint ContentMask
            {
                get { return m_ContentMask; }
            }
            public uint TradeConditionIndex
            {
                get { return m_TradeConditionIndex; }
            }
            public List<TagNumAndValue> Properties
            {
                get { return m_Properties; }
            }
            #endregion
        }

        public class HistoIntradayDataExtended
        {
            #region Members
            private uint m_InstrCode;
            private String m_TradeId;
            private HistoIntradayPointExtended m_Point;
            #endregion

            #region Constructor
            public HistoIntradayDataExtended(uint instrCode, String tradeId, HistoIntradayPointExtended point)
            {
                m_InstrCode = instrCode;
                m_TradeId = tradeId;
                m_Point = point;
            }
            #endregion

            #region Properties
            public uint InstrCode
            {
                get { return m_InstrCode; }
            }
            public String TradeId
            {
                get { return m_TradeId; }
            }
            public HistoIntradayPointExtended Point
            {
                get { return m_Point; }
            }
            #endregion
        }

        public class HistoIntradayCancel
        {
            #region Members
            private uint m_InstrCode;
            private String m_TradeId;
            private DateTime m_OriginalMarketTimestamp;
            private bool m_FromVenue;
            private bool m_OffBook;
            #endregion

            #region Constructor
            public HistoIntradayCancel(uint instrCode, String tradeId, ValueType originalMarketTimestamp, bool fromVenue, bool offBook)
            {
                m_InstrCode = instrCode;
                m_TradeId = tradeId;
                m_OriginalMarketTimestamp = (DateTime)originalMarketTimestamp;
                m_FromVenue = fromVenue;
                m_OffBook = offBook;
            }
            #endregion

            #region Properties
            public uint InstrCode
            {
                get { return m_InstrCode; }
            }
            public String TradeId
            {
                get { return m_TradeId; }
            }
            public DateTime OriginalMarketTimestamp
            {
                get { return m_OriginalMarketTimestamp; }
            }
            public bool FromVenue
            {
                get { return m_FromVenue; }
            }
            public bool OffBook
            {
                get { return m_OffBook; }
            }
            #endregion
        }

        public class HistoIntradayCorrection
        {
            #region Members
            private uint m_InstrCode;
            private String m_TradeId;
            private DateTime m_OriginalMarketTimestamp;
            private bool m_FromVenue;
            private bool m_OffBook;
            private sbyte m_TradingSessionId;
            private TradeData m_Trade;
            #endregion

            #region Constructor
            public HistoIntradayCorrection(uint instrCode, String tradeId, ValueType originalMarketTimestamp, bool fromVenue, bool offBook, sbyte tradingSessionId, TradeData trade)
            {
                m_InstrCode = instrCode;
                m_TradeId = tradeId;
                m_OriginalMarketTimestamp = (DateTime)originalMarketTimestamp;
                m_FromVenue = fromVenue;
                m_OffBook = offBook;
                m_TradingSessionId = tradingSessionId;
                m_Trade = trade;
            }
            #endregion

            #region Properties
            public uint InstrCode
            {
                get { return m_InstrCode; }
            }
            public String TradeId
            {
                get { return m_TradeId; }
            }
            public DateTime OriginalMarketTimestamp
            {
                get { return m_OriginalMarketTimestamp; }
            }
            public bool FromVenue
            {
                get { return m_FromVenue; }
            }
            public bool OffBook
            {
                get { return m_OffBook; }
            }
            public sbyte TradingSessionId
            {
                get { return m_TradingSessionId; }
            }
            public TradeData Trade
            {
                get { return m_Trade; }
            }
            #endregion
        }

        public class TradeConditionsDictionaryEntry
        {
            #region Members
            private uint m_Index;
            private List<TagNumAndValue> m_Value; 
            #endregion

            #region Constructor
            public TradeConditionsDictionaryEntry(uint index, List<TagNumAndValue> value)
            {
                m_Index = index;
                m_Value = value;
            }
            #endregion
            
            #region Properties
            public uint Index
            {
                get { return m_Index; }
            }
            public List<TagNumAndValue> Value
            {
                get { return m_Value; }
            }
            #endregion
        }

        public class TagDeclaration
        {
            #region Members
            private String m_TagUsage;
            private uint m_TagSyntax;
            private ushort m_TagNumericId;
            private String m_TagName;
            private String m_TagDescription;
            #endregion

            #region Constructor
            public TagDeclaration(String usage, uint syntax, ushort id, String name, String description)
            {
                m_TagUsage = usage;
                m_TagSyntax = syntax;
                m_TagNumericId = id;
                m_TagName = name;
                m_TagDescription = description;
            }
            #endregion

            #region Properties
            public String TagUsage
            {
                get { return m_TagUsage; }
            }
            public uint TagSyntax
            {
                get { return m_TagSyntax; }
            }
            public ushort TagNumericId
            {
                get { return m_TagNumericId; }
            }
            public String TagName
            {
                get { return m_TagName; }
            }
            public String TagDescription
            {
                get { return m_TagDescription; }
            }
            #endregion
        }

        public class EnumValueDeclaration
        {
            #region Members
            Object m_Value;
            String m_ValueName;
            String m_ValueDescription;
            #endregion

            #region Constructor
            public EnumValueDeclaration(Object value, String name, String description)
            {
                m_Value = value;
                m_ValueName = name;
                m_ValueDescription = description;
            }
            #endregion

            #region Properties
            public Object Value
            {
                get { return m_Value; }
            }
            public String ValueName
            {
                get { return m_ValueName; }
            }
            public String ValueDescription
            {
                get { return m_ValueDescription; }
            }
            #endregion
        }

        public class EnumTypeDeclaration
        {
            #region Members
            private String m_EnumName;
            private String m_EnumDescription;
            private List<ushort> m_RelatedTags;
            private List<EnumValueDeclaration> m_EnumValues;
            #endregion

            #region Constructor
            public EnumTypeDeclaration(String name, String description, List<ushort> related_tags, List<EnumValueDeclaration> values)
            {
                m_EnumName = name;
                m_EnumDescription = description;
                m_RelatedTags = related_tags;
                m_EnumValues = values;
            }
            #endregion

            #region Properties
            public String EnumName
            {
                get { return m_EnumName; }
            }
            public String EnumDescription
            {
                get { return m_EnumDescription; }
            }
            public List<ushort> RelatedTags
            {
                get { return m_RelatedTags; }
            }
            public List<EnumValueDeclaration> EnumValues
            {
                get { return m_EnumValues; }
            }
            #endregion
        }
    }
}
