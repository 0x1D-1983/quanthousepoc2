///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2010 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MBLDeltaRefresh
        {
            #region Constructor
            public MBLDeltaRefresh(
                uint instrumentCode, 
                uint layerId,
                UTCTimestamps timestamps,
                OrderBookDeltaAction action,
                FOSOrderBookLevel level,
                FOSPrice price,
                MBLQty qty,
                bool coninuationFlag,
                List<TagNumAndValue> otherValues) 
            {
                m_InstrumentCode = instrumentCode;
                m_LayerId = layerId;
                m_Timestamps = timestamps;
                m_Action = action;
                m_Level = level;
                m_Price = price;
                m_Qty = qty;
                m_ContinuationFlag = coninuationFlag;
                m_OtherValues = otherValues;
            }
            #endregion

            #region print facilities
            public static Dictionary<OrderBookDeltaAction, string> s_ActionNames;
            static MBLDeltaRefresh()
            {
                if (null == s_ActionNames)
                {
                    s_ActionNames = new Dictionary<OrderBookDeltaAction,string>(9);     
                }
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_ALLClearFromLevel] = "ALLClearFromLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_BidClearFromLevel] = "BidClearFromLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_AskClearFromLevel] = "AskClearFromLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_BidInsertAtLevel] = "BidInsertAtLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_AskInsertAtLevel] = "AskInsertAtLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_BidRemoveLevel] = "BidRemoveLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_AskRemoveLevel] = "AskRemoveLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_BidChangeQtyAtLevel] = "BidChangeQtyAtLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_AskChangeQtyAtLevel] = "AskChangeQtyAtLevel";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_BidRemoveLevelAndAppend] = "BidRemoveLevelAndAppend";
                s_ActionNames[OrderBookDeltaAction.OrderBookDeltaAction_AskRemoveLevelAndAppend] = "AskRemoveLevelAndAppend";
            }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public uint LayerId
            {
                get { return m_LayerId; }
            }
            public UTCTimestamps Timestamps
            {
                get { return m_Timestamps; }    
            }
            public OrderBookDeltaAction Action
            {
                get { return m_Action; }
            }
            public FOSOrderBookLevel Level
            {
                get { return m_Level; }
            }
            public FOSPrice Price
            {
                get { return m_Price; }
            }
            public MBLQty Qty
            {
                get { return m_Qty; }
            }
            public bool ContinuationFlag    
            {
                get { return m_ContinuationFlag; }
            }           
            public  List<TagNumAndValue> OtherValues
            {
                get { return m_OtherValues; }
            }
            #endregion

            #region Members
            protected uint                  m_InstrumentCode;
            protected uint                  m_LayerId;
            protected UTCTimestamps         m_Timestamps;
            protected OrderBookDeltaAction  m_Action;
            protected FOSOrderBookLevel     m_Level;
            protected FOSPrice              m_Price;
            protected MBLQty                m_Qty;
            protected bool                  m_ContinuationFlag; 
            protected List<TagNumAndValue>  m_OtherValues;
            #endregion
        }
    }
}
