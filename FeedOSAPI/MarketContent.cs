///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketContent
        {
            #region Constructor
            public MarketContent(ushort FOSLowLevelMarketId, List<MarketBranchContent> MarketBranchContentList)
            {
                m_FOSLowLevelMarketId = FOSLowLevelMarketId;
                m_MarketBranchContentList = MarketBranchContentList;
            }
            #endregion

            #region Properties
            public ushort FOSMarketId
            {
                get { return m_FOSLowLevelMarketId; }
            }
            public List<MarketBranchContent> MarketBranchContentList
            {
                get { return m_MarketBranchContentList; }
            }
            #endregion

            #region Members
            protected ushort	m_FOSLowLevelMarketId;
            protected List<MarketBranchContent> m_MarketBranchContentList;
            #endregion
        }
    }
}
