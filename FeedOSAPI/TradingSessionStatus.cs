///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class TradingSessionStatus
        {
            #region Constructor
            public TradingSessionStatus(byte SessionId, FIXTradSesStatus TradSesStatusEnum, ValueType TradSesStartTime, ValueType TradSesOpenTime,
                ValueType TradSesPreCloseTime, ValueType TradSesCloseTime, ValueType TradSesEndTime, double TotalVolumeTraded, ValueType LastUpdateTimestamp)
            {
                m_SessionId = SessionId;
                m_TradSesStatusEnum = TradSesStatusEnum;
                m_TradSesStartTime = TradSesStartTime;
                m_TradSesOpenTime = TradSesOpenTime;
                m_TradSesPreCloseTime = TradSesPreCloseTime;
                m_TradSesCloseTime = TradSesCloseTime;
                m_TradSesEndTime = TradSesEndTime;
                m_TotalVolumeTraded = TotalVolumeTraded;
                m_LastUpdateTimestamp = LastUpdateTimestamp;
            }
            #endregion

            #region Properties
            public byte SessionId
            {
                get { return m_SessionId; }
            }
            public FIXTradSesStatus TradSesStatusEnum
            {
                get { return m_TradSesStatusEnum; }
            }
            public ValueType TradSesStartTime
            {
                get { return m_TradSesStartTime; }
            }
            public ValueType TradSesOpenTime
            {
                get { return m_TradSesOpenTime; }
            }
            public ValueType TradSesPreCloseTime
            {
                get { return m_TradSesPreCloseTime; }
            }
            public ValueType TradSesCloseTime
            {
                get { return m_TradSesCloseTime; }
            }
            public ValueType TradSesEndTime
            {
                get { return m_TradSesEndTime; }
            }
            public double TotalVolumeTraded
            {
                get { return m_TotalVolumeTraded; }
            }
            public ValueType LastUpdateTimestamp
            {
                get { return m_LastUpdateTimestamp; }
            }
            #endregion

            #region Members
            protected byte		m_SessionId;
		    protected FIXTradSesStatus	m_TradSesStatusEnum;
            protected ValueType m_TradSesStartTime;
            protected ValueType m_TradSesOpenTime;
            protected ValueType m_TradSesPreCloseTime;
            protected ValueType m_TradSesCloseTime;
            protected ValueType m_TradSesEndTime;
		    protected double	m_TotalVolumeTraded;
		    protected ValueType	m_LastUpdateTimestamp;
            #endregion
        }
    }
}
