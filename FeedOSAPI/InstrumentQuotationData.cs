///-------------------------------
/// FeedOS C# Client API
/// copyright QuantHouse 2003-2015
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class InstrumentQuotationData
        {
            #region Constructor
            public InstrumentQuotationData(uint internal_instrument_code)
            {
                m_InstrumentCode = internal_instrument_code;
                reset();
            }
            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected SortedDictionary<ushort, object> m_QuotationValues;

            protected OrderBookBestLimitsExt m_BestLimits;
            protected bool m_ProcessTradeCancelCorrection;

            protected bool m_BestBidUpdated;
            protected bool m_BestAskUpdated;
            protected bool m_TrackUpdatesEnabled;
            protected SortedDictionary<ushort, bool?> m_TrackedTags;
            protected SortedDictionary<ushort, bool?> m_UpdatedTags;
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
                set { m_InstrumentCode = value; }
            }
            public SortedDictionary<ushort, object> QuotationValues
            {
                get { return m_QuotationValues; }
            }
            public OrderBookBestLimitsExt BestLimits
            {
                get { return m_BestLimits; }
                set { m_BestLimits = value; }
            }
            public bool TrackUpdatesEnabled
            {
                get { return m_TrackUpdatesEnabled; }
                set { m_TrackUpdatesEnabled = value; }
            }
            public SortedDictionary<ushort, bool?> TrackedTags
            {
                get { return m_TrackedTags; }
            }
            public SortedDictionary<ushort, bool?> UpdatedTags
            {
                get { return m_UpdatedTags; }
            }
            public bool BestBidUpdated
            {
                get { return m_BestBidUpdated; }
            }
            public bool BestAskUpdated
            {
                get { return m_BestAskUpdated; }
            }
            #endregion

            public void reset()
            {
                m_ProcessTradeCancelCorrection = true;
                m_TrackUpdatesEnabled = false;
                m_BestBidUpdated = false;
                m_BestAskUpdated = false;

                m_BestLimits = new OrderBookBestLimitsExt(new OrderBookEntryExt(0.0, 0.0, 0), new OrderBookEntryExt(0.0, 0.0, 0));
                m_QuotationValues = new SortedDictionary<ushort, Object>();
                m_UpdatedTags = new SortedDictionary<ushort, bool?>();
                m_TrackedTags = new SortedDictionary<ushort, bool?>();

                fill_default_values();
            }

            public void resetTracking()
            {
                m_BestBidUpdated = false;
                m_BestAskUpdated = false;
                m_UpdatedTags.Clear();
            }

            public void resetTrackedTags()
            {
                m_TrackedTags.Clear();
            }

            public void addTrackedTag(ushort tag_num)
            {
                m_TrackedTags.Add(tag_num, true);
            }

            public object getTagByNumber(ushort tag_num)
            {
                object value;
                m_QuotationValues.TryGetValue(tag_num, out value);
                return value;
            }

            public bool removeTagByNumber(ushort tag_num)
            {
                markAsUpdated(tag_num);
                return m_QuotationValues.Remove(tag_num);
            }

            public void setTag(TagNumAndValue tag)
            {
                markAsUpdated(tag.Num);
                m_QuotationValues[tag.Num] = tag.Value;
            }

            public void setTag(ushort tag_num, object value)
            {
                markAsUpdated(tag_num);
                m_QuotationValues[tag_num] = value;
            }

            public void setOrDeleteTimestampTag(ushort tag_num, DateTime ts)
            {
                markAsUpdated(tag_num);
                if (ts == DateTime.MinValue)
                {
                    m_QuotationValues.Remove(tag_num);
                }
                else
                {
                    m_QuotationValues[tag_num] = (object)ts;
                }
            }

            private void markAsUpdated(ushort tag_num)
            {
                if (m_TrackUpdatesEnabled && (m_TrackedTags.Count == 0 || m_TrackedTags.ContainsKey(tag_num)))
                {
                    m_UpdatedTags[tag_num] = true;
                }
            }

            private void fill_default_values()
            {
                setTag(TagsQuotation.TAG_TradingStatus, FIXTradSesStatus.FIXSecurityTradingStatus_UnknownOrInvalid);
            }

            public void reloadValues(SortedDictionary<ushort, object> newValues)
            {
                foreach (KeyValuePair<ushort, object> tagNumAndValue in newValues)
                {
                    setTag(tagNumAndValue.Key, tagNumAndValue.Value);
                }
            }

            public void update_with_TradeEventExt(DateTime server_timestamp, DateTime market_timestamp, QuotationTradeEventExt trade_event_ext)
            {
                // Parse the context tags (if context content mask set) when:
                // - it is a trade to extract trading session id, trade impact indicator and trade execution timestamp
                // - it is a session price update not related to an off-book trade to extract trading session id and trade impact indicator
                uint trade_impact_indicator = 0;
                bool has_actual_trading_session_id = false;
                DateTime trade_execution_timestamp = market_timestamp;
                

                if (trade_event_ext.Context != null && QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Context) &&
                    (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastTradeQty)
                            ||
                    (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Session) &&
                     QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice) &&
                     !QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade))))
                {
                    extract_trade_context(trade_event_ext.Context, ref has_actual_trading_session_id, ref trade_impact_indicator, ref trade_execution_timestamp);
                }

                bool can_update_session_price_tags =  QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Session)
                                                &&  QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice)
                                                && !QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade)
                                                && !has_actual_trading_session_id;

                // PriceActivity timestamps
                if (   QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice)
                    || QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Bid)
                    || QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Ask))
                {
                    setTag(TagsQuotation.TAG_InternalPriceActivityTimestamp, server_timestamp);
                    

                    
                    object price_activity_market_timestamp = getTagByNumber(TagsQuotation.TAG_PriceActivityMarketTimestamp);

                    if (market_timestamp != DateTime.MinValue && // Fix 10867 : PriceActivityMarketTimestamp should not be set to NULL
                       (price_activity_market_timestamp == null || 
                        DateTime.Compare(market_timestamp, (DateTime)price_activity_market_timestamp) > 0)) // Fix 38598 never decrease PriceActivityMarketTimestamp
                    {
                        setTag(TagsQuotation.TAG_PriceActivityMarketTimestamp, market_timestamp);
                    }
                }

                // best limits
                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Bid))
                {
                    m_BestLimits.BestBid = trade_event_ext.BestBid;
                    if (m_TrackUpdatesEnabled)
                    {
                        m_BestBidUpdated = true;
                    }
                }

                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Ask))
                {
                    m_BestLimits.BestAsk = trade_event_ext.BestAsk;
                    if (m_TrackUpdatesEnabled)
                    {
                        m_BestAskUpdated = true;
                    }
                }

                // close
                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Close))
                {
                    if (can_update_session_price_tags)
                    {
                        setTag(TagsQuotation.TAG_SessionClosingPrice, trade_event_ext.Price);
                    }
                    if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OCHL_daily))
                    {
                        setTag(TagsQuotation.TAG_InternalDailyCloseTimestamp,server_timestamp);

                        if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice))
                        {
                            setTag(TagsQuotation.TAG_DailyClosingPrice, trade_event_ext.Price);
                            setOrDeleteTimestampTag(TagsQuotation.TAG_DailyClosingPriceTimestamp, trade_execution_timestamp);
                        }
                    }
                }

                // change business day (cbd)
                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_ChangeBusinessDay))
                {
                    reset_daily_aggregates(server_timestamp, trade_event_ext.ContentMask);
                    reset_session_aggregates(trade_event_ext.ContentMask);
                    removeTagByNumber(TagsQuotation.TAG_TradingSessionId);
                }

                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Open))
                {
                    if (!has_actual_trading_session_id)
                    {
                        object tmp_price = getTagByNumber(TagsQuotation.TAG_SessionClosingPrice);
                        if (tmp_price != null)
                        {
                            setTag(TagsQuotation.TAG_PreviousSessionClosingPrice, tmp_price);
                        }
                        else
                        {
                            removeTagByNumber(TagsQuotation.TAG_PreviousSessionClosingPrice);
                        }

                        reset_session_aggregates(trade_event_ext.ContentMask);
                    }

                    // session open
                    if (can_update_session_price_tags)
                    {
                        setTag(TagsQuotation.TAG_SessionOpeningPrice, trade_event_ext.Price);
                    }

                    // daily open
                    if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OCHL_daily))
                    {
                        if (!QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Session))
                        {
                            removeTagByNumber(TagsQuotation.TAG_TradingSessionId);
                        }

                        setTag(TagsQuotation.TAG_InternalDailyOpenTimestamp, server_timestamp);

                        if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice))
                        {
                            setTag(TagsQuotation.TAG_DailyOpeningPrice, trade_event_ext.Price);
                            setOrDeleteTimestampTag(TagsQuotation.TAG_DailyOpeningPriceTimestamp, trade_execution_timestamp);
                        }
                        else
                        {
                            removeTagByNumber(TagsQuotation.TAG_DailyOpeningPrice);
                            removeTagByNumber(TagsQuotation.TAG_DailyOpeningPriceTimestamp);
                        }
                    }
                }

                // low
                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_Low))
                {
                    if (can_update_session_price_tags)
                    {
                        setTag(TagsQuotation.TAG_SessionLowPrice, trade_event_ext.Price);
                    }

                    if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OCHL_daily))
                    {
                        setTag(TagsQuotation.TAG_DailyLowPrice, trade_event_ext.Price);
                        setTag(TagsQuotation.TAG_InternalDailyLowTimestamp, server_timestamp);
                        setOrDeleteTimestampTag(TagsQuotation.TAG_DailyLowPriceTimestamp, trade_execution_timestamp);
                    }
                }

                // high
                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_High))
                {
                    if (can_update_session_price_tags)
                    {
                        setTag(TagsQuotation.TAG_SessionHighPrice, trade_event_ext.Price);
                    }

                    if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OCHL_daily))
                    {
                        setTag(TagsQuotation.TAG_DailyHighPrice, trade_event_ext.Price);
                        setTag(TagsQuotation.TAG_InternalDailyHighTimestamp, server_timestamp);
                        setOrDeleteTimestampTag(TagsQuotation.TAG_DailyHighPriceTimestamp, trade_execution_timestamp);
                    }
                }

                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice))
                {
                    setTag(TagsQuotation.TAG_LastPrice, trade_event_ext.Price);

                    if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastTradeQty))
                    {
                        consume_trade_event(trade_event_ext.Price, trade_event_ext.LastTradeQty, trade_execution_timestamp, trade_event_ext.ContentMask, has_actual_trading_session_id, trade_impact_indicator);
                    }
                }

                // other values
                if (QuotationContentMaskHelper.contains(trade_event_ext.ContentMask, QuotationContentMaskHelper.QuotationContentBit_OtherValues) && trade_event_ext.Values != null)
                {
                    update_values(server_timestamp, market_timestamp, trade_execution_timestamp, trade_event_ext.Values, !has_actual_trading_session_id);
                }
            }

            public void extract_trade_execution_timestamp(List<TagNumAndValue> data, ref DateTime trade_execution_timestamp)
            {
                if (data == null)
                {
                    return;
                }
                foreach(TagNumAndValue tagNumAndValue in data)
                {
                    if (tagNumAndValue.Num == TagsContext.TAG_TradeExecutionTimestamp && tagNumAndValue.Value != null)
                    {
                        trade_execution_timestamp = (DateTime)tagNumAndValue.Value;
                        break;
                    }
                }
            }

            public void update_with_TradeCancelCorrection(DateTime server_timestamp, QuotationTradeCancelCorrection data)
            {
                double delta_volume = 0.0, delta_asset = 0.0, delta_offbook_volume = 0.0, delta_offbook_asset = 0.0;

                uint original_tii = data.OriginalTrade.TradeImpactIndicator;
                uint corrected_tii = data.CorrectedTrade.TradeImpactIndicator;

                bool is_correction = TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_IsCorrection);

                bool original_impact_volume =
                        (       TradeImpactIndicatorHelper.contains(original_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasVolume)
                            &&  TradeImpactIndicatorHelper.contains(original_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_Volume)
                        )
                        ||
                        (      !TradeImpactIndicatorHelper.contains(original_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasVolume)
                            && !TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_OffBookTrade)
                        );

                bool original_impact_offbook_volume =
                        (       TradeImpactIndicatorHelper.contains(original_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasOffBookVolume)
                            &&  TradeImpactIndicatorHelper.contains(original_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_OffBookVolume)
                        )
                        ||
                        (      !TradeImpactIndicatorHelper.contains(original_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasOffBookVolume)
                            &&  TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_OffBookTrade)
                        );

                if (original_impact_volume)
                {
                    delta_volume -= data.OriginalTrade.Quantity;
                    delta_asset -= data.OriginalTrade.Price * data.OriginalTrade.Quantity;
                }

                if (original_impact_offbook_volume)
                {
                    delta_offbook_volume -= data.OriginalTrade.Quantity;
                    delta_offbook_asset -= data.OriginalTrade.Price * data.OriginalTrade.Quantity;
                }

                if (is_correction)
                {
                    bool corrected_impact_volume =
                            (       TradeImpactIndicatorHelper.contains(corrected_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasVolume)
                                &&  TradeImpactIndicatorHelper.contains(corrected_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_Volume)
                            )
                            ||
                            (      !TradeImpactIndicatorHelper.contains(corrected_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasVolume)
                                && !TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_OffBookTrade)
                            );

                    bool corrected_impact_offbook_volume =
                            (       TradeImpactIndicatorHelper.contains(corrected_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasOffBookVolume)
                                &&  TradeImpactIndicatorHelper.contains(corrected_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_OffBookVolume)
                            )
                            ||
                            (      !TradeImpactIndicatorHelper.contains(corrected_tii, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasOffBookVolume)
                                &&  TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_OffBookTrade)
                            );

                    if (corrected_impact_volume)
                    {
                        delta_volume += data.CorrectedTrade.Quantity;
                        delta_asset += data.CorrectedTrade.Price * data.CorrectedTrade.Quantity;
                    }

                    if (corrected_impact_offbook_volume)
                    {
                        delta_offbook_volume += data.CorrectedTrade.Quantity;
                        delta_offbook_asset += data.CorrectedTrade.Price * data.CorrectedTrade.Quantity;
                    }
                }

                if (delta_volume != 0.0)
                {
                    object dv = getTagByNumber(TagsQuotation.TAG_DailyTotalVolumeTraded);
                    if (dv != null)
                    {
                        setTag(TagsQuotation.TAG_DailyTotalVolumeTraded, (double)dv + delta_volume);
                    }
                }
                if (delta_asset != 0.0)
                {
                    object da = getTagByNumber(TagsQuotation.TAG_DailyTotalAssetTraded);
                    if (da != null)
                    {
                        setTag(TagsQuotation.TAG_DailyTotalAssetTraded, (double)da + delta_asset);
                    }
                }
                if (delta_offbook_volume != 0.0)
                {
                    object dov = getTagByNumber(TagsQuotation.TAG_DailyTotalOffBookVolumeTraded);
                    if (dov != null)
                    {
                        setTag(TagsQuotation.TAG_DailyTotalOffBookVolumeTraded, (double)dov + delta_offbook_volume);
                    }
                }
                if (delta_offbook_asset != 0.0)
                {
                    object doa = getTagByNumber(TagsQuotation.TAG_DailyTotalOffBookAssetTraded);
                    if (doa != null)
                    {
                        setTag(TagsQuotation.TAG_DailyTotalOffBookAssetTraded, (double)doa + delta_offbook_asset);
                    }
                }

                // other values
                if (TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_CorrectedValues) && data.CorrectedValues != null)
                {
                    DateTime trade_execution_timestamp = new DateTime();

                    extract_trade_execution_timestamp(data.CorrectedTrade.TradeProperties, ref trade_execution_timestamp);
                    update_values(server_timestamp, new DateTime(), trade_execution_timestamp, data.CorrectedValues,
                        TradeCancelCorrectionContentMaskHelper.contains(data.ContentMask, TradeCancelCorrectionContentMaskHelper.TradeCancelCorrectionContentBit_CurrentSession));
                }
            }

            private void update_values(DateTime server_timestamp, DateTime market_timestamp, DateTime trade_execution_timestamp, List<TagNumAndValue> values, bool update_session_tags)
            {
                bool daily_opening_price_explicit = false;
                bool daily_opening_price_timestamp_explicit = false;
                bool daily_closing_price_explicit = false;
                bool daily_closing_price_timestamp_explicit = false;
                bool daily_high_price_explicit = false;
                bool daily_high_price_timestamp_explicit = false;
                bool daily_low_price_explicit = false;
                bool daily_low_price_timestamp_explicit = false;
                bool previous_daily_closing_price_explicit = false;
                bool previous_daily_closing_price_timestamp_explicit = false;
                bool last_auction_explicit = false;

                foreach (TagNumAndValue tagNumAndValue in values)
                {
                    ushort tag_num = tagNumAndValue.Num;
                    object tag_value = tagNumAndValue.Value;

                    if (tag_value == null)
                    {
                       // for the tags enumerated in the following switch we automatically delete the associated timestamp
                       // Note: if a (not null) timestamp follows in the values it will be inserted again
                       // this would be a bug of the FH :
                       // who would reset the TAG_DailyOpeningPrice and set TAG_DailyOpeningPriceTimestamp at the same time?
                        switch (tag_num)
                        {
                            case TagsQuotation.TAG_DailyOpeningPrice:
                                removeTagByNumber(TagsQuotation.TAG_DailyOpeningPriceTimestamp);
                                break;
                            case TagsQuotation.TAG_DailyClosingPrice:
                                removeTagByNumber(TagsQuotation.TAG_DailyClosingPriceTimestamp);
                                break;
                            case TagsQuotation.TAG_DailyHighPrice:
                                removeTagByNumber(TagsQuotation.TAG_DailyHighPriceTimestamp);
                                break;
                            case TagsQuotation.TAG_DailyLowPrice:
                                removeTagByNumber(TagsQuotation.TAG_DailyLowPriceTimestamp);
                                break;
                            case TagsQuotation.TAG_PreviousDailyClosingPrice:
                                removeTagByNumber(TagsQuotation.TAG_PreviousDailyClosingPriceTimestamp);
                                break;
                            default:
                                break;
                        }

                        removeTagByNumber(tag_num);
                        continue; // CONTINUE HERE
                    }

                    switch (tag_num)
                    {
                        case TagsQuotation.TAG_SessionOpeningPrice:
                        case TagsQuotation.TAG_SessionHighPrice:
                        case TagsQuotation.TAG_SessionLowPrice:
                        case TagsQuotation.TAG_SessionClosingPrice:
                        case TagsQuotation.TAG_SessionTotalVolumeTraded:
                        case TagsQuotation.TAG_SessionTotalAssetTraded:
                        case TagsQuotation.TAG_SessionTotalOffBookVolumeTraded:
                        case TagsQuotation.TAG_SessionTotalOffBookAssetTraded:
                            if (!update_session_tags)
                            {
                                continue; // CONTINUE HERE
                            }
                            break;

                        case TagsQuotation.TAG_TradingStatus:
                            FeedOSAPI.Types.FIXTradSesStatus ts = (FeedOSAPI.Types.FIXTradSesStatus)(uint)tag_value;
                            if (ts > FeedOSAPI.Types.FIXTradSesStatus.FIXSecurityTradingStatus_Cross)
                            {
                                ts = FeedOSAPI.Types.FIXTradSesStatus.FIXSecurityTradingStatus_UnknownOrInvalid;
                            }
                            setTag(TagsQuotation.TAG_TradingStatus, ts);
                            continue;
                        case TagsQuotation.TAG_LastAuctionPrice: // switch fall through
                        case TagsQuotation.TAG_LastAuctionVolume:
                            last_auction_explicit = true;
                            break;

                        // Track explicit values for timestamps, so that we do not override them with auto values
                        case TagsQuotation.TAG_DailyOpeningPriceTimestamp:
                            daily_opening_price_timestamp_explicit = true;
                            break;
                        case TagsQuotation.TAG_DailyClosingPriceTimestamp:
                            daily_closing_price_timestamp_explicit = true;
                            break;
                        case TagsQuotation.TAG_DailyHighPriceTimestamp:
                            daily_high_price_timestamp_explicit = true;
                            break;
                        case TagsQuotation.TAG_DailyLowPriceTimestamp:
                            daily_low_price_timestamp_explicit = true;
                            break;
                        case TagsQuotation.TAG_PreviousDailyClosingPriceTimestamp:
                            previous_daily_closing_price_timestamp_explicit = true;
                            break;

                        // Compute or reset timestamps for quotation data values, iff not explicitly set in received payload.
                        case TagsQuotation.TAG_DailyOpeningPrice:
                            daily_opening_price_explicit = true;
                            break;
                        case TagsQuotation.TAG_DailyClosingPrice:
                            daily_closing_price_explicit = true;
                            break;
                        case TagsQuotation.TAG_DailyHighPrice:
                            daily_high_price_explicit = true;
                            break;
                        case TagsQuotation.TAG_DailyLowPrice:
                            daily_low_price_explicit = true;
                            break;
                        case TagsQuotation.TAG_PreviousDailyClosingPrice:
                            previous_daily_closing_price_explicit = true;
                            break;
                    }
                    setTag(tag_num, tag_value);
                }

                // set last-auction related internal timestamp if needed
                if (last_auction_explicit)
                {
                    setTag(TagsQuotation.TAG_InternalLastAuctionTimestamp, server_timestamp);
                }

                // Compute or reset timestamps for quotation data values, if not explicitly set in received payload.
                // Following tags are to be set with trade_execution_timestamp (Fix 38598)
                if (daily_opening_price_explicit && !daily_opening_price_timestamp_explicit)
                {
                     setOrDeleteTimestampTag(TagsQuotation.TAG_DailyOpeningPriceTimestamp, trade_execution_timestamp);
                }
                if (daily_closing_price_explicit && !daily_closing_price_timestamp_explicit)
                {
                    setOrDeleteTimestampTag(TagsQuotation.TAG_DailyClosingPriceTimestamp, trade_execution_timestamp);
                }
                if (daily_high_price_explicit && !daily_high_price_timestamp_explicit)
                {
                    setOrDeleteTimestampTag(TagsQuotation.TAG_DailyHighPriceTimestamp, trade_execution_timestamp);
                }
                if (daily_low_price_explicit && !daily_low_price_timestamp_explicit)
                {
                    setOrDeleteTimestampTag(TagsQuotation.TAG_DailyLowPriceTimestamp, trade_execution_timestamp);
                }
                if (previous_daily_closing_price_explicit && !previous_daily_closing_price_timestamp_explicit)
                {
                    setOrDeleteTimestampTag(TagsQuotation.TAG_PreviousDailyClosingPriceTimestamp, trade_execution_timestamp);
                }
            }

            void reset_daily_aggregates(DateTime server_timestamp, uint content_mask)
            {
                // set PreviousBusinessDay only if there is a DailyClosingPrice or a DailySettlementPrice
                if (m_QuotationValues.ContainsKey(TagsQuotation.TAG_DailyClosingPrice) || m_QuotationValues.ContainsKey(TagsQuotation.TAG_DailySettlementPrice))
                {
                    object prev_day = getTagByNumber(TagsQuotation.TAG_CurrentBusinessDay);
                    if (prev_day != null)
                    {
                        setTag(TagsQuotation.TAG_PreviousBusinessDay, prev_day);
                    }
                    else
                    {
                        prev_day = getTagByNumber(TagsQuotation.TAG_InternalDailyCloseTimestamp);
                        if (prev_day != null)
                        {
                            // reset time
                            DateTime t = (DateTime)prev_day;
                            setTag(TagsQuotation.TAG_PreviousBusinessDay, t.Date);
                        }
                    }
                }

                // set current business day
                DateTime cur_biz_day = server_timestamp.Date;
                if (QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_OpeningNextCalendarDay))
                {
                    cur_biz_day.AddDays(1);
                }
                setTag(TagsQuotation.TAG_CurrentBusinessDay, cur_biz_day);
                setTag(TagsQuotation.TAG_InternalDailyBusinessDayTimestamp, server_timestamp);

                // set PreviousDaily* from Daily*, reset aggregates
                object prev;

                prev = getTagByNumber(TagsQuotation.TAG_DailyClosingPrice);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousDailyClosingPrice, prev);

                    prev = getTagByNumber(TagsQuotation.TAG_DailyClosingPriceTimestamp);
                    if (prev != null)
                    {
                        setTag(TagsQuotation.TAG_PreviousDailyClosingPriceTimestamp, prev);
                    }
                }

                prev = getTagByNumber(TagsQuotation.TAG_InternalDailyClosingPriceType);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousInternalDailyClosingPriceType, prev);
                }

                prev = getTagByNumber(TagsQuotation.TAG_DailySettlementPrice);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousDailySettlementPrice, prev);
                }

                prev = getTagByNumber(TagsQuotation.TAG_SettlementPriceDate);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousSettlementPriceDate, prev);
                }

                prev = getTagByNumber(TagsQuotation.TAG_SettlementPriceType);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousSettlementPriceType, prev);
                }

                prev = getTagByNumber(TagsQuotation.TAG_DailyTotalVolumeTraded);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousDailyTotalVolumeTraded, prev);
                    setTag(TagsQuotation.TAG_DailyTotalVolumeTraded, 0.0);
                }

                prev = getTagByNumber(TagsQuotation.TAG_DailyTotalAssetTraded);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousDailyTotalAssetTraded, prev);
                    setTag(TagsQuotation.TAG_DailyTotalAssetTraded, 0.0);
                }

                if (m_QuotationValues.ContainsKey(TagsQuotation.TAG_DailyTotalOffBookVolumeTraded))
                {
                    setTag(TagsQuotation.TAG_DailyTotalOffBookVolumeTraded, 0.0);
                }

                if (m_QuotationValues.ContainsKey(TagsQuotation.TAG_DailyTotalOffBookAssetTraded))
                {
                    setTag(TagsQuotation.TAG_DailyTotalOffBookAssetTraded, 0.0);
                }

                prev = getTagByNumber(TagsQuotation.TAG_OpenInterest);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousOpenInterest, prev);
                }

                prev = getTagByNumber(TagsQuotation.TAG_OpenInterestDate);
                if (prev != null)
                {
                    setTag(TagsQuotation.TAG_PreviousOpenInterestDate, prev);
                }

                // clear tags
                removeTagByNumber(TagsQuotation.TAG_DailyOpeningPrice);
                removeTagByNumber(TagsQuotation.TAG_DailyOpeningPriceTimestamp);
                removeTagByNumber(TagsQuotation.TAG_DailyClosingPrice);
                removeTagByNumber(TagsQuotation.TAG_DailyClosingPriceTimestamp);
                removeTagByNumber(TagsQuotation.TAG_InternalDailyClosingPriceType);
                removeTagByNumber(TagsQuotation.TAG_DailySettlementPrice);
                removeTagByNumber(TagsQuotation.TAG_SettlementPriceDate);
                removeTagByNumber(TagsQuotation.TAG_SettlementPriceType);

                // Auction tags are not reset because: feedhandlers must take care of it
                // themselves.

                removeTagByNumber(TagsQuotation.TAG_LastPrice);
                removeTagByNumber(TagsQuotation.TAG_LastTradePrice);
                removeTagByNumber(TagsQuotation.TAG_LastTradeQty);
                removeTagByNumber(TagsQuotation.TAG_LastTradeTimestamp);

                removeTagByNumber(TagsQuotation.TAG_LastOffBookTradePrice);
                removeTagByNumber(TagsQuotation.TAG_LastOffBookTradeQty);
                removeTagByNumber(TagsQuotation.TAG_LastOffBookTradeTimestamp);

                removeTagByNumber(TagsQuotation.TAG_DailyHighPrice);
                removeTagByNumber(TagsQuotation.TAG_DailyHighPriceTimestamp);
                removeTagByNumber(TagsQuotation.TAG_DailyLowPrice);
                removeTagByNumber(TagsQuotation.TAG_DailyLowPriceTimestamp);

                removeTagByNumber(TagsQuotation.TAG_LastEligibleTradePrice);
                removeTagByNumber(TagsQuotation.TAG_LastEligibleTradeQty);
                removeTagByNumber(TagsQuotation.TAG_LastEligibleTradeTimestamp);

                removeTagByNumber(TagsQuotation.TAG_OpenInterest);
                removeTagByNumber(TagsQuotation.TAG_OpenInterestDate);
            }

            void reset_session_aggregates(uint content_mask)
            {
                if (QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_Session))
                {
                    setTag(TagsQuotation.TAG_SessionTotalVolumeTraded, 0.0);
                    setTag(TagsQuotation.TAG_SessionTotalAssetTraded, 0.0);
                    setTag(TagsQuotation.TAG_SessionTotalOffBookVolumeTraded, 0.0);
                    setTag(TagsQuotation.TAG_SessionTotalOffBookAssetTraded, 0.0);
                }
                else
                {
                    removeTagByNumber(TagsQuotation.TAG_SessionTotalVolumeTraded);
                    removeTagByNumber(TagsQuotation.TAG_SessionTotalAssetTraded);
                    removeTagByNumber(TagsQuotation.TAG_SessionTotalOffBookVolumeTraded);
                    removeTagByNumber(TagsQuotation.TAG_SessionTotalOffBookAssetTraded);
                }
                removeTagByNumber(TagsQuotation.TAG_SessionHighPrice);
                removeTagByNumber(TagsQuotation.TAG_SessionLowPrice);
                removeTagByNumber(TagsQuotation.TAG_SessionOpeningPrice);
                removeTagByNumber(TagsQuotation.TAG_SessionClosingPrice);
            }

            void extract_trade_context(List<TagNumAndValue> context, ref bool has_actual_trading_session_id, ref uint trade_impact_indicator, ref DateTime trade_execution_timestamp)
            {
                bool has_trade_impact_indicator = false;
                bool has_trade_execution_timestamp = false;

                for (int i = 0; i < context.Count && (!has_trade_impact_indicator || !has_trade_execution_timestamp || !has_actual_trading_session_id); i++)
                {
                    TagNumAndValue tagNumAndValue = context[i];

                    if (tagNumAndValue.Num == TagsContext.TAG_ActualTradingSessionId)
                    {
                        has_actual_trading_session_id = true;
                    }
                    else if (tagNumAndValue.Num == TagsContext.TAG_TradeImpactIndicator)
                    {
                        trade_impact_indicator = (uint)tagNumAndValue.Value;
                        has_trade_impact_indicator = true;
                    }
                    else if (tagNumAndValue.Num == TagsContext.TAG_TradeExecutionTimestamp && tagNumAndValue.Value != null)
                    {
                        trade_execution_timestamp = (DateTime)tagNumAndValue.Value;
                        has_trade_execution_timestamp = true;
                    }
                }
            }

            void consume_trade_event(double price, double qty, DateTime trade_execution_timestamp, uint content_mask, bool has_actual_trading_session_id, uint trade_impact_indicator)
            {
                if (!QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade))
                {
                    object last_trade_timestamp = getTagByNumber(TagsQuotation.TAG_LastTradeTimestamp);

                    if (trade_execution_timestamp == DateTime.MinValue ||
                        last_trade_timestamp == null ||
                        DateTime.Compare(trade_execution_timestamp, (DateTime)last_trade_timestamp) >= 0)
                    {
                        setTag(TagsQuotation.TAG_LastTradePrice, price);
                        setTag(TagsQuotation.TAG_LastTradeQty, qty);
                        setOrDeleteTimestampTag(TagsQuotation.TAG_LastTradeTimestamp, trade_execution_timestamp);
                    }
                }
                else
                {
                    object last_offbook_trade_timestamp = getTagByNumber(TagsQuotation.TAG_LastOffBookTradeTimestamp);

                    if (trade_execution_timestamp == DateTime.MinValue ||
                        last_offbook_trade_timestamp == null ||
                        DateTime.Compare(trade_execution_timestamp, (DateTime)last_offbook_trade_timestamp) >= 0)
                    {
                        setTag(TagsQuotation.TAG_LastOffBookTradePrice, price);
                        setTag(TagsQuotation.TAG_LastOffBookTradeQty,qty);
                        setOrDeleteTimestampTag(TagsQuotation.TAG_LastOffBookTradeTimestamp, trade_execution_timestamp);
                    }
                }


                if (    TradeImpactIndicatorHelper.contains(trade_impact_indicator, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasLast)
                    &&  TradeImpactIndicatorHelper.contains(trade_impact_indicator, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_Last))
                {
                    object last_eligible_trade_timestamp = getTagByNumber(TagsQuotation.TAG_LastEligibleTradeTimestamp);

                    if (trade_execution_timestamp == DateTime.MinValue ||
                        last_eligible_trade_timestamp == null ||
                        DateTime.Compare(trade_execution_timestamp, (DateTime)last_eligible_trade_timestamp) >= 0)
                    {
                        setTag(TagsQuotation.TAG_LastEligibleTradePrice, price);
                        setTag(TagsQuotation.TAG_LastEligibleTradeQty,qty);
                        setOrDeleteTimestampTag(TagsQuotation.TAG_LastEligibleTradeTimestamp, trade_execution_timestamp);
                    }
                }

                bool impact_daily_onbook_volume;
                bool impact_daily_offbook_volume;

                if (TradeImpactIndicatorHelper.contains(trade_impact_indicator, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasVolume))
                {
                    impact_daily_onbook_volume = TradeImpactIndicatorHelper.contains(trade_impact_indicator, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_Volume);
                }
                else
                {
                    // fallback to legacy behavior
                    impact_daily_onbook_volume = !QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade);
                }

                if (TradeImpactIndicatorHelper.contains(trade_impact_indicator, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_HasOffBookVolume))
                {
                    impact_daily_offbook_volume = TradeImpactIndicatorHelper.contains(trade_impact_indicator, TradeImpactIndicatorHelper.TradeImpactIndicatorBit_OffBookVolume);
                }
                else
                {
                    // fallback to legacy behavior
                    impact_daily_offbook_volume = QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade);
                }

                bool impact_session_onbook_volume = QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_Session) && !QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade) && !has_actual_trading_session_id;
                bool impact_session_offbook_volume = QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_Session) && QuotationContentMaskHelper.contains(content_mask, QuotationContentMaskHelper.QuotationContentBit_OffBookTrade) && !has_actual_trading_session_id;

                double asset_delta = qty * price;

                if (impact_daily_offbook_volume)
                {
                    object v = getTagByNumber(TagsQuotation.TAG_DailyTotalOffBookVolumeTraded);
                    if (v == null)
                    {
                        v = qty;
                    }
                    else
                    {
                        v = (double)v + qty;
                    }
                    setTag(TagsQuotation.TAG_DailyTotalOffBookVolumeTraded, v);

                    object a = getTagByNumber(TagsQuotation.TAG_DailyTotalOffBookAssetTraded);
                    if (a == null)
                    {
                        a = asset_delta;
                    }
                    else
                    {
                        a = (double)a + asset_delta;
                    }
                    setTag(TagsQuotation.TAG_DailyTotalOffBookAssetTraded, a);
                }

                if (impact_daily_onbook_volume)
                {
                    object v = getTagByNumber(TagsQuotation.TAG_DailyTotalVolumeTraded);
                    if (v == null)
                    {
                        v = qty;
                    }
                    else
                    {
                        v = (double)v + qty;
                    }
                    setTag(TagsQuotation.TAG_DailyTotalVolumeTraded, v);

                    object a = getTagByNumber(TagsQuotation.TAG_DailyTotalAssetTraded);
                    if (a == null)
                    {
                        a = asset_delta;
                    }
                    else
                    {
                        a = (double)a + asset_delta;
                    }
                    setTag(TagsQuotation.TAG_DailyTotalAssetTraded, a);
                }

                if (impact_session_offbook_volume)
                {
                    object v = getTagByNumber(TagsQuotation.TAG_SessionTotalOffBookVolumeTraded);
                    if (v == null)
                    {
                        v = qty;
                    }
                    else
                    {
                        v = (double)v + qty;
                    }
                    setTag(TagsQuotation.TAG_SessionTotalOffBookVolumeTraded, v);

                    object a = getTagByNumber(TagsQuotation.TAG_SessionTotalOffBookAssetTraded);
                    if (a == null)
                    {
                        a = asset_delta;
                    }
                    else
                    {
                        a = (double)a + asset_delta;
                    }
                    setTag(TagsQuotation.TAG_SessionTotalOffBookAssetTraded, a);
                }

                if (impact_session_onbook_volume)
                {
                    object v = getTagByNumber(TagsQuotation.TAG_SessionTotalVolumeTraded);
                    if (v == null)
                    {
                        v = qty;
                    }
                    else
                    {
                        v = (double)v + qty;
                    }
                    setTag(TagsQuotation.TAG_SessionTotalVolumeTraded, v);

                    object a = getTagByNumber(TagsQuotation.TAG_SessionTotalAssetTraded);
                    if (a == null)
                    {
                        a = asset_delta;
                    }
                    else
                    {
                        a = (double)a + asset_delta;
                    }
                    setTag(TagsQuotation.TAG_SessionTotalAssetTraded, a);
                }
            }
        }
    }
}
