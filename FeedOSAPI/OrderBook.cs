///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class OrderBook
        {
            #region Constructor
            public OrderBook(ValueType ServerUTCDateTime, List<OrderBookEntryExt> BidLimits, List<OrderBookEntryExt> AskLimits)
            {
                m_ServerUTCDateTime = ServerUTCDateTime;
                m_BidLimits = BidLimits;
                m_AskLimits = AskLimits;
            }
            #endregion

            #region Properties
            public ValueType ServerUTCDateTime
            {
                get { return m_ServerUTCDateTime; }
            }
            public List<OrderBookEntryExt> BidLimits
            {
                get { return m_BidLimits; }
            }
            public List<OrderBookEntryExt> AskLimits
            {
                get { return m_AskLimits; }
            }
            #endregion

            #region Update
            public void Update(ValueType ServerUTCDateTime, OrderBookRefresh orderBookRefresh)  
            {
                m_ServerUTCDateTime = ServerUTCDateTime;
                if ((orderBookRefresh.BidLimits != null) && (orderBookRefresh.IsBidEndReached)) {
                    while (m_BidLimits.Count > (orderBookRefresh.BidStartLevel + orderBookRefresh.BidLimits.Count)) {
                        m_BidLimits.RemoveAt(m_BidLimits.Count - 1);
                    }
                }
                if ((orderBookRefresh.AskLimits != null) && (orderBookRefresh.IsAskEndReached)) {
                    while (m_AskLimits.Count > (orderBookRefresh.AskStartLevel + orderBookRefresh.AskLimits.Count)) {
                        m_AskLimits.RemoveAt(m_AskLimits.Count - 1);
                    }
                }
                if (orderBookRefresh.BidLimits != null) {
                    if (orderBookRefresh.BidStartLevel <= m_BidLimits.Count) {
                        int bidStartLevel = orderBookRefresh.BidStartLevel;
                        foreach (OrderBookEntryExt orderBookEntry in orderBookRefresh.BidLimits) {
                            if (bidStartLevel == m_BidLimits.Count) {
                                m_BidLimits.Insert(bidStartLevel, orderBookEntry);
                            } else if (bidStartLevel < m_BidLimits.Count) {
                                m_BidLimits[bidStartLevel] = orderBookEntry;
                            }
                            ++bidStartLevel;
                        }
                    }
                }
                if (orderBookRefresh.AskLimits != null) {
                    if (orderBookRefresh.AskStartLevel <= m_AskLimits.Count) {
                        int askStartLevel = orderBookRefresh.AskStartLevel;
                        foreach (OrderBookEntryExt orderBookEntry in orderBookRefresh.AskLimits) {
                            if (askStartLevel == m_AskLimits.Count) {
                                m_AskLimits.Insert(askStartLevel, orderBookEntry);
                            } else if (askStartLevel < m_AskLimits.Count) {
                                m_AskLimits[askStartLevel] = orderBookEntry;
                            }
                            ++askStartLevel;
                        }
                    }
                }
            }

            public void Update(ValueType ServerUTCDateTime, OrderBookDeltaRefresh orderBookDeltaRefresh) // DateTime serverUTCDataTime, 
            {
                m_ServerUTCDateTime = ServerUTCDateTime;
                switch (orderBookDeltaRefresh.Action) {
                    case OrderBookDeltaAction.OrderBookDeltaAction_ALLClearFromLevel: /** delete ALL entries, starting from Level (included) */
                        if (m_BidLimits.Count > orderBookDeltaRefresh.Level) {
                            m_BidLimits.RemoveRange(orderBookDeltaRefresh.Level, m_BidLimits.Count - orderBookDeltaRefresh.Level);
                        }
                        if (m_AskLimits.Count > orderBookDeltaRefresh.Level) {
                            m_AskLimits.RemoveRange(orderBookDeltaRefresh.Level, m_AskLimits.Count - orderBookDeltaRefresh.Level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidClearFromLevel: /** delete Bid entries, starting from Level (included) */
                        if (m_BidLimits.Count > orderBookDeltaRefresh.Level) {
                            m_BidLimits.RemoveRange(orderBookDeltaRefresh.Level, m_BidLimits.Count - orderBookDeltaRefresh.Level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskClearFromLevel: /** delete Ask entries: starting from Level (included) */
                        if (m_AskLimits.Count > orderBookDeltaRefresh.Level) {
                            m_AskLimits.RemoveRange(orderBookDeltaRefresh.Level, m_AskLimits.Count - orderBookDeltaRefresh.Level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidInsertAtLevel: /** insert line (Price,Qty) at Level and shift subsequent lines up */
                        if (orderBookDeltaRefresh.Level <= m_BidLimits.Count) {
                            m_BidLimits.Insert(orderBookDeltaRefresh.Level, new OrderBookEntryExt(orderBookDeltaRefresh.Price, orderBookDeltaRefresh.Qty, OrderBookEntryExt.ORDERBOOK_NB_ORDERS_UNKNOWN));
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskInsertAtLevel: /** insert line (Price,Qty) at Level and shift subsequent lines up */
                        if (orderBookDeltaRefresh.Level <= m_AskLimits.Count) {
                            m_AskLimits.Insert(orderBookDeltaRefresh.Level, new OrderBookEntryExt(orderBookDeltaRefresh.Price, orderBookDeltaRefresh.Qty, OrderBookEntryExt.ORDERBOOK_NB_ORDERS_UNKNOWN));
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidRemoveLevel: /** remove line at Level and shift subsequent lines down */
                        if (orderBookDeltaRefresh.Level < m_BidLimits.Count) {
                            m_BidLimits.RemoveAt(orderBookDeltaRefresh.Level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskRemoveLevel: /** remove line at Level and shift subsequent lines down */
                        if (orderBookDeltaRefresh.Level < m_AskLimits.Count) {
                            m_AskLimits.RemoveAt(orderBookDeltaRefresh.Level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidChangeQtyAtLevel:/** change Qty at Level */
                        if (orderBookDeltaRefresh.Level < m_BidLimits.Count) {
                            m_BidLimits[orderBookDeltaRefresh.Level].Size = orderBookDeltaRefresh.Qty;
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskChangeQtyAtLevel: /** change Qty at Level */
                        if (orderBookDeltaRefresh.Level < m_AskLimits.Count) {
                            m_AskLimits[orderBookDeltaRefresh.Level].Size = orderBookDeltaRefresh.Qty;
                        }
                        break;
                }
            }

            public void Update(ValueType ServerUTCDateTime, int maxVisibleDepth) // DateTime serverUTCDataTime, 
            {
                m_ServerUTCDateTime = ServerUTCDateTime;
                if (m_BidLimits.Count > maxVisibleDepth) {
                    m_BidLimits.RemoveRange(maxVisibleDepth, m_BidLimits.Count - maxVisibleDepth);
                }
                if (m_AskLimits.Count > maxVisibleDepth) {
                    m_AskLimits.RemoveRange(maxVisibleDepth, m_AskLimits.Count - maxVisibleDepth);
                }
            }

            #endregion

            #region Members
            protected ValueType m_ServerUTCDateTime;
            protected List<OrderBookEntryExt> m_BidLimits;
            protected List<OrderBookEntryExt> m_AskLimits;
            #endregion
        }
    }
}
