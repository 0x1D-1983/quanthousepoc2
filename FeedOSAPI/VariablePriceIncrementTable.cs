///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class VariableIncrementPriceBand
        {
            #region Constructor
            public VariableIncrementPriceBand(bool Inclusive, double LowerBoundary, double PriceIncrement)
		    {
			    m_Inclusive = Inclusive;
			    m_LowerBoundary = LowerBoundary;
			    m_PriceIncrement = PriceIncrement;
		    }
            #endregion

            #region Properties
		    public bool Inclusive
		    {
			    get { return m_Inclusive; }
		    }
            public double LowerBoundary
		    {
			    get { return m_LowerBoundary; }
		    }
		    public double PriceIncrement
		    {
			    get { return m_PriceIncrement; }
		    }
            #endregion

            #region Members
            protected bool m_Inclusive;
            protected double m_LowerBoundary;
            protected double m_PriceIncrement;
            #endregion
        }

        public class VariablePriceIncrementTable
        {
            #region Constructor
            public VariablePriceIncrementTable(uint Id, string Description, List<VariableIncrementPriceBand> PriceBands)
		    {
                m_Id = Id;
                m_Description = Description;
                m_PriceBands = PriceBands;
		    }
            #endregion

            #region Properties
            public uint Id
		    {
                get { return m_Id; }
		    }
            public string Description
		    {
                get { return m_Description; }
		    }
            public IEnumerable<VariableIncrementPriceBand> PriceBands
		    {
                get { return m_PriceBands; }
		    }
            #endregion

            #region Members
            protected uint m_Id;
            protected string m_Description;
            protected List<VariableIncrementPriceBand> m_PriceBands;
            #endregion
        }
    }
}
