///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    public class LocalInstrumentCode
    {
        #region Constructor
        public LocalInstrumentCode(ushort FOSMarketId, string LocalCodeStr)
        {
            m_FOSMarketId = FOSMarketId;
            m_LocalCodeStr = LocalCodeStr;
        }
        #endregion

        #region Properties
        public ushort FOSMarketId
        {
            get { return m_FOSMarketId; }
        }
        public string LocalCodeStr
        {
            get { return m_LocalCodeStr; }
        }
        #endregion

        #region Members
        protected ushort m_FOSMarketId;
        protected string m_LocalCodeStr;
        #endregion
    }
}
