///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketBranchId
        {
            #region Constructor
            public MarketBranchId ( ushort FOSMarketId, string SecurityType, string CFICode )
            {
                m_FOSMarketId = FOSMarketId;
                m_SecurityType = SecurityType;
                m_CFICode = CFICode;
            }
            #endregion

            #region Properties
            public ushort FOSMarketId 
		    {
			    get { return m_FOSMarketId; }
		    }
		    public string SecurityType 
		    {
			    get { return m_SecurityType; }
		    }
		    public string CFICode 
		    {
			    get { return m_CFICode; }
		    }
            #endregion

            #region Members
            protected ushort m_FOSMarketId;
		    protected string m_SecurityType;
		    protected string m_CFICode;
            #endregion

        }
    }
}
