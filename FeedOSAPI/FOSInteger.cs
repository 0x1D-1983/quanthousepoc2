///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2010 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    public class FOSInteger
    {
        #region Constructor
        public FOSInteger(long value)
        {
            m_Num = value;
        }
        public override string ToString()
        {
            return m_Num.ToString();
        }
        #endregion

        #region Properties
        public long Num
        {
            get { return m_Num; }
            set { m_Num = value; }
        }
        #endregion

        #region Members
        protected long m_Num;
        #endregion
    }
}
