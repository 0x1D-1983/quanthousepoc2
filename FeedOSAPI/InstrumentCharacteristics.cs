///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {

        public class InstrumentCharacteristics
        {
            #region Constructor
            public InstrumentCharacteristics(uint InstrumentCode, List<TagNumAndValue> ReferentialAttributes)
            {
			    m_InstrumentCode = InstrumentCode;
                m_ReferentialAttributes = ReferentialAttributes;
		    }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public List<TagNumAndValue> ReferentialAttributes
            {
                get { return m_ReferentialAttributes; }
            }
            // PriceCurrency
            private Predicate<TagNumAndValue> FirstPriceCurrency = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_PriceCurrency;
            };
            public string PriceCurrency
            {
                get
                {
                    TagNumAndValue tagNumAndValuePriceCurrency = m_ReferentialAttributes.Find(FirstPriceCurrency);
                    if (tagNumAndValuePriceCurrency != null) {
                        return (tagNumAndValuePriceCurrency.Value as string);
                    }
                    return null;
                }
            }
            // Symbol
            private Predicate<TagNumAndValue> FirstSymbol = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_Symbol;
            };
            public string Symbol
            {
                get
                {
                    TagNumAndValue tagNumAndValueSymbol = m_ReferentialAttributes.Find(FirstSymbol);
                    if (tagNumAndValueSymbol != null) {
                        return (tagNumAndValueSymbol.Value as string);
                    }
                    return null;
                }
            }
            // Description
            private Predicate<TagNumAndValue> FirstDescription = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_Description;
            };
            public string Description
            {
                get
                {
                    TagNumAndValue tagNumAndValueDescription = m_ReferentialAttributes.Find(FirstDescription);
                    if (tagNumAndValueDescription != null) {
                        return (tagNumAndValueDescription.Value as string);
                    }
                    return null;
                }
            }
            // SecurityType
            private Predicate<TagNumAndValue> FirstSecurityType = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_SecurityType;
            };
            public string SecurityType
            {
                get
                {
                    TagNumAndValue tagNumAndValueSecurityType = m_ReferentialAttributes.Find(FirstSecurityType);
                    if (tagNumAndValueSecurityType != null) {
                        return (tagNumAndValueSecurityType.Value as string);
                    }
                    return null;
                }
            }
            // FOSMarketId
            private Predicate<TagNumAndValue> FirstFOSMarketId = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_FOSMarketId;
            };
            public ushort FOSMarketId
            {
                get
                {
                    TagNumAndValue tagNumAndValueFOSMarketId = m_ReferentialAttributes.Find(FirstFOSMarketId);
                    if (tagNumAndValueFOSMarketId != null) {
                        return (ushort)tagNumAndValueFOSMarketId.Value;
                    }
                    return 0;
                }
            }
            // ContractMultiplier
            private Predicate<TagNumAndValue> FirstContractMultiplier = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_ContractMultiplier;
            };
            public ValueType ContractMultiplier
            {
                get
                {
                    TagNumAndValue tagNumAndValueContractMultiplier = m_ReferentialAttributes.Find(FirstContractMultiplier);
                    if (tagNumAndValueContractMultiplier != null) {
                        return (double)tagNumAndValueContractMultiplier.Value;
                    }
                    return null;
                }
            }
            // CFICode
            private Predicate<TagNumAndValue> FirstCFICode = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_CFICode;
            };
            public string CFICode
            {
                get
                {
                    TagNumAndValue tagNumAndValueCFICode = m_ReferentialAttributes.Find(FirstCFICode);
                    if (tagNumAndValueCFICode != null) {
                        return (tagNumAndValueCFICode.Value as string);
                    }
                    return null;
                }
            }
            // SecuritySubType
            private Predicate<TagNumAndValue> FirstSecuritySubType = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_SecuritySubType;
            };
            public string SecuritySubType
            {
                get
                {
                    TagNumAndValue tagNumAndValueSecuritySubType = m_ReferentialAttributes.Find(FirstSecuritySubType);
                    if (tagNumAndValueSecuritySubType != null) {
                        return (tagNumAndValueSecuritySubType.Value as string);
                    }
                    return null;
                }
            }
            // StrikeCurrency
            private Predicate<TagNumAndValue> FirstStrikeCurrency = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_StrikeCurrency;
            };
            public string StrikeCurrency
            {
                get
                {
                    TagNumAndValue tagNumAndValueStrikeCurrency = m_ReferentialAttributes.Find(FirstStrikeCurrency);
                    if (tagNumAndValueStrikeCurrency != null) {
                        return (tagNumAndValueStrikeCurrency.Value as string);
                    }
                    return null;
                }
            }
            // InternalCreationDate
            private Predicate<TagNumAndValue> FirstInternalCreationDate = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_InternalCreationDate;
            };
            public ValueType InternalCreationDate
            {
                get
                {
                    TagNumAndValue tagNumAndValueInternalCreationDate = m_ReferentialAttributes.Find(FirstInternalCreationDate);
                    if (tagNumAndValueInternalCreationDate != null) {
                        return (DateTime)tagNumAndValueInternalCreationDate.Value;
                    }
                    return null;
                }
            }
            // InternalModificationDate
            private Predicate<TagNumAndValue> FirstInternalModificationDate = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_InternalModificationDate;
            };
            public ValueType InternalModificationDate
            {
                get
                {
                    TagNumAndValue tagNumAndValueInternalModificationDate = m_ReferentialAttributes.Find(FirstInternalModificationDate);
                    if (tagNumAndValueInternalModificationDate != null) {
                        return (DateTime)tagNumAndValueInternalModificationDate.Value;
                    }
                    return null;
                }
            }
            // InternalSourceId
            private Predicate<TagNumAndValue> FirstInternalSourceId = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_InternalSourceId;
            };
            public ValueType InternalSourceId
            {
                get
                {
                    TagNumAndValue tagNumAndValueInternalSourceId = m_ReferentialAttributes.Find(FirstInternalSourceId);
                    if (tagNumAndValueInternalSourceId != null) {
                        return (ushort)tagNumAndValueInternalSourceId.Value;
                    }
                    return null;
                }
            }
            // LocalCodeStr
            private Predicate<TagNumAndValue> FirstLocalCodeStr = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_LocalCodeStr;
            };
            public string LocalCodeStr
            {
                get
                {
                    TagNumAndValue tagNumAndValueLocalCodeStr = m_ReferentialAttributes.Find(FirstLocalCodeStr);
                    if (tagNumAndValueLocalCodeStr != null) {
                        return (tagNumAndValueLocalCodeStr.Value as string);
                    }
                    return null;
                }
            }
            // ISIN
            private Predicate<TagNumAndValue> FirstISIN = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_ISIN;
            };
            public string ISIN
            {
                get
                {
                    TagNumAndValue tagNumAndValueISIN = m_ReferentialAttributes.Find(FirstISIN);
                    if (tagNumAndValueISIN != null) {
                        return (tagNumAndValueISIN.Value as string);
                    }
                    return null;
                }
            }
            // SEDOL
            private Predicate<TagNumAndValue> FirstSEDOL = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_SEDOL;
            };
            public string SEDOL
            {
                get
                {
                    TagNumAndValue tagNumAndValueSEDOL = m_ReferentialAttributes.Find(FirstSEDOL);
                    if (tagNumAndValueSEDOL != null) {
                        return (tagNumAndValueSEDOL.Value as string);
                    }
                    return null;
                }
            }
            // PriceIncrement_static
            private Predicate<TagNumAndValue> FirstPriceIncrement_static = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_PriceIncrement_static;
            };
            public ValueType PriceIncrement_static
            {
                get
                {
                    TagNumAndValue tagNumAndValuePriceIncrement_static = m_ReferentialAttributes.Find(FirstPriceIncrement_static);
                    if (tagNumAndValuePriceIncrement_static != null) {
                        return (double)tagNumAndValuePriceIncrement_static.Value;
                    }
                    return null;
                }
            }
            // StrikePrice
            private Predicate<TagNumAndValue> FirstStrikePrice = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_StrikePrice;
            };
            public ValueType StrikePrice
            {
                get
                {
                    TagNumAndValue tagNumAndValueStrikePrice = m_ReferentialAttributes.Find(FirstStrikePrice);
                    if (tagNumAndValueStrikePrice != null) {
                        return (double)tagNumAndValueStrikePrice.Value;
                    }
                    return null;
                }
            }
            //// PriceIncrement_static
            //private Predicate<TagNumAndValue> FirstPriceIncrement_static = delegate(TagNumAndValue tagNumAndValue)
            //{
            //    return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_PriceIncrement_static;
            //};
            //public ValueType PriceIncrement_static
            //{
            //    get
            //    {
            //        TagNumAndValue tagNumAndValuePriceIncrement_static = m_ReferentialAttributes.Find(FirstPriceIncrement_static);
            //        if (tagNumAndValuePriceIncrement_static != null) {
            //            return (double)tagNumAndValuePriceIncrement_static.Value;
            //        }
            //        return null;
            //    }
            //}
            // MaturityYear
            private Predicate<TagNumAndValue> FirstMaturityYear = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_MaturityYear;
            };
            public ValueType MaturityYear
            {
                get
                {
                    TagNumAndValue tagNumAndValueMaturityYear = m_ReferentialAttributes.Find(FirstMaturityYear);
                    if (tagNumAndValueMaturityYear != null) {
                        return (ushort)tagNumAndValueMaturityYear.Value;
                    }
                    return null;
                }
            }
            // MaturityMonth
            private Predicate<TagNumAndValue> FirstMaturityMonth = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_MaturityMonth;
            };
            public ValueType MaturityMonth
            {
                get
                {
                    TagNumAndValue tagNumAndValueMaturityMonth = m_ReferentialAttributes.Find(FirstMaturityMonth);
                    if (tagNumAndValueMaturityMonth != null) {
                        return (byte)tagNumAndValueMaturityMonth.Value;
                    }
                    return null;
                }
            }
            // MaturityDay
            private Predicate<TagNumAndValue> FirstMaturityDay = delegate(TagNumAndValue tagNumAndValue)
            {
                return tagNumAndValue.Num == FeedOSAPI.Types.TagsReferential.TAG_MaturityDay;
            };
            public ValueType MaturityDay
            {
                get
                {
                    TagNumAndValue tagNumAndValueMaturityDay = m_ReferentialAttributes.Find(FirstMaturityDay);
                    if (tagNumAndValueMaturityDay != null) {
                        return (byte)tagNumAndValueMaturityDay.Value;
                    }
                    return null;
                }
            }

            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected List<TagNumAndValue> m_ReferentialAttributes;
            #endregion
        }
    }
}
