using System;
using System.Collections.Generic;
using System.Text;
// FeedOS
using FeedOSAPI.Types;

namespace FeedOSAPI
{
    namespace Types
    {
        public class TradeEventExt
        {
            #region Constructor
            public TradeEventExt(uint instrumentCode, ValueType serverUTCDateTime, ValueType marketUTCDateTime, QuotationTradeEventExt quotationTradeEventExt)
            {
                m_InstrumentCode = instrumentCode;
                m_ServerUTCDateTime = serverUTCDateTime;
                m_MarketUTCDateTime = marketUTCDateTime;
                m_QuotationTradeEventExt = quotationTradeEventExt;
            }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public ValueType ServerUTCDateTime
            {
                get { return m_ServerUTCDateTime; }
            }
            public ValueType MarketUTCDateTime
            {
                get { return m_MarketUTCDateTime; }
            }
            public QuotationTradeEventExt QuotationTradeEventExt
            {
                get { return m_QuotationTradeEventExt; }
            }
            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected ValueType m_ServerUTCDateTime;
            protected ValueType m_MarketUTCDateTime;
            protected QuotationTradeEventExt m_QuotationTradeEventExt;
            #endregion

        }
    }
}
