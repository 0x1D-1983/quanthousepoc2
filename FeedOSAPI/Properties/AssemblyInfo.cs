﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FeedOSAPI")]
[assembly: AssemblyDescription("FeedOS API")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ae2c1510-6932-4d5f-afd3-cdc8995c0ae5")]

[assembly: AssemblyCopyrightAttribute("QuantHouse 2017")]
[assembly: AssemblyTrademarkAttribute("QuantFEED")]
// WARNING: format is ruled my Microsoft "major.minor.build.revision"
// Use the four-part version format: mmmmm.nnnnn.ooooo.ppppp. 
// Each of the parts separated by periods can be 0-65535 non inclusive.
[assembly: AssemblyVersionAttribute("2.6.4.1")]
[assembly: AssemblyFileVersionAttribute("2.6.4.1")]
