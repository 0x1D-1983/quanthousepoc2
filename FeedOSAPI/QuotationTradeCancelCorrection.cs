///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2015 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class TradeCancelCorrectionContentMaskHelper
        {
            public static byte TradeCancelCorrectionContentBit_IsCorrection = 1 << (byte)TradeCancelCorrectionContent.TradeCancelCorrectionContent_IsCorrection;
            public static byte TradeCancelCorrectionContentBit_IsFromVenue = 1 << (byte)TradeCancelCorrectionContent.TradeCancelCorrectionContent_IsFromVenue;
            public static byte TradeCancelCorrectionContentBit_CorrectedValues = 1 << (byte)TradeCancelCorrectionContent.TradeCancelCorrectionContent_CorrectedValues;
            public static byte TradeCancelCorrectionContentBit_OffBookTrade = 1 << (byte)TradeCancelCorrectionContent.TradeCancelCorrectionContent_OffBookTrade;
            public static byte TradeCancelCorrectionContentBit_CurrentSession = 1 << (byte)TradeCancelCorrectionContent.TradeCancelCorrectionContent_CurrentSession;

            public static bool contains(uint contentMask, uint mask)
            {
                return (mask == (contentMask & mask));
            }

            public static void PrintContentMask(byte contentMask, bool humanReadable)
            {
                if (humanReadable)
                {
                    if (contains(contentMask, TradeCancelCorrectionContentBit_IsCorrection)) { Console.Write(" IsCorrection"); }
                    if (contains(contentMask, TradeCancelCorrectionContentBit_IsFromVenue)) { Console.Write(" IsFromVenue"); }
                    if (contains(contentMask, TradeCancelCorrectionContentBit_CorrectedValues)) { Console.Write(" CorrectedValues"); }
                    if (contains(contentMask, TradeCancelCorrectionContentBit_OffBookTrade)) { Console.Write(" OffBookTrade"); }
                    if (contains(contentMask, TradeCancelCorrectionContentBit_CurrentSession)) { Console.Write(" CurrentSession"); }
                }
                else
                {
                    Console.Write(contentMask);
                }
            }
        }

        public class QuotationTradeCancelCorrection
        {
            #region Members
            protected byte m_ContentMask;
            protected TradeData m_OriginalTrade;
            protected sbyte m_TradingSessionId;
            protected TradeData m_CorrectedTrade;
            protected List<TagNumAndValue> m_CorrectedValues;
            #endregion

            #region Constructors
            public QuotationTradeCancelCorrection(byte contentMask, TradeData originalTrade, sbyte trading_session_id, TradeData correctedTrade, List<TagNumAndValue> Values)
            {
                m_ContentMask = contentMask;
                m_OriginalTrade = originalTrade;
                m_TradingSessionId = trading_session_id;
                m_CorrectedTrade = correctedTrade;
                m_CorrectedValues = Values;
            }

            public QuotationTradeCancelCorrection()
            {
                m_ContentMask = 0;
                m_OriginalTrade = new TradeData();
                m_TradingSessionId = 0;
                m_CorrectedTrade = new TradeData();
                m_CorrectedValues = new List<TagNumAndValue>();
            }
            #endregion

            #region Properties
            public byte ContentMask
            {
                get { return m_ContentMask; }
                set { m_ContentMask = value; }
            }
            public TradeData OriginalTrade
            {
                get { return m_OriginalTrade; }
                set { m_OriginalTrade = value; }
            }
            public sbyte TradingSessionId
            {
                get { return m_TradingSessionId; }
                set { m_TradingSessionId = value; }
            }
            public TradeData CorrectedTrade
            {
                get { return m_CorrectedTrade; }
                set { m_CorrectedTrade = value; }
            }
            public List<TagNumAndValue> CorrectedValues
            {
                get { return m_CorrectedValues; }
                set { m_CorrectedValues = value; }
            }
            #endregion
        }
    }
}
