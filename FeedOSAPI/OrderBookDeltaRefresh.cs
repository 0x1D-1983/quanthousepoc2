///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        #region Enum OrderBookDeltaAction
        public enum OrderBookDeltaAction
        {
            OrderBookDeltaAction_ALLClearFromLevel /** delete ALL entries, starting from Level (included) */,
            OrderBookDeltaAction_BidClearFromLevel /** delete Ask entries, starting from Level (included) */,
            OrderBookDeltaAction_AskClearFromLevel /** delete Ask entries, starting from Level (included) */,
            OrderBookDeltaAction_BidInsertAtLevel /** insert line (Price,Qty) at Level and shift subsequent lines up */,
            OrderBookDeltaAction_AskInsertAtLevel /** insert line (Price,Qty) at Level and shift subsequent lines up */,
            OrderBookDeltaAction_BidRemoveLevel /** remove line at Level and shift subsequent lines down, append "worst visible" limit */,
            OrderBookDeltaAction_AskRemoveLevel /** remove line at Level and shift subsequent lines down, append "worst visible" limit  */,
            OrderBookDeltaAction_BidChangeQtyAtLevel /** change Qty at Level */,
            OrderBookDeltaAction_AskChangeQtyAtLevel /** change Qty at Level */,
            OrderBookDeltaAction_BidRemoveLevelAndAppend /** -- MBLExt only -- remove line at Level and shift subsequent lines down, append "worst visible" limit  */,
            OrderBookDeltaAction_AskRemoveLevelAndAppend /** -- MBLExt only -- remove line at Level and shift subsequent lines down, append "worst visible" limit  */
        }
        #endregion

        public class OrderBookDeltaRefresh
        {
            #region Constructor
            public OrderBookDeltaRefresh(OrderBookDeltaAction action, byte level, double price, double qty) 
            {
                m_Action = action;
                m_Level = level;
                m_Price = price;
                m_Qty = qty;
            }
            #endregion

            #region Properties
            public OrderBookDeltaAction Action
            {
                get { return m_Action; }
            }
            public byte Level
            {
                get { return m_Level; }
            }
            public double Price
            {
                get { return m_Price; }
            }
            public double Qty
            {
                get { return m_Qty; }
            }
            #endregion

            #region Members
            private OrderBookDeltaAction m_Action;
            private byte m_Level;
            private double m_Price;
            private double m_Qty;
            #endregion
        }
    }
}
