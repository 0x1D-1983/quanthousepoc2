///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2012 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        // Legacy L2 snapshot
        public class InstrumentStatusL2
        {
            #region Constructor
            public InstrumentStatusL2(uint instrumentCode, OrderBook OrderBook)
            {
                m_InstrumentCode = instrumentCode;
                m_OrderBook = OrderBook;
            }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public OrderBook OrderBook
            {
                get { return m_OrderBook; }
            }
            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected OrderBook m_OrderBook;
            #endregion
        }

        // MBL Layer
        public class MBLLayer
        {
            public const int DEFAULT_LAYER_ID = 0;
            public const int IMPLIED_LAYER_ID = 1;
            public const int RETAIL_LAYER_ID = 2;
            public const int ODDLOT_LAYER_ID = 3;
            public const int TERM_LAYER_ID = 4;
            public const int FIRM_QUOTES_LAYER_ID = 5;

            #region Constructor
            public MBLLayer(uint layer_id,
                            FOSOrderBookLevel maxVisibleDepth,
                            UTCTimestamps timestamps,
                            List<MBLOrderBookEntry> bidLimits,
                            List<MBLOrderBookEntry> askLimits,
                            List<TagNumAndValue> otherValues)
            {
                m_LayerId = layer_id;
                m_MaxVisibleDepth = maxVisibleDepth;
                m_Timestamps = timestamps;
                m_BidLimits = bidLimits;
                m_AskLimits = askLimits;
                m_OtherValues = otherValues;

				if (otherValues != null)
				{
					FeedOSAPI.TagFinder tag_finder = new FeedOSAPI.TagFinder(TagsQuotation.TAG_HasContinuationFlag);
					bool incomplete_layer = -1 != otherValues.FindIndex(tag_finder.FindTagWithNum);
					if (incomplete_layer)
					{
						m_ContinuationFlagSet = true;
					}
				}
            }

            public MBLLayer(uint layer_id)
            {
                m_LayerId = layer_id;
                m_MaxVisibleDepth = new FOSOrderBookLevel(-1);
                m_ContinuationFlagSet = false;
            }
            #endregion 
 
            #region Properties
            public uint LayerId
            {
                get { return m_LayerId; }
            }
            public FOSOrderBookLevel MaxVisibleDepth
            {
                set
                {
                    m_MaxVisibleDepth = value;
                    StripMBLOrderBookToVisibleDepth();
                }

                get { return m_MaxVisibleDepth; }
            }
            public UTCTimestamps Timestamps
            {
                get
                {
                    if (null == m_Timestamps)
                    {
                        m_Timestamps = new UTCTimestamps(new DateTime(1970, 1, 1, 0, 0, 0), new DateTime(1970, 1, 1, 0, 0, 0));
                    }
                    return m_Timestamps;
                }
            }
            public List<MBLOrderBookEntry> BidLimits
            {
                get
                {
                    if (null == m_BidLimits)
                    {
                        m_BidLimits = new List<MBLOrderBookEntry>();
                    }
                    return m_BidLimits;
                }
            }
            public List<MBLOrderBookEntry> AskLimits
            {
                get
                {
                    if (null == m_AskLimits)
                    {
                        m_AskLimits = new List<MBLOrderBookEntry>();
                    }
                    return m_AskLimits;
                }
            }
            public List<TagNumAndValue> OtherValues
            {
                get
                {
                    if (null == m_OtherValues)
                    {
                        m_OtherValues = new List<TagNumAndValue>();
                    }
                    
                    return m_OtherValues;
 }
            }
            #endregion

            #region internals
            protected bool hasOtherValues()
            {
                return (null != m_OtherValues);
            }

            // Othervalues is a repository of tags that are enriched by incoming events
            // LifeCycle description
            // 1- When a new tag comes in it is appended to the otherValues
            // 2- When an existing tag comes in, if incoming tag value is not null the otherValues tag'value is updated
            // 3- When an existing tag comes in, if incoming tag value is null, the tag is deleted from otherValues
            protected void updateOtherValues(List<TagNumAndValue> otherValues)
            {
                if (null == otherValues) return;
                foreach (TagNumAndValue tagAndValue in otherValues)
                {
                    TagNumAndValue foundTag = OtherValues.Find(
                     delegate(TagNumAndValue target)
                     { return (tagAndValue.Num == target.Num); }
                     );

                    if (null == tagAndValue.Value)
                    {
                        m_OtherValues.Remove(foundTag);
                    }
                    else
                    {
                        if (null == foundTag)
                        {
                            m_OtherValues.Insert(0,tagAndValue);
                        }
                        else
                        {
                            foundTag = tagAndValue;
                        }
                    }
                }
            }
            #endregion

            #region Update
            // MBLLayer is updated by an incoming overlap refresh event
            public void Update(MBLOverlapRefresh overlap)
            {
                m_Timestamps = overlap.Timestamps;
                bool bidIsComplete = false;
                int startBidLevel = 0;
                MBLOverlapRefresh.splitOrderBookChangeIndicator(overlap.BidChangeIndicator, ref bidIsComplete,ref startBidLevel);
                // Truncate Bid side if bid side is complete
                if (bidIsComplete)
                {
                    int bidLastLimitIndex = startBidLevel + (null != overlap.BidLimits ? overlap.BidLimits.Count : 0);
                    if ((null != m_BidLimits) && (m_BidLimits.Count > bidLastLimitIndex))
                    {
                        m_BidLimits.RemoveRange(bidLastLimitIndex, m_BidLimits.Count - bidLastLimitIndex);
                    }
                }
                bool askIsComplete = false;
                int startAskLevel = 0;
                MBLOverlapRefresh.splitOrderBookChangeIndicator(overlap.AskChangeIndicator, ref askIsComplete, ref startAskLevel);
                // Truncate Ask side if ask side is complete
                if (askIsComplete)
                {
                    int askLastLimitIndex = startAskLevel + (null !=  overlap.AskLimits ? overlap.AskLimits.Count : 0);
                    if ((null != m_AskLimits) && (m_AskLimits.Count > askLastLimitIndex))
                    {
                        m_AskLimits.RemoveRange(askLastLimitIndex, m_AskLimits.Count - askLastLimitIndex);
                    }
                }
                if (overlap.BidLimits != null)
                {
                    if (startBidLevel <= BidLimits.Count)
                    {
                        int bidIndex = (int)startBidLevel;
                        foreach (MBLOrderBookEntry orderBookEntry in overlap.BidLimits)
                        {
                            if (bidIndex == BidLimits.Count)
                            {
                                BidLimits.Insert(bidIndex, orderBookEntry);
                            }
                            else if (bidIndex < BidLimits.Count)
                            {
                                BidLimits[bidIndex] = orderBookEntry;
                            }
                            ++bidIndex;
                        }
                    }
                }
                if (overlap.AskLimits != null)
                {
                    if (startAskLevel <= AskLimits.Count)
                    {
                        int askIndex = (int)startAskLevel;
                        foreach (MBLOrderBookEntry orderBookEntry in overlap.AskLimits)
                        {
                            if (askIndex == AskLimits.Count)
                            {
                                AskLimits.Insert(askIndex, orderBookEntry);
                            }
                            else if (askIndex < AskLimits.Count)
                            {
                                AskLimits[askIndex] = orderBookEntry;
                            }
                            ++askIndex;
                        }
                    }
                }
                updateOtherValues(overlap.OtherValues);
                if (m_ContinuationFlagSet)
                {
                    // sequence error: expected the continuation flag to be cleared by a Delta Refresh before dispatching an Overlap
                    m_ContinuationFlagSet = false;
                    if (hasOtherValues())
                    {
                        TagFinder tag_finder = new TagFinder(TagsQuotation.TAG_HasContinuationFlag);
                        int pos = m_OtherValues.FindIndex(tag_finder.FindTagWithNum);
                        if (-1 != pos)
                        {
                            m_OtherValues.RemoveAt(pos);
                        }
                    }
                }
            }

            // MBLLayer is updated by an incoming delta refresh event
            // returns false when refresh has ContinuationFlag, true otherwise
            public bool Update(MBLDeltaRefresh refresh) 
            {
                int actual_level = (int)(refresh.Level.Num);
                if (actual_level < 0)
                {
                    return true;     
                }
                int bidSize = BidLimits.Count;
                int askSize = AskLimits.Count;
                m_Timestamps = refresh.Timestamps;
                switch (refresh.Action)
                {
                    case OrderBookDeltaAction.OrderBookDeltaAction_ALLClearFromLevel: /** delete ALL entries, starting from Level (included) */
                        if (bidSize > actual_level)
                        {
                            BidLimits.RemoveRange(actual_level, bidSize - actual_level);
                        }
                        if (askSize > actual_level)
                        {
                            AskLimits.RemoveRange(actual_level, askSize - actual_level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidClearFromLevel: /** delete Bid entries, starting from Level (included) */
                        if (bidSize > actual_level)
                        {
                            BidLimits.RemoveRange(actual_level, bidSize - actual_level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskClearFromLevel: /** delete Ask entries: starting from Level (included) */
                        if (askSize > actual_level)
                        {
                            AskLimits.RemoveRange(actual_level, askSize - actual_level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidInsertAtLevel: /** insert line (Price,Qty) at Level and shift subsequent lines up */
                        if (actual_level <= bidSize)
                        {
                            BidLimits.Insert(actual_level, new MBLOrderBookEntry(refresh.Price, refresh.Qty));
                            int maxVisibleDepth_value = (int)(m_MaxVisibleDepth.Num);
                            if (maxVisibleDepth_value >= 0 && BidLimits.Count > maxVisibleDepth_value)
                            {
                                BidLimits.RemoveRange(maxVisibleDepth_value, BidLimits.Count - maxVisibleDepth_value);
                            }
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskInsertAtLevel: /** insert line (Price,Qty) at Level and shift subsequent lines up */
                        if (actual_level <= askSize)
                        {
                            AskLimits.Insert(actual_level, new MBLOrderBookEntry(refresh.Price, refresh.Qty));
                            int maxVisibleDepth_value = (int)(m_MaxVisibleDepth.Num);
                            if (maxVisibleDepth_value >= 0 && AskLimits.Count > maxVisibleDepth_value)
                            {
                                AskLimits.RemoveRange(maxVisibleDepth_value, AskLimits.Count - maxVisibleDepth_value);
                            }
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidRemoveLevel: /** remove line at Level and shift subsequent lines down, append "worst visible" limit  */
                        if (actual_level < bidSize)
                        {
                            BidLimits.RemoveAt(actual_level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskRemoveLevel: /** remove line at Level and shift subsequent lines down, append "worst visible" limit  */
                        if (actual_level < askSize)
                        {
                            AskLimits.RemoveAt(actual_level);
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidChangeQtyAtLevel:/** change Qty at Level */
                        if (actual_level < bidSize)
                        {
                            BidLimits[actual_level].Qty = refresh.Qty;
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskChangeQtyAtLevel: /** change Qty at Level */
                        if (actual_level < askSize)
                        {
                            AskLimits[actual_level].Qty = refresh.Qty;
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_BidRemoveLevelAndAppend:/** remove line at Level and shift subsequent lines down, append "worst visible" limit  */
                        if (actual_level < bidSize)
                        {
                            BidLimits.RemoveAt(actual_level);
                            BidLimits.Add(new MBLOrderBookEntry(refresh.Price, refresh.Qty));
                        }
                        break;
                    case OrderBookDeltaAction.OrderBookDeltaAction_AskRemoveLevelAndAppend: /** remove line at Level and shift subsequent lines down, append "worst visible" limit  */
                        if (actual_level < askSize)
                        {
                            AskLimits.RemoveAt(actual_level);
                            AskLimits.Add(new MBLOrderBookEntry(refresh.Price, refresh.Qty));
                        }
                        break;
                }
                updateOtherValues(refresh.OtherValues);

                if (refresh.ContinuationFlag)
                {
                    if (!m_ContinuationFlagSet)
                    {
                        m_ContinuationFlagSet = true;
                        OtherValues.Add(new TagNumAndValue(TagsQuotation.TAG_HasContinuationFlag, true));
                    }
                }
                else
                {
                    if (m_ContinuationFlagSet)
                    {
                        m_ContinuationFlagSet = false;
                        if (hasOtherValues())
                        {
                            TagFinder tag_finder = new TagFinder(TagsQuotation.TAG_HasContinuationFlag);
                            int pos = m_OtherValues.FindIndex(tag_finder.FindTagWithNum);
                            if (-1 != pos)
                            {
                                m_OtherValues.RemoveAt(pos);
                            }
                        }
                    }
                }

                return !(refresh.ContinuationFlag);
            }

            // MBLLayer is updated by a MaxVisibleDepth event
            public void Update(MBLMaxVisibleDepth maxVisibleDepth)
            {
                m_MaxVisibleDepth = maxVisibleDepth.MaxVisibleDepth;
                int maxVisibleDepth_value = (int)(m_MaxVisibleDepth.Num);
                if (maxVisibleDepth_value < 0)
                {
                    return;      
                }
                int bidSize = BidLimits.Count;
                int askSize = AskLimits.Count;
                if (bidSize > maxVisibleDepth_value)
                {
                    BidLimits.RemoveRange(maxVisibleDepth_value, bidSize - maxVisibleDepth_value);
                }
                if (askSize > maxVisibleDepth_value)
                {
                    AskLimits.RemoveRange(maxVisibleDepth_value, askSize - maxVisibleDepth_value);
                }
            }
            #endregion

            #region Merge

            private static void InsertByPrice(List<MBLOrderBookEntry> result, MBLOrderBookEntry new_entry, MBLOrderBookEntry.IsBetterThan isbetterthan)
            {
                foreach (MBLOrderBookEntry result_entry in result)
                {
                    if( MBLOrderBookEntry.EqualPrices(result_entry.Price.Num, new_entry.Price.Num) )
                    {
                        MBLOrderBookEntry.MergeQty(result_entry.Qty,new_entry.Qty);
                        return;
                    }

                    if (isbetterthan(new_entry.Price.Num, result_entry.Price.Num))
                    {
                        result.Insert(result.IndexOf(result_entry), new MBLOrderBookEntry(new FOSPrice(new_entry.Price.Num), new MBLQty(new FOSQty(new_entry.Qty.CumulatedUnits.Num),new FOSQty(new_entry.Qty.NbOrders.Num))));
                        return;
                    }
                }

                result.Add(new MBLOrderBookEntry(new FOSPrice(new_entry.Price.Num), new MBLQty(new FOSQty(new_entry.Qty.CumulatedUnits.Num),new FOSQty(new_entry.Qty.NbOrders.Num))));
            }

            private static void MergeLimitsByPrice(List<MBLOrderBookEntry> targetLimits, List<MBLOrderBookEntry> inputLimits, MBLOrderBookEntry.IsBetterThan isbetterthan)
            {
                foreach( MBLOrderBookEntry inputLimit in inputLimits )
                {
                    InsertByPrice(targetLimits, inputLimit, isbetterthan);
                }
            }

            private void StripMBLOrderBookToVisibleDepth()
            {
			    if (m_MaxVisibleDepth.Num == MBLOrderBookEntry.ORDERBOOK_UNLIMITED_DEPTH)
			    {
				    return; 
			    }
                else if (m_MaxVisibleDepth.Num == 0)
                {
                    m_BidLimits.Clear();
                    m_AskLimits.Clear();
                    return;
                }

                if (m_BidLimits.Count > m_MaxVisibleDepth.Num)
                {
                    m_BidLimits.RemoveRange((int)m_MaxVisibleDepth.Num, m_BidLimits.Count - (int)m_MaxVisibleDepth.Num);
                }
                if (m_AskLimits.Count > m_MaxVisibleDepth.Num)
                {
                    m_AskLimits.RemoveRange((int)m_MaxVisibleDepth.Num, m_AskLimits.Count - (int)m_MaxVisibleDepth.Num);
                }
		    }

            // MBLLayer is updated by a MaxVisibleDepth event
            public void MergeByPrice(   MBLLayer layer,
                                        bool use_latest_server_timestamp,
							            bool use_latest_market_timestamp,
							            bool merge_other_values)
            {
                // Update timestamps
                if( use_latest_server_timestamp &&
                    ( (DateTime)Timestamps.Server) < ((DateTime)layer.Timestamps.Server) )
                {
                    m_Timestamps = new UTCTimestamps(layer.Timestamps.Server, m_Timestamps.Market);
                }

                if( use_latest_market_timestamp &&
                    ( (DateTime)Timestamps.Market) < ((DateTime)layer.Timestamps.Market) )
                {
                    m_Timestamps = new UTCTimestamps(m_Timestamps.Server, layer.Timestamps.Market);
                }

                TagFinder tag_finder = new TagFinder(0);
                // Merge others values
                if (merge_other_values && (layer.OtherValues.Count > 0))
                {
                    foreach( TagNumAndValue layerValue in layer.OtherValues )
                    {
                        tag_finder.TagNum = layerValue.Num;
                        TagNumAndValue currentValue = OtherValues.Find(tag_finder.FindTagWithNum);
                        if (currentValue != null)
                        {
                            int iValue = OtherValues.IndexOf(currentValue);
                            OtherValues[iValue] = new TagNumAndValue(layerValue.Num, layerValue.Value);
                        }
                        else
                        {
                            OtherValues.Add(new TagNumAndValue(layerValue.Num, layerValue.Value));
                        }
                    }
                }

                // Merge order book entries
                MergeLimitsByPrice(BidLimits, layer.BidLimits, MBLOrderBookEntry.GreaterBidPrice);
                MergeLimitsByPrice(AskLimits, layer.AskLimits, MBLOrderBookEntry.LowerAskPrice);

                StripMBLOrderBookToVisibleDepth();
            }

            #endregion

            #region Members
            protected uint                      m_LayerId;
            protected FOSOrderBookLevel         m_MaxVisibleDepth;
            protected UTCTimestamps             m_Timestamps;
            protected List<MBLOrderBookEntry>   m_BidLimits;
            protected List<MBLOrderBookEntry>   m_AskLimits;
            protected List<TagNumAndValue>      m_OtherValues;
            protected bool                      m_ContinuationFlagSet;
            #endregion

            public override string ToString()
            {
                string bid_limits = "<null>";
                if (m_BidLimits != null)
                {
                    bid_limits = m_BidLimits.Count.ToString();
                }
                string ask_limits = "<null>";
                if (m_AskLimits != null)
                {
                    ask_limits = m_AskLimits.Count.ToString();
                }
                string other_values = "<null>";
                if (hasOtherValues())
                {
                    other_values = m_OtherValues.Count.ToString();
                }
                return "Id:" + m_LayerId + " Bid:" + bid_limits + " Ask:" + ask_limits + " Mvd:" + m_MaxVisibleDepth.Num + " OV:" + other_values;

            }
        }

        // MBL Snapshot aggregates for a single instrument, several MBLLayer identified by its "layer id"
        public class MBLSnapshot
        {
            #region Constructor
            public MBLSnapshot(uint instrumentCode, List<MBLLayer> layers)
            {
                m_InstrumentCode = instrumentCode;
                m_Layers = new Dictionary<uint,MBLLayer>();
                if (null != layers)
                {
                    foreach (MBLLayer l in layers)
                    {
                        MBLLayer existingLayer = null;
                        if (!m_Layers.TryGetValue(l.LayerId,out existingLayer))
                        {
                            m_Layers.Add(l.LayerId, l);
                        }
                        else
                        {
                            existingLayer = l;
                        }
                    }
                }
            }
            #endregion

            #region Properties
            public uint InstrumentCode
            {
                get { return m_InstrumentCode; }
            }
            public List<MBLLayer> Layers
            {
                get
                {
                    if (null != m_Layers)
                    {
                        return new List<MBLLayer>(m_Layers.Values);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            #endregion

            #region Update
            public void Update(MBLOverlapRefresh overlap)
            {
                getOrCreateLayer(overlap.LayerId).Update(overlap);
            }

            public bool Update(MBLDeltaRefresh refresh)
            {
                return getOrCreateLayer(refresh.LayerId).Update(refresh);
            }

            public void Update(MBLMaxVisibleDepth maxVisibleDepth)
            {
                getOrCreateLayer(maxVisibleDepth.LayerId).Update(maxVisibleDepth);
            }

            public bool RemoveLayer(uint layer_id)
            {
                return m_Layers.Remove(layer_id);
            }

            public const bool DONT_USE_LATEST_SERVER_TIMESTAMP = false;
            public const bool USE_LATEST_SERVER_TIMESTAMP = true;
            public const bool DONT_USE_LATEST_MARKET_TIMESTAMP = false;
            public const bool USE_LATEST_MARKET_TIMESTAMP = true;
            public const bool DONT_MERGE_OTHER_VALUES = false;
            public const bool MERGE_OTHER_VALUES = true;
            public const bool DONT_PRESERVE_MERGED_LAYERS = false;
            public const bool PRESERVE_MERGED_LAYERS = true;

            public bool MergeAllLayers( uint layer_id_merged,
							            bool use_latest_server_timestamp,
							            bool use_latest_market_timestamp,
							            bool merge_other_values,
                                        bool preserve_merged_layers)
		    {
                if (Layers.Count == 0)
                {   // No layer in the snapshot => nothing to do
                    return false;
                }

                Predicate<MBLLayer> layerByIdFinder = delegate(MBLLayer layer)
                {
                    return layer.LayerId == layer_id_merged;
                };

                MBLLayer merged_layer = Layers.Find(layerByIdFinder);
                if( ( merged_layer != null ) && // The merged layer is already in the snaphsot ...
                    ((Layers.Count == 1) || preserve_merged_layers)) // ... and the snapshot has only one layer or the existing layers must be kept alive
                {
                    return false; // => nothing to do
                }

                // Do the merge                    	
			    merged_layer = getOrCreateLayer(layer_id_merged);
			    uint merge_count = 0;
			    for (	int layer = Layers.Count-1 ;
					    layer >= 0 ;
					    --layer )
			    {
				    MBLLayer current_layer = Layers[layer];
				    uint layer_id = current_layer.LayerId;

				    if( layer_id == layer_id_merged )
					    continue; // Nothing to do in such case
    				
				    long former_merged_MVD = merged_layer.MaxVisibleDepth.Num;
				    long cur_layer_MVD = current_layer.MaxVisibleDepth.Num;
    				
				    merged_layer.MaxVisibleDepth.Num = MBLOrderBookEntry.ORDERBOOK_UNLIMITED_DEPTH;
				    merged_layer.MergeByPrice(current_layer,use_latest_server_timestamp,use_latest_market_timestamp,merge_other_values);
				    ++merge_count;
				    if( !preserve_merged_layers )
				    {
                        m_Layers.Remove(layer_id);
				    }

				    // if cur_layer_MVD == -1									-> consider former_merged_MVD
				    // if former_merged_MVD == -1 and first merge				-> consider cur_layer_MVD
				    // if former_merged_MVD == -1 and first merge has occured	-> consider min(former_merged_MVD,cur_layer_MVD)
				    // else former_merged_MVD != -1 and cur_layer_MVD != -1		-> consider min(former_merged_MVD,former_merged_MVD)
				    if ((1 == merge_count) && (former_merged_MVD <= 0))
				    {
					    merged_layer.MaxVisibleDepth.Num = cur_layer_MVD;
				    }
				    else if (cur_layer_MVD < 0)
				    {
					    merged_layer.MaxVisibleDepth.Num = former_merged_MVD;
				    }
				    else 
				    {
					    merged_layer.MaxVisibleDepth.Num = Math.Min(former_merged_MVD,cur_layer_MVD);
				    }
			    }
			    return true;
		    }

            #endregion

            #region Tools
            public MBLLayer getOrCreateLayer(uint layerId)
            {
                MBLLayer layer = null;
                if (!m_Layers.TryGetValue(layerId, out layer))
                {
                    layer = new MBLLayer(layerId);
                    m_Layers.Add(layerId, layer);
                }
                return layer;
            }
            #endregion

            #region Members
            protected uint m_InstrumentCode;
            protected Dictionary<uint,MBLLayer> m_Layers;
            #endregion

            public override string ToString()
            {
                string layers = "<null>";
                if (m_Layers != null)
                {
                    layers = m_Layers.Count.ToString();
                }
                return "Instrument:" +  m_InstrumentCode + " Layers:" + layers;
            }
        }
    }
}
