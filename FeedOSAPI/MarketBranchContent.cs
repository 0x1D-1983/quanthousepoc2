///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MarketBranchContent
        {
            #region Constructor
            public MarketBranchContent(MarketBranchId MarketBranchId, uint Quantity)
            {
                m_MarketBranchId = MarketBranchId;
                m_Quantity = Quantity;
            }
            #endregion

            #region Properties
            public MarketBranchId MarketBranchId
            {
                get { return m_MarketBranchId; }
            }
            public uint Quantity
            {
                get { return m_Quantity; }
            }
            #endregion

            #region Members
            protected MarketBranchId m_MarketBranchId;
		    protected uint m_Quantity;
            #endregion
        }
    }
}
