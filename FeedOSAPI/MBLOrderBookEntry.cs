///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2012 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using System.Diagnostics;

namespace FeedOSAPI
{
    namespace Types
    {
        public class MBLOrderBookEntry
        {
            #region Constants
            public const int ORDERBOOK_NB_ORDERS_UNKNOWN   = -1;
            // Constant to get the information that there is no more Bid/Ask provdied
            public const int ORDERBOOK_NB_ORDERS_EMPTY     = 0;

            public const long ORDERBOOK_UNLIMITED_DEPTH = -1;

            // Comparison results
            public const int RESULT_x_GREATER_THAN_y = 1;
            public const int RESULT_x_EQUALS_y = 0;
            public const int RESULT_x_LOWER_THAN_y = -1;

            // Special price values
            // decimal flavor
            public const decimal UNQUOTED =                      666666.666m;
            public const decimal ORDERBOOK_MAGIC_PRICE_AT_BEST = 999999.999m;
            public const decimal ORDERBOOK_MAGIC_PRICE_AT_OPEN = 999999.989m;
            public const decimal ORDERBOOK_MAGIC_PRICE_AT_CLOSE= 999999.988m;
            public const decimal ORDERBOOK_MAGIC_PRICE_PEG     = 999999.979m;
            public const decimal PRICE_EPSILON = 1e-12m;
            // double flavor
            public const double UNQUOTEDd                      = 666666.666;
            public const double ORDERBOOK_MAGIC_PRICE_AT_BESTd = 999999.999;
            public const double ORDERBOOK_MAGIC_PRICE_AT_OPENd = 999999.989;
            public const double ORDERBOOK_MAGIC_PRICE_AT_CLOSEd= 999999.988;
            public const double ORDERBOOK_MAGIC_PRICE_PEGd     = 999999.979;
            public const double PRICE_EPSILONd = 1e-12;
            #endregion

            #region Constructor
            public MBLOrderBookEntry(FOSPrice price, MBLQty qty)
            {
                m_Price = price;
                m_Qty   = qty;
            }
            #endregion

            #region Properties
            public FOSPrice Price
            {
                get { return m_Price; }
            }
            public MBLQty Qty
            {
                get { return m_Qty; }
                set { m_Qty = value; }
            }
            #endregion

            #region Dump One Side limit
            // Manage special values in case of PreOpen or PreClose 
            public static bool isASpecialValuePrice(FOSPrice price, ref string price_str)
            {
                if (MBLOrderBookEntry.UNQUOTEDd == price.Num)
                {
                    price_str = "UNQUOTED";
                    return true;
                }
                else if (MBLOrderBookEntry.ORDERBOOK_MAGIC_PRICE_AT_BESTd == price.Num)
                {
                    price_str = "AT_BEST";
                    return true;
                }
                else if (MBLOrderBookEntry.ORDERBOOK_MAGIC_PRICE_AT_OPENd == price.Num)
                {
                    price_str = "AT_OPEN";
                    return true;
                }
                else if (MBLOrderBookEntry.ORDERBOOK_MAGIC_PRICE_AT_CLOSEd == price.Num)
                {
                    price_str = "AT_CLOSE";
                    return true;
                }
                else if (MBLOrderBookEntry.ORDERBOOK_MAGIC_PRICE_PEGd == price.Num)
                {
                    price_str = "PEG";
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // Manage special values for nb_orders
            public static bool isASpecialNbOrders(FOSQty nb_orders, ref string nb_order_str)
            {
                if (MBLOrderBookEntry.ORDERBOOK_NB_ORDERS_UNKNOWN == nb_orders.Num)
                {
                    nb_order_str = "?";
                    return true;
                }
                else if (MBLOrderBookEntry.ORDERBOOK_NB_ORDERS_EMPTY == nb_orders.Num)
                {
                    nb_order_str = "EMPTY";
                    return true;
                }
                  else
                {
                    return false;
                }
            }

            public string DumpOneSideLimit(string BidOrAsk, CultureInfo culture)
            {
                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append(BidOrAsk);
                strBuilder.Append("\tprice=");
                string special_price = null;
                if (!isASpecialValuePrice(m_Price, ref special_price)) 
                {
                    strBuilder.Append(m_Price.Num.ToString("F3", culture));
                }
                else
                {
                    strBuilder.Append(special_price);
                }
 
                strBuilder.Append("\tqty=" + m_Qty.CumulatedUnits.Num);

                string special_nb_orders = null;
                if (!isASpecialNbOrders(m_Qty.NbOrders, ref special_nb_orders))
                {
                    strBuilder.Append("\tnb_orders=" + m_Qty.NbOrders.Num);
                }
                else
                {
                    strBuilder.Append(special_nb_orders);    
                }
                strBuilder.Append('\n');
                return strBuilder.ToString();
            }
            #endregion Dump One Side limit

            #region Price Tools
           
            public static bool EqualPrices(double price_x, double price_y)
            {
                return Math.Abs(price_x - price_y) < PRICE_EPSILONd;
            }

            public static int CompareBidPrices(double price_x, double price_y)
            {
                if (EqualPrices(price_x, price_y)) return RESULT_x_EQUALS_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_AT_BESTd) return RESULT_x_GREATER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_AT_BESTd) return RESULT_x_LOWER_THAN_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_AT_OPENd) return RESULT_x_GREATER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_AT_OPENd) return RESULT_x_LOWER_THAN_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_AT_CLOSEd) return RESULT_x_GREATER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_AT_CLOSEd) return RESULT_x_LOWER_THAN_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_PEGd) return RESULT_x_GREATER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_PEGd) return RESULT_x_LOWER_THAN_y;
                if (price_x > price_y) return RESULT_x_GREATER_THAN_y;

                return RESULT_x_LOWER_THAN_y;
            }

            public static int CompareAskPrices(double price_x, double price_y)
            {
                if (EqualPrices(price_x, price_y)) return RESULT_x_EQUALS_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_AT_BESTd) return RESULT_x_LOWER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_AT_BESTd) return RESULT_x_GREATER_THAN_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_AT_OPENd) return RESULT_x_LOWER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_AT_OPENd) return RESULT_x_GREATER_THAN_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_AT_CLOSEd) return RESULT_x_LOWER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_AT_CLOSEd) return RESULT_x_GREATER_THAN_y;
                if (price_x == ORDERBOOK_MAGIC_PRICE_PEGd) return RESULT_x_LOWER_THAN_y;
                if (price_y == ORDERBOOK_MAGIC_PRICE_PEGd) return RESULT_x_GREATER_THAN_y;
                if (price_x > price_y) return RESULT_x_GREATER_THAN_y;

                return RESULT_x_LOWER_THAN_y;
            }
            
            public delegate bool IsBetterThan(double price_x, double price_y);

            public static bool GreaterBidPrice(double price_x, double price_y)
            {
                return (CompareBidPrices(price_x, price_y) == RESULT_x_GREATER_THAN_y);
            }

            public static bool LowerAskPrice(double price_x, double price_y)
            {
                return (CompareAskPrices(price_x, price_y) == RESULT_x_LOWER_THAN_y);
            }

            #endregion

            #region Qty Tools

            public static void MergeQty(MBLQty output, MBLQty input)
            {
                output.CumulatedUnits.Num += input.CumulatedUnits.Num;
                if (output.NbOrders.Num == MBLOrderBookEntry.ORDERBOOK_NB_ORDERS_UNKNOWN)
                {
                    if (input.NbOrders.Num != MBLOrderBookEntry.ORDERBOOK_NB_ORDERS_UNKNOWN)
                    {
                        // unknown + something = 1 + something
                        output.NbOrders.Num = 1 + input.NbOrders.Num;
                    }
                }
                else if (input.NbOrders.Num == MBLOrderBookEntry.ORDERBOOK_NB_ORDERS_UNKNOWN)
                {
                    // something + unknown = something + 1
                    ++output.NbOrders.Num;
                }
                else
                {
                    // somethingA + somethingB = A + B
                    output.NbOrders.Num += input.NbOrders.Num;
                }
            }

            #endregion

            #region Members
            protected FOSPrice  m_Price;
            protected MBLQty    m_Qty;
            #endregion

            public override string ToString()
            {
                return m_Price.Num.ToString() + " x " + m_Qty.CumulatedUnits.Num + " @ " + m_Qty.NbOrders.Num;
            }
        }
    }
}
