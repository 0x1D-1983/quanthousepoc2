///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

using FeedOSAPI.Types;

namespace FeedOSAPI
{
    namespace Types
    {
        /// <summary>
        /// Description of a FeedStatus or FeedStatusEvent, 
        /// embedding the name of the source and it's internal ids
        /// </summary>
        #region class FeedDescription
        public class FeedDescription 
        {
            #region Constructor
            public FeedDescription(String feedName, List<ushort> internalSourceIds)
            {
                m_FeedName = feedName;
                m_InternalSourceIds = internalSourceIds;
            }
            #endregion

            #region Properties
            public String FeedName
            {
                get { return m_FeedName; }
            }
            public List<ushort> InternalSourceIds
            {
                get { return m_InternalSourceIds; }
            }
            #endregion

            #region overrides
            public override string ToString()
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append("Feed ");
                sBuilder.Append(m_FeedName.ToString());
                if (null != m_InternalSourceIds)
                {
                    foreach (uint intId in m_InternalSourceIds) sBuilder.Append("id " + intId.ToString() + ", ");
                }

                return sBuilder.ToString();
            }
            #endregion
            
            #region Members
            protected String m_FeedName;
            protected List<ushort> m_InternalSourceIds;
            #endregion
        }
        #endregion

        /// <summary>
        /// FeedUsability of a FeedStatus, embedding a FeedState describing a source state.
        /// Additional flags depicts the risk attached to the state
        /// </summary>
        #region class FeedUsability
        public class FeedUsability 
        {
            private static Dictionary<Types.FeedState, String> mapStatus;
            static FeedUsability()
            {
                mapStatus = new Dictionary<Types.FeedState, String>(5);
                mapStatus[Types.FeedState.FeedState_Active] = "Active";
                mapStatus[Types.FeedState.FeedState_ProbablyNormal] = "ProbablyNormal";
                mapStatus[Types.FeedState.FeedState_ProbablyDisrupted] = "ProbablyDisrupted";
                mapStatus[Types.FeedState.FeedState_Disrupted_TechnicalLevel] = "Disrupted_TechnicalLevel";
                mapStatus[Types.FeedState.FeedState_Disrupted_ExchangeLevel] = "Disrupted_ExchangeLevel";
            }
            #region Constructor
            public FeedUsability(FeedState feedState, bool latencyPenalty, bool outOfDateValues, bool badDataQuality)
            {
                m_FeedState = feedState;
                m_LatencyPenalty = latencyPenalty;
                m_OutOfDateValues = outOfDateValues;
                m_BadDataQuality = badDataQuality;
            }
            #endregion

            #region Properties
            public FeedState FeedState
            {
                get { return m_FeedState; }
            }
            public bool LatencyPenalty
            {
                get { return m_LatencyPenalty; }
            }
            public bool OutOfDateValues
            {
                get { return m_OutOfDateValues; }
            }
            public bool BadDataQuality
            {
                get { return m_BadDataQuality; }
            }
            #endregion

            #region Overrides
            public override string ToString()
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append("FeedUsability is: ");
                sBuilder.Append((mapStatus[FeedState]).ToString());
                if (BadDataQuality) sBuilder.Append(" ,BadDataQuality");
                if (LatencyPenalty) sBuilder.Append(" ,LatencyPenalty");
                if (OutOfDateValues) sBuilder.Append(" ,OutOfDateValues");
                return sBuilder.ToString();
            }
            #endregion

            #region Members
            protected FeedState m_FeedState;
            protected bool m_LatencyPenalty;
            protected bool m_OutOfDateValues;
            protected bool m_BadDataQuality;
            #endregion
        }
        #endregion

        /// <summary>
        /// FeedServiceStatus is a FeedUsability related to a service,
        /// it embeds the service name
        /// </summary>
        #region class FeedServiceStatus
        public class FeedServiceStatus 
        {
            #region Constructor
            public FeedServiceStatus(String serviceName, FeedUsability usability)
            {
                m_ServiceName = serviceName;
                m_Usability = usability;
            }
            #endregion

            #region Properties
            public String ServiceName
            {
                get { return m_ServiceName; }
            }
            public FeedUsability Usability
            {
                get { return m_Usability; }
            }
            #endregion

            #region Overrides
            public override string ToString()
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append("FeedServiceStatus " + m_ServiceName.ToString() + " ,");
                sBuilder.Append(m_Usability.ToString());
                sBuilder.Append("\n");
                return sBuilder.ToString();
            }
            #endregion
            
            #region Members
            protected String        m_ServiceName;
            protected FeedUsability m_Usability;
            #endregion
        }
        #endregion
        /// <summary>
        /// FeedStatus composed of a FeedDescription, an overall FeedUsability and a List of FeedServiceStatus.
        /// </summary>
        #region class FeedStatus
        public class FeedStatus 
        {
            #region Constructor
            public FeedStatus(FeedDescription feedDesc, FeedUsability overallUsability, List<FeedServiceStatus> serviceStatus)
            {
                m_FeedDescription = feedDesc;
                m_OverallUsability = overallUsability;
                m_ServiceStatus = serviceStatus;
            }
            #endregion

            #region Properties
            public FeedDescription FeedDescription
            {
                get { return m_FeedDescription; }
            }

            [Obsolete("bad named property, use OverallFeedUsability")]
            public FeedUsability FeedUsability
            {
                get { return m_OverallUsability; }
            }
            public FeedUsability OverallFeedUsability
            {
                get { return m_OverallUsability; }
            }
            public List<FeedServiceStatus> ServiceStatus
            {
                get { return m_ServiceStatus; }
            }
            #endregion

            #region overrides
            public override string ToString()
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append("FeedStatus ");
                sBuilder.Append(m_FeedDescription.ToString());
                sBuilder.Append(" ,");                
                sBuilder.Append(m_OverallUsability.ToString());
                sBuilder.Append(" ,"); 
                if (null != m_ServiceStatus)
                {
                    foreach (FeedServiceStatus fss in m_ServiceStatus)
                    {
                        sBuilder.Append(fss.ToString());
                    }
                }
                sBuilder.Append("\n");
                return sBuilder.ToString();
            }
            #endregion

            #region Members
            protected FeedDescription m_FeedDescription;
            protected FeedUsability m_OverallUsability;
            protected List<FeedServiceStatus> m_ServiceStatus;
            #endregion
        }
        #endregion

        /// <summary>
        /// A FeedStatusEvent is the equivalent of a FeedStatus, but all informations are put into a String Format,
        /// more for a debug/trace behavior, as not behavior can be triggered on a particular FeedState modification.
        /// </summary>
        #region class FeedStatusEvent
        public class FeedStatusEvent
        {
            #region Constructor
            public FeedStatusEvent(FeedDescription feedDesc, String eventType, List<String> eventDetails)
            {
                m_FeedDescription = feedDesc;
                m_EventType = eventType;
                m_EventDetails = eventDetails;
            }
            #endregion

            #region Properties

            public FeedDescription FeedDescription
            {
                get { return m_FeedDescription; }
            }
            public String EventType
            {
                get { return m_EventType; }
            }
            public List<String> EventDetails
            {
                get { return m_EventDetails; }
            }
            #endregion

            #region overrides
            public override string ToString()
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append("FeedStatusEvent ");
                sBuilder.Append(m_FeedDescription.ToString());
                sBuilder.Append(" ,");
                sBuilder.Append(m_EventType.ToString());
                sBuilder.Append(" ,"); 
                if (null != m_EventDetails)
                {
                    foreach (String eventDetail in m_EventDetails)
                    {
                        sBuilder.Append(eventDetail.ToString());
                    }
                }
                sBuilder.Append("\n");
                return sBuilder.ToString();
            }
            #endregion

            #region Members
            protected FeedDescription m_FeedDescription;
            protected String m_EventType;
            protected List<String> m_EventDetails;
            #endregion
        }
        #endregion
    }
}
