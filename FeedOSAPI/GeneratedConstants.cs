///-------------------------------
/// FeedOS C# Client API    
/// copyright QuantHouse 2003-2009 
///-------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedOSAPI
{
    namespace Types
    {
        public class QuotationContent
        {
            public const uint Mask_EVERYTHING = UInt32.MaxValue;

            // values used in Notifs & Subscribe Requests
            public const uint Bit_TradingStatus = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_TradingStatus;
            public const uint Bit_Bid = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Bid;
            public const uint Bit_Ask = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Ask;
            public const uint Bit_LastPrice = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_LastPrice;
            public const uint Bit_LastTradeQty = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_LastTradeQty;
            public const uint Bit_Open = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Open;
            public const uint Bit_Close = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Close;
            public const uint Bit_High = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_High;
            public const uint Bit_Low = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Low;
            public const uint Bit_OCHL_daily = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OCHL_daily;
            public const uint Bit_OtherValues = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OtherValues;
            public const uint Bit_OpeningNextCalendarDay = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OpeningNextCalendarDay;
            public const uint Bit_OffBookTrade = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_OffBookTrade;
            public const uint Bit_Context = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Context;
            public const uint Bit_ChangeBusinessDay = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_ChangeBusinessDay;
            public const uint Bit_Session = 1 << (int)QuotationUpdateContent.QuotationUpdateContent_Session;

            public const uint Bit_BidOrAsk = Bit_Bid | Bit_Ask;

            #region namespace-style
            private QuotationContent() { }
            private QuotationContent(QuotationContent quotationContent) { }
            #endregion
        }

		public class ClosingPriceType
		{
			public const char Undefined = '0';
			public const char OfficialClose = 'a';
			public const char OfficialIndicative = 'b';
			public const char OfficialCarryOver = 'c';
			public const char LastPrice = 'd';
			public const char LastEligiblePrice = 'e';
			public const char Manual = 'z';
		}

		public class SettlementPriceType
		{
			public const char Official = 'a';
			public const char Preliminary = 'b';
			public const char Manual = 'z';
			public const char Undefined = '0';
		}

        public class AdjustmentFactor
        {
            public const uint None = 0;
            public const uint All = UInt32.MaxValue;

            public const uint Bit_RightsSameClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_RightsSameClass;
            public const uint Bit_RightsDifferentClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_RightsDifferentClass;
            public const uint Bit_EntitlementSameClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_EntitlementSameClass;
            public const uint Bit_EntitlementDifferentClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_EntitlementDifferentClass;
            public const uint Bit_Subdivision = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_Subdivision;
            public const uint Bit_Consolidation = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_Consolidation;
            public const uint Bit_Demerger = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_Demerger;
            public const uint Bit_CapitalReturn = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_CapitalReturn;
            public const uint Bit_Distribution = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_Distribution;
            public const uint Bit_BonusSameClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_BonusSameClass;
            public const uint Bit_BonusDifferentClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_BonusDifferentClass;
            public const uint Bit_CapitalReduction = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_CapitalReduction;
            public const uint Bit_CashDividend = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_CashDividend;
            public const uint Bit_ScriptDividendSameClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_ScriptDividendSameClass;
            public const uint Bit_ScriptDividendDifferentClass = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_ScriptDividendDifferentClass;
            public const uint Bit_CapitalCall = 1 << (int)AdjustmentFactorEventType.AdjustmentFactorEventType_CapitalCall;
        }

        public class SessionId
        {
            public const sbyte InvalidSessionId = 0;
            public const sbyte CoreSessionId = 127;
        }

        public class SecurityStatus
        {
            public const sbyte Active = 1;
            public const sbyte Inactive = 2;
            public const sbyte Suspended = 3;
            public const sbyte PendingActivation = 4;
        };
    }
}
