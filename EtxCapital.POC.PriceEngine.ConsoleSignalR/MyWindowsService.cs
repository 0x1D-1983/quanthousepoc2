﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR
{
    public class MyWindowsService
    {
        public void Start()
        {
            string url = "http://127.0.0.1:8088";
            WebApp.Start(url);
        }

        public void Stop()
        {
        }
    }
}
