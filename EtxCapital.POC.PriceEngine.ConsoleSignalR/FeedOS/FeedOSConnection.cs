﻿using FeedOSManaged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR.FeedOS
{
    public class FeedOSConnection : Connection, IDisposable
    {
        private static int _requestId = int.MaxValue - 1;

        public static uint NextRequestId
        {
            get
            {
                return unchecked((uint)Interlocked.Increment(ref _requestId));
            }
        }

        public FeedOSConnection()
        {
            // Initialize the FeedOS API
            API.Init("ETXCapital-Request");
            API.InitTraceAllAreas("FeedOSSample_" + DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss") + ".log",
                true,  // append_mode
                true,   // flush_every_line
                true,   // enable_error_level
                true,   // enable_warning_level
                true,   // enable_info_level
                true,   // enable_debug_level 
                false    // enable_scope_level 
            );

            // Initialize the FeedOS connection and register the related callback methods


            Disconnected += DisconnectedHandler;
            AdminMessage += AdminMessageHandler;
            HeartBeat += HeartBeatHandler;
            MsgDispatchingHookBegin += MsgDispatchingHookBeginHandler;
            MsgDispatchingHookEnd += MsgDispatchingHookEndHandler;
        }

        private void MsgDispatchingHookEndHandler()
        {
            Console.WriteLine("_connection_MsgDispatchingHookEnd");

        }

        private void MsgDispatchingHookBeginHandler(uint nbPendingMsg)
        {
            Console.WriteLine("_connection_MsgDispatchingHookBegin");
        }

        private void HeartBeatHandler(ValueType serverUTCDateTime, uint heartbeatPeriodSec)
        {
            Console.WriteLine("_connection_HeartBeat");
        }

        private void AdminMessageHandler(bool isUrgent, string origin, string headline, string content)
        {
            Console.WriteLine("_connection_AdminMessage");
        }

        private void DisconnectedHandler(uint returnCode)
        {
            Console.WriteLine("_connection_Disconnected");
        }

        public uint Connect()
        {
            return Connect("109.236.198.13", 6051, "etx_test", "XCe4UENMQg");
        }
    }
}
