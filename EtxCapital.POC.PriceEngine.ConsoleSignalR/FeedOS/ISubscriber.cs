﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR.FeedOS
{
    public interface ISubscriber
    {
        Task Subscribe();

        Task Unsubscribe();
    }
}
