﻿using EtxCapital.POC.PriceEngine.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeedOSAPI.Types;
using EtxCapital.POC.PriceEngine.ConsoleSignalR.Hubs;
using Microsoft.AspNet.SignalR;
using System.Threading;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR.FeedOS
{
    public class ReferentialGetInstruments : ISimpleRequest<List<StockDetails>>
    {
        private readonly FeedOSConnection _connection;

        private CancellationTokenSource _autoRunningCancellationToken;

        private Task _autoRunningTask;

        public ReferentialGetInstruments(FeedOSConnection connection)
        {
            _connection = connection;
        }

        public List<StockDetails> SendRequest()
        {
            var result = new List<StockDetails>();

            _connection.InstrumentsHandler += (requestId, instrumentCharacteristicsList) =>
            {
                foreach (var instrumentCharacteristics in instrumentCharacteristicsList)
                {
                    var stockdetails = new StockDetails(instrumentCharacteristics.InstrumentCode, instrumentCharacteristics.Symbol, instrumentCharacteristics.Description, instrumentCharacteristics.ISIN);

                    //var context = GlobalHost.ConnectionManager.GetHubContext<StockTickerHub>();
                    //context.Clients.All.UpdateStockDetails(instrumentDetails);

                    result.Add(stockdetails);
                }
            };

            _connection.ReferentialGetInstruments(StockStore.Codes, null, false, FeedOSConnection.NextRequestId);

            return result;
        }

        private void _connection_InstrumentsHandler(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        {
            
        }
    }
}
