﻿using EtxCapital.POC.PriceEngine.Business.Models;
using EtxCapital.POC.PriceEngine.ConsoleSignalR.Hubs;
using FeedOSAPI.Types;
using FeedOSManaged;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR.FeedOS
{
    public class SubscriberL1 : ISubscriber
    {
        private CancellationTokenSource _autoRunningCancellationToken;
        private Task _autoRunningTask;
        private readonly FeedOSConnection _connection;
        private ConcurrentDictionary<uint, Stock> _stocks = new ConcurrentDictionary<uint, Stock>();

        public SubscriberL1(FeedOSConnection connection)
        {
            _connection = connection;
        }

        public async Task Subscribe()
        {
            _autoRunningCancellationToken = new CancellationTokenSource();
            _autoRunningTask = Task.Run(() =>
            {
                uint returnCode = _connection.Connect();
                if (returnCode == 0)
                {
                    _connection.TradeEventExtHandler += _connection_TradeEventExtHandler;
                    _connection.SnapshotsHandler += _connection_SnapshotsHandler;
                    _connection.SubscribeL1(StockStore.Codes, null, QuotationContent.Mask_EVERYTHING, false, FeedOSConnection.NextRequestId);
                }
                else
                {
                    Console.WriteLine("Connection failed: " + API.ErrorString(returnCode));
                    _autoRunningCancellationToken.Cancel();
                }


                while (!_autoRunningCancellationToken.IsCancellationRequested)
                {
                }
            });
        }

        private void _connection_TradeEventExtHandler(uint requestId, uint instrumentCode, ValueType serverUTCDateTime, ValueType marketUTCDateTime, QuotationTradeEventExt quotationTradeEventExt)
        {
            Console.WriteLine("_connection_TradeEventExtHandler");
            //_contextHolder.TickerHubClients.Caller.MessageReceived("_connection_TradeEventExtHandler");

            var stock = new Stock(instrumentCode);

            if (quotationTradeEventExt.BestAsk != null)
            {
                stock.AskPrice = (decimal)quotationTradeEventExt.BestAsk.Price;
            }

            if (quotationTradeEventExt.BestBid != null)
            {
                stock.BidPrice = (decimal)quotationTradeEventExt.BestBid.Price;
            }

            //_store.UpdateStockPrice(instrumentCode, (decimal)quotationTradeEventExt.Price);

            if (null != quotationTradeEventExt.Values)
            {
                foreach (var tagNumAndValue in quotationTradeEventExt.Values)
                {
                    // set
                    switch (tagNumAndValue.Num)
                    {
                        case TagsQuotation.TAG_LastPrice:
                            stock.LastPrice = (decimal)(double)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_LastTradePrice:
                            stock.LastTradePrice = (decimal)(double)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_LastTradeTimestamp:
                            stock.LastTradeTimestamp = (DateTime)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_CurrentBusinessDay:
                            stock.CurrentBusinessDay = (DateTime)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_DailyClosingAskPrice:
                            stock.DailyClosingAskPrice = (decimal)(double)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_DailyClosingAskTimestamp:
                            stock.DailyClosingAskTimestamp = (DateTime)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_DailyClosingBidPrice:
                            stock.DailyClosingBidPrice = (decimal)(double)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_DailyClosingBidTimestamp:
                            stock.DailyClosingBidTimestamp = (DateTime)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_DailyOpeningPrice:
                            stock.DailyOpeningPrice = (decimal)(double)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_DailyOpeningPriceTimestamp:
                            stock.DailyOpeningPriceTimestamp = (DateTime)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_PreviousDailyClosingPrice:
                            stock.PreviousDailyClosingPrice = (decimal)(double)tagNumAndValue.Value;
                            break;
                        case TagsQuotation.TAG_PreviousDailyClosingPriceTimestamp:
                            stock.PreviousDailyClosingPriceTimestamp = (DateTime)tagNumAndValue.Value;
                            break;
                    }
                }
            }


            UpdateDictionary(stock);

            var context = GlobalHost.ConnectionManager.GetHubContext<StockTickerHub>();
            //context.Clients.All.Send("Admin", "stop the chat");
            context.Clients.All.UpdateStock(_stocks[stock.InternalCode]);
        }

        private void _connection_SnapshotsHandler(uint requestId, List<InstrumentStatusL1> instrumentStatusL1List)
        {
            Stock stock;

            Console.WriteLine("_connection_SnapshotsHandler");
            //_contextHolder.TickerHubClients.Caller.MessageReceived("_connection_SnapshotsHandler");


            foreach (var statusL1 in instrumentStatusL1List)
            {
                stock = new Stock(statusL1.InstrumentCode);


                Console.WriteLine("-- " + API.FOSMarketIDInstrument(statusL1.InstrumentCode) + "/" + API.LocalCodeInstrument(statusL1.InstrumentCode));

                Console.WriteLine($"\tBID: {statusL1.OrderBookBestLimitsExt.BestBid.Price,7}\t{statusL1.OrderBookBestLimitsExt.BestBid.Size}");

                // set
                stock.BidPrice = (decimal)statusL1.OrderBookBestLimitsExt.BestBid.Price;

                Console.WriteLine($"\tASK: {statusL1.OrderBookBestLimitsExt.BestAsk.Price,7}\t{statusL1.OrderBookBestLimitsExt.BestAsk.Size}");

                // set
                stock.AskPrice = (decimal)statusL1.OrderBookBestLimitsExt.BestAsk.Price;

                if (null != statusL1.QuotationVariables)
                {
                    foreach (var tagNumAndValue in statusL1.QuotationVariables)
                    {
                        // set
                        switch (tagNumAndValue.Num)
                        {
                            case TagsQuotation.TAG_LastPrice:
                                stock.LastPrice = (decimal)(double)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_LastTradePrice:
                                stock.LastTradePrice = (decimal)(double)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_LastTradeTimestamp:
                                stock.LastTradeTimestamp = (DateTime)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_CurrentBusinessDay:
                                stock.CurrentBusinessDay = (DateTime)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_DailyClosingAskPrice:
                                stock.DailyClosingAskPrice = (decimal)(double)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_DailyClosingAskTimestamp:
                                stock.DailyClosingAskTimestamp = (DateTime)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_DailyClosingBidPrice:
                                stock.DailyClosingBidPrice = (decimal)(double)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_DailyClosingBidTimestamp:
                                stock.DailyClosingBidTimestamp = (DateTime)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_DailyOpeningPrice:
                                stock.DailyOpeningPrice = (decimal)(double)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_DailyOpeningPriceTimestamp:
                                stock.DailyOpeningPriceTimestamp = (DateTime)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_PreviousDailyClosingPrice:
                                stock.PreviousDailyClosingPrice = (decimal)(double)tagNumAndValue.Value;
                                break;
                            case TagsQuotation.TAG_PreviousDailyClosingPriceTimestamp:
                                stock.PreviousDailyClosingPriceTimestamp = (DateTime)tagNumAndValue.Value;
                                break;
                        }
                    }
                }




                UpdateDictionary(stock);

                var context = GlobalHost.ConnectionManager.GetHubContext<StockTickerHub>();
                //context.Clients.All.Send("Admin", "stop the chat");
                context.Clients.All.UpdateStock(_stocks[stock.InternalCode]);


            }
        }

        public async Task Unsubscribe()
        {
            if (_autoRunningCancellationToken != null)
            {
                _autoRunningCancellationToken.Cancel();

                // Publisher is not thread safe, so while the auto ticker is running only the autoticker is 
                // allowed to access the publisher. Therefore before we can stop the publisher we have to 
                // wait for the autoticker task to complete
                _autoRunningTask.Wait();

                _autoRunningCancellationToken = null;
                _connection.Disconnect();
            }
        }


        private void UpdateDictionary(Stock stock)
        {
            if (!_stocks.ContainsKey(stock.InternalCode))
            {
                _stocks.TryAdd(stock.InternalCode, stock);
            }
            else
            {
                if (stock.AskPrice != null)
                {
                    _stocks[stock.InternalCode].AskPricePrev = _stocks[stock.InternalCode].AskPrice;
                    _stocks[stock.InternalCode].AskPrice = stock.AskPrice;
                }

                if (stock.BidPrice != null)
                {
                    _stocks[stock.InternalCode].BidPricePrev = _stocks[stock.InternalCode].BidPrice;
                    _stocks[stock.InternalCode].BidPrice = stock.BidPrice;
                }

                if (stock.LastPrice != null) _stocks[stock.InternalCode].LastPrice = stock.LastPrice;
                if (stock.LastTradePrice != null) _stocks[stock.InternalCode].LastTradePrice = stock.LastTradePrice;
                if (stock.CurrentBusinessDay != null) _stocks[stock.InternalCode].CurrentBusinessDay = stock.CurrentBusinessDay;
                if (stock.DailyClosingAskPrice != null) _stocks[stock.InternalCode].DailyClosingAskPrice = stock.DailyClosingAskPrice;
                if (stock.DailyClosingAskTimestamp != null) _stocks[stock.InternalCode].DailyClosingAskTimestamp = stock.DailyClosingAskTimestamp;
                if (stock.DailyClosingBidPrice != null) _stocks[stock.InternalCode].DailyClosingBidPrice = stock.DailyClosingBidPrice;
                if (stock.DailyClosingBidTimestamp != null) _stocks[stock.InternalCode].DailyClosingBidTimestamp = stock.DailyClosingBidTimestamp;
                if (stock.DailyOpeningPrice != null) _stocks[stock.InternalCode].DailyOpeningPrice = stock.DailyOpeningPrice;
                if (stock.DailyOpeningPriceTimestamp != null) _stocks[stock.InternalCode].DailyOpeningPriceTimestamp = stock.DailyOpeningPriceTimestamp;
                if (stock.PreviousDailyClosingPrice != null) _stocks[stock.InternalCode].PreviousDailyClosingPrice = stock.PreviousDailyClosingPrice;
                if (stock.PreviousDailyClosingPriceTimestamp != null) _stocks[stock.InternalCode].PreviousDailyClosingPriceTimestamp = stock.PreviousDailyClosingPriceTimestamp;
            }
        }
    }
}
