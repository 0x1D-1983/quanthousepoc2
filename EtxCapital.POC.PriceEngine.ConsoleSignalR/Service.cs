﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR
{
    //using Configs;
    using Microsoft.Owin.Hosting;
    using Topshelf;

    public class Service
    {
        // private readonly IKernel kernel;

        // inject the kernel into the Service class for later use
        //public Service(IKernel kernel)
        //    : base()
        //{
        //    this.kernel = kernel;
        //}

        //protected IKernel Kernel
        //{
        //    get
        //    {
        //        return this.kernel;
        //    }
        //}

        protected IDisposable WebAppHolder
        {
            get;
            set;
        }

        protected int Port
        {
            get
            {
                return 8088;
            }
        }

        public bool Start(HostControl hostControl)
        {
            if (WebAppHolder == null)
            {
                // don't use the OwinStartupAttribute to bootstrap OWIN
                //
                // provide an Action<IAppBiulder> action to
                // the WebApp.Start instead; this action will be
                // called at start up
                //
                // adjust the signature of the Configure method to take
                // an IKernel instance in addition to
                // an IAppBuilder instance
                //
                // use this instance of the pre-instantiated kernel
                // passed in to the Service class (and subsequently to
                // the StartupConfig.Configure method) to plug in as the
                // middleware
                WebAppHolder = WebApp.Start("http://127.0.0.1:8088", appBuilder =>
                {
                    new Startup().Configuration(appBuilder);
                });
            }

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            if (WebAppHolder != null)
            {
                WebAppHolder.Dispose();
                WebAppHolder = null;
            }

            return true;
        }
    }
}
