﻿using EtxCapital.POC.PriceEngine.ConsoleSignalR;
using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using System;
using Topshelf;

[assembly: OwinStartup(typeof(Startup))]
namespace EtxCapital.POC.PriceEngine.ConsoleSignalR
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    HostFactory.Run(hostConfig =>
        //    {
        //        hostConfig.Service<MyWindowsService>(serviceConfig =>
        //        {
        //            serviceConfig.ConstructUsing(() => new MyWindowsService());
        //            serviceConfig.WhenStarted(s => s.Start());
        //            serviceConfig.WhenStopped(s => s.Stop());
        //        });
        //    });


        //    string url = "http://127.0.0.1:8088";
        //    WebApp.Start(http://127.0.0.1:8088);
        //    Console.ReadKey();
        //}

        public static int Main(string[] args)
        {
            var exitCode = HostFactory.Run
            (
                c =>
                {

                    c.Service<Service>
                    (
                        sc =>
                        {
                            sc.ConstructUsing(() => new Service());

                            sc.WhenStarted
                            (
                                (service, hostControl) => service.Start(hostControl)
                            );
                            sc.WhenStopped
                            (
                                (service, hostControl) => service.Stop(hostControl)
                            );
                        }
                    );

                    c.SetServiceName("QuantHousePOC");
                    c.SetDisplayName("QuantHouse POC");
                    c.SetDescription("Proof of concept for consuming the QuantHouse API");

                    c.EnablePauseAndContinue();
                    c.EnableShutdown();

                    c.StartAutomaticallyDelayed();
                    c.RunAsLocalSystem();

                    c.DependsOnEventLog();
                    //c.DependsOnMsSql();
                    //c.DependsOnIis();
                }
            );

            return (int)exitCode;
        }
    }
}