﻿using EtxCapital.POC.PriceEngine.Business.Models;
using EtxCapital.POC.PriceEngine.ConsoleSignalR.FeedOS;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EtxCapital.POC.PriceEngine.ConsoleSignalR.Hubs
{
    public class StockTickerHub : Hub
    {
        private readonly SubscriberL1 _subscriberL1;
        private readonly ReferentialGetInstruments _refdatarequest;
        private static int _connectionCount = 0;


        public StockTickerHub()
        {
            var fosConnect = new FeedOSConnection();

            _subscriberL1 = new SubscriberL1(fosConnect);
            _refdatarequest = new ReferentialGetInstruments(fosConnect);
        }

        public async Task SubscribeL1()
        {
            await _subscriberL1.Subscribe();
        }

        public async Task UnsubscribeL1()
        {
            await _subscriberL1.Unsubscribe();
        }

        public List<StockDetails> RequestStockDetails()
        {
            return _refdatarequest.SendRequest();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public override Task OnConnected()
        {
            Interlocked.Increment(ref _connectionCount);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Interlocked.Decrement(ref _connectionCount);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            Interlocked.Increment(ref _connectionCount);
            return base.OnReconnected();
        }
    }
}
